<?php
use yii\helpers\Html;
?>
<div id="lang">
    <div class="btn-group">
  <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span id="current-lang"><?= $current->name;?><span><span class="caret"></span>
  </button>
    <ul id="langs" class="dropdown-menu">
        <?php foreach ($langs as $lang):?>
            <li class="item-lang">
                <?= Html::a($lang->name, ['lang/set','lang' => $lang->local]) ?>
            </li>
        <?php endforeach;?>
    </ul>
</div>
</div>
