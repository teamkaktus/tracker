<?php
namespace app\widgets;
use app\models\Lang;
use Yii;

class WLang extends \yii\bootstrap\Widget
{
    public function init(){}

    public function run() {
        return $this->render('lang/view', [
            'current' => Lang::find()->where('local = :local',[':local' => Yii::$app->language])->one(),
            'langs' => Lang::find()->where('id != :current_id', [':current_id' => Lang::find()->where('local = :local',[':local' => Yii::$app->language])->one()->id])->all(),
        ]);
    }
}
