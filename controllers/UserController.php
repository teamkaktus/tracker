<?php

namespace app\controllers;
use yii\web\HttpException;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use yii\data\ActiveForm;

use Yii;
use yii\helpers\BaseJson;
use yii\web\Controller;
use app\models\Tracker;
use app\models\User;
use app\models\Trackeruser;
use app\models\Trackerinfo;
use yii\helpers\Html;

class UserController extends Controller
{
    public function actions()
    {
        return array(
            'error' => array(
                'class' => 'yii\web\ErrorAction',
            ),
            'captcha' => array(
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ),
        );
    }

    public function actionIndex()
    {
        if(Yii::$app->user->isGuest){
            throw new HttpException(404);
        }else{
            $this->redirect('user/'.Yii::$app->user->identity->getUsername(Yii::$app->user->id));
        }
    }

    public function actionShow($username=null)
    {
        if(Yii::$app->user->isGuest){
            throw new HttpException(404);
        }else{
            //var_dump();exit;
            $modelUser = User::find()->where(['username' => $username])->one();
            if(($modelUser->id == Yii::$app->user->id) || (Yii::$app->user->id == '1') || (Yii::$app->user->identity->parent_id == $modelUser->id) ){

                $arrayTrackerUser = Trackeruser::find()->where(['user_id' => $modelUser->id])->all();
                    $trackers_id = '';
                    foreach($arrayTrackerUser as $tracker_users){
                        $trackers_id[] = $tracker_users->tracker_id;
                    }

                $query = Tracker::find()->where(['user_id' => $modelUser->id])->orWhere(['IMEI' => $trackers_id])->orderBy('id DESC');
                $modelTracker = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);

                echo $this->render('index', [
                    'username' => $username,
                    'modelTracker' => $modelTracker->getModels(),
                    'pagination' => $modelTracker->pagination,
                    'count' => $modelTracker->pagination->totalCount,
                ]);
            }else{
                throw new HttpException(404);
            }
        }
    }

    public function actionCompany()
    {
        echo $this->render('userscompany_page', [

        ]);
    }

    public function actionAccount($username=null)
    {
        $userAccount = User::find()->one();
        if(!empty($_POST)){
            $userAccount->username =  $_POST['User']['username'];
            $userAccount->phone_number =  $_POST['User']['phone_number'];
            $userAccount->email =  $_POST['User']['email'];
            $userAccount->city =  $_POST['User']['city'];
            $userAccount->country =  $_POST['User']['country'];
            $userAccount->zip =  $_POST['User']['zip'];
            $userAccount->first_name =  $_POST['User']['first_name'];
            $userAccount->last_name =  $_POST['User']['last_name'];
            $userAccount->language =  $_POST['User']['language'];
            $userAccount->type_distance =  $_POST['User']['type_distance'];

            if($userAccount->save()){
                Yii::$app->session->setFlash('AcountUpdate');
            }else{
                Yii::$app->session->setFlash('AcountNotUpdate');
            }
        }
        $userAccount = User::find()->one();
        echo $this->render('userAccount', [
            'user' => $userAccount,
        ]);
    }
    public function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
