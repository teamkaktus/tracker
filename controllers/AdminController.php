<?php

namespace app\controllers;
use yii\web\HttpException;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;

use Yii;
use app\models\PasswordResetRequestForm;
use yii\helpers\BaseJson;
use yii\web\Controller;
use app\models\Tracker;
use app\models\User;
use app\models\Trackeruser;
use app\models\Company;
use app\models\Drivercar;
use app\models\Driver;
use app\models\Payment;
use app\models\Lists;
use app\models\Tracker_report;
use app\models\Trackerinfo;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class AdminController extends Controller
{
    public function actions()
    {
        return array(
            'error' => array(
                'class' => 'yii\web\ErrorAction',
            ),
            'captcha' => array(
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ),
        );
    }

    //
    //User function
    public function actionIndex($username = null, $email = null)
    {
        if(Yii::$app->user->isGuest){
            throw new HttpException(404);
        }else{
            if((Yii::$app->user->identity->users_type == '1') && (Yii::$app->user->identity->company_id == '0')){
//                $companyModel = Company::find()->where(['parent_id' => 0])->all();
//                $company_id = [];
//                foreach($companyModel as $company){
//                    $company_id[] = $company->id;
//                }
                
                $query = User::find()->where(['company_id' => '0'])->orderBy('id DESC');

                if(isset($_GET['searchInput'])){
                    $query = User::find()->where(['company_id' => '0'])->andWhere(['LIKE', 'username', $_GET['searchInput']])->andWhere(['LIKE', 'email', $_GET['searchInput']])->andWhere(['LIKE', 'phone_number', $_GET['searchInput']])->orderBy('id DESC');
                }
                
                $statistic = [];
                $userCount = User::find()->where(['company_id' => '0'])->count();
                $activeUserCount = User::find()->where(['company_id' => '0', 'active' => 1])->count();
                $activeUserProcent = ($activeUserCount / $userCount) * 100;
                
                $deactiveUserCount = User::find()->where(['company_id' => '0', 'active' => 0])->count();
                $deactiveUserProcent = ($deactiveUserCount / $userCount) * 100;
                
                $statistic = ['userCount' => $userCount, 'activeUserCount' => $activeUserCount, 'activeUserProcent' => round($activeUserProcent,1), 'deactiveUserCount' => $deactiveUserCount, 'deactiveUserProcent' => round($deactiveUserProcent,1)];
                $arrayCompany = ArrayHelper::map(Company::find()->where(['parent_id' => 0])->all(), 'id', 'organization_name');
                $newModelUser = new User;
                $newModelUser->scenario = 'adduser';
                $modelUser = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
                
                echo $this->render('index', [
                    'modelCompany' => $arrayCompany,
                    'newModelUser' => $newModelUser,
                    'modelUser' => $modelUser->getModels(),
                    'pagination' => $modelUser->pagination,
                    'statistic' => $statistic,
                    'count' => $modelUser->pagination->totalCount,
                ]);
            }else{
                throw new HttpException(404);
            }
        }
    }

    public function actionAdduser(){
        if(Yii::$app->user->isGuest){
            throw new HttpException(404);
        }else{
            if((Yii::$app->user->identity->users_type == '1') && (Yii::$app->user->identity->company_id == '0')){
                $modelNewUser = new User;
                $modelNewUser->scenario =  'adduser';
                
                $companyModel = Company::find()->where(['parent_id' => 0])->all();
                $company_id = [];
                foreach($companyModel as $company){
                    $company_id[] = $company->id;
                }
                $query = User::find()->where(['company_id' => $company_id])->orderBy('id DESC');
                
                $modelUser = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
                $arrayCompany = ArrayHelper::map(Company::find()->all(), 'id', 'organization_name');
                if($modelNewUser->load($_POST)){
                    $modelNewUser->username = $_POST['User']['username'];
                    $modelNewUser->email = $_POST['User']['email'];
                    if($_POST['UserType'] == 1){
                        $modelNewUser->users_type = 1;
                        $modelNewUser->company_id = 0;
                    }

                    if($_POST['UserType'] == 2){
                        $modelNewUser->users_type = 2;
                        $modelNewUser->company_id = 0;
                    }

                    if($_POST['UserType'] == 3){
                        $modelNewUser->users_type = $_POST['User']['users_type'];
                        $modelNewUser->company_id = $_POST['User']['company_id'];
                    }

                    $modelNewUser->phone_number = $_POST['User']['phone_number'];
                    
                    
                    if($modelNewUser->save()&&$modelNewUser->validate()){
                    $model = new PasswordResetRequestForm();
                    $model['email'] = $modelNewUser->email;
                        Yii::$app->session->setFlash('UserAdd');
                    if ($model->sendEmail()){
                        Yii::$app->session->setFlash('EMAIL_sended');
                    } else {
                        Yii::$app->session->setFlash('Email_error');
                    }
                        //var_dump($modelNewUser);exit;
                      //  Yii::$app->session->setFlash('UserAdd');
                    }else{
                          Yii::$app->session->setFlash('ErrorValidating');
                        }
                        return $this->render('index', [
                            'modelCompany' => $arrayCompany,
                            'newModelUser' => $modelNewUser,
                            'modelUser' => $modelUser->getModels(),
                            'pagination' => $modelUser->pagination,
                            'count' => $modelUser->pagination->totalCount,
                          ]);

                    }
                }else{
                throw new HttpException(404);
            }
        }
    }

    public function actionUserupdate(){
        $modelUser = User::find()->where(['id' => $_POST['user_id']])->one();
        $modelUser->scenario = 'update';
        $modelUser->username = $_POST['User']['username'];
        $modelUser->phone_number = $_POST['User']['phone_number'];
        $modelUser->city = $_POST['User']['city'];
        $modelUser->address = $_POST['User']['address'];
        $modelUser->country = $_POST['User']['country'];
        $modelUser->zip = $_POST['User']['zip'];
        $modelUser->first_name = $_POST['User']['first_name'];
        $modelUser->last_name = $_POST['User']['last_name'];
        
        if($_POST['UserTypeUpdate'] == 3){
            $modelUser->users_type = $_POST['User']['users_type'];
            $modelUser->company_id = $_POST['User']['company_id'];
        }else{
            $modelUser->users_type = $_POST['User']['users_type'];
            $modelUser->company_id = 0;
        }
        if($modelUser->save()){
            Yii::$app->session->setFlash('UserUpdate');
            $this->redirect('index');
        }else{
            Yii::$app->session->setFlash('UserNotUpdate');
            $this->redirect('index');
        }
    }
        
    public function actionUserdelete($id){
        if(Yii::$app->user->isGuest){
                throw new HttpException(404);
        }else{
            if((Yii::$app->user->identity->users_type == '1') && (Yii::$app->user->identity->company_id == '0')){
                $userModel = User::find()->where(['id'=>$id])->one();
                if ($userModel) {
                    $userModel->delete();
                }
                Yii::$app->session->setFlash('UserDeleted');
                $this->redirect('index');
            }else{
                throw new HttpException(404);
            }
        }
    }

    public function actionUseractivepayment(){
        $user_id = $_POST['user_id'];
        $userModel = User::find()->where(['id' => $user_id])->one();
        $userModel->scenario = 'timeDeactive';
        $userModel->active = 1;
        $userModel->time_deactive = $_POST['timeDeactive'];
            if($userModel->save()){
                $userUpdateStatus = 'update';
            }else{
                $userUpdateStatus = 'not update';
            }
            
            $paymentModel = Payment::find()->where(['id_buyer' => $user_id, 'type' => 1])->one();
        if($paymentModel != null){
            $daletePayment = Payment::deleteAll(['id_buyer' => $user_id, 'type' => 1]);
        }    
        $newPaymentModel = new Payment();
        $newPaymentModel->id_buyer = $user_id;
        $newPaymentModel->type = 2;
        $newPaymentModel->count_money = $_POST['user_money'];
        //1 - company
        //2 - user
        $newPaymentModel->time_active = $_POST['timeActive'];
        $newPaymentModel->time_deactive = $_POST['timeDeactive'];
        $newPaymentModel->scenario = 'add_payment';
            if($newPaymentModel->save()){
                $paymentAddStatus = 'update';
            }else{
                $paymentAddStatus = 'not update';
            }
           
        echo json_encode(['user_update_status' => $userUpdateStatus, 'payment_add_status' => $paymentAddStatus]);
    }
    
    public function actionUserdeactive(){
        $user_id = $_POST['user_id'];
        $daletePayment = Payment::deleteAll(['id_buyer' => $_POST['user_id'], 'type' => 2]);
        
        $userModel = User::find()->where(['id' => $user_id])->one();
        $userModel->scenario = 'timeDeactive';
        $userModel->active = 0;
        $userModel->time_deactive = '0000-00-00 00:00:00';
        if($userModel->save()){
            $status = 'deactive';
        }else{
            $status = 'not deactive';
        }
        
        echo json_encode(['status' => $status]);
    }
    
    public function actionUseractive(){
        //$value = $_POST;
        if($_POST['action'] == 'Active'){
            $userModell = User::find()->where(['id' => $_POST['id']])->one();
            $userModell->scenario = 'active';
            $userModell->active = 1;
            $userModell->save();
            $res = 'Active';
        }else{
            $userModell = User::find()->where(['id' => $_POST['id']])->one();
            $userModell->active = 0;
            $userModell->scenario = 'active';
            $userModell->save();
            $res = 'Deactive';
        }

        echo json_encode($res);
    }
    //end User function
    //

    //_______________________________________________________________________________
    
    //
    //end Tracker function
    public function actionUpdatetracker(){
        $trackerModel = Tracker::find()->where(['id' => $_POST['tracker_id']])->one();
        
        if($_POST['TrackerTypeUpdate'] == 1){
            if(isset($_POST['tracker_user'])){
                $tarcker_user_delete = Trackeruser::deleteAll(['tracker_id' => $trackerModel->IMEI]);
                    foreach($_POST['tracker_user'] as $id => $user_id){
                        $tracker_user = new Trackeruser();
                        $tarcker_user_check = Trackeruser::find()->where(['tracker_id' => $trackerModel->id])->andWhere(['user_id' => $user_id])->one();
                        if($tarcker_user_check == null){
                            $tracker_user->tracker_id = $trackerModel->IMEI;
                            $tracker_user->user_id = $user_id;
                                if($tracker_user->save()){
                                    Yii::$app->session->setFlash('TrackerUpdate');
                                }else{
                                    Yii::$app->session->setFlash('TrackerUpdate');
                                }
                        }
                    }
                    Yii::$app->session->setFlash('TrackerUpdate');
                    $trackerModel->name = $_POST['Tracker']['name'];
                    $trackerModel->discription = $_POST['Tracker']['discription'];
                    $trackerModel->speed_limit = $_POST['Tracker']['speed_limit'];
                    $trackerModel->company_id = 0;
                    $trackerModel->save();
                }else{
                    Yii::$app->session->setFlash('TrackerNotUpdate');
                    $trackerModel->save();
                }
        }else{
            $tarcker_user_delete = Trackeruser::deleteAll(['tracker_id' => $trackerModel->IMEI]);
            $trackerModel->name = $_POST['Tracker']['name'];
            $trackerModel->discription = $_POST['Tracker']['discription'];
            $trackerModel->user_id = 0;
            $trackerModel->company_id = $_POST['Tracker']['company_id'];
            if($trackerModel->save()){
                Yii::$app->session->setFlash('TrackerUpdate');
            }else{
                Yii::$app->session->setFlash('TrackerNotUpdate');
            }
        }
        $this->redirect('../admin/addtrackerpage');
    }
    
    public function actionAddtrackerpage(){
        if(Yii::$app->user->isGuest){
            throw new HttpException(404);
        }else{
            if((Yii::$app->user->identity->users_type == '1') && (Yii::$app->user->identity->company_id == '0')){
                $modelNewTracker = new Tracker;

                $arrayUser = ArrayHelper::map(User::find()->all(), 'id', 'username');
                $arrayCompany = ArrayHelper::map(Company::find()->all(), 'id', 'organization_name');
                $query = Tracker::find()->orderBy('time_added DESC');
                $modelTracker = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);

                if($_GET['searchInput']){
                    $query = Tracker::find()->where(['LIKE', 'IMEI', $_GET['searchInput']])->orWhere(['LIKE', 'name', $_GET['searchInput']])->orWhere(['LIKE', 'discription', $_GET['searchInput']])->orWhere(['LIKE', 'time_added', $_GET['searchInput']])->orderBy('time_added DESC');
                    $modelTracker = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
                }

                $tracker_user = Trackeruser::find()->all();
                    $user_id_for_select = [];
                    foreach($tracker_user as $tr_us){
                        $user_id_for_select[] = ['user_id' => $tr_us->user_id, 'tracker_id' => $tr_us->tracker_id];
                    }
                
                if($modelNewTracker->load($_POST)){
                        if($modelNewTracker->user_type == 1){
                            $modelNewTracker->company_id = 0;
                            $tracker_user = new Trackeruser();
                            $tracker_user->tracker_id = $modelNewTracker->IMEI;
                            $tracker_user->user_id = $modelNewTracker->user_id;
                            $tracker_user->save();
                            $modelNewTracker->user_id = 0;
                        }else{
                            $modelNewTracker->user_id = 0;
                        }

                        if($modelNewTracker->save()&&$modelNewTracker->validate()){
                            Yii::$app->session->setFlash('TrackerAdd');
                            $modelNewTracker = new Tracker;
                        }else{
                            if($modelNewTracker->errors){
                              Yii::$app->session->setFlash('ErrorValidating');
                            }else{
                              throw new Exception("Error Processing Request", 1);
                            }
                        }
                }
                          
                $statistic = [];
                $trackerCount = Tracker::find()->count();
                $activeTrackerCount = Tracker::find()->where(['active' => 1])->count();
                $activeTrackerProcent = ($activeTrackerCount / $trackerCount) * 100;

                $deactiveTrackerCount = Tracker::find()->where(['active' => 0])->count();
                $deactiveTrackerProcent = ($deactiveTrackerCount / $trackerCount) * 100;

                $statistic = ['trackerCount' => $trackerCount, 'activeTrackerCount' => $activeTrackerCount, 'activeTrackerProcent' => round($activeTrackerProcent),
                    'deactiveTrackerCount' => $deactiveTrackerCount, 'deactiveTrackerProcent' => round($deactiveTrackerProcent,1)];

                $reportModel = Lists::find()->where(['type'=>'report'])->all();
                
                return $this->render('adminAddTracker', [
                    'statistic' => $statistic,
                    'reportModel' => $reportModel,
                    'tracker_user' => $user_id_for_select,
                    'arrayCompany' => $arrayCompany,
                    'arrayUser' => $arrayUser,
                    'modelNewTracker' => $modelNewTracker,
                    'modelTracker' => $modelTracker->getModels(),
                    'pagination' => $modelTracker->pagination,
                    'count' => $modelTracker->pagination->totalCount,
                ]);

            }
        }
    }

    public function actionTrackerdelete($imei){
        if(Yii::$app->user->isGuest){
            throw new HttpException(404);
        }else{
            if((Yii::$app->user->identity->users_type == '1') && (Yii::$app->user->identity->company_id == '0')){
                $trackerModel = Tracker::find()->where(['IMEI'=>$imei])->one();
                if ($trackerModel) {
                    $trackerModel->delete();
                }
                Yii::$app->session->setFlash('TrackerDeleted');
                $this->redirect('addtrackerpage');
            }else{
                throw new HttpException(404);
            }
        }
    }

    public function actionTrackeractive(){
        //$value = $_POST;
        if($_POST['action'] == 'Active'){
            $trackerModel = Tracker::find()->where(['id' => $_POST['id']])->one();
            $trackerModel->active = 1;
            $trackerModel->save();
            $res = 'Active';
        }else{
            $trackerModel = Tracker::find()->where(['id' => $_POST['id']])->one();
            $trackerModel->active = 0;
            $trackerModel->save();
            $res = 'Deactive';
        }

        echo json_encode($res);
    }
    // end Tracker function
    //

    //_______________________________________________________________________________
    
    //
    //Company function
    public function actionCompanypage()
    {
        if(Yii::$app->user->isGuest){
            throw new HttpException(404);
        }else{
            if((Yii::$app->user->identity->users_type == '1') && (Yii::$app->user->identity->company_id == '0')){
                $modelNewCompany = new Company;
                $query = Company::find()->where(['parent_id' => 0])->orderBy('time_added DESC');
                $modelCompany = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);

                if($_GET['searchInput']){
                    $query = Company::find()->where(['parent_id' => 0])->andWhere(['LIKE', 'organization_name', $_GET['searchInput']])->orWhere(['LIKE', 'phone_number', $_GET['searchInput']])->orWhere(['LIKE', 'email', $_GET['searchInput']])->orderBy('time_added DESC');
                    $modelCompany = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
                }
                $modelNewCompany->scenario = 'addcompany';

                if($modelNewCompany->load($_POST)){
                    $modelNewCompany->type_company = 1;
                    //$modelNewCompany->urlname = ($modelNewCompany->urlname);
                    if($modelNewCompany->save()&&$modelNewCompany->validate()){
                        Yii::$app->session->setFlash('CompanyAdd');
                        $modelNewCompany = new Company;

                    }else{
                        if($modelNewCompany->errors){
                          Yii::$app->session->setFlash('ErrorValidating');
                        }else{
                          throw new HttpException("Error Processing Request", 1);
                        }
                        }
                }
                
                $statistic = [];
                $companyCount = Company::find()->where(['parent_id' => 0])->count();
                $activeCompanyCount = Company::find()->where(['parent_id' => 0, 'active' => 1])->count();
                $activeCompanyProcent = ($activeCompanyCount / $companyCount) * 100;
                
                $deactiveCompanyCount = Company::find()->where(['parent_id' => 0, 'active' => 0])->count();
                $deactiveCompanyProcent = ($deactiveCompanyCount / $companyCount) * 100;
                
                $statistic = ['companyCount' => $companyCount, 'activeCompanyCount' => $activeCompanyCount, 'activeCompanyProcent' => round($activeCompanyProcent,1), 'deactiveCompanyCount' => $deactiveCompanyCount, 'deactiveCompanyProcent' => round($deactiveCompanyProcent,1)];
                
                        return $this->render('addCompany', [
                            'statistic' => $statistic,
                            'modelNewCompany' => $modelNewCompany,
                            'modelCompany' => $modelCompany->getModels(),
                            'pagination' => $modelCompany->pagination,
                            'count' => $modelCompany->pagination->totalCount,
                    ]);
                }else{
                  throw new Exception("Error Processing Request", 1);
                }
            }
    }

    public function actionCompanydelete($id,$company_id=null){
        if(Yii::$app->user->isGuest){
            throw new HttpException(404);
        }else{
            if(Yii::$app->user->identity->users_type == '1'){
                $companyModel = Company::find()->where(['id'=>$id])->one();
                if ($companyModel) {
                    $companyModel->delete();
                }
                if($company_id == null){
                    Yii::$app->session->setFlash('CompanyDeleted');
                    $this->redirect('companypage');
                }else{
                    $companyModelR = Company::find()->where(['id' => $company_id])->one();
                    Yii::$app->session->setFlash('CompanyDeleted');
                    $this->redirect('/company/'.$companyModelR->urlname.'/companysubcompany');
                }
            }else{
                throw new HttpException(404);
            }
        }
    }

    public function actionCompanyactivepayment(){
        $company_id = $_POST['company_id'];
        $companyModel = Company::find()->where(['id' => $company_id])->one();
        $companyModel->scenario = 'timeDeactive';
        $companyModel->active = 1;
        $companyModel->time_deactive = $_POST['timeDeactive'];
            if($companyModel->save()){
                $companyUpdateStatus = 'update';
            }else{
                $companyUpdateStatus = 'not update';
            }
            
            $paymentModel = Payment::find()->where(['id_buyer' => $company_id, 'type' => 1])->one();
        if($paymentModel != null){
            $daletePayment = Payment::deleteAll(['id_buyer' => $company_id, 'type' => 1]);
        }    
        $newPaymentModel = new Payment();
        $newPaymentModel->id_buyer = $company_id;
        $newPaymentModel->type = 1;
        $newPaymentModel->count_money = $_POST['company_money'];
        //1 - company
        //2 - user
        $newPaymentModel->time_deactive = $_POST['timeDeactive'];
        $newPaymentModel->time_active = $_POST['timeActive'];
        $newPaymentModel->scenario = 'add_payment';
            if($newPaymentModel->save()){
                $paymentAddStatus = 'update';
            }else{
                $paymentAddStatus = 'not update';
            }
           
        echo json_encode(['company_update_status' => $companyUpdateStatus, 'payment_add_status' => $paymentAddStatus]);
    }
    
    public function actionCompanyactive(){
        $companyModels = Company::find()->where(['id' => $_POST['id']])->one();
        if($_POST['action'] == 'Active'){
            $companyModels->scenario = 'activecompany';
            $companyModels->active = 1;
            $companyModels->save();
            $res = 'Active';
        }else{
            $companyModels->scenario = 'timeDeactive';
            $companyModels->active = 0;
            $companyModels->time_deactive = '0000-00-00 00:00:00';
            $companyModels->save();
            $daletePayment = Payment::deleteAll(['id_buyer' => $_POST['id'], 'type' => 1]);
            $res = 'Deactive';
        }

        echo json_encode($res);
    }
    //end Company function
    //
    
    //__________________________________________________________________________
    
    //
    //Reports
    public function actionReports(){
        
        return $this->render('reports_page', [
        ]);
    }
    
    public function actionReportsvalue(){
        $role = $_POST['data'];
            if($role == 'user'){
                $user_model = User::find()->where(['company_id' => 0])->all();
                $users_id = '';
                    foreach($user_model as $users){
                        $users_id[] = $users->id;
                    }
                $trackerUser = Trackeruser::find()->where(['user_id' => $users_id])->all();
            $arrayUser = [];
            foreach($trackerUser as $user_t){
                $arrayUser[Yii::$app->user->identity->getUsername($user_t->user_id)][] = $user_t->tracker_id;
            }
            $lastArray = [];
            foreach($arrayUser as $key => $user){
                $lastArray[$key] = count($user);
            }
            
            echo json_encode($lastArray);
            
        }else{
            
            $company_model = Company::find()->where(['parent_id' => 0])->all();
            foreach($company_model as $company){
                //$company;
                $subCompanyModel = Company::find()->where(['parent_id' => $company->id])->all();
                $companys_id = '';
                    foreach($subCompanyModel as $sub_company){
                        $companys_id[] = $sub_company->id;
                    }

                    $arrayCompanyUsers = User::find()->where(['company_id' => $companys_id])->orWhere(['company_id' => $company->id])->all();
                    $users_id = '';
                        foreach($arrayCompanyUsers as $users){
                            $users_id[] = $users->id;
                        }

                    $arrayTrackerUser = Trackeruser::find()->where(['user_id' => $users_id])->all();
                    $trackers_id = '';
                        foreach($arrayTrackerUser as $tracker_users){
                            $trackers_id[] = $tracker_users->tracker_id;
                        } 
                    
                    $arrayTrackerCompany = Tracker::find()->where(['company_id' => $company->id])->all();
                    $tracker_id = '';
                        foreach($arrayTrackerCompany as $tracker){
                            $tracker_id[] = $tracker->id;
                        }    
                        
                    $lastArray[$company->organization_name] = ['sub_company' => count($companys_id), 
                        'user_company' => count($users_id), 'trackers_company' => count($trackers_id) + count($tracker_id)];
            }
            
            //var_dump($lastArray);
            echo json_encode($lastArray);
        }
    }
    //end Reports
    //
   
    //__________________________________________________________________________
    
    //
    //drivers
    
//    public function actionDriverspage(){
//        if(Yii::$app->user->isGuest){
//                throw new HttpException(404);
//        }else{
//            if((Yii::$app->user->identity->users_type == '1') && (Yii::$app->user->identity->company_id == '0')){
//                if($_POST['Driver']){
//                   $newDriversModel = new Driver();
//                   $newDriversModel->scenario = 'adddriver';
//                   $newDriversModel->first_name = $_POST['Driver']['first_name'];
//                   $newDriversModel->last_name = $_POST['Driver']['last_name'];
//                   $newDriversModel->phone_number = $_POST['Driver']['phone_number'];
//                   if($newDriversModel->save()){
//                        Yii::$app->session->setFlash('DriverAdded');
//                   }else{
//                        Yii::$app->session->setFlash('DriverNotAdded');
//                   }
//                   $driverSave = Driver::find()->where(['first_name' => $_POST['Driver']['first_name'],'last_name' => $_POST['Driver']['last_name'], 'phone_number' => $_POST['Driver']['phone_number']])->one();
//                   $newDriverCar = new DriverCar();
//                   $newDriverCar->car_id = $_POST['tracker_driver'];
//                   $newDriverCar->driver_id = $driverSave->id;
//                   $newDriverCar->save();
//                }
//                
//                $newDriversModel = new Driver();
//                $trackerArray = ArrayHelper::map(Tracker::find()->all(), 'IMEI', 'name');
//                
//                $driverTracker = Drivertracker::find()->all();
//                
//                $query = Driver::find()->orderBy('time_added DESC');
//                $driversModel = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
//                
//                if($_GET['searchInput']){
//                    $query = Driver::find()->where(['LIKE', 'first_name', $_GET['searchInput']])->orWhere(['LIKE', 'phone_number', $_GET['searchInput']])->orWhere(['LIKE', 'last_name', $_GET['searchInput']])->orderBy('time_added DESC');
//                    $driversModel = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
//                }
//
//                return $this->render('driver_page',[
//                    'driverTracker' => $driverTracker,
//                    'trackerArray' => $trackerArray,
//                    'driversModel' => $driversModel->getModels(),
//                    'pagination' => $driversModel->pagination,
//                    'count' => $driversModel->pagination->totalCount,
//                    'newDriversModel' => $newDriversModel
//                ]);
//            }else{
//                throw new HttpException(404);
//            }
//        }
//    }
    
//    public function actionDriverdelete($id){
//        if(Yii::$app->user->isGuest){
//                throw new HttpException(404);
//        }else{
//            if((Yii::$app->user->identity->users_type == '1') && (Yii::$app->user->identity->company_id == '0')){
//                $driverModel = Driver::find()->where(['id'=>$id])->one();
//                if ($driverModel) {
//                    $driverModel->delete();
//                }
//                Yii::$app->session->setFlash('DriverDeleted');
//                $this->redirect('../admin/driverspage');
//            }else{
//                throw new HttpException(404);
//            }
//        }
//    }
//    
//    public function actionDriverupdate(){
//        if(Yii::$app->user->isGuest){
//                throw new HttpException(404);
//        }else{
//            if((Yii::$app->user->identity->users_type == '1') && (Yii::$app->user->identity->company_id == '0')){
//                if($_POST['Driver']){
//                    $driverModel = Driver::find()->where(['id' => $_POST['driver_id']])->one();
//                    $driverModel->scenario = 'updatedriver';
//                    $driverModel->first_name = $_POST['Driver']['first_name'];
//                    $driverModel->last_name = $_POST['Driver']['last_name'];
//                    $driverModel->phone_number = $_POST['Driver']['phone_number'];
//                    
//                    $driverTracker = Drivertracker::deleteAll(['tracker_id' => $_POST['tracker_driver'], 'driver_id' => $_POST['driver_id']]);
//                    $newDriverTracker = new Drivertracker();
//                    $newDriverTracker->tracker_id = $_POST['tracker_driver'];
//                    $newDriverTracker->driver_id = $_POST['driver_id'];
//                    
//                    if($driverModel->save() && $newDriverTracker->save()){
//                        Yii::$app->session->setFlash('DriverUpdate');
//                    }else{
//                        Yii::$app->session->setFlash('DriverNotUpdate');
//                    }
// 
//                    $this->redirect('../admin/driverspage');
//                }else{
//                    throw new HttpException(404);
//                }
//            }else{
//                throw new HttpException(404);
//            }
//        }
//    }
    
    //end drivers 
    //
    
    
     public function actionPaymentpage(){
        if($_GET['time_vid']){
            $search_vid = $_GET['time_vid'];
            $search_do = $_GET['time_do'];
        }else{
            $search_vid = '2000-01-01 00:00:01';
            $search_do = '2100-01-01 00:00:01';
        }
         
        $selectUserPayment = new Query;
        $selectUserPayment ->select(['*'])
            ->from('payment')
            ->join( 'LEFT JOIN',
              'users',
              'users.id = payment.id_buyer'
             )->where("(payment.time_active > '".$search_vid."'AND payment.time_active <'".$search_do."')")->andWhere(['payment.type' => 2]);
        $userModel = new ActiveDataProvider(['query' => $selectUserPayment, 'pagination' => ['pageSize' => 20]]);
        
        
        
        $selectCompanyPayment = new Query;
        $selectCompanyPayment ->select(['*'])
            ->from('payment')
            ->join( 'LEFT JOIN',
              'company',
              'company.id = payment.id_buyer'
             )->where("(payment.time_active > '".$search_vid."'AND payment.time_active <'".$search_do."')")->andWhere(['payment.type' => 1]);
        
        $companyModel = new ActiveDataProvider(['query' => $selectCompanyPayment, 'pagination' => ['pageSize' => 20]]);
        
        return $this->render('paymentpage',[
            'userModel' => $userModel->getModels(),
            'paginationUser' => $userModel->pagination,
            'companyModel' => $companyModel->getModels(),
            'paginationCompany' => $companyModel->pagination,
        ]);
    }
    
    public function actionReportselected(){
        $tracker_id = $_POST['tracker_id'];
        //$tracker_id = ['35','36'];
        $tracker_report = Tracker_report::find()->where(['tracker_id' => $tracker_id])->all();
        //var_dump($tracker_report);
        if($tracker_report != null){
            $reportSelect = [];
            foreach($tracker_report as $report){
                $reportSelect[$report->report_id][] = $report->tracker_id;
            }
        }
        
        echo json_encode($reportSelect);
    }
    
    public function actionReporttype(){
        $reportChecked = $_POST['arrayIdReportChecked'];
        $trackerChecked = $_POST['arrayIdTrackerForReportChecked'];
        $reportIndeterminate = json_decode($_POST['arrayIdReportIndeterminate']);
        $tracker_report = Tracker_report::deleteAll(['tracker_id' => $trackerChecked]);
//        
        if($reportChecked != null){
            foreach($trackerChecked as $tracker_id){
                foreach($reportChecked as $report_id){
                    $tracker_report_model = new Tracker_report;
                    $tracker_report_model->tracker_id = $tracker_id;
                    $tracker_report_model->scenario = 'tracker_report';
                    $tracker_report_model->report_id = $report_id;
                    $tracker_report_model->save();
                    
                }
            }
        }
        
        if($reportIndeterminate != null){
            $report;
            foreach($reportIndeterminate as $report_in_id => $array){
                if($array != null){
                    foreach($array as $tracker_in_id){
                        if($report_in_id != ""){
                            //$report[$report_in_id][] = $tracker_in_id;
                            $tracker_report_model = new Tracker_report;
                            $tracker_report_model->scenario = 'tracker_report';
                            $tracker_report_model->tracker_id = $tracker_in_id;
                            $tracker_report_model->report_id = $report_in_id;
                            $tracker_report_model->save();
                        }
                    }
                }
            }
        }
        echo json_encode('good');
    }
}
