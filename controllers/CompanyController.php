<?php

namespace app\controllers;
use yii\web\HttpException;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;

use Yii;
use app\models\PasswordResetRequestForm;
use yii\helpers\BaseJson;
use yii\web\Controller;
use app\models\Tracker;
use app\models\Company;
use app\models\Driver;
use app\models\Carinfo;
use app\models\Drivercar;
use app\models\User;
use app\models\Tasks;
use app\models\Subtasks;
use app\models\OBD;
use app\models\Lists;
use app\models\Tracker_report;
use app\models\Trackerinfo;
use app\models\Trackeruser;
use app\models\Taskstracker;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class CompanyController extends Controller
{
    public function actions()
    {
        return array(
            'error' => array(
                'class' => 'yii\web\ErrorAction',
            ),
            'captcha' => array(
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ),
        );
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
    public function actionIndex()
    {
        throw new HttpException(404);
    }

    public function actionShow($urlname=null)
    {
        $companyModel = Company::find()->where(['urlname' => $urlname])->one();
        if($companyModel == null){
            throw new HttpException(404);
        }else{
            if((Yii::$app->user->identity->company_id == $companyModel->id) || (Yii::$app->user->identity->getUserAdmin(Yii::$app->user->identity->id) != null)){
                if(Yii::$app->user->identity->users_type == '1'){
                    if($companyModel->active == '1'){
                        $newModelUser = new User();
                        $allCompany = Company::find()->where(['parent_id' => $companyModel->id])->orWhere(['id' => $companyModel->id])->all();
                        $companys_id = '';
                            foreach($allCompany as $company){
                                $companys_id[] = $company->id;
                            }
                        $query = User::find()->where(['company_id' => $companys_id])->andWhere(['!=', 'username', Yii::$app->user->identity->username]);
                        $modelUser = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
                        $subCompanyModel = ArrayHelper::map(Company::find()->where(['parent_id' => $companyModel->id])->all(), 'id', 'organization_name');
                        if($companyModel->parent_id != 0){
                            $parentCompany = Company::find()->where(['id' => $companyModel->parent_id])->one();
                        }else{
                            $parentCompany = null;
                        }

                        $newModelUser->scenario = 'addusercompany';
                      if(isset($_POST['User'])){
                        $newModelUser->username = $_POST['User']['username'];
                        $newModelUser->phone_number = $_POST['User']['phone_number'];
                        $newModelUser->email = $_POST['User']['email'];
                        $newModelUser->users_type = $_POST['User']['users_type'];
                        $newModelUser->company_id = $companyModel->id;
                        $newModelUser->password_hash = \Yii::$app->security->generatePasswordHash($_POST['User']['password']);

                        if($newModelUser->save()&&$newModelUser->validate()){
                            Yii::$app->session->setFlash('UserAdd');
                            $model = new PasswordResetRequestForm();
                                $model['email'] = $newModelUser->email;
                                if ($model->sendEmail()){
                                    Yii::$app->session->setFlash('EMAIL_sended');
                                } else {
                                    Yii::$app->session->setFlash('Email_error');
                                }
                          $newModelUser = new User();
                        }else{
                          Yii::$app->session->setFlash('ErrorValidating');
                          }
                        }
                        if($_GET['SearchUser']){
                            $query = User::find()->where(['company_id' => $companys_id])->andWhere(['!=', 'username', Yii::$app->user->identity->username])->andWhere(['LIKE', 'username', $_GET['username']])->andWhere(['LIKE', 'users_type', $_GET['user_type']])->andWhere(['LIKE', 'email', $_GET['email']]);
                            $modelUser = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
                        
                        }
                        
                        $statistic = [];
                        $userCount = User::find()->where(['company_id' => $companys_id])->count();
                        $activeUserCount = User::find()->where(['company_id' => $companys_id, 'active' => 1])->count();
                        $activeUserProcent = ($activeUserCount / $userCount) * 100;

                        $deactiveUserCount = User::find()->where(['company_id' => $companys_id, 'active' => 0])->count();
                        $deactiveUserProcent = ($deactiveUserCount / $userCount) * 100;

                        $statistic = ['userCount' => $userCount, 'activeUserCount' => $activeUserCount, 'activeUserProcent' => $activeUserProcent, 'deactiveUserCount' => $deactiveUserCount, 'deactiveUserProcent' => $deactiveUserProcent];
                        
                        echo $this->render('company_page', [
                            'parentCompany' => $parentCompany,
                            'sub_company' => $subCompanyModel,
                            'user_company_role' => $user_company_role,
                            'companyModel' => $companyModel,
                            'newModelUser' => $newModelUser,
                            'modelUser' => $modelUser->getModels(),
                            'pagination' => $modelUser->pagination,
                            'count' => $modelUser->pagination->totalCount,
                            'urlname' => $urlname,
                            'statistic' => $statistic,
                        ]);
                    }else{
                        throw new HttpException(404, 'Sorry, but this company is not active, contact the Administration!');
                    }
                }else{
                    $this->redirect('../../user/'.Yii::$app->user->identity->username);
                }
            }else{
                throw new HttpException(404);
            }
        }
    }

    public function actionCompanyupdatetracker()
    {
        $trackerModel = Tracker::find()->where(['id' => $_POST['tracker_id']])->one();
            
            if(($_POST['TypeT'] == '1') || ($_POST['company_parent'] == '1')){
                 if(isset($_POST['tracker_user'])){
                    $tarcker_user_delete = Trackeruser::deleteAll(['tracker_id' => $trackerModel->IMEI]);
                    foreach($_POST['tracker_user'] as $id => $user_id){
                        $tracker_user = new Trackeruser();
                        $tarcker_user_check = Trackeruser::find()->where(['tracker_id' => $trackerModel->id])->andWhere(['user_id' => $user_id])->one();
                        if($tarcker_user_check == null){
                            $tracker_user->tracker_id = $trackerModel->IMEI;
                            $tracker_user->user_id = $user_id;
                                if($tracker_user->save()){
                                    Yii::$app->session->setFlash('TrackerUpdate');
                                }else{
                                    Yii::$app->session->setFlash('TrackerUpdate');
                                }
                        }
                    }
                    Yii::$app->session->setFlash('TrackerUpdate');
                    $trackerModel->company_id = 0;
                    $trackerModel->name = $_POST['Tracker']['name'];
                    $trackerModel->discription = $_POST['Tracker']['discription'];
                    $trackerModel->speed_limit = $_POST['Tracker']['speed_limit'];
                    $trackerModel->save();
                }else{
                    $tarcker_user_delete = Trackeruser::deleteAll(['tracker_id' => $trackerModel->IMEI]);
                    $trackerModel->company_id = $_POST['company_id'];
                    $trackerModel->name = $_POST['Tracker']['name'];
                    $trackerModel->discription = $_POST['Tracker']['discription'];
                    $trackerModel->speed_limit = $_POST['Tracker']['speed_limit'];
                    $trackerModel->save();
                }
            }else{
                $tarcker_user_delete = Trackeruser::deleteAll(['tracker_id' => $trackerModel->IMEI]);
                $trackerModel->user_id = 0;
                $trackerModel->company_id = $_POST['Tracker']['company_id'];
                $trackerModel->name = $_POST['Tracker']['name'];
                $trackerModel->discription = $_POST['Tracker']['discription'];
                $trackerModel->speed_limit = $_POST['Tracker']['speed_limit'];
                if($trackerModel->save()){
                    Yii::$app->session->setFlash('TrackerUpdate');
                }else{
                    Yii::$app->session->setFlash('TrackerNotUpdate');
                }
            }
        
        
        $companyModel = Company::find()->where(['id' => $_POST['company_id']])->one();
        $this->redirect($companyModel->urlname.'/companytrackers');
        
    }


    public function actionCompanyupdate()
    {
        //var_dump($_POST);exit;
        $companyModel = Company::find()->where(['id' => $_POST['id']])->one();
        $companyModel->scenario = 'updatecompany';
        $companyModel->last_update = date("Y-m-d H:i:s");

        if($companyModel->load($_POST)){
            if($companyModel->save()){
                Yii::$app->session->setFlash('CompanyUpdate');
                $this->redirect('../../company/'.$companyModel->urlname.'/companyinfo');
            }else{
                Yii::$app->session->setFlash('CompanyNotUpdate');
                $this->redirect('../../company/'.$companyModel->urlname.'/companyinfo');
            }
        }else{
            echo 'bad';
        }


    }

    public function actionCompanyupdateuser()
    {
        $companyModel = Company::find()->where(['id' => $_POST['company_id']])->one();
        $userModel = User::find()->where(['id' => $_POST['user_id']])->one();
        if($_POST['User']['company_id'] == '0'){
            $userModel->company_id =  $companyModel->id;
            $userModel->users_type = $_POST['User']['users_type'];
        }else{
            $userModel->company_id = $_POST['User']['company_id'];
            $userModel->users_type = $_POST['User']['users_type'];
        }
        $userModel->username = $_POST['User']['username'];
        $userModel->email = $_POST['User']['email'];
        $userModel->phone_number = $_POST['User']['phone_number'];
        $userModel->city = $_POST['User']['city'];
        $userModel->address = $_POST['User']['address'];
        $userModel->country = $_POST['User']['country'];
        $userModel->zip = $_POST['User']['zip'];
        $userModel->first_name = $_POST['User']['first_name'];
        $userModel->last_name = $_POST['User']['last_name'];
        $userModel->scenario = 'update';
        
        if($userModel->save()){
            Yii::$app->session->setFlash('UserUpdate');
            $this->redirect($companyModel->urlname);
        }else{
            Yii::$app->session->setFlash('UserNotUpdate');
            $this->redirect($companyModel->urlname);
        }
    }

    public function actionUserdelete($id,$company_id)
    {
        $userModel = User::find()->where(['id'=>$id])->one();
        if ($userModel) {
            $userModel->delete();
        }
        $userModel = Company::find()->where(['id'=>$company_id])->one();
        Yii::$app->session->setFlash('UserDeleted');
        $this->redirect("../../company/".$userModel->urlname);
    }

    public function actionTrackerdelete($id, $company_id)
    {
        $trackerModel = Tracker::find()->where(['id'=>$id])->one();
        $companyModel = Company::find()->where(['id'=>$company_id])->one();
        if ($trackerModel) {
            if($trackerModel->delete()){
                Yii::$app->session->setFlash('TrackerDeleted');
                $this->redirect($companyModel->urlname.'/companytrackers');
            }else{
                Yii::$app->session->setFlash('TrackerNotDeleted');
                $this->redirect($companyModel->urlname.'/companytrackers');
            }
        }else{
            Yii::$app->session->setFlash('TrackerNotDeleted');
            $this->redirect($companyModel->urlname.'/companytrackers');
        }
    }


    public function actionCompanytrackers($urlname=null)
    {
        $companyModel = Company::find()->where(['urlname' => $urlname])->one();
        if($companyModel == null){
            throw new HttpException(404);
        }else{
    if(((Yii::$app->user->identity->company_id == $companyModel->id) && (Yii::$app->user->identity->users_type == '1')) || (Yii::$app->user->identity->getUserAdmin(Yii::$app->user->identity->id) != null)){
                if($companyModel->active == '1'){
                    $arraySubCompanys = Company::find()->where(['parent_id' => $companyModel->id])->orWhere(['id' => $companyModel->id])->all();
                    $companys_id = '';
                        foreach($arraySubCompanys as $company){
                            $companys_id[] = $company->id;
                        }

                    $arrayCompanyUsers = User::find()->where(['company_id' => $companys_id])->all();
                    $users_id = '';
                        foreach($arrayCompanyUsers as $users){
                            $users_id[] = $users->id;
                        }
                    
                    $arrayTrackerUser = Trackeruser::find()->where(['user_id' => $users_id])->all();
                    $trackers_id = '';
                        foreach($arrayTrackerUser as $tracker_users){
                            $trackers_id[] = $tracker_users->tracker_id;
                        }    
                    $query = Tracker::find()->where(['IMEI' => $trackers_id])->orWhere(['company_id' => $companys_id])->orderBy('time_added DESC');
                    $modelTracker = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);

                    $tracker_user = Trackeruser::find()->all();
                    
                    $user_id_for_select = [];
                    foreach($tracker_user as $tr_us){
                        $user_id_for_select[] = ['user_id' => $tr_us->user_id, 'tracker_id' => $tr_us->tracker_id];
                    }
                    //end trackers for tracker_user table
                    
                    $modelNewTracker = new Tracker();
                    $subCompanyModel = ArrayHelper::map(Company::find()->where(['parent_id' => $companyModel->id])->all(), 'id', 'organization_name');
                    $arrayUser = ArrayHelper::map(User::find()->where(['company_id' => $companyModel->id])->all(), 'id', 'username');
                    
                    if($companyModel->parent_id != 0){
                        $parentCompany = Company::find()->where(['id' => $companyModel->parent_id])->one();
                    }else{
                        $parentCompany = null;
                    }
                    
                    if(isset($_POST['Tracker'])){
                      $modelNewTracker->IMEI = $_POST['Tracker']['IMEI'];
                      $modelNewTracker->name = $_POST['Tracker']['name'];
                      $modelNewTracker->discription = $_POST['Tracker']['discription'];
                      $modelNewTracker->speed_limit = $_POST['Tracker']['speed_limit'];
                      $modelNewTracker->tracker_marker = $_POST['Tracker']['tracker_marker'];
                      $modelNewTracker->company_id = $companyModel->id;
                      $modelNewTracker->user_type = 0;
                      $modelNewTracker->user_id = 0;

                      if($modelNewTracker->save()&&$modelNewTracker->validate()){
                        $modelNewTracker = new Tracker;
                        Yii::$app->session->setFlash('TrackerAdd');
                      }else{
                        Yii::$app->session->setFlash('ErrorValidating');
                        }

                    }
                    
                    if(isset($_GET['SearchTracker'])){
                        $query = Tracker::find()->where(['IMEI' => $trackers_id])->orWhere(['company_id' => $companys_id])->andWhere(['LIKE','IMEI',$_GET['IMEI']])->andWhere(['LIKE','name',$_GET['name']])->orderBy('time_added DESC');
                        $modelTracker = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
                    }
                    if(count($arrayUser) == 0){
                        $arrayUser[] = 'No Data';
                    }
                    
                    $statistic = [];
                    $trackerCount = Tracker::find()->where(['IMEI' => $trackers_id])->orWhere(['company_id' => $companys_id])->count();
                    $activeTrackerCount = Tracker::find()->where(['IMEI' => $trackers_id, 'active' => 1])->orWhere(['company_id' => $companys_id, 'active' => 1])->count();
                    $activeTrackerProcent = ($activeTrackerCount / $trackerCount) * 100;

                    $deactiveTrackerCount = Tracker::find()->where(['IMEI' => $trackers_id, 'active' => 0])->orWhere(['company_id' => $companys_id, 'active' => 0])->count();
                    $deactiveTrackerProcent = ($deactiveTrackerCount / $trackerCount) * 100;

                    $statistic = ['trackerCount' => $trackerCount, 'activeTrackerCount' => $activeTrackerCount, 'activeTrackerProcent' => $activeTrackerProcent, 'deactiveTrackerCount' => $deactiveTrackerCount, 'deactiveTrackerProcent' => $deactiveTrackerProcent];
                        
                    
                        echo $this->render('trackerscompany_page', [
                            'tracker_user' => $user_id_for_select,
                            'parentCompany' => $parentCompany,
                            'arrayUser' => $arrayUser,
                            'sub_company' => $subCompanyModel,
                            'modelNewTracker' => $modelNewTracker,
                            'companyModel' => $companyModel,
                            'modelTracker' => $modelTracker->getModels(),
                            'pagination' => $modelTracker->pagination,
                            'count' => $modelTracker->pagination->totalCount,
                            'urlname' => $urlname,
                            'statistic' => $statistic,
                        ]);
                }else{
                    throw new HttpException(404, 'Sorry, but this company is not active, contact the Administration!');
                }
            }else{
                throw new HttpException(404);
            }
        }
    }

    public function actionCompanyaddtracker($urlname=null)
    {
        $companyModel = Company::find()->where(['urlname' => $_POST['company_name']])->one();

        $modelNewTracker = new Tracker();
        $modelNewTracker->IMEI = $_POST['Tracker']['IMEI'];
        $modelNewTracker->name = $_POST['Tracker']['name'];
        $modelNewTracker->discription = $_POST['Tracker']['discription'];
        $modelNewTracker->tracker_marker = $_POST['Tracker']['tracker_marker'];
        $modelNewTracker->company_id = $companyModel->id;
        $modelNewTracker->user_type = 0;
        $modelNewTracker->user_id = 0;

        if($modelNewTracker->save()&&$modelNewTracker->validate()){
            Yii::$app->session->setFlash('TrackerAdd');
            $this->redirect('../'.$companyModel->urlname.'/companytrackers');
        }else{
            $this->redirect('../'.$companyModel->urlname.'/companytrackers');
            //throw new HttpException(404,'Tracker not save, maybe you not have access');
        }

    }

    public function actionCompanyadduser($urlname=null)
    {
        $companyModel = Company::find()->where(['urlname' => $_POST['company_name']])->one();

        $modelNewUser = new User();
        $modelNewUser->scenario = 'addusercompany';
        $modelNewUser->username = $_POST['User']['username'];
        $modelNewUser->phone_number = $_POST['User']['phone_number'];
        $modelNewUser->email = $_POST['User']['email'];
        $modelNewUser->users_type = $_POST['User']['users_type'];
        $modelNewUser->company_id = $companyModel->id;
        $modelNewUser->password_hash = \Yii::$app->security->generatePasswordHash($_POST['User']['password']);

        if($modelNewUser->save()){
            Yii::$app->session->setFlash('UserAdd');
            $model = new PasswordResetRequestForm();
                $model['email'] = $modelNewUser->email;
                if ($model->sendEmail()){
                    Yii::$app->session->setFlash('EMAIL_sended');
                } else {
                    Yii::$app->session->setFlash('Email_error');
                }
            $this->redirect('../'.$companyModel->urlname);
        }else{
            throw new HttpException(404,'User not save');
        }
    }

    public function actionCompanysubcompany($urlname=null)
    {
        $companyModel = Company::find()->where(['urlname' => $urlname])->one();
        if($companyModel == null){
            throw new HttpException(404);
        }else{
            if((Yii::$app->user->identity->company_id == $companyModel->id) || (Yii::$app->user->identity->getUserAdmin(Yii::$app->user->identity->id) != null)){
                if(Yii::$app->user->identity->users_type == '1'){
                    $modelNewSubCompany = new Company();
                    $modelNewSubCompany->scenario = 'addsubcompany';
                    if(isset($_POST['Company'])){
                      $modelNewSubCompany->organization_name = $_POST['Company']['organization_name'];
                      $modelNewSubCompany->urlname = ($_POST['Company']['urlname']);
                      //$modelNewCompany->urlname = urlencode($modelNewCompany->urlname);

                      $modelNewSubCompany->email = $_POST['Company']['email'];
                      $modelNewSubCompany->phone_number = $_POST['Company']['phone_number'];
                      $modelNewSubCompany->parent_id = $companyModel->id;
                      $modelNewSubCompany->type_company = 2;
                      //var_dump([$modelNewSubCompany,$_POST]);exit;

                        if($modelNewSubCompany->save()&&$modelNewSubCompany->validate()){
                            Yii::$app->session->setFlash('SubCompanyAdd');
                            $modelNewSubCompany = new Company;
                        }else {
                          Yii::$app->session->setFlash('ErrorValidating');
                        }
                    }
                    
                    $query = Company::find()->where(['parent_id' => $companyModel->id, 'type_company' => 2])->orderBy('time_added DESC');
                    $modelSubCompany = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
                    
                    if($_GET['SearchSubCompany']){
                        $query = Company::find()->where(['parent_id' => $companyModel->id, 'type_company' => 2])->andWhere(['LIKE', 'organization_name', $_GET['organization_name']])->andWhere(['LIKE','phone_number', $_GET['phone_number']])->andWhere(['LIKE', 'email', $_GET['email']])->orderBy('time_added DESC');
                        $modelSubCompany = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
                    }
                    
                    $statistic = [];
                    $subcompanyCount = Company::find()->where(['parent_id' => $companyModel->id, 'type_company' => 2])->count();
                    $activeSubcompanyCount = Company::find()->where(['parent_id' => $companyModel->id, 'type_company' => 2, 'active' => 1])->count();
                    $activeSubcompanyProcent = ($activeSubcompanyCount / $subcompanyCount) * 100;

                    $deactiveSubcompanyCount = Company::find()->where(['parent_id' => $companyModel->id, 'type_company' => 2, 'active' => 1])->count();
                    $deactiveSubcompanyProcent = ($deactiveSubcompanyCount / $subcompanyCount) * 100;

                    $statistic = ['subcompanyCount' => $subcompanyCount, 'activeSubcompanyCount' => $activeSubcompanyCount, 'activeSubcompanyProcent' => $activeSubcompanyProcent, 'deactiveSubcompanyCount' => $deactiveSubcompanyCount, 'deactiveSubcompanyProcent' => $deactiveSubcompanyProcent];
                      
                        echo $this->render('companysubcompany_page', [
                            'modelNewSubCompany' => $modelNewSubCompany,
                            'modelSubCompany' => $modelSubCompany->getModels(),
                            'pagination' => $modelSubCompany->pagination,
                            'count' => $modelSubCompany->pagination->totalCount,
                            'companyModel' => $companyModel,
                            'statistic' => $statistic,
                            'urlname' => $urlname
                        ]);

                }else{
                    throw new HttpException(404);
                }
            }else{
                throw new HttpException(404);
            }
        }
    }

    public function actionCompanyaddsubcompany()
    {
        $companyModel = Company::find()->where(['urlname' => $_POST['company_name']])->one();
       // var_dump($companyModel->id);exit;

        $modelNewSubCompany = new Company();
        $modelNewSubCompany->scenario = 'addsubcompany';

        if($modelNewSubCompany->load($_POST)){
            $modelNewSubCompany->parent_id = $companyModel->id;
            $modelNewSubCompany->type_company = 2;

            if($modelNewSubCompany == null){
                throw new HttpException(404);
            }else{
                if($modelNewSubCompany->save()){
                    Yii::$app->session->setFlash('SubCompanyAdd');
                    $this->redirect('../'.$companyModel->urlname.'/companysubcompany');
                }else{
                    throw new HttpException(404);
                }
            }

        }else{
            throw new HttpException(404);
        }
    }

    public function actionCompanytrackergps()
    {
        $arraySubCompanys = Company::find()->where(['parent_id' => $_POST['company_id']])->orWhere(['id' => $_POST['company_id']])->all();
            $companys_id = '';
                foreach($arraySubCompanys as $company){
                    $companys_id[] = $company->id;
                }

            $arrayCompanyUsers = User::find()->where(['company_id' => $companys_id])->all();
            $users_id = '';
                foreach($arrayCompanyUsers as $users){
                    $users_id[] = $users->id;
                }

            $trackersModel = Tracker::find()->where(['user_id' => $users_id])->orWhere(['company_id' => $companys_id])->all();

            $trackers_id = '';
                foreach($trackersModel as $trackers){
                    $trackers_id[] = $trackers->IMEI;
                }

            $trackerInfo = [];
            foreach($trackers_id as $key => $value){
                $trackerInfo[$value] = Trackerinfo::find()->where(['tracker_id' => $value])->orderBy('time DESC')->one();
            }

            $trackerInfo2 = [];
            foreach($trackerInfo as $key2 => $value2){
                $trackerInfo2[$key2] = ['imei' => $value2->tracker_id, 'lat' => $value2->lat,
                    'lng' => $value2->lng, 'time' => $value2->time, 'speed' => $value2->speed,
                    'course' => $value2->course, 'milage' => $value2->milage];
            }

        echo json_encode($trackerInfo2);
    }
    
    public function actionCompanygettracker()
    {
        
        $vid = $_POST['newVid'];
        $do =  $_POST['newDo'];
        $tracker_id = $_POST['tracker_id'];  
     
        $query = new Query;
        $query	->select(['*'])
        		->from('tracker_info')
        		->join(	'LEFT JOIN',
        				'tracker',
        				'tracker.IMEI =tracker_info.tracker_id'
        			)
            ->where("(tracker_info.time > '".$vid."'AND tracker_info.time <'".$do."')")->andWhere(['IMEI' => $tracker_id]);
        $command = $query->createCommand();
        $trackerModel = $command->queryAll();

        //$trackerModel = Trackerinfo::find()->joinWith('tracker')->where("time > '".$vid."' ")->all();
        $tra = array();
        foreach($trackerModel as $keyOne => $trackerObject){
            foreach($trackerObject as $key => $tracker){
                $tra[$keyOne][$key] = $tracker;
            };
        };
        //$qw = array('tracker_id'=> 'good', 'vid'=> $vid, 'do'=> $do);
        echo json_encode($tra);
        
    }
    
    public function actionCompanyreport($urlname=null)
    {
        $companyModel = Company::find()->where(['urlname' => $urlname])->one();
        if($companyModel == null){
            throw new HttpException(404);
        }else{
            if((Yii::$app->user->identity->company_id == $companyModel->id) || (Yii::$app->user->identity->getUserAdmin(Yii::$app->user->identity->id) != null)){
                if(Yii::$app->user->identity->users_type == '1'){
                    
                    $arraySubCompanys = Company::find()->where(['parent_id' => $companyModel->id])->orWhere(['id' => $companyModel->id])->all();
                    $companys_id = '';
                        foreach($arraySubCompanys as $company){
                            $companys_id[] = $company->id;
                        }

                    $arrayCompanyUsers = User::find()->where(['company_id' => $companys_id])->all();
                    $users_id = '';
                        foreach($arrayCompanyUsers as $users){
                            $users_id[] = $users->id;
                        }
                    
                    $arrayTrackerUser = Trackeruser::find()->where(['user_id' => $users_id])->all();
                    $trackers_id = '';
                        foreach($arrayTrackerUser as $tracker_users){
                            $trackers_id[] = $tracker_users->tracker_id;
                        } 
                     //var_dump($trackers_id);exit;   
                    $arrayTracker = ArrayHelper::map(Tracker::find()->where(['IMEI' => $trackers_id, 'active' => 1])->orWhere(['company_id' => $companys_id, 'active' => 1])->all(), 'IMEI', 'name');
                    if($companyModel->parent_id != 0){
                        $parentCompany = Company::find()->where(['id' => $companyModel->parent_id])->one();
                    }else{
                        $parentCompany = null;
                    } 
                    
                    echo $this->render('companyreport_page', [
                        'parentCompany' => $parentCompany,
                        'companyModel' => $companyModel,
                        'urlname' => $urlname,
                        'arrayTracker' => $arrayTracker,
                    ]);
                    
                    }else{
                        throw new HttpException(404);
                    }
                }else{
                throw new HttpException(404);
            }
        }
    }
    
    public function actionUsergraph()
    {
        $company_id = $_POST['data'];
        $role = $_POST['role'];
//        $company_id = 25;
//        $role = 'subCompany';
        
        if($role == 'user'){
            $companyUser = User::find()->where(['company_id' => $company_id])->all();
            $users_id = [];
                foreach($companyUser as $users){
                    $users_id[] = $users->id;
                }
            $TrackerUser = Trackeruser::find()->where(['user_id' => $users_id])->all();
                $arrayUserTracker = [];
                foreach($TrackerUser as $tracker_user){
                    $arrayUserTracker[$tracker_user['user_id']][] = $tracker_user['tracker_id'];
                }
            //tracker_id
                $tracker_id =[];
                foreach($arrayUserTracker as $trackers){
                    foreach($trackers as $tracker){
                        $tracker_id[] = $tracker;
                    }
                }
            //
            $trackerModel = Tracker::find()->where(['IMEI' => $tracker_id])->all();
            $lastTrackersModel = [];

            foreach($trackerModel as $trackers){
                $lastTrackersModel[$trackers->IMEI] = ['name' => $trackers->name, 
                    'time_added' => $trackers->time_added, 
                    'discription' => $trackers->discription,];
            }

            $user_tracker_count = [];
            foreach($arrayUserTracker as $key => $userArrayTracker){
                $user_tracker_count[Yii::$app->user->identity->getUsername($key)] = count($userArrayTracker);
            }
            $res = [];
            $res['tracker_count'] = $user_tracker_count;
            $res['trackerModel'] = $lastTrackersModel;
            $res['UserTracker'] = $arrayUserTracker;

            //var_dump($res);
        }else{
            $subCompanyModel = Company::find()->where(['parent_id' => $company_id])->all();
            $sub_company_id = [];
            foreach($subCompanyModel as $subModel){
                $sub_company_id[] = $subModel->id;
            }
            
            $lastTrackerArray = [];
            foreach($sub_company_id as $s_company_id){
                $usersModel = User::find()->where(['company_id' => $s_company_id])->all();
                $userArray = [];
                foreach($usersModel as $userModel){
                    $userArray[] = $userModel->id;
                }

                $trackersUser = Trackeruser::find()->where(['user_id' => $userArray])->all();
                $trackersModel = Tracker::find()->where(['company_id' => $s_company_id])->all();
                $trackerUsersId = [];
                foreach($trackersUser as $trackerUser){
                    $trackerUsersId[] = $trackerUser->tracker_id;
                }

                $trackersModelId = [];
                foreach($trackersModel as $trackerModel){
                    $trackersModelId[] = $trackerModel->IMEI;
                }
                
                $res['dataGraph'][Company::getCompanyname($s_company_id)] = count($trackerUsersId) + count($trackersModelId);
                
                $tracker_m = Tracker::find()->where(['IMEI' => $trackersModelId])->orWhere(['IMEI' => $trackerUsersId])->all();
                
                foreach($tracker_m as $t_m){
                    $lastTrackerArray[] = ['imei' => $t_m->IMEI, 'name' => $t_m->name, 'discription' => $t_m->discription];
                }
                $res['dataTable'][Company::getCompanyname($s_company_id)] = $lastTrackerArray;
                
            }
        }
        echo json_encode($res); 
    }
    
    public function actionCompanycar($urlname=null){
        $companyModel = Company::find()->where(['urlname' => $urlname])->one();
        if($companyModel == null){
            throw new HttpException(404);
        }else{
            if(((Yii::$app->user->identity->company_id == $companyModel->id) && (Yii::$app->user->identity->users_type == '1')) || (Yii::$app->user->identity->getUserAdmin(Yii::$app->user->identity->id) != null)){
                if($companyModel->active == '1'){
                    if($companyModel->parent_id != 0){
                        $parentCompany = Company::find()->where(['id' => $companyModel->parent_id])->one();
                    }else{
                        $parentCompany = null;
                    }
                    if($_POST['Carinfo']){
                        $newModelCar = new Carinfo;
                        $request = Yii::$app->request;
                        $newModelCar->scenario = 'addcar';
                        if($newModelCar->load($request->post())){
                            if ($newModelCar->save()){
                                 Yii::$app->session->setFlash('CarAdded');
                            }else{
                                 Yii::$app->session->setFlash('CarNotAdded');
                            }
                        };
                    }
                    
                    $arraySubCompanys = Company::find()->where(['parent_id' => $companyModel->id])->orWhere(['id' => $companyModel->id])->all();
                    $companys_id = '';
                        foreach($arraySubCompanys as $company){
                            $companys_id[] = $company->id;
                        }

                    $arrayCompanyUsers = User::find()->where(['company_id' => $companys_id])->all();
                    $users_id = '';
                        foreach($arrayCompanyUsers as $users){
                            $users_id[] = $users->id;
                        }
                    
                    $arrayTrackerUser = Trackeruser::find()->where(['user_id' => $users_id])->all();
                    $trackers_id = '';
                        foreach($arrayTrackerUser as $tracker_users){
                            $trackers_id[] = $tracker_users->tracker_id;
                        }    
                    
                    $trackerArray = ArrayHelper::map(Tracker::find()->where(['IMEI' => $trackers_id])->orWhere(['company_id' => $companys_id])->all(), 'IMEI', 'name');
                    if(count($trackerArray) == 0){
                        $trackerArray[] = 'No mathes';
                    }
                    
                    if($companyModel->parent_id != 0){
                        $parentCompany = Company::find()->where(['id' => $companyModel->parent_id])->one();
                    }else{
                        $parentCompany = null;
                    }
                    
                    $query = Carinfo::find()->where(['company_id' => $companyModel->id]);
                    $modelCarinfo = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
                    
                    $newModelCar = new Carinfo;
                    $newModelCar->scenario = 'addcar';
                    
                    return $this->render('companycar_page',[
                        'parentCompany' => $parentCompany, 
                        'newModelCar' => $newModelCar, 
                        'modelCarinfo' => $modelCarinfo->getModels(),
                        'pagination' => $modelCarinfo->pagination,
                        'count' => $modelCarinfo->pagination->totalCount,
                        'parentCompany' => $parentCompany,
                        'urlname' => $urlname, 
                        'companyModel' => $companyModel,
                        'trackerArray' => $trackerArray, 
                    ]);
                }
            }else{
                throw new HttpException(404);
            }
        }
    }
    
    public function actionCardelete($id,$company_id){
        $carModel = Carinfo::find()->where(['id'=>$id])->one();
        if ($carModel) {
            $carModel->delete();
        }
        
        $companyModel = Company::find()->where(['id' => $company_id])->one();
        Yii::$app->session->setFlash('CarDelete');
        $this->redirect('../../company/'.$companyModel->urlname.'/companycar');
    }
    
    public function actionCarupdate(){
        if($_POST['Carinfo']){
            $carModel = Carinfo::find()->where(['id' => $_POST['car_id']])->one();
            $request = Yii::$app->request;
            $carModel->scenario = 'updatecar';
            if($carModel->load($request->post())){
                if ($carModel->save()){
                     Yii::$app->session->setFlash('CarUpdate');
                }else{
                     Yii::$app->session->setFlash('CarNotUpdate');
                }
            }
            
            $this->redirect('../../company/'.$_POST['company_urlname'].'/companycar');
        }else{
            throw new HttpException(404);
        }
    }
    
    public function actionCompanydriver($urlname=null){
        $companyModel = Company::find()->where(['urlname' => $urlname])->one();
        if($companyModel == null){
            throw new HttpException(404);
        }else{
            if(((Yii::$app->user->identity->company_id == $companyModel->id) && (Yii::$app->user->identity->users_type == '1')) || (Yii::$app->user->identity->getUserAdmin(Yii::$app->user->identity->id) != null)){
                if($companyModel->active == '1'){
   
                    $newDriver = new Driver();
                    $newDriver->scenario = 'adddriver';
                    
                    if($_POST['Driver']){
                        $request = Yii::$app->request;
                        if($newDriver->load($request->post())){
                            $newDriver->company_id = $companyModel->id;
                            if($newDriver->save()){
                                Yii::$app->session->setFlash('DriverAdded');
                                $newDrivercar = new Drivercar();
                                $newDrivercar->car_id = $_POST['Driver']['car_id'];
                                $newDrivercar->driver_id = $newDriver->id;
                                $newDrivercar->save();
                            }else{
                                Yii::$app->session->setFlash('DriverNotAdded');
                            }
                        }
                    }
                    
                    $query = Driver::find()->where(['company_id' => $companyModel->id]);
                    $modelDriver = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
                    
                    
                    $modelCars = Carinfo::find()->where(['company_id' => $companyModel->id])->all();
                    $arrayCars = ArrayHelper::map($modelCars, 'id', 'number');
                    
                    if($companyModel->parent_id != 0){
                        $parentCompany = Company::find()->where(['id' => $companyModel->parent_id])->one();
                    }else{
                        $parentCompany = null;
                    }
                    
                    foreach($modelDriver->getModels() as $driver){
                        $modelDrivercar = Drivercar::find()->where(['driver_id' => $driver->id])->one();
                        $modelCarinfo = Carinfo::find()->where(['id' => $modelDrivercar->car_id])->one();
                        $driver->car_id = $modelCarinfo->number;
                    }
                    
                    return $this->render('companydriver_page',[
                        'parentCompany' => $parentCompany,
                        'urlname' => $urlname,
                        'newDriverModel' => $newDriver,
                        'carsArray' => $arrayCars,
                        'companyModel' => $companyModel,
                        'modelDriver' => $modelDriver->getModels(),
                        'pagination' => $modelDriver->pagination,
                        'count' => $modelDriver->pagination->totalCount,
                    ]);
                }
            }else{
                throw new HttpException(404);
            }
        }
    }
    
    public function actionDriverdelete($id,$company_id){
        $driverModel = Driver::find()->where(['id'=>$id])->one();
        $deleteAll = Drivercar::deleteAll(['driver_id' => $driverModel->id]);
        if ($driverModel) {
            $driverModel->delete();
        }
        $companyModel = Company::find()->where(['id' => $company_id])->one();
        Yii::$app->session->setFlash('DriverDelete');
        $this->redirect('../../company/'.$companyModel->urlname.'/companydriver');
    }
    
    public function actionDriverupdate(){
        if($_POST['Driver']){
            $driverModel = Driver::find()->where(['id' => $_POST['driver_id']])->one();
            $driverModel->scenario = 'updatedriver';
            $request = Yii::$app->request;
                if($driverModel->load($request->post())){
                }
                $driverCar = Drivercar::deleteAll(['driver_id' => $_POST['driver_id']]);
                $newDrivercar = new Drivercar();
                $newDrivercar->car_id = $_POST['Driver']['car_id'];
                $newDrivercar->driver_id = $_POST['driver_id'];


            if($driverModel->save() && $newDrivercar->save()){
                Yii::$app->session->setFlash('DriverUpdate');
            }else{
                Yii::$app->session->setFlash('DriverNotUpdate');
            }
            
            $this->redirect('../../company/'.$_POST['company_urlname'].'/companydriver');
        }else{
            throw new HttpException(404);
        }
    }
    
    public function actionTaskspage($urlname=null) {
        $companyModel = Company::find()->where(['urlname' => $urlname])->one();
        if($companyModel == null){
            throw new HttpException(404);
        }else{
            if(((Yii::$app->user->identity->company_id == $companyModel->id) && (Yii::$app->user->identity->users_type == '1')) || (Yii::$app->user->identity->getUserAdmin(Yii::$app->user->identity->id) != null)){
                if($companyModel->active == '1'){
                    
                    //add task
                    if($_POST['Tasks']){
                        //var_dump($_POST);exit;
                        $newTasksModel = new Tasks();
                        if($_POST['Tasks']['type'] == 1){
                            $newTasksModel->scenario = 'addtask';
                        }else{
                            $newTasksModel->scenario = 'addroute';
                            $newTasksModel->time_start = $_POST['time_start'];
                            $newTasksModel->time_end = $_POST['time_end'];
                            $newTasksModel->for_map = $_POST['for_map'];
                            $newTasksModel->travel_mode = $_POST['Tasks']['travel_mode'];
                        }
                        
                            $newTasksModel->type = $_POST['Tasks']['type'];
                            $newTasksModel->description = $_POST['Tasks']['description'];
                            $newTasksModel->due = $_POST['Tasks']['due'];
                            $newTasksModel->company_id = $_POST['company_id'];
                            //$newTasksModel->tracker_id = $_POST['tracker_task'];
                            $newTasksModel->status = 1;
                            
                            if($newTasksModel->save()){
                                 Yii::$app->session->setFlash('TasksAdded');
                            }else{
                                 Yii::$app->session->setFlash('TasksNotAdded');
                            }
                            $taskSave = Tasks::find()->where(['description' => $_POST['Tasks']['description'], 'due' => $_POST['Tasks']['due']])->one();
                            
                            foreach($_POST['tracker_task'] as $tracker_id){
                                Taskstracker::deleteAll(['task_id' => $taskSave->id, 'tracker_id' => $tracker_id]);
                                $newTaskTracker = new Taskstracker();
                                $newTaskTracker->tracker_id = $tracker_id;
                                $newTaskTracker->task_id = $taskSave->id;
                                $newTaskTracker->save();
                                
                            }
                            
                            $alfav = ['A','B','C','D','E','F','G','H','I','J'];
                            $subTaskAdd = json_decode($_POST['distance']);
                            foreach($subTaskAdd as $key => $subTasks){
                                $subTaskModel = new Subtasks();
                                $subTaskModel->address = $subTasks->address;
                                $subTaskModel->scenario = 'add_sub_task';
                                $subTaskModel->distance = $subTasks->distance;
                                $subTaskModel->marking = $alfav[$key];
                                $subTaskModel->task_id = $taskSave->id;
                                $subTaskModel->description = $_POST['description'.$alfav[$key]];
                                $subTaskModel->start_time = $_POST['time_start'.$alfav[$key]];
                                $subTaskModel->end_time = $_POST['time_end'.$alfav[$key]];
                                $subTaskModel->start_date = $_POST['date_start'.$alfav[$key]];
                                $subTaskModel->end_date = $_POST['date_end'.$alfav[$key]];
                                $subTaskModel->save();
                            }
                    }
                    //end add task
                    
                    $arraySubCompanys = Company::find()->where(['parent_id' => $companyModel->id])->orWhere(['id' => $companyModel->id])->all();
                    $companys_id = '';
                        foreach($arraySubCompanys as $company){
                            $companys_id[] = $company->id;
                        }

                    $arrayCompanyUsers = User::find()->where(['company_id' => $companys_id])->all();
                    $users_id = '';
                        foreach($arrayCompanyUsers as $users){
                            $users_id[] = $users->id;
                        }
                    
                    $arrayTrackerUser = Trackeruser::find()->where(['user_id' => $users_id])->all();
                    $trackers_id = '';
                        foreach($arrayTrackerUser as $tracker_users){
                            $trackers_id[] = $tracker_users->tracker_id;
                        }
                        
                    $trackersModel = Tracker::find()->where(['IMEI' => $trackers_id])->orWhere(['company_id' => $companys_id])->all();
                    $all_trackers_id = '';
                        foreach($trackersModel as $tracker_model){
                            $all_trackers_id[] = $tracker_model->IMEI;
                        }
                    
                        $trackerTaskk = Taskstracker::find()->where(['tracker_id' => $all_trackers_id])->all();
                        $tasks_id = [];
                        foreach($trackerTaskk as $tracker_task){
                            $tasks_id[] = $tracker_task->task_id;
                        }
                    
                    $query = Tasks::find()->where(['id' => $tasks_id])->orWhere(['company_id' => $companyModel->id]);
                    $modelTasks = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
                    
                    
                    $trackerArray = ArrayHelper::map($trackersModel, 'IMEI', 'name');
                    $trackerTask = Taskstracker::find()->where(['tracker_id' => $all_trackers_id])->all();
                    
                    if($companyModel->parent_id != 0){
                            $parentCompany = Company::find()->where(['id' => $companyModel->parent_id])->one();
                        }else{
                            $parentCompany = null;
                        }
                        
                        
                    if($_GET['SearchTask']){
                        $TaskTracker = $_GET['tracker_task'];
                        if($_GET['OrderBy'] == 2){
                            $OrderBy = 'DESC';
                        }else{
                            $OrderBy = 'ASC';
                        }
                        
                        if($_GET['typeTask'] == 0){
                            $_GET['typeTask'] = [1,2];
                        }
                        if(count($TaskTracker) > 0){
                            $trackerTaskk = Taskstracker::find()->where(['tracker_id' => $TaskTracker])->all();
                            $task_id = [];
                            foreach($trackerTaskk as $tracker_task){
                                $task_id[] = $tracker_task->task_id;
                            }
                            $query = Tasks::find()->where(['id'=>$task_id])->andWhere(['Like', 'due', $_GET['due']])->andWhere(['Like', 'description', $_GET['description']])->andWhere(['type' => $_GET['typeTask']])->orderBy('time_added '.$OrderBy);
                            $modelTasks = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
                            //var_dump($task_id);exit;
                        }else{
                            $query = Tasks::find()->where(['id'=>$tasks_id])->andWhere(['Like', 'due', $_GET['due']])->andWhere(['Like', 'description', $_GET['description']])->andWhere(['type' => $_GET['typeTask']])->orderBy('time_added '.$OrderBy);
                            $modelTasks = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
                        }
                        
                    }
                    
                    if(count($trackerArray) == 0){
                        $trackerArray[] = 'No data';
                    }
                    
                    $newTasksModel = new Tasks();
                    return $this->render('companytasks_page',[
                        'parentCompany' => $parentCompany,
                        'trackerTask' => $trackerTask,
                        'trackerArray' => $trackerArray,
                        'modelTasks' => $modelTasks->getModels(),
                        'pagination' => $modelTasks->pagination,
                        'count' => $modelTasks->pagination->totalCount,
                        'newTasksModel' => $newTasksModel,
                        'companyModel' => $companyModel,
                        'urlname' => $urlname
                    ]);
                }
            }else{
                throw new HttpException(404);
            }
        }
    }
    
    public function actionTaskdelete($id,$company_id){
        $taskModel = Tasks::find()->where(['id'=>$id])->one();
        $trackerTask = Taskstracker::deleteAll(['task_id' => $id]);

        if ($taskModel){
            $taskModel->delete();
        }
        $companyModel = Company::find()->where(['id' => $company_id])->one();
        Yii::$app->session->setFlash('TaskDelete');
        $this->redirect('../../company/'.$companyModel->urlname.'/taskspage');
    }
    
    public function actionTaskupdate() {
            echo '<pre>';
            print_r($_POST);
            echo '</pre>';
            exit;
        if($_POST['Tasks']){
            $taskModel = Tasks::find()->where(['id' => $_POST['task_id']])->one();
            if($_POST['Tasks']['type'] == 1){
                $taskModel->scenario = 'addtask';
            }else{
                $taskModel->scenario = 'addroute';
                $taskModel->time_start = $_POST['time_start'];
                $taskModel->time_end = $_POST['time_end'];
                $taskModel->for_map = $_POST['for_map'];
                $taskModel->travel_mode = $_POST['Tasks']['travel_mode'];
            }
            $taskModel->description = $_POST['Tasks']['description'];
            $taskModel->due = $_POST['Tasks']['due'];
            $taskModel->status = $_POST['Tasks']['status'];

            if($taskModel->save()){
                Yii::$app->session->setFlash('TasksUpdate');
            }else{
                Yii::$app->session->setFlash('TasksNotUpdate');
            }
            
            $newTaskTracker = Taskstracker::deleteAll(['task_id' => $_POST['task_id']]);
            
            foreach($_POST['tracker_id'] as $tracker_id){
                $newTaskTracker = new Taskstracker();
                $newTaskTracker->tracker_id = $tracker_id;
                $newTaskTracker->task_id = $_POST['task_id'];
                $newTaskTracker->save();
            }
            
            
            $this->redirect('../../company/'.$_POST['company_urlname'].'/taskspage');
        }else{
            throw new HttpException(404);
        }
    }
    
    public function actionGetcompanyinfo()
    {
//        $_POST['role'] = 'Tracker';
//        $_POST['companyId'] = '25';
//        $_POST['reportTypeValue'] = 4;
        
        if($_POST){
            $data = [];
            
            //function if(tracker=report)
            function getTrackerForReport($trackerIdArray){
                $reportId = $_POST['reportTypeValue'];
                $trackerIdR = [];
                foreach($trackerIdArray as $tracker_one_id){
                    $tracker_report = Tracker_report::find()->where(['tracker_id' => $tracker_one_id, 'report_id' => $reportId])->one();
                    if($tracker_report != null){
                        $trackerIdR[] = $tracker_one_id;
                    }
                }
                return $trackerIdR;
            }
            //end function tracker=report
            
            //function get SubCompanytracker
            function subCompanyTracker($subcompanyid){
                $subCompanyUserModel = User::find()->where(['company_id' => $subcompanyid])->all();
                $userSubCompanyId = [];
                    foreach($subCompanyUserModel as $companyUser){
                        $userSubCompanyId[] = $companyUser->id;
                    }   
                
                $arraySubCompanyTrackerUser = Trackeruser::find()->where(['user_id' => $userSubCompanyId])->all();
                    $trackers_id = '';
                    foreach($arraySubCompanyTrackerUser as $tracker_users){
                        $trackers_id[] = $tracker_users->tracker_id;
                    }
                    
                    $subCompanyTrackerArray = Tracker::find()->where(['IMEI' => $trackers_id])->orWhere(['company_id' => $subcompanyid])->all();
                    
                    $last_trackers_id = [];
                    foreach($subCompanyTrackerArray as $trackers_users){
                        $last_trackers_id[] = $trackers_users->IMEI;
                    }
                    //var_dump($last_trackers_id);
                    $subCompanyTrackerArray = ArrayHelper::map(Tracker::find()->where(['IMEI' => getTrackerForReport($last_trackers_id)])->all(),'IMEI','name');
                    
                    
                return $subCompanyTrackerArray;
            }
            //
            
            
            //
            // if select SubCompany
            if($_POST['company_id']){
                $subCompanyModel = ArrayHelper::map(Company::find()->where(['parent_id' => $_POST['company_id'], 'type_company' => 2])->all(), 'id', 'organization_name');
                $data['subCompanyArray'] = $subCompanyModel;
                $subcompanyid = [];
                    foreach($subCompanyModel as $key => $companyModel){
                        $subcompanyid[] = $key;
                    }
                
                    $trackerArray = subCompanyTracker($subcompanyid);
                     
                $data['subCompanyTrackerArray'] = $trackerArray;
                echo json_encode($data);
            }
            
                //
                //get array sub company
                if($_POST['subCompanyArray']){
                    $userModel = ArrayHelper::map(User::find()->where(['company_id' => $_POST['subCompanyArray']])->all(), 'id', 'username');
                    $data['userArray'] = $userModel;
                    $trackerArray = subCompanyTracker($_POST['subCompanyArray']);
                    
                    if($_POST['subCompanyArray'] == 'maybe null'){
                        $subCompanyModel = ArrayHelper::map(Company::find()->where(['parent_id' => $_POST['companyId'], 'type_company' => 2])->all(), 'id', 'organization_name');
                            //$data['subCompanyArray'] = $subCompanyModel;
                            $subcompanyid = [];
                                foreach($subCompanyModel as $key => $companyModel){
                                    $subcompanyid[] = $key;
                                }
                        $usersModel = ArrayHelper::map(User::find()->where(['company_id' => $subcompanyid])->all(), 'id', 'username');
                        $data['userArray'] = $usersModel;
                        
                        $trackerArray = subCompanyTracker($subcompanyid);
                    }
                    
                    
                    
                    $data['subCompanyTrackerArray'] = $trackerArray;
                    echo json_encode($data);
                }
                //end get array sub company
                //
                
                if($_POST['subCompanyUserArray']){
                    if($_POST['subCompanyUserArray'] != 'maybe null'){
                        $arraySubCompanyTrackerUser = Trackeruser::find()->where(['user_id' => $_POST['subCompanyUserArray']])->all();
                        $trackers_id = '';
                        foreach($arraySubCompanyTrackerUser as $tracker_users){
                            $trackers_id[] = $tracker_users->tracker_id;
                        }
                        $subCompanyTrackerArray = ArrayHelper::map(Tracker::find()->where(['IMEI' => getTrackerForReport($trackers_id)])->all(),'IMEI','name');
                        $data['subCompanyTrackerArray'] = $subCompanyTrackerArray;
                    }else{
                        $subCompanyModel = ArrayHelper::map(Company::find()->where(['parent_id' => $_POST['companyId'], 'type_company' => 2])->all(), 'id', 'organization_name');
                        if($_POST['forUser'] == 'User'){
                            $subCompanyModel = ArrayHelper::map(Company::find()->where(['parent_id' => $_POST['companyId']])->orWhere(['id' => $_POST['companyId']])->all(), 'id', 'organization_name');
                        }
                            //$data['subCompanyArray'] = $subCompanyModel;
                            $subcompanyid = [];
                                foreach($subCompanyModel as $key => $companyModel){
                                    $subcompanyid[] = $key;
                                }
                        $trackerArray = subCompanyTracker($subcompanyid);
                        $data['subCompanyTrackerArray'] = $trackerArray;
                    }
                    echo json_encode($data);
                }
            //end select SubCompany
            //
            
                
            if($_POST['role'] == 'User'){
                $modelCompany = Company::find()->where(['parent_id' => $_POST['companyId']])->orWhere(['id' => $_POST['companyId']])->all();
                    $companys_id = '';
                        foreach($modelCompany as $company){
                            $companys_id[] = $company->id;
                        }
                $modelUser = ArrayHelper::map(User::find()->where(['company_id' => $companys_id])->all(), 'id', 'username');
                $data['modelUser'] = $modelUser;
                
                $data['modelTracker'] = subCompanyTracker($companys_id);
                //var_dump($data['modelTracker']);exit;
                echo json_encode($data);
            }
            
            
            if($_POST['role'] == 'Tracker'){
                $arraySubCompanys = Company::find()->where(['parent_id' => $_POST['companyId']])->orWhere(['id' => $_POST['companyId']])->all();
                    $companys_id = '';
                        foreach($arraySubCompanys as $company){
                            $companys_id[] = $company->id;
                        }

                    $arrayCompanyUsers = User::find()->where(['company_id' => $companys_id])->all();
                    $users_id = '';
                        foreach($arrayCompanyUsers as $users){
                            $users_id[] = $users->id;
                        }
                    
                    $arrayTrackerUser = Trackeruser::find()->where(['user_id' => $users_id])->all();
                    $trackers_id = '';
                        foreach($arrayTrackerUser as $tracker_users){
                            $trackers_id[] = $tracker_users->tracker_id;
                        }    
                    $TrackersArray = Tracker::find()->where(['IMEI' => $trackers_id])->orWhere(['company_id' => $companys_id])->all();
                    
                    $last_trackers_id = [];
                    foreach($TrackersArray as $trackers_m){
                        $last_trackers_id[] = $trackers_m->IMEI;
                    }
                    
                    $data['TrackerData'] = ArrayHelper::map(Tracker::find()->where(['IMEI' => getTrackerForReport($last_trackers_id)])->all(),'IMEI','name');
                
                echo json_encode($data);
            }
            
        }else{
            throw new HttpException(404);
        }
    }
    
    public function actionGetdataforreport(){
        $TrackerArray = $_POST['TrackerArray'];
        $ReportType = $_POST['ReportType'];
        $TimeStart = $_POST['TimeStart']." 00:00:01";
        $TimeEnd = $_POST['TimeEnd']." 23:59:59";
        ini_set('memory_limit', '-1');
        
//        $TrackerArray = ['0' => "359710049048870", '2' => "868683021037738"];
//        $TimeStart = "2015-11-01 00:00:01";
//        $TimeEnd = "2015-12-05 00:00:01";
//        $ReportType = 4;
//        ini_set('memory_limit', '-1');
        
        switch ($ReportType){
            case '1':
                $trackerInfoModel = Trackerinfo::find()->select(['tracker_id','lat', 'lng','time'])->where(['tracker_id' => array_values($TrackerArray)])->andWhere("time > '".$TimeStart."' and time < '".$TimeEnd."'")->andWhere(['not', ['speed' => '']])->andWhere(['not', ['speed' => '0.00']])->orderBy('time ASC')->all();
                $data_tracker = [];
                foreach($trackerInfoModel as $trackerInfo){
                    $data_tracker[$trackerInfo->tracker_id][] = [$trackerInfo->lat, $trackerInfo->lng, $trackerInfo->time];
                }
                //var_dump($data_tracker);
                
                echo json_encode($data_tracker);
                break;
            
            case '2':
                $trackerInfoModel = Trackerinfo::find()->where(['tracker_id' => array_values($TrackerArray)])->andWhere("time > '".$TimeStart."' and time < '".$TimeEnd."'")->andWhere(['not', ['speed' => '']])->andWhere(['not', ['speed' => '0.00']])->orderBy('time ASC')->all();
                //var_dump($trackerInfoModel);exit;
                $resultData = [];
                foreach($trackerInfoModel as $tracker){
                    $speed = $tracker->speed;
                    $date = date_create($tracker->time);
                    
                    $resultData[$tracker->tracker_id][date_format($date, 'Y-m-d')]['speed'] = $resultData[$tracker->tracker_id][date_format($date, 'Y-m-d')]['speed'] + floatval($tracker->speed);
                    $resultData[$tracker->tracker_id][date_format($date, 'Y-m-d')]['countSpeed'] = $resultData[$tracker->tracker_id][date_format($date, 'Y-m-d')]['countSpeed'] + 1;
                    
                    if(is_float(floatval($tracker->speed)) == true){
                        if($resultData[$tracker->tracker_id][date_format($date, 'Y-m-d')]['MaxSpeed'] <= $speed){
                            $resultData[$tracker->tracker_id][date_format($date, 'Y-m-d')]['MaxSpeed'] = $speed;
                        }
                    }
                }
                
                foreach($resultData as $trackerIMEI => $trackerInfo){
                    foreach($trackerInfo as $day => $info){
                        $resultDataS[$trackerIMEI][$day]['MaxSpeed'] = $info['MaxSpeed'];
                        $resultDataS[$trackerIMEI][$day]['averageSpeed'] = $info['speed'] / $info['countSpeed'];
                    }
                }
                //var_dump($resultDataS);exit;
                echo json_encode($resultDataS);
                break;
                
                
                
            case '4':
                $trackerInfoModel = Trackerinfo::find()->select(['tracker_id','fuel','time'])->where(['tracker_id' => array_values($TrackerArray)])->andWhere(['not', ['fuel' => '']])->andWhere(['not', ['fuel' => '0.00%']])->andWhere(['not', ['fuel' => 'null']])->orderBy('time ASC')->all();
                $data_tracker = [];
                $k = 0;
                foreach($trackerInfoModel as $trackerInfo){
                    if($data_tracker[$trackerInfo->tracker_id][date_format(date_create($trackerInfo->time), 'Y-m-d')][$k] != null){
                        if(end($data_tracker[$trackerInfo->tracker_id][date_format(date_create($trackerInfo->time), 'Y-m-d')][$k]) < floatval($trackerInfo->fuel)){
                            $k = $k + 1;
                        }
                    }
                    $data_tracker[$trackerInfo->tracker_id][date_format(date_create($trackerInfo->time), 'Y-m-d')][$k][] = floatval($trackerInfo->fuel);
                }
                $lastArray = [];
                foreach($data_tracker as $trackerImei => $trackersData){
                    foreach($trackersData as $trackerDays => $trackerData){
                        $trackerSubDayData = 0;
                        foreach($trackerData as $trackerDataDay){
                             $trackerSubDayData =  $trackerSubDayData + ($trackerDataDay[0] - end($trackerDataDay));
                        }
                             $lastArray[$trackerImei][$trackerDays] = $trackerSubDayData;
                    }
                }
                
//                echo "<pre>";
//                print_r($lastArray);exit;
//                echo "</pre>";
                echo json_encode($lastArray);
                break;
                
                
                
            case '6':
                
//                    $datetime1 = date_create('2015-10-11 12:02:03');
//                    $datetime2 = date_create('2015-12-11 13:05:24');
//                    $interval = date_diff($datetime1, $datetime2);
//                    var_dump($interval);exit;
                    //echo $interval->format('%g дней').'----------------------------<br>';
                
                
                $trackerInfoModel = Trackerinfo::find()->where(['tracker_id' => array_values($TrackerArray)])->andWhere("time > '".$TimeStart."' and time < '".$TimeEnd."'")->andWhere(['not', ['acc' => '']])->all();
                foreach($trackerInfoModel as $tracker){
                    $trackerModel[$tracker->tracker_id][] = ['time' => $tracker->time, 'acc' => $tracker->acc];
                }
                
                $arrayDayTrackerInfo = [];
                foreach($trackerModel as $trackerImei => $trackersInfo){
                    $arrayAccOnMini = [];;
                    $arrayAccOn = [];
                    foreach($trackersInfo as $tinfo){
                        $date = date_create($tinfo['time']);
                        if($tinfo['acc'] == 1){
                            if(count($arrayAccOnMini[date_format($date, 'Y-m-d')]) > 0){
                                //echo $arrayAccOnMini[date_format($date, 'Y-m-d')][count($arrayAccOnMini[date_format($date, 'Y-m-d')])-1];
                                //echo $tinfo['time'];
                                $interval = date_diff(date_create($arrayAccOnMini[date_format($date, 'Y-m-d')][count($arrayAccOnMini[date_format($date, 'Y-m-d')])-1]), date_create($tinfo['time']));
                                    if(((int)$interval->h > 0) || ((int)$interval->i >= 5)){
                                        $arrayAccOn[] = $arrayAccOnMini;
                                        $arrayAccOnMini = array();
                                    }else{
                                        $arrayAccOnMini[date_format($date, 'Y-m-d')][] = $tinfo['time'];
                                    }
                            }else{
                                $arrayAccOnMini[date_format($date, 'Y-m-d')][] = $tinfo['time'];
                            }
                        }else{
                            if(count($arrayAccOnMini) != 0){
                                $arrayAccOn[] = $arrayAccOnMini;
                                //$arrayAccOn[] = [$arrayAccOnMini[0], $arrayAccOnMini[count($arrayAccOnMini)-1]];
                            }
                            $arrayAccOnMini = array();
                        }
                    }
                    $arrayDayTrackerInfo[$trackerImei] = $arrayAccOn;
                }
                
                $countSecondsForDay = [];
                foreach($arrayDayTrackerInfo as $imei => $trackersInfo){
                    //var_dump($imei);
                    foreach($trackersInfo as $trackerInfo){
                        foreach($trackerInfo as $key => $tinfo){
                            $interval = date_diff(date_create($tinfo[0]), date_create($tinfo[count($tinfo)-1]));
                            $countSecondsForDay[$imei][$key] = $countSecondsForDay[$imei][$key] + (($interval->h * 3600)+ ($interval->i * 60) + $interval->s);
                        }
                    }
                }
                
                $last_array = [];
                foreach($countSecondsForDay as $imei => $trackerDayInfo){
                    foreach($trackerDayInfo as $day => $info){
                       $last_array[$imei][$day] = (float)date("H", mktime(0, 0, $info)).','.date("i", mktime(0, 0, $info));
                        //$last_array[$imei][$day] = $info;
                    };
                };
                
                echo json_encode($last_array);
                break;
            default:
                $last_array = 'not have';
                echo json_encode($last_array);
        } 
    }
    public function actionCompanyinfo($urlname=null){
        
        $companyModel = Company::find()->where(['urlname' => $urlname])->one();
        
        return $this->render('companyinfo_page',[
            'companyModel' => $companyModel,    
            'urlname' => $urlname,
        ]);
    }
    
    public function actionTaskdataformap(){
        $task_id = $_POST['task_id'];
        $taskModel = Tasks::find()->where(['id' => $task_id])->one();
        echo json_encode($taskModel->for_map);
    }
    
    public function actionSavedropedfile(){
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/content/avto/';   //2
        // mkdir($storeFolder);
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder)){
              mkdir($storeFolder, 0777, true);
            }
              $tempFile = $_FILES['file']['tmp_name'];          //3

              $targetPath =  $storeFolder;  //4

              $for_name = time();

              $targetFile =  $targetPath.$for_name.'.jpg';  //5

              move_uploaded_file($tempFile,$targetFile); //6
              return $for_name.'.jpg';
        }
    }
    
    public function actionDeletefile(){
        $storeFolder = \Yii::getAlias('@webroot').'/images/avto/';
        $path = $storeFolder.$_POST['file_name'];
        if(unlink($path)){
            echo 'delete';
        }else{
            echo 'problem';
        }
    }
}
