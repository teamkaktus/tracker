<?php

namespace app\controllers;

use Yii;
use app\models\Lang;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
 error_reporting( E_ERROR );
/**
 * LangController implements the CRUD actions for Lang model.
 */
class LangController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Lang models.
     * @return mixed
     */
    public function actionIndex()
    {
        $newModel = new Lang();
        
        $dataProvider = new ActiveDataProvider([
            'query' => Lang::find(),
        ]);
        if ($newModel->load(Yii::$app->request->post())) {
            $newModel = new Lang();
            if($newModel->save()){
                Yii::$app->session->setFlash('LangAdd');
            }else{
                Yii::$app->session->setFlash('LangNotAdd');
            }
        }
        
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'newModel' => $newModel,
        ]);
    }

    /**
     * Displays a single Lang model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Lang model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Lang();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Lang model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Lang model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Lang model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Lang the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Lang::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionSet($lang = null) {
      $lang_array = [];
      $langs = Lang::find()->all();
      foreach ($langs as $value) {
        $lang_array[] =$value->local;
      }
      if(in_array($lang,$lang_array)){
        if(!Yii::$app->user->isGuest){
          $user = User::findOne(Yii::$app->user->id);
          $user->language = $lang;
          if(!$user->save()){
            throw new NotFoundHttpException('The requested page does not exist.');
          }
          Yii::$app->session->set('language', $lang);
          Yii::$app->response->cookies->add(new \yii\web\Cookie([
                 'name' => 'language',
                 'value' => $lang,
                 'expire' => time() + 60 * 60 * 24 * 180
             ]));
        }else{
          Yii::$app->session->set('language', $lang);
          Yii::$app->response->cookies->add(new \yii\web\Cookie([
                 'name' => 'language',
                 'value' => $lang,
                 'expire' => time() + 60 * 60 * 24 * 180
             ]));
        }
      }else{

      }



        return $this->redirect(Yii::$app->request->referrer);
    //возвращаем пользователя откуда пришел
    }
}
