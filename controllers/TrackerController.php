<?php

namespace app\controllers;
use yii\web\HttpException;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;

use Yii;
use yii\helpers\BaseJson;
use yii\web\Controller;
use app\models\Tracker;
use app\models\Tasks;
use app\models\Subtasks;
use app\models\Driver;
use app\models\DriverCar;
use app\models\Taskstracker;
use app\models\Trackerinfo;
use app\models\Carinfo;
use yii\helpers\Html;
use yii\db\Query;

class TrackerController extends Controller
{
    public function actions()
    {
        return array(
            'error' => array(
                'class' => 'yii\web\ErrorAction',
            ),
            'captcha' => array(
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ),
        );
    }

    public function actionIndex()
    {
        $arrayTrackerUser = Trackeruser::find()->where(['user_id' => Yii::$app->user->identity->id])->all();
            $trackers_id = '';
                foreach($arrayTrackerUser as $tracker_users){
                    $trackers_id[] = $tracker_users->tracker_id;
                }

        $query = Tracker::find()->where(['user_id' => Yii::$app->user->identity->id])->orWhere(['id' => $trackers_id])->orderBy('id DESC');
        $modelTracker = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 5]]);
        echo $this->render('index', [
            'modelTracker' => $modelTracker->getModels(),
            'pagination' => $modelTracker->pagination,
            'count' => $modelTracker->pagination->totalCount,
        ]);
    }

    public function actionTrackergps()
    {
        ini_set('memory_limit', '-1');
        //$id = '2015-06-06 18:03:24';
        $vid = $_POST['newVid'];
        $do =  $_POST['newDo'];
        $tracker_id = $_POST['tracker_id'];
        $query = new Query;
        $query	->select(['*','id'=>'tracker_info.id'])
        		->from('tracker_info')
        		->join(	'LEFT JOIN',
        				'tracker',
        				'tracker.IMEI =tracker_info.tracker_id'
        			)
            ->where("(tracker_info.time > '".$vid."'AND tracker_info.time <'".$do."')")->andWhere(['IMEI' => $tracker_id])->andWhere(['or',['acc'=> '1'],['and',['<>','type','001'],['<>','type','tracker']]]);
        $command = $query->createCommand();
        $trackerModel = $command->queryAll();

        //$trackerModel = Trackerinfo::find()->joinWith('tracker')->where("time > '".$vid."' ")->all();
        $tra = array();
        foreach($trackerModel as $keyOne => $trackerObject){
            foreach($trackerObject as $key => $tracker){
                $tra[$keyOne][$key] = $tracker;
            };
        };
        //$qw = array('tracker_id'=> 'good', 'vid'=> $vid, 'do'=> $do);
        echo json_encode($tra);
        //var_dump($tra);

    }
    public function actionNewdata()
    {
        ini_set('memory_limit', '-1');
        //$id = '2015-06-06 18:03:24';
        $from = $_POST['from'];
        $tracker_id = $_POST['tracker_id'];
        $query = new Query;
        $query  ->select(['*','id'=>'tracker_info.id'])
        		->from('tracker_info')
        		->join(	'LEFT JOIN',
        				'tracker',
        				'tracker.IMEI =tracker_info.tracker_id'
        			)
            ->where("(tracker_info.time > '".$from."')")->andWhere(['IMEI' => $tracker_id])->andWhere(['or',['acc'=> '1'],['and',['<>','type','001'],['<>','type','tracker']]]);
        $command = $query->createCommand();
        $trackerModel = $command->queryAll();

        //$trackerModel = Trackerinfo::find()->joinWith('tracker')->where("time > '".$vid."' ")->all();
        $tra = array();
        foreach($trackerModel as $keyOne => $trackerObject){
            foreach($trackerObject as $key => $tracker){
                $tra[$keyOne][$key] = $tracker;
            };
        };
        //$qw = array('tracker_id'=> 'good', 'vid'=> $vid, 'do'=> $do);
        echo json_encode($tra);
        //var_dump($tra);

    }
    public function actionTrackeractions()
    {
        ini_set('memory_limit', '-1');
        //$id = '2015-06-06 18:03:24';
        $vid = $_POST['newVid'];
        $do =  $_POST['newDo'];
        $tracker_id = $_POST['tracker_id'];
        $query = new Query;
        $query  ->select(['*','id'=>'tracker_info.id'])
        		->from('tracker_info')
        		->join(	'LEFT JOIN',
        				'tracker',
        				'tracker.IMEI =tracker_info.tracker_id'
        			)
            ->where("(tracker_info.time > '".$vid."'AND tracker_info.time <'".$do."')")->andWhere(['IMEI' => $tracker_id])->andWhere(['<>', 'type', 'tracker'])->andWhere(['<>', 'type', '001']);
        $command = $query->createCommand();
        $trackerModel = $command->queryAll();

        //$trackerModel = Trackerinfo::find()->joinWith('tracker')->where("time > '".$vid."' ")->all();
        $tra = array();
        foreach($trackerModel as $keyOne => $trackerObject){
            foreach($trackerObject as $key => $tracker){
                $tra[$keyOne][$key] = $tracker;
            };
        };
        //$qw = array('tracker_id'=> 'good', 'vid'=> $vid, 'do'=> $do);
        echo json_encode($tra);
        //var_dump($tra);

    }
    public function actionShowlast()
    {
        ini_set('memory_limit', '-1');
        $tracker_id = $_POST['tracker_id'];
        $query = new Query;
        $query  ->select(['*','id'=>'tracker_info.id'])
        		->from('tracker_info')
        		->join(	'LEFT JOIN',
        				'tracker',
        				'tracker.IMEI =tracker_info.tracker_id'
        			)
            ->where(['IMEI' => $tracker_id])->limit(1)->orderBy(['tracker_info.id' => SORT_DESC]);
        $command = $query->createCommand();
        $trackerModel = $command->queryAll();

        //$trackerModel = Trackerinfo::find()->joinWith('tracker')->where("time > '".$vid."' ")->all();
        $tra = array();
        foreach($trackerModel as $keyOne => $trackerObject){
            foreach($trackerObject as $key => $tracker){
                $tra[$keyOne][$key] = $tracker;
            };
        };
        echo json_encode($tra);
        //var_dump($tra);

    }

    public function actionShow($id=null,$request='show')
    {
        if($request !='show'){
            $id = $request;
        }
        $tracker_id = $id;
        $trackerModel = Tracker::find()->where(['IMEI' => $tracker_id])->one();

        $trackerInfoModel = [];// Trackerinfo::find()->where(['tracker_id' => $tracker_id])->all();
        foreach($trackerInfoModel as $keyOne => $trackerObject){
            foreach($trackerObject as $key => $tracker){
                $tra[$keyOne][$key] = $tracker;
            };
        };
        $trackerTask = Taskstracker::find()->where(['tracker_id' => $tracker_id])->all();
        $tasks_id = [];
        foreach($trackerTask as $trackertask){
            $tasks_id[] = $trackertask->task_id;
        }
        $tasksModel = Tasks::find()->where(['id' => $tasks_id])->all();

//        $driverTracker = Drivertracker::find()->where(['tracker_id' => $tracker_id])->all();
//        $driver_id = [];
//        foreach($driverTracker as $trackerD){
//            $driver_id[] = $trackerD->driver_id;
//        }
        $driverModel = Driver::find()->where(['id' => $driver_id])->all();

        $carModel = Carinfo::find()->where(['tracker_imei' => $tracker_id])->one();
        
        echo $this->render('my_tracker', [
             'tasksModel' => $tasksModel,
             //'driverModel' => $driverModel,
             'tracker_info' => $trackerInfoModel,
             'trackerModel' => $trackerModel,
             'carModel' => $carModel,
        ]);
    }

    public function actionSubtasksview()
    {
        $task_id = $_POST['id_task'];
        $subtaskModal = Subtasks::find()->where(['task_id'=>$task_id])->all();
        
        $lastArraySubTasks = [];
        foreach($subtaskModal as $subTask){
            $lastArraySubTasks[] = ['marking'=>$subTask->marking,
                'address'=>$subTask->address,
                'distance'=>$subTask->distance,
                'description'=>$subTask->description,
                'start_time'=>$subTask->start_time, 
                'end_time'=>$subTask->end_time, 
                'start_date'=>$subTask->start_date, 
                'end_date'=>$subTask->end_date];
        }
        echo json_encode($lastArraySubTasks);
    }
    
    public function actionConnections()
    {
        echo $this->render('connections',[
            
        ]);
    }
}
