<?php

namespace app\controllers;
use yii\web\HttpException;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;

use Yii;
use yii\helpers\BaseJson;
use yii\web\Controller;
use app\models\Tracker;
use app\models\Trackerinfo;
use app\models\Trackeruser;
use yii\helpers\Html;
use yii\db\Query;
use app\models\LoginForm;
use app\models\User;

    class TrackerapiController extends Controller
    {
    public function actions()
    {
        return array(
            'error' => array(
                'class' => 'yii\web\ErrorAction',
            ),
            'captcha' => array(
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ),
        );
    }

    public function actionLogin(){
      $model = new LoginForm();
      //var_dump($_GET);
      $model->username = $_GET['username'];
      $model->password = $_GET['password'];
      //var_dump($model->loginApi());
      if (($model->loginApi())) {
          //var_dump($model);
          return json_encode($model->loginApi());
      } else {
          //var_dump($model->getErrors());
          return json_encode($model->getErrors());
      }
    }

    public function actionIndex()
     {

       if(User::findIdentityByAccessToken($_GET["access_token"])){
         //var_dump(User::findIdentityByAccessToken($_GET["access_token"])->id);
     $trackerModel = Trackeruser::find()->where(['user_id' => User::findIdentityByAccessToken($_GET["access_token"])->id])->all();
     $tracker_id = [];
     foreach($trackerModel as $tracker){
         $tracker_id[] = $tracker->tracker_id;
     }

     $modelTracker = Tracker::find()->where(['IMEI' => $tracker_id])->orderBy('id DESC')->all();
     //var_dump($modelTracker);

     $some_array = [];
     foreach($modelTracker as $key2 => $value2){
     foreach($value2 as $key => $value){
     $some_array[$key2][$key] = $value;
     }
     }
     return json_encode($some_array);
   }
     }


     public function actionShow($imei)
     {
     $vid = $_GET['vid'];
     $do = $_GET['do'];;

     //var_dump([$vid,$do,$date]);
     $tracker_id = $imei;
     $query = new Query;
     $query ->select(['*'])
       ->from('tracker_info')
       ->join( 'LEFT JOIN',
         'tracker',
         'tracker.IMEI =tracker_info.tracker_id'
        )
     ->where("(tracker_info.time > '".$vid."'AND tracker_info.time <'".$do."')")->andWhere(['tracker_info.tracker_id' => $tracker_id]);
     $command = $query->createCommand();
     $trackerModel = $command->queryAll();

     $tra = array();
     foreach($trackerModel as $keyOne => $trackerObject){
     foreach($trackerObject as $key => $tracker){
     $tra[$keyOne][$key] = $tracker;
     };
     };

     //$qw = array('tracker_id'=> 'good', 'vid'=> $vid, 'do'=> $do);
     echo json_encode($tra);
     //var_dump($tra);
     }
 //public function actionTrackergps()
//    {
//        //$id = '2015-06-06 18:03:24';
//        $vid = $_POST['newVid'];
//        $do =  $_POST['newDo'];
//        $tracker_id = $_POST['tracker_id'];
//        $query = new Query;
//        $query	->select(['*'])
//        		->from('tracker_info')
//        		->join(	'LEFT JOIN',
//        				'tracker',
//        				'tracker.IMEI =tracker_info.tracker_id'
//        			)
//            ->where("(tracker_info.time > '".$vid."'AND tracker_info.time <'".$do."')")->andWhere(['IMEI' => $tracker_id]);
//        $command = $query->createCommand();
//        $trackerModel = $command->queryAll();
//
//        //$trackerModel = Trackerinfo::find()->joinWith('tracker')->where("time > '".$vid."' ")->all();
//        $tra = array();
//        foreach($trackerModel as $keyOne => $trackerObject){
//            foreach($trackerObject as $key => $tracker){
//                $tra[$keyOne][$key] = $tracker;
//            };
//        };
//
//        //$qw = array('tracker_id'=> 'good', 'vid'=> $vid, 'do'=> $do);
//        echo json_encode($tra);
//        //var_dump($tra);
//
//    }
//
//    public function actionShow($id=null)
//    {
//        $tracker_id = $id;
//        $trackerModel = Tracker::find()->where(['IMEI' => $tracker_id])->one();
//
//        $trackerInfoModel = Trackerinfo::find()->where(['tracker_id' => $tracker_id])->all();
//        foreach($trackerInfoModel as $keyOne => $trackerObject){
//            foreach($trackerObject as $key => $tracker){
//                $tra[$keyOne][$key] = $tracker;
//            };
//        };
//
//        echo $this->render('my_tracker', [
//             'tracker_info' => $trackerInfoModel,
//             'trackerModel' => $trackerModel,
//        ]);
//    }

}
