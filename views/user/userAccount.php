<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use app\assets\HomepageAsset;

HomepageAsset::register($this);
?>
<?php
    if(Yii::$app->session->hasFlash('AcountUpdate')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => \Yii::t('app','Зміни збережено'),
        ]);
    endif;
    
    if(Yii::$app->session->hasFlash('AcountNotUpdate')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => \Yii::t('app','Зміни не збережено'),
        ]);
    endif;
?>
<div class="container">
    <div class="sub-nav hidden-sm hidden-xs">
      <ul>
        <li>
            <a href="../../../site/index" style="margin-left:9px;font-size:15px;padding:0px;"><i class="fa fa-home"></i> Home </a><a style="font-size:10px;padding:0px"> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;"><?php echo \Yii::t('app','Сторінка водія'); ?></a>
        </li>
      </ul>
    </div>
    <!-- Sub Nav End -->

    <!-- Dashboard Wrapper Start -->
    <div class="dashboard-wrapper-lg">
        <div class="col-sm-3">
            <div class="widget">
                <div class="widget-header">
                  <div class="title">
                      <?= $user->username; ?>
                    <span class="mini-title displayNone">
                    </span>
                  </div>
                </div>
                <div class="widget-body">
                    <img src="../../../content/default_user_avatar.png" width="100%">
                </div>
            </div>
        </div>
        <div class="col-sm-9">
            <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <?= \Yii::t('app','Моя сторінка'); ?>
                    <span class="mini-title displayNone">
                    <?= \Yii::t('app','сторінка інформації користувача'); ?>
                    </span>
                  </div>
                    <a data-toggle="modal" class="KursorShow" data-target="#myModal<?= $user->id; ?>" style="float:right;"><i style="color:#3187bf;" class='fa fa-pencil-square-o fa-2x'></i></a>
                </div>
                <div class="widget-body">
                    <table class="table">
                        <tr>
                            <td>
                                <b><?= \Yii::t('app','Логін'); ?>:</b>
                            </td>
                            <td>
                                <?= $user->username."<br>"; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                               <b> <?= \Yii::t('app','E-mail'); ?>:</b>
                            </td>
                            <td>
                                <?= $user->email."<br>";?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                               <b> <?= \Yii::t('app','Час реєстрації'); ?>:</b>
                            </td>
                            <td>
                                <?= $user->time_added."<br>"; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                               <b> <?= \Yii::t('app','Ім`я'); ?>:</b>
                            </td>
                            <td>
                                <?= $user->first_name."<br>"; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b><?= \Yii::t('app','Прізвище'); ?>:</b>
                            </td>
                            <td>
                                <?= $user->last_name."<br>"; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b><?= \Yii::t('app','Zip/Postal Code'); ?>:</b>
                            </td>
                            <td>
                                <?= $user->zip."<br>"; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b><?= \Yii::t('app','Місто'); ?>:</b>
                            </td>
                            <td>
                                <?= $user->city."<br>"; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b><?= \Yii::t('app','Країна'); ?>:</b>
                            </td>
                            <td>
                                <?= $user->country."<br>"; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b><?= \Yii::t('app','Адреса'); ?>:</b>
                            </td>
                            <td>
                                <?= $user->address."<br>"; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b><?= \Yii::t('app','Одиниці виміру відстані'); ?>:</b>
                            </td>
                            <td>
                                <?php if($user->type_distance == '1'){ echo \Yii::t('app','Кіломерти')."<br>";}elseif($user->type_distance == '2'){ echo \Yii::t('app','Милі')."<br>"; } ?>
                            </td>
                        </tr>
                            
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="myModal<?= $user->id; ?>" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" style='padding:0px;' align='center'>Change user</h4>
            </div>
            <div class="modal-body">
                <?php $arrayLanguage = ['1'=> \Yii::t('app','Українська'),'2'=> \Yii::t('app','Англійська')]; ?>
                <?php $arrayTypeDistance = ['1'=> \Yii::t('app','Кіломерти'),'2'=> \Yii::t('app','Милі')]; ?>
                <?php $form = ActiveForm::begin(['id' => 'UpdateUser', 'action' => '/user/account', 'class' => 'form-control', 'method' => 'POST']); ?>
                    <input type="hidden" name='user_id' value='<?= $user->id; ?>'>
                    <?= $form->field($user, 'username')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Логін'), 'class' => 'form-control']); ?>
                    <?= $form->field($user, 'phone_number')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Номер телефону'), 'class' => 'form-control']); ?>
                    <?= $form->field($user, 'email')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Email'), 'class' => 'form-control']); ?>
                    <?= $form->field($user, 'city')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Місто'), 'class' => 'form-control']); ?>
                    <?= $form->field($user, 'address')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Адреса'), 'class' => 'form-control']); ?>
                    <?= $form->field($user, 'country')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Країна'), 'class' => 'form-control']); ?>
                    <?= $form->field($user, 'zip')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Zip/Postal Code'), 'class' => 'form-control']); ?>
                    <?= $form->field($user, 'first_name')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Ім`я'), 'class' => 'form-control']); ?>
                    <?= $form->field($user, 'last_name')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Прізвище'), 'class' => 'form-control']); ?>
                    <?= $form->field($user, 'type_distance')->dropDownList($arrayTypeDistance); ?>
            </div>
            <div class="modal-footer">
                <?php echo Html::submitButton(\Yii::t('app', 'Зберегти'), array('class' => 'btn btn-primary')); ?>
                <?php ActiveForm::end(); ?>
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('app','Закрити'); ?></button>
            </div>
        </div>

    </div>
    </div>