<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\assets\HomepageAsset;

HomepageAsset::register($this);
?>
<div class="dashboard-container">

      <div class="container">
        <!-- Top Nav End -->

        <!-- Sub Nav End -->
        <div class="sub-nav">
          <ul>
            <li>
                <a href="../user/<?= $username; ?>" style="margin-left:9px;font-size:15px;padding:0px;"><i class="fa fa-home"></i> Home </a>
            </li>
          </ul>
        </div>
        <!-- Sub Nav End -->

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg" style="min-height: 0px;">
          
          <!-- Row ends -->

          <!-- Row Start -->
          <div class="row wrap">
              
            <h3 align="center" style="color:black;"><?= $username; ?> trackers</h3>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    Tracker
                    <span class="mini-title">
                        All trackers this user
                    </span>
                  </div>
                </div>
                <div class=" widget-body">
                    <table class="table table-responsive table-striped table-bordered table-hover no-margin">
                        <tr style="font-weight:bold;color:black;">
                            <td><b>Imei</b></td>
                            <td><b>name</b></td>
                            <td class='hidden-sm hidden-xs'><b>time added</b></td>
                            <td class='hidden-sm hidden-xs'><b>discription</b></td>
                        </tr>
                        <?php foreach ($modelTracker as $tracker){ ?>
                        <tr>
                            <td style="color:black;"><?= HTML::a($tracker->IMEI, [Url::to('').'/tracker/show', 'id' => $tracker->IMEI]); ?></td>
                            <td style="color:black;"><?= $tracker->name; ?></td>
                            <td style="color:black;" class='hidden-sm hidden-xs'><?= $tracker->time_added; ?></td>
                            <td style="color:black;" class='hidden-sm hidden-xs'><?= $tracker->discription; ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                    <?php echo LinkPager::widget(['pagination'=>$pagination]); ?>
                </div>
                </div>
            </div>
        
              
          </div>
          <!-- Row End -->
        
        </div>
        <!-- Dashboard Wrapper End -->

        <footer>
          <p>© Tracker system 2015</p>
        </footer>

      </div>
    </div>
