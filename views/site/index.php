<?php
/* @var $this yii\web\View */
    use dosamigos\multiselect\MultiSelect;
    use app\assets\NewhomeAsset;

    NewhomeAsset::register($this);
?>
  
    <!-- =========================
     PRE LOADER       
    ============================== -->
    <div class="preloader" style="display: none;">
        <div class="status" style="display: none;">
            &nbsp;
        </div>
    </div>
    
    <!-- =========================
     SECTION: HOME / HEADER  
    ============================== -->
    <header class="header" data-stellar-background-ratio="0.5" id="home" style="background-position: 50% 0%;">
        
        <!-- COLOR OVER IMAGE -->
        <div class="overlay-layer">
            
            <!-- STICKY NAVIGATION -->
            <div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top sticky-navigation appear-on-scroll" role="navigation" style="top: -70px; opacity: 0;">
                <!-- /END CONTAINER -->
            </div>
            <!-- /END STICKY NAVIGATION -->
            
            <!-- CONTAINER -->
            <div class="container">
                
                <!-- ONLY LOGO ON HEADER -->
                <div class="only-logo">
                    <div class="navbar">
                        <div class="navbar-header">
                            <img src="../../content/logo-2.png" alt="">
                        </div>
                    </div>
                </div>
                
                <!-- /END ONLY LOGO ON HEADER -->
                <div class="row">
                    <div class="col-md-12">
                        
                        <!-- HEADING AND BUTTONS -->
                        <div class="intro-section">
                            
                            <!-- WELCOM MESSAGE -->
                            <h1 class="intro white-text"><span class="rotate" style="display: inline; opacity: 0.9903;"> Tracker system</span></h1>
                            <h5 class="white-text">Контроль переміщення персоналу і транспортних засобів</h5>
                            
                            <!-- BUTTON -->
                            <div class="button">
                                <a href="../../user/admin/tracker/show?id=359710046190618" class="btn btn-primary standard-button inpage-scroll">View DEMO</a>
                            </div>
                            <!-- /END BUTTON -->
                            
                        </div>
                        <!-- /END HEADNING AND BUTTONS -->
                    </div>
                </div>
            </div>
        </div>
    </header>
    
    
    <!-- =========================
     SECTION: SERVICES
    ============================== -->
    <section class="services grey-bg" id="section1">
        <div class="container">
            
            <!-- SECTION HEADER -->
            <div class="section-header">
                
                <div class="small-text-medium uppercase colored-text">
                    SERVICES
                </div>
                <h2 class="dark-text"><strong><?php echo \Yii::t('app','Основні можливості'); ?></strong></h2>
                <div class="colored-line">
                </div>
                <div class="sub-heading">
                    <?php echo \Yii::t('app', 'Наша система пропонує велику кількість можливостей, з яких є три основні'); ?>
                </div>
            </div>
            
            <div class="row">
                
                <!-- SINGLE SERVICE -->
                <div class="col-md-4 wow fadeInRight animated" data-wow-offset="10" data-wow-duration="1.75s" style="visibility: visible; animation-duration: 1.75s; animation-name: fadeInRight;">
                    <div class="single-service border-bottom-hover">
                        <div class="service-icon colored-text">
                            <span class="icon-basic-geolocalize-05"></span>
                        </div>
                        <h3 class="colored-text"><?php echo \Yii::t('app','Track Location'); ?></h3>
                        <p>
                           <?php echo \Yii::t('app','Відображення транспортного засобу в режимі реального часу, а також вивід всіх можливих даних: швидкість, стан пального та мастила, роботу мотора, температуру та ін.') ?> 
                        </p>
                    </div>
                </div>
                
                <div class="col-md-4 wow fadeInLeft animated" data-wow-offset="10" data-wow-duration="1.75s" style="visibility: visible; animation-duration: 1.75s; animation-name: fadeInLeft;">
                    <div class="single-service border-bottom-hover">
                        <div class="service-icon colored-text">
                            <span class="icon-basic-geolocalize-01"></span>
                        </div>
                        <h3 class="colored-text"><?php echo \Yii::t('app','Будування маршрутів'); ?></h3>
                        <p>
                            <?php echo \Yii::t('app','Будування маршрутів по днях, вивід повної інформації про стан транспортного засобу в кожній контрольній точці трека'); ?>
                        </p>
                    </div>
                </div>
                <!-- SINGLE SERVICE -->
                <div class="col-md-4 wow flipInY animated" data-wow-offset="10" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: flipInY;">
                    <div class="single-service border-bottom-hover">
                        <div class="service-icon colored-text">
                            <span class="icon-ecommerce-graph3"></span>
                        </div>
                        <h3 class="colored-text"><?php echo \Yii::t('app','Аналітика і звіти'); ?></h3>
                        <p>
                           <?php echo \Yii::t('app','Зведений звіт по роботі транспортного засобу і персоналу, відображення повної картини характеристик руху, а саме: пройденої відстані, часу перебування на маршруті, швидкісного  режиму та ін.'); ?>
                        </p>
                    </div>
                </div>
                
                <!-- SINGLE SERVICE -->
                
            </div>
        </div>
    </section>
    
    <!-- =========================
     SECTION: BRIEF LEFT
    ============================== -->
    <section class="brief white-bg-border text-left" id="section2">
        <div class="container">
            <div class="row">
                
                <!-- BRIEF IMAGE -->
                <div class="col-md-6 pull-right wow fadeInLeft animated" data-wow-offset="20" data-wow-duration="1.75s" style="visibility: visible; animation-duration: 1.75s; animation-name: fadeInLeft;">
                    <div class="brief-image-right">
                        <img src="../../content/about-us.jpg" alt="">
                    </div>
                </div>
                
                <!-- BRIEF HEADING -->
                <div class="col-md-6 content-section pull-left wow fadeInRight animated" data-wow-offset="20" data-wow-duration="1.75s" style="visibility: visible; animation-duration: 1.75s; animation-name: fadeInRight;">
                    <div class="small-text-medium uppercase colored-text">
                        <?php echo \Yii::t('app','Про нас'); ?>
                    </div>
                    <h2 class="text-left dark-text"><strong><?php echo \Yii::t('app','Наша'); ?></strong> <?php echo \Yii::t('app','маленька історія');?> </h2>
                    <div class="colored-line-left">
                    </div>
                    <p class="text-left">
                                <?php echo \Yii::t('app','"Tracker system" - система GPS моніторингу,  яка дає можливість повного контролю над будь-якими рухомими об`єктами в режимі реального часу. Наша система – це  web-сервіс, що дозволяє підключатися до системи з будь-якої точки Землі, де є доступ до Інтернету, для заповнення даних або отримання інформації. Джерелом інформації в системі GpsLOG служать дані, отримані від пристрою GPS- трекер, яке встановлюється на транспортному засобі  або видається співробітнику. Цей пристрій веде статистику, отримує  інформацію  із супутників GPS і передає ці дані, а також покази  різних датчиків на сервер за допомогою GPRS.'); ?>          
                    </p>
                    
                    <!-- FEATURE LIST -->
                    <div class="row">
                        <div class="col-md-8 col-sm-8">
                            <ul class="feature-list text-left">
                                <li style="padding:0px;margin:0px"><span class="icon-paperclip colored-text"></span>
                                    <?php echo \Yii::t('app','Зниження витрат на обслуговування транспортних засобів;'); ?>
                                </li>
                                <li style="padding:0px;margin:0px"><span class="icon-paperclip colored-text"></span>
                                     <?php echo \Yii::t('app','Контроль і керування маршрутами в реальному часі;'); ?>
                                </li>
                                <li style="padding:0px;margin:0px"><span class="icon-paperclip colored-text"></span>
                                     <?php echo \Yii::t('app','Ефективність використання транспортних засобів;'); ?>
                                </li>
                                <li style="padding:0px;margin:0px"><span class="icon-paperclip colored-text"></span> 
                                     <?php echo \Yii::t('app','Оптимізацію маршрутів і якісну логістику;'); ?>
                                </li>
                                <li style="padding:0px;margin:0px"><span class="icon-paperclip colored-text"></span>
                                     <?php echo \Yii::t('app','Контроль швидкісних режимів руху та ін. норм;'); ?>
                                </li>
                                <li style="padding:0px;margin:0px"><span class="icon-paperclip colored-text"></span> 
                                     <?php echo \Yii::t('app','Виняток нецільового використання техніки;'); ?>
                                </li>
                                <li style="padding:0px;margin:0px"><span class="icon-paperclip colored-text"></span> 
                                     <?php echo \Yii::t('app','Безпека водіїв, вантажу і транспорту;'); ?>
                                </li>
                                <li style="padding:0px;margin:0px"><span class="icon-paperclip colored-text"></span>
                                     <?php echo \Yii::t('app','Деталізація переміщень в кожній точці руху;'); ?>
                                </li>
                                <li style="padding:0px;margin:0px"><span class="icon-paperclip colored-text"></span> 
                                     <?php echo \Yii::t('app','Постійний контроль місцерозташування;'); ?>
                                </li>
                                <li style="padding:0px;margin:0px"><span class="icon-paperclip colored-text"></span> 
                                     <?php echo \Yii::t('app','Постійний Online-доступ до сервісу через Internet;'); ?>
                                </li>
                                <li style="padding:0px;margin:0px"><span class="icon-paperclip colored-text"></span>
                                     <?php echo \Yii::t('app','Задоволення  клієнтів.'); ?>
                                </li>

                                
                            </ul>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    
    <!-- =========================
     SECTION: BRIEF RIGHT
    ============================== -->
    <section class="brief grey-bg text-left" id="section3">
        <div class="container">
            <div class="row">
                
                <!-- BRIEF IMAGE -->
                <div class="col-md-6 wow fadeInLeft animated" data-wow-offset="20" data-wow-duration="1.75s" style="visibility: visible; animation-duration: 1.75s; animation-name: fadeInLeft;">
                    <div class="brief-image-left">
                        <img src="../../content/imac-ipad.png" alt="">
                    </div>
                </div>
                
                <!-- BRIEF HEADING -->
                <div class="col-md-6 content-section wow fadeInRight animated" data-wow-offset="20" data-wow-duration="1.75s" style="visibility: visible; animation-duration: 1.75s; animation-name: fadeInRight;">
                    <div class="small-text-medium uppercase colored-text">
                        LOREMIPSUM
                    </div>
                    <h2 class="text-left dark-text"><strong><?php echo \Yii::t('app','Сфери застосування моніторинга'); ?></strong></h2>
                    <div class="colored-line-left">
                    </div>
                    <p class="text-left">
                        
                    </p>
                    
                    <!-- FEATUR ACCORDION WITH ICON -->
                    <div class="panel-group" id="accordion">
                        
                        <!-- SINGLE ACORDIION -->
                        <div class="panel panel-default">
                            
                            <!-- ACORDIION HEADING / TITLE -->
                            <div class="panel-heading" id="headingOne">
                                <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="http://templateocean.com/stamp/geometric-polygon-bg/5-home-style-five/index.html#collapseOne" aria-expanded="false" aria-controls="collapseOne">
						<span class="icon-container color-bg"><span class="icon-map-alt white-text"></span></span><span class="title-text"><?php echo \Yii::t('app','Перевезення'); ?></span>
						</a>
						</h4>
                            </div>
                            
                            <!-- ACORDIION DESCRIPTION / TEXT -->
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul class="package-list">
                                        <li><?php echo \Yii::t('app','Детальні звіти по пробігу'); ?></li>
                                        <li><?php echo \Yii::t('app','Контроль великих паливних баків'); ?></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <!-- SINGLE ACORDIION -->
                        <div class="panel panel-default">
                            
                            <!-- ACORDIION HEADING / TITLE -->
                            <div class="panel-heading" id="headingTwo">
                                <h4 class="panel-title">
						<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="http://templateocean.com/stamp/geometric-polygon-bg/5-home-style-five/index.html#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
						<span class="icon-container color-bg"><span class="icon-compass-alt white-text"></span></span><span class="title-text">Сільське господарство</span>
						</a>
						</h4>
                            </div>
                            
                            <!-- ACORDIION DESCRIPTION / TEXT -->
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <ul class="package-list">
                                        <li><?php echo \Yii::t('app','Контроль площі висадки'); ?></li>
                                        <li><?php echo \Yii::t('app','Звіти про виконання роботи'); ?></li>
                                        <li><?php echo \Yii::t('app','Контроль великих паливних баків'); ?></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <!-- SINGLE ACORDIION -->
                        <div class="panel panel-default">
                            
                            <!-- ACORDIION HEADING / TITLE -->
                            <div class="panel-heading" id="headingThree">
                                <h4 class="panel-title">
						<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="http://templateocean.com/stamp/geometric-polygon-bg/5-home-style-five/index.html#collapseThree" aria-expanded="false" aria-controls="collapseThree">
						<span class="icon-container color-bg"><span class="icon-lightbulb-alt white-text"></span></span><span class="title-text">Служба курєрської доставки</span>
						</a>
						</h4>
                            </div>
                            
                            <!-- ACORDIION DESCRIPTION / TEXT -->
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <ul class="package-list">
                                        <li><?php echo \Yii::t('app','Моніторинг авто в реальному часі');?></li>
                                        <li><?php echo \Yii::t('app','Контроль добового  пробігу');?></li>
                                        <li><?php echo \Yii::t('app','Контроль витрат  палива');?></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    
    <!-- =========================
     SECTION: SOME STATS   
    ============================== -->
    <section class="stats" id="section4" data-stellar-background-ratio="0.5" style="background-position: 50% 0%;">
        <div class="overlay-layer-2">
            <div class="container">
                <div class="row no-gutters">
                    
                    <!-- SINGLE COLUMN WITH ICON -->
                    <div class="col-md-4 col-sm-4 col-xs-4 single-stats wow fadeInLeft animated" data-wow-offset="10" data-wow-duration="1.75s" style="visibility: visible; animation-duration: 1.75s; animation-name: fadeInLeft;">
                        <div class="icon-container">
                            <span class="colored-text fa fa-users"></span>
                        </div>
                        <h4 class="white-text">780+</h4>
                        <p class="uppercase transparent-text small-text">
                            <?php echo \Yii::t('app','Кількість користувачів'); ?>
                        </p>
                    </div>
                    
                     <!-- SINGLE COLUMN WITH ICON -->
                    <div class="col-md-4 col-sm-4 col-xs-4 single-stats border-left-colored border-right-colored wow fadeIn animated" data-wow-offset="10" data-wow-duration="1.75s" style="visibility: visible; animation-duration: 1.75s; animation-name: fadeIn;">
                        <div class="icon-container">
                            <span class="colored-text fa fa-briefcase"></span>
                        </div>
                        <h4 class="white-text">1250+</h4>
                        <p class="uppercase transparent-text small-text">
                            <?php echo \Yii::t('app','Кількість компаній'); ?>
                        </p>
                    </div>
                    
                     <!-- SINGLE COLUMN WITH ICON -->
                    <div class="col-md-4 col-sm-4 col-xs-4 single-stats wow fadeInRight animated" data-wow-offset="10" data-wow-duration="1.75s" style="visibility: visible; animation-duration: 1.75s; animation-name: fadeInRight;">
                        <div class="icon-container">
                            <span class="colored-text fa fa-map-marker"></span>
                        </div>
                        <h4 class="white-text">235+</h4>
                        <p class="uppercase transparent-text small-text">
                            <?php echo \Yii::t('app','Кількість трекерів'); ?>
                        </p>
                    </div>
                </div>
                
                 <!-- CALL TO ACTION -->
                <div class="stats-footer">
                    <div class="long-transparent-line">
                    </div>
                    <p class="white-text">
                        We are ready to help you - <a href="../../user/admin/tracker/show?id=359710046190618" class="strong inpage-scroll">View DEMO Now »</a>
                    </p>
                </div>
                
            </div>
        </div>
    </section>
    
    <!-- =========================
     SECTION: BENEFITS   
    ============================== -->
    <section class="unique-features white-bg" id="section5">
        <div class="container">
            
            <!-- SECTION HEADER -->
            <div class="section-header">
                <div class="small-text-medium uppercase colored-text">
                    FEATURES
                </div>
                <h2 class="dark-text"><strong><?php echo \Yii::t('app','Для чого потрібна наша система?'); ?></strong></h2>
                <div class="colored-line">
                </div>
                <div class="sub-heading">
                    <?php echo \Yii::t('app','Основні завдання, які вирішує наша система.'); ?>
                </div>
            </div>
            
            <!-- FIRST ROW OF FEATURES -->
            <div class="row">
                <div class="col-md-4 wow fadeInRight animated" data-wow-duration="1.75s" data-wow-offset="20" style="visibility: visible; animation-duration: 1.75s; animation-name: fadeInRight;">
                    
                    <!-- SINGLE FEATURE -->
                    <div class="feature">
                        <div class="icon-container">
                            <span class="icon-cloud-alt colored-text"></span>
                        </div>
                        <div class="description text-left">
                            <h4 class="dark-text"><?php echo \Yii::t('app','зменшення часу, необхідного для виконання роботи;'); ?></h4>
                            <div class="grey-line-short pull-left">
                            </div>
                            
                        </div>
                    </div>
                    
                    <!-- SINGLE FEATURE -->
                    <div class="feature">
                        <div class="icon-container">
                            <span class="icon-map-alt colored-text"></span>
                        </div>
                        <div class="description text-left">
                            <h4 class="dark-text"><?php echo \Yii::t('app','Оптимізація маршрутів пересування водіїв;'); ?></h4>
                            <div class="grey-line-short pull-left">
                            </div>
                        </div>
                    </div>
                    
                    <!-- SINGLE FEATURE -->
                    <div class="feature">
                        <div class="icon-container">
                            <span class="icon-cloud-alt colored-text"></span>
                        </div>
                        <div class="description text-left">
                            <h4 class="dark-text"><?php echo \Yii::t('app','Підвищення рівня дисципліни працівників.'); ?></h4>
                            <div class="grey-line-short pull-left">
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- IPAD IMAGE -->
                <div class="col-md-4">
                    <div class="ipad-image wow bounceIn animated" data-wow-duration="1.75s" data-wow-offset="20" style="visibility: visible; animation-duration: 1.75s; animation-name: bounceIn;">
                        <img src="../../content/question.jpg" alt="">
                    </div>
                </div>
                
                <div class="col-md-4 wow fadeInLeft animated" data-wow-duration="1.75s" data-wow-offset="20" style="visibility: visible; animation-duration: 1.75s; animation-name: fadeInLeft;">
                    
                    <!-- SINGLE FEATURE -->
                    <div class="feature">
                        <div class="icon-container">
                            <span class="icon-gift-alt colored-text"></span>
                        </div>
                        <div class="description text-left">
                            <h4 class="dark-text"><?php echo \Yii::t('app','Контроль транспортного засобу;'); ?></h4>
                            <div class="grey-line-short pull-left">
                            </div>
                        </div>
                    </div>
                    
                    <!-- SINGLE FEATURE -->
                    <div class="feature">
                        <div class="icon-container">
                            <span class="icon-star-alt colored-text"></span>
                        </div>
                        <div class="description text-left">
                            <h4 class="dark-text"><?php echo \Yii::t('app','Контроль витрат палива;'); ?></h4>
                            <div class="grey-line-short pull-left">
                            </div>
                        </div>
                    </div>
                    
                    <!-- SINGLE FEATURE -->
                    <div class="feature">
                        <div class="icon-container">
                            <span class="icon-chat-alt colored-text"></span>
                        </div>
                        <div class="description text-left">
                            <h4 class="dark-text"><?php echo \Yii::t('app','Віддалене отримання завдань;'); ?></h4>
                            <div class="grey-line-short pull-left">
                            </div>
                        </div>
                    </div>
                    
                    <div class="feature">
                        <div class="icon-container">
                            <span class="icon-chat-alt colored-text"></span>
                        </div>
                        <div class="description text-left">
                            <h4 class="dark-text"><?php echo \Yii::t('app','Створення звітів без стороннього програмного забезпечення.'); ?></h4>
                            <div class="grey-line-short pull-left">
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    
    <!-- =========================
     SECTION: PRICING   
    ============================== -->
<!--    <section class="pricing grey-bg" id="section6">
        <div class="container">
            
             SECTION HEADER 
            <div class="section-header">
                <div class="small-text-medium uppercase colored-text">
                    Pricing
                </div>
                <h2 class="dark-text"><strong>Affordable</strong> Packages</h2>
                <div class="colored-line">
                </div>
                <div class="sub-heading">
                    Cloud computing subscription model out of the box proactive solution.
                </div>
            </div>
            
             PRICING TABLE 
            <div class="row pricing-table">
                
                 SINGLE PACKAGE 
                <div class="col-md-4 wow fadeInLeft animated" data-wow-offset="20" data-wow-duration="1.75s" style="visibility: visible; animation-duration: 1.75s; animation-name: fadeInLeft;">
                    <div class="single-pricing border-bottom-hover">
                        
                        <div class="package-title">
                            <h3 class="colored-text">Basic Plan</h3>
                            <div class="colored-line">
                            </div>
                        </div>
                        
                        <h2 class="price"><span class="superscript">$</span>150<span class="month small-text">Per Month</span></h2>
                        
                        <ul class="package-list">
                            <li><span class="dark-text strong">50GB</span>File Storage</li>
                            <li><span class="dark-text strong">Secure</span>Online Transfer</li>
                            <li><span class="dark-text strong">5</span>User Access</li>
                            <li><span class="dark-text strong">30</span>Projects Per Month</li>
                            <li><span class="dark-text strong">24/7</span>Customer Service</li>
                        </ul>
                        
                        <div class="button">
                            <a href="../../user/admin/tracker/show?id=359710046190618" class="btn standard-button inpage-scroll">View DEMO</a>
                        </div>
                        
                        <div class="small-text uppercase">
                            30 Days free Trial
                        </div>
                        
                    </div>
                </div>
                
                 SINGLE PACKAGE 
                <div class="col-md-4 wow flipInY animated" data-wow-offset="10" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: flipInY;">
                    <div class="single-pricing border-bottom-hover">
                        
                        <div class="package-title color-bg">
                            <h3 class="white-text">Standard Plan</h3>
                        </div>
                        
                        <h2 class="price"><span class="superscript">$</span>199<span class="month small-text">Per Month</span></h2>
                        
                        <ul class="package-list">
                            <li><span class="colored-text strong">150GB</span>File Storage</li>
                            <li><span class="colored-text strong">Secure</span>Online Transfer</li>
                            <li><span class="colored-text strong">20</span>User Access</li>
                            <li><span class="colored-text strong">65</span>Projects Per Month</li>
                            <li><span class="colored-text strong">24/7</span>Customer Service</li>
                        </ul>
                        
                        <div class="button">
                            <a href="../../user/admin/tracker/show?id=359710046190618" class="btn standard-button inpage-scroll">View DEMO</a>
                        </div>
                        
                        <div class="small-text uppercase">
                            <span class="colored-text">40%</span> Discount on first year
                        </div>
                        
                    </div>
                </div>
                
                 SINGLE PACKAGE 
                <div class="col-md-4 wow fadeInRight animated" data-wow-offset="20" data-wow-duration="1.75s" style="visibility: visible; animation-duration: 1.75s; animation-name: fadeInRight;">
                    <div class="single-pricing border-bottom-hover">
                        
                        <div class="package-title">
                            <h3 class="colored-text">Medium Plan</h3>
                            <div class="colored-line">
                            </div>
                        </div>
                        <h2 class="price"><span class="superscript">$</span>175<span class="month small-text">Per Month</span></h2>
                        
                        <ul class="package-list">
                            <li><span class="dark-text strong">80GB</span>File Storage</li>
                            <li><span class="dark-text strong">Secure</span>Online Transfer</li>
                            <li><span class="dark-text strong">10</span>User Access</li>
                            <li><span class="dark-text strong">45</span>Projects Per Month</li>
                            <li><span class="dark-text strong">24/7</span>Customer Service</li>
                        </ul>
                        
                        <div class="button">
                            <a href="../../user/admin/tracker/show?id=359710046190618" class="btn standard-button inpage-scroll">View DEMO</a>
                        </div>
                        
                        <div class="small-text uppercase">
                            30 Days free Trial
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>-->
    
    <!-- =========================
     SECTION: SCREENSHOTS
    ============================== -->
<!--    <section class="features" id="section7" data-stellar-background-ratio="0.5" style="background-position: 50% 0%;">
        <div class="overlay-layer-2">
            <div class="container">
                
                 SECTION HEADER 
                <div class="section-header">
                    <div class="small-text-medium uppercase colored-text">
                        Screenshots
                    </div>
                    <h2 class="white-text"><span class="strong white-text">Explore</span> Screenshots</h2>
                    <div class="colored-line">
                    </div>
                    <div class="sub-heading white-text">
                        Cloud computing subscription model out of the box proactive solution.
                    </div>
                </div>
                
                <div class="row">
                    
                     LEFT SIDE ELEMENTS 
                    <div class="col-md-3 text-right left-side wow fadeInRight animated" data-wow-offset="10" data-wow-duration="1.75s" style="visibility: visible; animation-duration: 1.75s; animation-name: fadeInRight;">
                        
                         SINGLE ELEMENT 
                        <div class="single-feature border-right-colored">
                            <h4 class="colored-text">Creative Design</h4>
                            <p class="transparent-text">
                                Coworking viral landing page user base minimum viable
                            </p>
                        </div>
                        
                         SINGLE ELEMENT 
                        <div class="single-feature border-right-colored">
                            <h4 class="colored-text">Easy to Customize</h4>
                            <p class="transparent-text">
                                Coworking viral landing page user base minimum viable
                            </p>
                        </div>
                        
                         SINGLE ELEMENT 
                        <div class="single-feature border-right-colored">
                            <h4 class="colored-text">Bootstrap 3.3.1</h4>
                            <p class="transparent-text">
                                Coworking viral landing page user base minimum viable
                            </p>
                        </div>
                        
                    </div>
                    
                     SCREENSHOTS 
                    <div class="col-md-6">
                        <div id="screenshots" class="owl-carousel owl-theme" style="opacity: 1; display: block;">
                            
                             SINGLE SCREENSHOT 
                            <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 3330px; left: 0px; display: block;"><div class="owl-item" style="width: 555px;"><div class="screenshot">
                                <a href="http://templateocean.com/stamp/geometric-polygon-bg/5-home-style-five/images/screenshots/1@2x.png" data-lightbox-gallery="gallery1">
                                    <img src="../../content/1(1).png" alt="">
                                </a>
                            </div></div><div class="owl-item" style="width: 555px;"><div class="screenshot">
                                <a href="http://templateocean.com/stamp/geometric-polygon-bg/5-home-style-five/images/screenshots/1@2x.png" data-lightbox-gallery="gallery1">
                                    <img src="../../content/1(1).png" alt="">
                                </a>
                            </div></div><div class="owl-item" style="width: 555px;"><div class="screenshot">
                                <a href="http://templateocean.com/stamp/geometric-polygon-bg/5-home-style-five/images/screenshots/1@2x.png" data-lightbox-gallery="gallery1">
                                    <img src="../../content/1(1).png" alt="">
                                </a>
                            </div></div></div></div>
                            
                             SINGLE SCREENSHOT 
                            
                            
                             SINGLE SCREENSHOT 
                            
                        <div class="owl-controls clickable"><div class="owl-pagination"><div class="owl-page active"><span class=""></span></div><div class="owl-page"><span class=""></span></div><div class="owl-page"><span class=""></span></div></div></div></div>
                    </div>
                    
                    
                     RIGHT SIDE FEATURES 
                    <div class="col-md-3 text-left right-side wow fadeInLeft animated" data-wow-offset="10" data-wow-duration="1.75s" style="visibility: visible; animation-duration: 1.75s; animation-name: fadeInLeft;">
                        
                         SINGLE ELEMENT 
                        <div class="single-feature border-left-colored">
                            <h4 class="colored-text">Online Support</h4>
                            <p class="transparent-text">
                                Coworking viral landing page user base minimum viable
                            </p>
                        </div>
                        
                         SINGLE ELEMENT 
                        <div class="single-feature border-left-colored">
                            <h4 class="colored-text">Easy Shipping</h4>
                            <p class="transparent-text">
                                Coworking viral landing page user base minimum viable
                            </p>
                        </div>
                        
                         SINGLE ELEMENT 
                        <div class="single-feature border-left-colored">
                            <h4 class="colored-text">Secure Payment</h4>
                            <p class="transparent-text">
                                Coworking viral landing page user base minimum viable
                            </p>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>-->
    
    <!-- =========================
     SECTION: TIMELINE   
    ============================== -->
<!--    <section class="brief timeline white-bg" id="section8">
        <div class="container">
            
            <div class="row">
                
                 TIMELINE HEADING / TEXT  
                <div class="col-md-6 timeline-text text-left wow fadeInLeft" data-wow-offset="20" data-wow-duration="1.75s" style="visibility: hidden; animation-duration: 1.75s; animation-name: none;">
                    <div class="small-text-medium uppercase colored-text text-left">
                        Timeline
                    </div>
                    <h2 class="text-left dark-text"><strong>Amazing</strong> Journey</h2>
                    <div class="colored-line-left">
                    </div>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia.
                        <br>
                        <br>Main differentiators business model micro economics marketplace equity augmented reality human computer interaction. Board members super angel preferred stock.
                    </p>
                </div>
                
                 TIMLEINE SCROLLER 
                <div class="col-md-6 timeline-section wow fadeInRight" data-wow-offset="20" data-wow-duration="1.75s" style="height: 432px; overflow: hidden; visibility: hidden; animation-duration: 1.75s; animation-name: none;">
                    <ul class="vertical-timeline" id="timeline-scroll">
                        
                         SINGLE TIMELINE 
                        
                        
                         SINGLE TIMELINE 
                        
                        
                         SINGLE TIMELINE 
                        
                        
                         SINGLE TIMELINE 
                        
                        
                         SINGLE TIMELINE 
                        
                        
                         SINGLE TIMELINE 
                        
                    </ul>
                <li class="first">
                            <div class="date small-text colored-text strong">
                                Jan, 12
                            </div>
                            <div class="icon-container color-bg white-text">
                                <span class="icon-ribbon-alt"></span>
                            </div>
                            <div class="info">
                                Drinking vinegar Echo Park selfies heirloom aesthetic, taxidermy keytar next level craft beer.
                            </div>
                        </li><li>
                            <div class="date small-text dark-text strong">
                                Apr, 14
                            </div>
                            <div class="icon-container color-bg white-text">
                                <span class="icon-piechart"></span>
                            </div>
                            <div class="info">
                                American Apparel meggings asymmetrical organic Marfa typewriter shabby chic Brooklyn, farm-to-table butcher.
                            </div>
                        </li><li>
                            <div class="date small-text dark-text strong">
                                Feb, 13
                            </div>
                            <div class="icon-container color-bg white-text">
                                <span class="icon-cart-alt"></span>
                            </div>
                            <div class="info">
                                Keytar stumptown hoodie, crucifix meggings messenger bag brunch deep v. High Life taxidermy mumblecore.
                            </div>
                        </li><li>
                            <div class="date small-text dark-text strong">
                                Dec, 13
                            </div>
                            <div class="icon-container color-bg white-text">
                                <span class="icon-paperclip"></span>
                            </div>
                            <div class="info">
                                Wayfarers Carles seitan, VHS bicycle rights normcore butcher skateboard bitters Portland cred.
                            </div>
                        </li><li>
                            <div class="date small-text dark-text strong">
                                May, 14
                            </div>
                            <div class="icon-container color-bg white-text">
                                <span class="icon-check"></span>
                            </div>
                            <div class="info">
                                Four loko Pitchfork cred irony selfies meggings lo-fi iPhone hashtag, craft beer Helvetica crucifix keytar.
                            </div>
                        </li><li>
                            <div class="date small-text dark-text strong">
                                Oct, 14
                            </div>
                            <div class="icon-container color-bg white-text">
                                <span class="icon-lightbulb-alt"></span>
                            </div>
                            <div class="info">
                                Cosby sweater viral Etsy lo-fi Carles. Paleo kogi kale chips McSweeney's bicycle rights behind the scene.
                            </div>
                        </li></div>
                
            </div>
        </div>
    </section>-->
    
    <!-- =========================
     SECTION: TEAM   
    ============================== -->
<!--    <section class="team grey-bg" id="section9">
        <div class="container">
            
             SECTION HEADER 
            <div class="section-header">
                <div class="small-text-medium uppercase colored-text">
                    Team
                </div>
                <h2 class="dark-text"><strong>Project</strong> Team</h2>
                <div class="colored-line">
                </div>
                <div class="sub-heading">
                    Cloud computing subscription model out of the box proactive solution.
                </div>
            </div>
            
             MEMBERS 
            <div class="row wow fadeIn animated" data-wow-offset="10" data-wow-duration="1.75s" style="visibility: visible; animation-duration: 1.75s; animation-name: fadeIn;">
                
                 MEMBER 
                <div class="col-md-3 col-sm-6">
                    <div class="team-member wow flipInY animated" data-wow-offset="10" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: flipInY;">
                        <div class="member-pic">
                            <img src="../../content/1.jpg" alt="">
                        </div>
                        <div class="member-details">
                            <h5 class="colored-text">Albert Jacobs</h5>
                            <div class="small-text">
                                Founder &amp; CEO
                            </div>
                        </div>
                        <ul class="social-icons">
                            <li>
                                <a href="">
                                    <span class="icon-social-facebook dark-text"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-social-twitter dark-text"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-social-pinterest dark-text"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                
                 MEMBER 
                <div class="col-md-3 col-sm-6">
                    <div class="team-member wow flipInY animated" data-wow-offset="10" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: flipInY;">
                        <div class="member-pic">
                            <img src="../../content/2.jpg" alt="">
                        </div>
                        <div class="member-details">
                            <h5 class="colored-text">George Walker</h5>
                            <div class="small-text">
                                Web Developer
                            </div>
                        </div>
                        <ul class="social-icons">
                            <li>
                                <a href="">
                                    <span class="icon-social-facebook dark-text"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-social-twitter dark-text"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-social-pinterest dark-text"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                
                 MEMBER 
                <div class="col-md-3 col-sm-6">
                    <div class="team-member wow flipInY animated" data-wow-offset="10" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: flipInY;">
                        <div class="member-pic">
                            <img src="../../content/3.jpg" alt="">
                        </div>
                        <div class="member-details">
                            <h5 class="colored-text">Frank Hanson</h5>
                            <div class="small-text">
                                UI Designer
                            </div>
                        </div>
                        <ul class="social-icons">
                            <li>
                                <a href="">
                                    <span class="icon-social-facebook dark-text"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-social-twitter dark-text"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-social-pinterest dark-text"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                
                 MEMBER 
                <div class="col-md-3 col-sm-6 team-member">
                    <div class="wow flipInY animated" data-wow-offset="10" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: flipInY;">
                        <div class="member-pic">
                            <img src="../../content/4.jpg" alt="">
                        </div>
                        <div class="member-details">
                            <h5 class="colored-text">Emily Smith</h5>
                            <div class="small-text">
                                App Developer
                            </div>
                        </div>
                        <ul class="social-icons">
                            <li>
                                <a href="">
                                    <span class="icon-social-facebook dark-text"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-social-twitter dark-text"></span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-social-pinterest dark-text"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            
        </div>
    </section>-->
    
    <!-- =========================
     SECTION: TESTIMONIALS   
    ============================== -->
<!--    <section class="testimonials white-bg" id="section10">
        <div class="container">
            
             SECTION HEADER 
            <div class="section-header">
                <div class="small-text-medium uppercase colored-text">
                    Testimonials
                </div>
                <h2 class="dark-text"><strong>Happy</strong> Customers</h2>
                <div class="colored-line">
                </div>
                <div class="sub-heading">
                    Cloud computing subscription model out of the box proactive solution.
                </div>
            </div>
            
            <div class="row no-gutters">
                
                 SINGLE FEEDBACK 
                <div class="col-md-4 wow fadeInLeft animated" data-wow-offset="10" data-wow-duration="1.75s" style="visibility: visible; animation-duration: 1.75s; animation-name: fadeInLeft;">
                    <div class="feedback">
                        <div class="pic-container">
                            <img src="../../content/1(2).jpg" alt="">
                        </div>
                        <p>
                            “Tablet publishing HTML5 mobile first really simple syndication meetups white board walls. User experience iterate algorithm gamification semantic web value add market research stealth.”
                        </p>
                        <h5 class="colored-text">-Eusebia Bărlâdeanu</h5>
                        <div class="small-text">
                            XYZ Interactive
                        </div>
                    </div>
                </div>
                
                 SINGLE FEEDBACK 
                <div class="col-md-4 wow fadeIn animated" data-wow-offset="10" data-wow-duration="1.75s" style="visibility: visible; animation-duration: 1.75s; animation-name: fadeIn;">
                    <div class="feedback border-left border-right">
                        <div class="pic-container">
                            <img src="../../content/2(1).jpg" alt="">
                        </div>
                        <p>
                            “Rockstar developer internet of things bleeding edge browser extension social capital. Sandboxing UDID content manag system ruby on rails continuous deployment big data infographic.”
                        </p>
                        <h5 class="colored-text">- Eusebia Bărlâdeanu</h5>
                        <div class="small-text">
                            AbZ Network
                        </div>
                    </div>
                </div>
                
                 SINGLE FEEDBACK 
                <div class="col-md-4 wow fadeInRight animated" data-wow-offset="10" data-wow-duration="1.75s" style="visibility: visible; animation-duration: 1.75s; animation-name: fadeInRight;">
                    <div id="feedback">
                        <div class="feedback">
                            <div class="pic-container">
                                <img src="../../content/3(1).jpg" alt="">
                            </div>
                            <p>
                                “Rockstar developer internet of things bleeding edge browser extension social capital. Sandboxing UDID content manag system ruby on rails continuous deployment big data infographic.”
                            </p>
                            <h5 class="colored-text">- Georgian Hrușcă</h5>
                            <div class="small-text">
                                ZyX LLC
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>-->
    
    <!-- =========================
     SECTION: CALL TO ACTION   
    ============================== -->
<!--    <section class="call-to-action" id="section11" data-stellar-background-ratio="0.1" style="background-position: 50% 0%;">
        <div class="overlay-layer-2">
            <div class="container">
                <div class="row">
                
                    <div class="col-md-8 col-md-offset-2">
                    
                        <div class="uppercase colored-text strong">
                            30 Days free trial
                        </div>
                    
                        <h2 class="white-text strong wow fadeInLeft animated" data-wow-duration="2s" data-wow-offset="40" style="visibility: visible; animation-duration: 2s; animation-name: fadeInLeft;">Start your FREE Trial Now!</h2>
                    
                        <button class="btn btn-primary standard-button wow fadeInRight animated" data-wow-duration="2s" data-wow-offset="40" type="button" data-toggle="modal" data-target="#stamp-modal" style="visibility: visible; animation-duration: 2s; animation-name: fadeInRight;">View DEMO</button>
                        
                    </div>
                
                </div>
            </div>
        </div>
    </section>-->
    
    
    <!-- REGISTRATION FORM FOR POPUP BOX -->
    <div class="modal fade bs-example-modal-sm" id="stamp-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="vertical-registration-form">
                    <h4 class="dark-text form-heading">Get 1 Month FREE Trial Now</h4>
                    <form class="registration-form" id="contact-form">
                        <input type="text" id="cf-name" name="name" class="form-control input-box placeholder" placeholder="Name">
                        <input type="email" id="cf-email" name="email" class="form-control input-box placeholder" placeholder="Email">
                        <input type="text" id="cf-subject" name="subject" class="form-control input-box placeholder" placeholder="Subject">
                        <textarea name="message" id="cf-message" class="form-control textarea-box placeholder" rows="4" placeholder="Message"></textarea>
                        <button class="btn btn-primary standard-button">View DEMO</button>
                    </form>
                    <p class="email-success dark-text">
                        <span class="icon-check-alt2 colored-text"></span>Email sent seuccessfully
                    </p>
                    <p class="email-error dark-text">
                        <span class="icon-close-alt2"></span>Error! Please check all fields filled correctly
                    </p>
                    <!-- MAILCHIMP ALERTS 
			        <p class="mailchimp-success dark-text"><span class="icon-check-alt2 colored-text"></span>We sent the confirmation to your email</p>
			        <p class="mailchimp-error dark-text"><span class="icon-close-alt2"></span>Error! Please check all fields filled correctly</p>
			        -->
                </div>
            </div>
        </div>
    </div>
    
    <!-- =========================
     SECTION: CONTACT INFO  
    ============================== -->
    <div class="contact-info white-bg">
        <div class="container">
            
            <!-- CONTACT INFO -->
            <div class="row contact-links">
                
                <div class="col-sm-4">
                    <div class="icon-container">
                        <span class="icon-basic-mail colored-text"></span>
                    </div>
                    <a href="mailto:hey@designlab.co" class="strong">hey@designlab.co</a>
                </div>
                
                <div class="col-sm-4">
                    <div class="icon-container">
                        <span class="icon-basic-geolocalize-01 colored-text"></span>
                    </div>
                    <a href="" class="strong">Glen Road, E13 8 London, UK</a>
                </div>
                
                <div class="col-sm-4">
                    <div class="icon-container">
                        <span class="icon-basic-tablet colored-text"></span>
                    </div>
                    <a href="tel:44-12-3456-7890" class="strong">+44-12-3456-7890</a>
                </div>
            </div>
            
        </div>
    </div>
    <!-- =========================
     GOOGEL MAP 
    ============================== -->
    <div id="container-fluid">
        <div id="cd-google-map">
            <address class="color-bg">Glen Road, E13 8 London, UK</address>
        </div>
    </div>
    
    <!-- FOOTER -->
    <footer class="footer grey-bg">
        ©2014 Stamp LLC. All Rights Reserved
        
        <!-- OPTIONAL FOOTER LINKS -->
        <ul class="footer-links small-text">
            <li><a href="" class="dark-text">About</a>
            </li>
            <li><a href="" class="dark-text">Terms</a>
            </li>
            <li><a href="" class="dark-text">Privacy</a>
            </li>
        </ul>
        
        <!-- SOCIAL ICONS -->
        <ul class="social-icons">
            <li><a href=""><span class="icon-social-facebook transparent-text-dark"></span></a>
            </li>
            <li><a href=""><span class="icon-social-twitter transparent-text-dark"></span></a>
            </li>
            <li><a href=""><span class="icon-social-pinterest transparent-text-dark"></span></a>
            </li>
            <li><a href=""><span class="icon-social-googleplus transparent-text-dark"></span></a>
            </li>
            <li><a href=""><span class="icon-social-dribbble transparent-text-dark"></span></a>
            </li>
        </ul>
        
    </footer>
    
    
    <!-- =========================
     SCRIPTS   
    ============================== -->
   <!-- =========================================================
     STYLE SWITCHER | ONLY FOR DEMO NOT INCLUDED IN MAIN FILES
============================================================== -->
