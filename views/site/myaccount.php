<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var common\models\User $model
 */

?>
    <div class="modal fade" id="myModalProfile" role="dialog" style="color:black;">
        <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" style="margin:0px;padding:0px;">Change Profile</h4>
                </div>
                <div class="modal-body">
                    <?php echo '<b>username - </b>'.$data_account->username."<br>"; ?>
                    <?php echo '<b>Phone number - </b>'.$data_account->phone_number."<br>"; ?>
                    <?php echo '<b>Time added - </b>'.$data_account->time_added."<br>"; ?>
                    <?php echo '<b>Company - </b>'.$data_account->company_id."<br>"; ?>
                </div>
            </div>
        </div>
    </div>
