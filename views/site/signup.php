<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var common\models\User $model
 */

?>
<div class="row wrap">
    <div class="col-sm-12">
        <h1><?= \Yii::t('app', 'Sign up')?></h1>
    </div>
    <div class="site-signup">
        <h1><?= Html::encode($this->title) ?></h1>
        <div class="row">
            <div class="col-lg-12">
                <?php 
                
                $form = ActiveForm::begin(['id' => 'form-signup',
                    'options' => ['class' => 'form-horizontal'],
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-7\">{error}</div>",
                        'labelOptions' => ['class' => 'col-lg-2 control-label'],
                    ],

                ]); ?>
                    <?= $form->field($model, 'username') ?>
                    <?= $form->field($model, 'password')->passwordInput() ?>
                    <?= $form->field($model, 'email') ?>
                    <?= $form->field($model, 'phone_number') ?>
                    <div style="margin-left:150px;" class="form-group">
                        <?= Html::submitButton(\Yii::t('app', 'SignUp'), ['class' => 'btn btn-primary']) ?>
                    </div>
                <?php ActiveForm::end(); ?>
                <div class="col-lg-offset-1 col-lg-11">
                    <a class="btn btn-primary" href="<?=  Url::toRoute('site/login') ?>"><?= \Yii::t('app', 'Login')?></a>
                </div>
            </div>
        </div>
    </div>
</div>
