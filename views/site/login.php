<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

use app\assets\HomepageAsset;    
HomepageAsset::register($this);

?>
<div class="container" >
<div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true" style="margin-top:51px;">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <p class="text-center" style="font-size:25px;margin:0px">Login</p>
      </div>
      <div class="modal-body">
            <div class="row">
                <div class="col-sm-12" style="padding-left:30px;padding-right:30px;">
                <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'options' => [],
                    
                ]); ?>
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>

                        <div class="form-group">
                                <?= $form->field($model, 'rememberMe', [])->checkbox() ?>
                                <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                        </div>
                        <div style="color:#999;margin:1em 0">
                            If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                        </div>

                    </div>
                </div>

                <?php ActiveForm::end(); ?>
      </div>
     </div>
  </div>
</div>

</div>
