<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>
<div class="text-center"> 
            <div class="divider-two" style="color:white;"></div>
                <?php
                  if($parentCompany != null){ ?>
                        <h1 class="center" style="letter-spacing:2; color:white;"><a href="../../../../company/<?= $parentCompany->urlname; ?>" style="text-decoration: none; color:white;"><?= $parentCompany->organization_name; ?></a></h1>
                        <h2 class="center" style="letter-spacing:2; color:white;"><?= $companyModel->organization_name; ?></h2>
                  <?php }else{ ?>
                        <h2 class="center" style="letter-spacing:2; color:white;"><?= $companyModel->organization_name; ?></h2>
                <?php  } ?>
            <div class='col-sm-12'>    
                <i class="fa fa-chevron-circle-down fa-2x" id="companyInfoClick" style="color:white;"></i>
            </div>
            
            <div class='col-sm-12' id="companyInfoShowHide" style='display: none;margin-top:15px;'>
                <div class='panel'>
                    <div class='panel-body'>    
                        <div class='col-sm-6' align='left' style='font-size:15px;'>        
                            <?php if($companyModel->phone_number != ''){ echo "<i class='fa fa-phone' style='color:rgba(91, 168, 255, 1);'><span style='color:black;'> ".$companyModel->phone_number."</span></i></br>"; } ?>
                            <?php if($companyModel->email != ''){ echo "<i class='fa fa-envelope' style='color:rgba(91, 168, 255, 1);'><span style='color:black;'>  ".$companyModel->email."</span></i></br>"; } ?>
                            <?php if($companyModel->country != ''){ echo "<i class='fa fa-flag' style='color:rgba(91, 168, 255, 1);'><span style='color:black;'>  ".$companyModel->country."</span></i></br>"; } ?>
                        </div>        
                        <div class='col-sm-6' align='left' style='font-size:15px;'>        
                            <?php if($companyModel->city != ''){ echo "<i class='fa fa-university' style='color:rgba(91, 168, 255, 1);'><span style='color:black;'>  ".$companyModel->city."</span></i></br>"; } ?>
                            <?php if($companyModel->zip != ''){ echo "<i class='fa fa-ticket' style='color:rgba(91, 168, 255, 1);'><span style='color:black;'>  ".$companyModel->zip."</span></i></br>"; } ?>
                        </div>
                        <div class='col-sm-12' align='left'>
                            <h5><b>Company info</b></h5>
                            <?php if($companyModel->discription != ''){ echo $companyModel->discription."</br>"; } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>