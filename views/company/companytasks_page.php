<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\widgets\LinkPager;
    use yii\bootstrap\Alert;
    use dosamigos\multiselect\MultiSelect;
    use app\assets\GraphUserAsset;
    use app\assets\HomepageAsset;
    use app\assets\ForrouteAsset;
    use app\assets\MyAsset;
    
    MyAsset::register($this);
    HomepageAsset::register($this);
    GraphUserAsset::register($this);
    ForrouteAsset::register($this);
    
?>
<div class="dashboard-container">

    <?php
        if(Yii::$app->session->hasFlash('TasksAdded')):
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-info',
                ],
                'body' => \Yii::t('app','Додати завдання'),
            ]);
        endif;

        if(Yii::$app->session->hasFlash('TasksNotAdded')):
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-warning',
                ],
                'body' => \Yii::t('app','Завдання не додано'),
            ]);

        endif;    
        
        
        if(Yii::$app->session->hasFlash('TasksUpdate')):
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-info',
                ],
                'body' => \Yii::t('app','Зміни збережено'),
            ]);
        endif;

        if(Yii::$app->session->hasFlash('TasksNotUpdate')):
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-warning',
                ],
                'body' => \Yii::t('app','Зміни не збережено'),
            ]);

        endif;    
        
        if(Yii::$app->session->hasFlash('TaskDelete')):
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-warning',
                ],
                'body' => \Yii::t('app','Завдання видалено'),
            ]);

        endif;    
    ?>   
      <div class="container">
        <!-- Top Nav Start -->
        <div id="cssmenu">
            <?php echo $this->render('menu',['urlname' => $urlname, 'parentCompany' => $parentCompany]); ?>
        </div>
        <!-- Top Nav End -->

        <!-- Sub Nav End -->
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <a href="../../../site/index" style="margin-left:9px;font-size:15px;padding:0px;"><i class="fa fa-home"></i> Home </a><a style="font-size:12px;padding:0px"> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;"><?= \Yii::t('app','Сторінка завдань'); ?></a>
            </li>
          </ul>
        </div>
        <!-- Sub Nav End -->

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
          
          <!-- Row starts -->
<!--          <div class="row displayNone">
            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">All tasks</div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 100<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-globe"></i>
                  </div>
                  <div class="pull-right number"></div>
                </div>
                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> 
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Active tasks</div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i><sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-twitter"></i>
                  </div>
                  <div class="pull-right number"></div>
                </div>
                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> 
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Deactive tasks</div>
                  <div class="pull-right"><i class="fa fa-angle-down"></i><sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-upload"></i>
                  </div>
                  <div class="pull-right number"></div>
                </div>
                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> 
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="mini-widget mini-widget-grey">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Signups</div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 67.1<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-coffee"></i>
                  </div>
                  <div class="pull-right number">1135</div>
                </div>
                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> 
              </div>
            </div>
          </div>-->
          <!-- Row ends -->

          <!-- Row Start -->
          <div class="row wrap">
              
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <i class="icon-angle-right boxClick" data-action="show"> </i><?= \Yii::t('app','Додати завдання'); ?>
                    <span class="mini-title">
                       <?= \Yii::t('app','натисніть на іконку щоб відкрити форму'); ?>
                    </span>
                  </div>
                </div>
                <div class=" widget-body boxShow">
                     <?php if($trackerArray[0] != 'No data'){?>
                    <?php  $form = ActiveForm::begin(['id' => 'NewTasks', 'action' => '/company/'.$urlname.'/taskspage','enableClientValidation' => true, 'class' => 'form-control', 'method' => 'POST']); ?>
                        <input type="hidden" name='company_id' value='<?= $companyModel->id; ?>'>
                        <?= $form->field($newTasksModel, 'type')->dropDownList(['1' => \Yii::t('app','Завдання'), '2' => \Yii::t('app','Маршрут')])->label(false); ?>
                        <?= $form->field($newTasksModel, 'description')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Опис')])->label(false); ?>
                        <?= $form->field($newTasksModel, 'due')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Дія')])->label(false); ?>
                        <div class="col-sm-12" id="mapContainer" style="display:none;">
                            <input type="hidden" id="for_map" name="for_map">
                            <div class="col-sm-12" style="padding:0px;">
                                <div id="datetimepicker1" class="input-group date" style="color:black;">
                                   <input data-format="yyyy-MM-dd hh:mm:ss" type="text" class='form-control' name="time_start">
                                   <span class="add-on input-group-addon">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"  style="color:black;">
                                    </i>
                                   </span>
                                </div><br>
                                <div id="datetimepicker2" class="input-group date" style="color:black;">
                                    <input data-format="yyyy-MM-dd hh:mm:ss" type="text" class='form-control'  name="time_end">
                                    <span class="add-on input-group-addon">
                                     <i data-time-icon="icon-time" data-date-icon="icon-calendar"  style="color:black;">
                                     </i>
                                    </span>
                                </div><br>
                                <div class="row" style="margin-bottom:20px;padding:0px;">
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="address" placeholder="<?= \Yii::t('app','Адреса'); ?>">
                                            <span class="input-group-btn">
                                              <button class="btn btn-default" id="submitAdderes" type="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <?= $form->field($newTasksModel, 'travel_mode')->dropDownList(['DRIVING' => 'Driving', 'WALKING' => 'Walking', 'BICYCLING' => 'Bicycling', 'TRANSIT' => 'Transit'],['id' => 'TravelType'])->label(false); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12" style="padding:0px;">
                                <div class="well">
                                    <div id="map-canvas" class="map-canvas" style="width:100%;height:300px;"></div>
                                        <button class="btn btn-default button_click" id="button_click"><?= \Yii::t('app','Побудувати маршрут'); ?></button>
                                        <button class="btn btn-default button_remove"><?= \Yii::t('app','Скинути маршрут'); ?></button>
                                </div>
                            </div>
                            <div class="for_way_list"></div>
                            <input type="hidden" name="distance" class="jsonArrayDistance" value="">
                        </div>
                        <?=  MultiSelect::widget([
                                'id'=>"multi".$driver->id,
                                'options' => ['multiple'=>"multiple"], // for the actual multiselect
                                'data' => $trackerArray,  
                                'name' => 'tracker_task', 
                                "clientOptions" =>
                                    [
                                        'numberDisplayed' => 2,
                                        'enableFiltering' => true,
                                        "includeSelectAllOption" => true,
                                    ],
                            ]); ?><br><br>
                        <?= Html::input('submit', 'submit_save',  \Yii::t('app', 'Додати'), ['class' => 'btn-submit btn btn-primary']); ?>
                    <?php ActiveForm::end();  
                    }else{
                        echo '<p style="color:black;">'.\Yii::t('app','Вибачте в вас немає жодного трекера').'</p>';
                    }
                    ?>
                    
                </div>
              </div>
            </div>
            
        
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  paddingNone">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <?= \Yii::t('app','Завдання'); ?>
                    <span class="mini-title">
                        <?= \Yii::t('app','Вивід всіх завданнь компанії'); ?>
                    </span>
                  </div>
                </div>
                <div class=" widget-body">
                    
                <table class="table table-responsive table-striped table-bordered table-hover no-margin">
        <tr style="font-weight:bold;color:black;">
            <td><b><?= \Yii::t('app','Опис'); ?></b></td>
            <td class="hidden-sm hidden-xs"><b><?= \Yii::t('app','Дія'); ?></b></td>
            <td><b><?= \Yii::t('app','Тип'); ?></b></td>
            <td class="hidden-xs hidden-sm"><b><?= \Yii::t('app','Час створення'); ?></td>
            <td class="hidden-xs"><b><?= \Yii::t('app','Трекер'); ?></b></td>
            <td colspan="2"></td>
        </tr>
        <tr style="font-weight:bold;">
            <form method="get" class="form-control">
                <td><input type="text" name="description" class="form-control" placeholder="<?= \Yii::t('app','Опис'); ?>" value="<?= $_GET['description']; ?>"></td>
                <td class="hidden-sm hidden-xs"><input type="text" name="due" class="form-control" placeholder="<?= \Yii::t('app','Дія'); ?>" value="<?= $_GET['due']; ?>"></td>
                <td>
                    <select name="typeTask" class="form-control">
                        <option value="0"><?= \Yii::t('app','Виберіть тип...'); ?></option>
                        <option value="1" <?php if($_GET['typeTask'] == 1){ echo 'selected'; }?>><?= \Yii::t('app','Завдання'); ?></option>
                        <option value="2" <?php if($_GET['typeTask'] == 2){ echo 'selected'; }?>><?= \Yii::t('app','Маршрут'); ?></option>
                    </select>
                </td>
                <td  class="hidden-sm hidden-xs">
                    <select name="OrderBy" class="form-control">
                        <option value="0"><?= \Yii::t('app','Сортувати по часу добавлення...'); ?></option>
                        <option value="1" <?php if($_GET['OrderBy'] == 1){ echo 'selected'; }?>><?= \Yii::t('app','По спаданню'); ?></option>
                        <option value="2" <?php if($_GET['OrderBy'] == 2){ echo 'selected'; }?>><?= \Yii::t('app','По зростанню'); ?></option>
                    </select>
                </td>
                <td class="hidden-xs">
                    <?=  MultiSelect::widget([
                        'id'=>"multiTrackerForTask",
                        'options' => ['multiple'=>"multiple"], // for the actual multiselect
                        'data' => $trackerArray,
                        'value' => $_GET['tracker_task'],
                        'name' => 'tracker_task', 
                        "clientOptions" =>
                            [
                                'numberDisplayed' => 2,
                                'enableFiltering' => true,
                                "includeSelectAllOption" => true,
                            ],
                    ]); ?>
                </td>
                <td colspan="2"><input type="submit" class="form-control" name="SearchTask" value="<?= \Yii::t('app','Пошук'); ?>"></td>
            </form>
        </tr>
        <?php

        foreach($modelTasks as $task){ ?>
            <?php
            $arrayStatus = ['1' => 'open', '2' => 'performed', '3' => 'not complete', '4' => 'complete'];
            ?>
            <tr style="color:black;">
                <td><?= $task->description; ?></td>
                <td  class="hidden-sm hidden-xs"><?= $task->due; ?></td>
                <td><?php  echo ($task->type == 1)?"Task":"Route";  ?></td>
                <td class="hidden-xs"><?= $task->time_added; ?></td>
                <td  class="hidden-xs">
                    <!--<a href='/../../../tracker/<?php //= $task->tracker_id; ?>'><?php //= $task->tracker_id; ?></a>-->
                    <?php 
                    $trackerChoiseArray = [];
                        foreach($trackerTask as $tracker){
                            if($tracker->task_id == $task->id){
                                echo "<a href='/../../../tracker/".$tracker->tracker_id."'>".$tracker->tracker_id."</a> ; ";
                                $trackerChoiseArray[] = $tracker->tracker_id;
                            }
                        }
                    ?>
                </td>
                <td align="center">
                    <a href='../../company/taskdelete?id=<?= $task->id; ?>&company_id=<?= $companyModel->id; ?>' class="delete" title='delete'><i class="fa fa-times fa-1x" style="color:#3187bf;"></i></a>
                </td>
                <td align="center">
                    <a data-toggle="modal" class="KursorShow" data-target="#myModal<?= $task->id; ?>"><i class='fa fa-pencil-square-o fa-1x' style="color:#3187bf;"></i></a>
                </td>
            </tr>
        <div class="modal fade" id="myModal<?= $task->id; ?>" name="modal" data-id_task="<?= $task->id; ?>" role="dialog">
            <div class="modal-dialog" style="width: 90%">
                <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title" style='color:black;'><?= \Yii::t('app','Редагування завдання'); ?></h4>
                    </div>
                    <div class="modal-body" style='color:black;'>
                    <?php $form = ActiveForm::begin(['id' => 'UpdateTask'.$task->id, 'action' => '../../company/taskupdate', 'method' => 'POST']);?>
                        <input type="hidden" name='company_urlname' value='<?= $companyModel->urlname; ?>'>
                        <input type="hidden" name='task_id' value='<?= $task->id; ?>'>
                        <input type="hidden" class="for_way_list_input<?= $task->id; ?>" name="for_way_list">
                        <input type="hidden" name='type' value='<?= $task->type; ?>'>
                        <?= $form->field($task, 'description')->textInput(['placeholder' => \Yii::t('app','Опис'), 'class' => 'form-control required']); ?>
                        <?= $form->field($task, 'due')->textInput(['placeholder' => \Yii::t('app','Дія'),'class' => 'form-control required']); ?>
                        <?= $form->field($task, 'status')->dropDownList($arrayStatus); ?>
                        <?php if($task->type == 2){ ?>
                            
                            <input type="hidden" class="for_map forMap<?= $task->id; ?>" name="for_map" value='<?= $task->for_map; ?>'>
                            <div id="datetimepicker1" class="input-group date" style="color:black;">
                                <input data-format="yyyy-MM-dd hh:mm:ss" type="text" class='form-control' name="time_start" value="<?= $task->time_start; ?>">
                               <span class="add-on input-group-addon">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"  style="color:black;">
                                </i>
                               </span>
                            </div><br>
                            <div id="datetimepicker2" class="input-group date" style="color:black;">
                                <input data-format="yyyy-MM-dd hh:mm:ss" type="text" class='form-control'  name="time_end" value="<?= $task->time_end; ?>" >
                                <span class="add-on input-group-addon">
                                 <i data-time-icon="icon-time" data-date-icon="icon-calendar"  style="color:black;">
                                 </i>
                                </span>
                            </div>
                            <div class="row" style="margin-bottom:20px;padding:0px;">
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="address" placeholder="Address...">
                                            <span class="input-group-btn">
                                              <button class="btn btn-default" id="submitAdderes" type="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <?= $form->field($task, 'travel_mode')->dropDownList(['DRIVING' => 'Driving', 'WALKING' => 'Walking', 'BICYCLING' => 'Bicycling', 'TRANSIT' => 'Transit'],['id' => 'TravelType', 'class' => 'form-control travel_map'])->label(false); ?>
                                        </div>
                                    </div>
                                </div>
                            <div id="map-canvas<?= $task->id; ?>" class="map-canvas" style="border:1px solid red;width:100%;height:300px;"></div>
                            <div class="btn btn-default button_click" data-task_id="<?= $task->id; ?>" id="button_click"><?= \Yii::t('app','Побудувати маршрут'); ?></div>
                            <div class="btn btn-default button_remove" id="button_remove"><?= \Yii::t('app','Скинути маршрут'); ?></div>
                            <div class="for_way_list<?= $task->id; ?>"></div>
                            <div class="jsonArrayDistance<?= $task->id; ?>"></div>
                            
                        <?php } ?>
                        <label>Tracker</label></br>
                             <?=  MultiSelect::widget([
                                'id'=>"multi".$task->id,
                                'options' => ['multiple' => 'multiple'],
                                'data' => $trackerArray, 
                                'value' => $trackerChoiseArray, 
                                'name' => 'tracker_id', 
                                "clientOptions" =>[
                                    'numberDisplayed' => 2,
                                    'enableFiltering' => true,
                                ],
                            ]); ?>
                    </div>
                    <div class="modal-footer">
                        <?php echo Html::submitButton(\Yii::t('app', 'Зберегти'), array('class' => 'btn btn-primary saveChangeInput')); ?>
                        <?php ActiveForm::end();  ?>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('app', 'Закрити'); ?></button>
                    </div>
                </div>
            </div>
        </div> 
             <?php       
             }
            ?>
                </table>
                    <?= LinkPager::widget(['pagination'=>$pagination]); ?>
                </div>
                </div>
            </div>
            </div>
        
              
          </div>
          <!-- Row End -->
        
        </div>
        <!-- Dashboard Wrapper End -->

        <footer>
          <p>© Tracker system 2015</p>
        </footer>

      </div>
    </div>
