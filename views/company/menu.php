<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>
    <?php    
        $class = ($this->context->getRoute() == 'company/show')?'active':''; 
        //$classForMenu = ($this->context->getRoute() == 'company/show')?'':'colorWhite';
        
        $class1 = ($this->context->getRoute() == 'company/companytrackers')?'active':''; 
        //$classForMenu1 = ($this->context->getRoute() == 'company/companytrackers')?'':'colorWhite';
        
        $class2 = ($this->context->getRoute() == 'company/companysubcompany')?'active':''; 
        //$classForMenu2 = ($this->context->getRoute() == 'company/companysubcompany')?'':'colorWhite';
        
        
        $class3 = ($this->context->getRoute() == 'company/companyreport')?'active':''; 
        //$classForMenu3 = ($this->context->getRoute() == 'company/companyreport')?'':'colorWhite';
        
        $class4 = ($this->context->getRoute() == 'company/companydriver')?'active':''; 
        //$classForMenu4 = ($this->context->getRoute() == 'company/companydriver')?'':'colorWhite';
        $class7 = ($this->context->getRoute() == 'company/companycar')?'active':''; 
        
        $class5 = ($this->context->getRoute() == 'company/taskspage')?'active':''; 
        //$classForMenu5 = ($this->context->getRoute() == 'company/taskspage')?'':'colorWhite';
        
        $class6 = ($this->context->getRoute() == 'company/companyinfo')?'active':'';
        
    ?>
    
    <ul>
            <li class="<?= $class; ?>">
              <a href="<?= '../../company/'.$urlname; ?>"><i class="fa fa-users"></i><?= \Yii::t('app', 'Користувачі'); ?></a>
            </li>
            
            <li class="<?= $class1; ?>">
              <a href="<?= '../../company/'.$urlname.'/companytrackers'; ?>">
                <i class="fa fa-map-marker"></i>
                <?= \Yii::t('app', 'Трекери'); ?>
              </a>
            </li>
            
            <li class="<?= $class4; ?>">
              <a href="<?= '../../company/'.$urlname.'/companydriver' ?>">
                <i class="fa fa-automobile"></i>
                <?= \Yii::t('app', 'Водії'); ?>
              </a>
            </li>
            
            <li class="<?= $class7; ?>">
              <a href="<?= '../../company/'.$urlname.'/companycar' ?>">
                <i class="fa fa-automobile"></i>
                <?= \Yii::t('app', 'Автомобілі'); ?>
              </a>
            </li>
            
            <li class="<?= $class5; ?>">
              <a href="<?= '../../company/'.$urlname.'/taskspage' ?>">
                <i class="fa fa-tasks"></i>
                <?= \Yii::t('app', 'Завдання'); ?>
              </a>
            </li>
            
        <?php if($parentCompany == null){ ?>
            <li class="<?= $class2; ?>">
              <a href="<?= '../../company/'.$urlname.'/companysubcompany'; ?>">
                <i class="fa fa-briefcase"></i>
                <?= \Yii::t('app', 'Під компанії'); ?>
              </a>
            </li>
        <?php } ?>
            
            <li class="<?= $class3; ?>">
              <a href="<?= '../../company/'.$urlname.'/companyreport' ?>">
                <i class="fa fa-bar-chart-o"></i>
                <?= \Yii::t('app', 'Графіки/Звіти'); ?>
              </a>
            </li>
            
            <li class="<?= $class6; ?>">
              <a href="<?= '../../company/'.$urlname.'/companyinfo' ?>">
                <i class="fa fa-info"></i>
                <?= \Yii::t('app', 'Компанія'); ?>
              </a>
            </li>
            
          </ul>
