<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\widgets\LinkPager;
    use yii\bootstrap\Alert;
    use dosamigos\multiselect\MultiSelect;
    use app\assets\MyAsset;
    use app\assets\GraphUserAsset;
    use app\assets\HomepageAsset;
    //use app\assets\LineAsset;
    
    MyAsset::register($this);
    //LineAsset::register($this);
    HomepageAsset::register($this);
    GraphUserAsset::register($this);
?>



<div class="dashboard-container">
    <div id="trackers_view" style="display:none;" data-action="<?= $companyModel->id; ?>">1</div>
    <?php
    if(Yii::$app->session->hasFlash('TrackerUpdate')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => \Yii::t('app','Трекер редаговано!'),
        ]);
    endif;

    if(Yii::$app->session->hasFlash('TrackerNotUpdate')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => \Yii::t('app','Трекер редаговано!'),
        ]);

    endif;
    if(Yii::$app->session->hasFlash('ErrorValidating')):

        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => 'Error Validating',
        ]);
    endif;

    if(Yii::$app->session->hasFlash('TrackerAdd')): ?>
            <?php
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-info',
                ],
                'body' => \Yii::t('app','Трекер додано'),
            ]);
            ?>
        <?php endif;

    if(Yii::$app->session->hasFlash('TrackerDeleted')): ?>
        <?php
        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => \Yii::t('app','Трекер видалено'),
        ]);
        ?>
    <?php endif; ?>
</div>

      <div class="container">
        <!-- Top Nav Start -->
        <div id="cssmenu">
            <?php echo $this->render('menu',['urlname' => $urlname, 'parentCompany' => $parentCompany]); ?>
        </div>
        <!-- Top Nav End -->

        <!-- Sub Nav End -->
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <a href="../../../site/index" style="margin-left:9px;font-size:15px;padding:0px;"><i class="fa fa-home"></i> Home </a><a style="font-size:12px;padding:0px"> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;"><?= \Yii::t('app','Сторінка трекерів'); ?></a>
            </li>
          </ul>
        </div>
        <!-- Sub Nav End -->

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
          
          <!-- Row starts -->
          <div class="row displayNone">
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left"><?= \Yii::t('app','Всіх трекерів'); ?></div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 100<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-globe"></i>
                  </div>
                  <div class="pull-right number"><?= $statistic['trackerCount']; ?></div>
                </div>
<!--                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> -->
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left"><?= \Yii::t('app','Активованих трекерів'); ?></div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i><?= $statistic['activeTrackerProcent']; ?><sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-twitter"></i>
                  </div>
                  <div class="pull-right number"><?= $statistic['activeTrackerCount']; ?></div>
                </div>
<!--                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> -->
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left"><?= \Yii::t('app','Неактивованих трекерів'); ?></div>
                  <div class="pull-right"><i class="fa fa-angle-down"></i><?= $statistic['deactiveTrackerProcent']; ?><sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-upload"></i>
                  </div>
                  <div class="pull-right number"><?= $statistic['deactiveTrackerCount']; ?></div>
                </div>
<!--                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> -->
              </div>
            </div>
<!--            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="mini-widget mini-widget-grey">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Signups</div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 67.1<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-coffee"></i>
                  </div>
                  <div class="pull-right number">1135</div>
                </div>
                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> 
              </div>
            </div>-->
          </div>
          <!-- Row ends -->

          <!-- Row Start -->
          <div class="row wrap">
              
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12  paddingNone">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <i class="icon-angle-right boxClick" data-action="show"> </i> <?= \Yii::t('app','Додати трекер'); ?>
                    <span class="mini-title">
                        <?= \Yii::t('app','натисніть на іконку щоб відкрити форму'); ?>
                    </span>
                  </div>
                </div>
                <div class=" widget-body boxShow" <?=(Yii::$app->session->hasFlash('ErrorValidating'))?'':'style="display:none;"'?>>
                   <?php  $form = ActiveForm::begin(['id' => 'TrackerNew', 'action' => '/company/'.$urlname.'/companytrackers','enableClientValidation' => true, 'class' => 'form-control', 'method' => 'POST']); ?>
                    <input type="hidden" name='company_name' value='<?= $companyModel->urlname; ?>'>
                    <?= $form->field($modelNewTracker, 'IMEI')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'IMEI'), 'class' => 'form-control'])->label(false); ?>
                    <?= $form->field($modelNewTracker, 'name')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Назва')])->label(false); ?>
                    <?= $form->field($modelNewTracker, 'discription')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Опис')])->label(false); ?>
                    <?= $form->field($modelNewTracker, 'speed_limit')->input('number'); ?>
                    <?= $form->field($modelNewTracker, 'tracker_marker')->hiddenInput(['class' => 'tracker_marker','value'=>'car'])->label(false); ?>
                    <div class="ChoosenIcon col-sm-12">
                        <label class="control-label" style="color:black;" for="marker"><?= \Yii::t('app','Виберіть іконку'); ?></label>
                        <img id="marker" class="showAllIcons" src="/web/content/icons/car.png">
                    </div>
                    <?= Html::input('submit', 'submit_save',  \Yii::t('app', 'Додати'), ['class' => 'btn-submit btn btn-primary']); ?>
                <?php ActiveForm::end();  ?>
                </div>
              </div>
            </div>
            
        
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <?= \Yii::t('app','Трекери'); ?>
                    <span class="mini-title">
                        <?= \Yii::t('app','вивід всіх трекерів компанії'); ?>
                    </span>
                  </div>
                </div>
                <div class=" widget-body">
                    
                <table class="table table-responsive table-striped table-bordered table-hover no-margin">
                
            <tr style="font-weight:bold;">
                <td style="color:black;"><b><?= \Yii::t('app','IMEI'); ?></b></td>
                <td class="hidden-xs" style="color:black;"><b><?= \Yii::t('app','Назва'); ?></b></td>
                <td class="hidden-xs hidden-sm" style="color:black;"><b><?= \Yii::t('app','Опис'); ?></b></td>
                <td class="hidden-xs hidden-sm" style="color:black;"><b><?= \Yii::t('app','Ліміт швидкості'); ?></b></td>
                <td class="hidden-xs" style="color:black;"><b><?= \Yii::t('app','Час додавання'); ?></b></td>
                <td colspan="3"></td>
            </tr>
            <form method='get' class="form-control">
                <tr style="font-weight:bold;">
                    <td><input type='text'  name='IMEI' class='form-control' placeholder="IMEI" value="<?= $_GET['IMEI']; ?>"></td>
                    <td class="hidden-xs"><input type='text'  name='name' class='form-control' placeholder="name" value="<?= $_GET['name']; ?>"></td>
                    <td class="hidden-xs hidden-sm"></td>
                    <td class="hidden-xs hidden-sm"></td>
                    <td class="hidden-xs"></td>
                    <td colspan="3"><input class="form-control" type="submit" name="SearchTracker" value="<?= \Yii::t('app','Опис'); ?>"></td>
                </tr>
            </form>
            <?php

            foreach($modelTracker as $tracker){ ?>
                <tr>
                    <td><a href="../../tracker/<?= $tracker->IMEI; ?>"><?= $tracker->IMEI; ?></a></td>
                    <td class="hidden-xs" style="color:black;"><?= $tracker->name; ?></td>
                    <td class="hidden-xs hidden-sm" style="color:black;"><?= $tracker->discription; ?></td>
                    <td class="hidden-xs hidden-sm" style="color:black;"><?= $tracker->speed_limit; ?></td>
                    <td class="hidden-xs" style="color:black;"><?= $tracker->time_added; ?></td>
                    <td align="center">
                        <a href='../../company/trackerdelete?id=<?= $tracker->id; ?>&company_id=<?= $companyModel->id; ?>' class="delete" title='delete'><i class="fa fa-times fa-1x" style="color:#3187bf;"></i></a>
                    </td>
                    <td align="center">
                        <a data-toggle="modal" class="KursorShow" data-target="#myModal<?= $tracker->id; ?>"><i class='fa fa-pencil-square-o fa-1x' style="color:#3187bf;"></i></a>
                    </td>  
                    <td align="center">
                        <?php if($tracker->active == '1'){ ?>
                            <i title="active" class='fa fa-circle activeTracker KursorShow' id='deactive' data-id='<?= $tracker->id; ?>' style="color:#3187bf;" data-action='Deactive'></i>
                        <?php }else{ ?>
                            <i title="deactive" class='fa fa-circle-o activeTracker KursorShow' id='active' data-id='<?= $tracker->id; ?>' style="color:#3187bf;" data-action='Active'></i>
                        <?php }?>
                            
                      <?php // if($tracker->active == '1'){ ?>
                            <!--<button class='btn btn-primary activeTracker' id='deactive' data-id='<?= $tracker->id; ?>' data-action='deactive'>Deactive</button>-->
                        <?php// }else{ ?>
                            <!--<button class='btn btn-primary activeTracker' id='active' data-id='<?= $tracker->id; ?>' data-action='active'>Active</button>-->
                        <?php// }?>
                    </td>
                </tr>
                <?php
                    $array = [];
                    foreach($tracker_user as $t_r){
                        if($t_r['tracker_id'] == $tracker->IMEI){
                            $array[] = $t_r['user_id'];
                        }
                    }
                    $array = array_values($array);
                    //var_dump($tracker_user);
                ?>
                
            <div class="modal fade" id="myModal<?= $tracker->id; ?>" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" style='color:black;'><?= \Yii::t('app','Редагування трекера'); ?></h4>
                </div>
                <div class="modal-body" style='color:black;'>
                
                <?php $form = ActiveForm::begin(['id' => 'UpdateTracker', 'action' => '/company/companyupdatetracker', 'class' => 'form-control', 'method' => 'POST']);
                    ?>
                    <input type="hidden" name='company_id' value='<?= $companyModel->id; ?>'>
                    <input type="hidden" name='company_name' value='<?= $companyModel->urlname; ?>'>
                    <input type="hidden" name='tracker_id' value='<?= $tracker->id; ?>'>
                    <?= '<b>IMEI:</b> '.$tracker->IMEI; ?></br>
                    <?= $form->field($tracker, 'name')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Назва'), 'class' => 'form-control'])->label(false); ?>
                    <?= $form->field($tracker, 'discription')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Опис'), 'class' => 'form-control'])->label(false); ?>
                    <?= $form->field($tracker, 'speed_limit')->input('number')->label(false); ?>
                                        
                    <?php if($tracker->company_id == 0){
                            $selec = '1';
                        }else{
                            $selec = '2';
                        }
                    ?>
                    <?php if($parentCompany == null){ ?>
                        <?= HTML::RadioList('TypeT', $selec, ['1' => \Yii::t('app','Користувач'), '2' => \Yii::t('app','Під компанія')],['inline' =>true]); ?>
                        <?php
                        
                        if($selec == '2' ){
                            ?>
                            <div class='divUser divHide'>
                                <?php
                                    echo MultiSelect::widget([
                                        'id'=>"multi".$tracker->id,
                                        'options' => ['multiple'=>"multiple"], // for the actual multiselect
                                        'data' => $arrayUser, // data as array
                                        'value' => $array, // if preselected
                                        'name' => 'tracker_user', // name for the form
                                        "clientOptions" =>
                                            [
                                                'numberDisplayed' => 2,
                                                'enableFiltering' => true,
                                            ],
                                    ]);
                                ?>
                            </div>
                            <div class='divCompany divShow'>
                                <?php
                                    echo  $form->field($tracker, 'company_id')->dropDownList($sub_company, ['class' => 'form-control classInputShow'])->label(false);
                                ?>
                            </div>
                            <?php
                        }else{
                            ?>
                            <div class='divUser divShow'>
                                <?php
                                    echo MultiSelect::widget([
                                        'id'=>"multiXX".$tracker->id,
                                        'options' => ['multiple'=>"multiple"], // for the actual multiselect
                                        'data' => $arrayUser, // data as array
                                        'value' => $array, // if preselected
                                        'name' => 'tracker_user', // name for the form
                                        "clientOptions" =>
                                            [
                                                'numberDisplayed' => 2,
                                                'enableFiltering' => true,
                                            ],
                                    ]);
                                ?>
                            </div>
                            <div class='divCompany divHide'>
                                <?= $form->field($tracker, 'company_id')->dropDownList($sub_company, ['class' => 'form-control classInputHide'])->label(false);?>
                            </div>
                            <?php
                        }
                    }else{
                    ?>
                        
                    <?php  echo MultiSelect::widget([
                            'id'=>"multiXX",
                            'options' => ['multiple'=>"multiple"], // for the actual multiselect
                            'data' => $arrayUser, // data as array
                            'value' => $array, // if preselected
                            'name' => 'tracker_user', // name for the form
                            "clientOptions" =>
                                [
                                    'numberDisplayed' => 2,
                                    'enableFiltering' => true,
                                ],
                        ]);
                    ?>
                        <input type="hidden" name='company_parent' value='1'>
                    <?php
                    }
                ?>

                </div>
                <div class="modal-footer">
                        <?php echo Html::submitButton(\Yii::t('app', 'Зберегти'), array('class' => 'btn btn-primary')); ?>
                        <?php ActiveForm::end(); ?>

                  <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('app','Закрити'); ?></button>
                </div>
              </div>

            </div>
          </div>


            <?php } ?>
                </table>
                    <?= LinkPager::widget(['pagination'=>$pagination]); ?>
                </div>
                </div>
            </div>
            </div>
              
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 paddingNone">
                  
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <?= \Yii::t('app','Карта'); ?>
                      <span class="mini-title">
                            <?= \Yii::t('app','Вивід місця знаходження'); ?>
                      </span>
                    </div>
                  </div>
                  <div class=" widget-body">
                    <div id="map" style='border:1px solid grey;width: 100%'></div>
                  </div>
                  </div>
                </div>
              </div>
              
          </div>
          <!-- Row End -->
          

          

        </div>
        <!-- Dashboard Wrapper End -->

        <footer>
          <p>© Tracker system 2015</p>
        </footer>

      </div>
    </div>
    
<!-- Modal -->
<div class="modal fade" id="ChooseIcon" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Choose Icon</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!-- Modal end -->