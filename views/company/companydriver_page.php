<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\widgets\LinkPager;
    use yii\bootstrap\Alert;
    use dosamigos\multiselect\MultiSelect;
    use app\assets\MyAsset;
    use app\assets\GraphUserAsset;
    use app\assets\HomepageAsset;
    
    MyAsset::register($this);
    HomepageAsset::register($this);
    GraphUserAsset::register($this);
?>
<div class="dashboard-container">
    <?php
    if(Yii::$app->session->hasFlash('DriverAdded')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => \Yii::t('app','Водій доданий!'),
        ]);
    endif;

    if(Yii::$app->session->hasFlash('DriverNotAdded')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => \Yii::t('app','Водій не доданий, повторіть спробу, або зверніться до адміністрації!'),
        ]);

    endif;
    
    if(Yii::$app->session->hasFlash('DriverDelete')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => \Yii::t('app','Водій видалений!'),
        ]);

    endif;
    
    
    if(Yii::$app->session->hasFlash('DriverUpdate')): ?>
        <?php
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => \Yii::t('app','Дані водія успішно змінено!'),
        ]);
    endif;

    if(Yii::$app->session->hasFlash('DriverNotUpdate')): ?>
        <?php
        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => \Yii::t('app','Дані водія не редаговано, повторіть спробу, або зверніться до адміністрації!'),
        ]);
    endif;
    ?>
      <div class="container">
        <!-- Top Nav Start -->
        <div id="cssmenu">
            <?php echo $this->render('menu',['urlname' => $urlname, 'parentCompany' => $parentCompany]); ?>
        </div>
        <!-- Top Nav End -->

        <!-- Sub Nav End -->
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <a href="../../../site/index" style="margin-left:9px;font-size:15px;padding:0px;"><i class="fa fa-home"></i> Home </a><a style="font-size:12px;padding:0px"> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;"><?= \Yii::t('app','Сторінка водіїв'); ?></a>
            </li>
          </ul>
        </div>
        <!-- Sub Nav End -->

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
 
          <div class="row wrap">
              
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <i class="icon-angle-right boxClick" data-action="show"> </i><?= \Yii::t('app','Додати водія'); ?>
                    <span class="mini-title displayNone">
                        <?= \Yii::t('app','натисніть на іконку щоб відкрити форму'); ?>
                    </span>
                  </div>
                </div>
                <div class=" widget-body boxShow" <?=(Yii::$app->session->hasFlash('ErrorValidating'))?'':'style="display:none;color:black;"'?>>
                    <?php $form = ActiveForm::begin(['id' => 'NewUser', 'action' => '../../company/'.$urlname.'/companydriver', 'enableClientValidation'=>true, 'class' => 'form-control', 'method' => 'POST']); ?>
                        <input type="hidden" name='company_id' value='<?= $companyModel->id; ?>'>
                        <?= $form->field($newDriverModel, 'first_name')->textInput(['placeholder' => 'Ім`я', 'class' => 'form-control required']); ?>
                        <?= $form->field($newDriverModel, 'last_name')->textInput(['placeholder' => 'Прізвище','class' => 'form-control required']); ?>
                        <?= $form->field($newDriverModel, 'phone_number')->textInput(['placeholder' => 'Номер телефону', 'class' => 'form-control']); ?>
                        <?= $form->field($newDriverModel, 'car_id')->dropDownList(
                                $carsArray);
                    ?>
                    
                    <?= Html::submitButton(\Yii::t('app', 'Додати'), ['class' => 'btn-submit btn btn-primary']); ?>
                <?php ActiveForm::end();   ?> 
                </div>
              </div>
            </div>
            
        
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <?= \Yii::t('app','Водії'); ?>
                    <span class="mini-title">
                        <?= \Yii::t('app','вивід всіх водіїв компанії'); ?>
                    </span>
                  </div>
                </div>
                <div class=" widget-body">
                    
                <table class="table table-responsive table-striped table-bordered table-hover no-margin">
                
        <tr style="font-weight:bold;color:black;">
            <td class="hidden-xs"><b><?= \Yii::t('app','Ім`я'); ?></b></td>
            <td><b><?= \Yii::t('app','Прізвище'); ?></b></td>
            <td class="hidden-xs hidden-sm"><b><?= \Yii::t('app','Номер телефону'); ?></b></td>
            <td class="hidden-xs"><b><?= \Yii::t('app','Номер автомобіля'); ?></b></td>
            <td colspan="2"></td>
        </tr>
        
        <?php

        foreach($modelDriver as $driver){ ?>
            <?php
//                $driverChoice = [];
//                    foreach($driverTracker as $driverC){
//                        if($driver->id == $driverC->driver_id){
//                            $driverChoice = $driverC->tracker_id;
//                        }
//                    } 
            ?>
            <tr style="color:black;">
                <td  class="hidden-xs"><?= $driver->first_name; ?></td>
                <td><?= $driver->last_name; ?></td>
                <td  class="hidden-xs hidden-sm"><?= $driver->phone_number; ?></td>
                <td  class="hidden-xs hidden-sm"><?= $driver->car_id; ?></td>
                <td align="center">
                    <a href='../../company/driverdelete?id=<?= $driver->id; ?>&company_id=<?= $companyModel->id; ?>' class="delete" title='delete'><i class="fa fa-times fa-1x" style="color:#3187bf;"></i></a>
                </td>
                <td align="center">
                    <a data-toggle="modal" class="KursorShow" data-target="#myModal<?= $driver->id; ?>"><i class='fa fa-pencil-square-o fa-1x' style="color:#3187bf;"></i></a>
                </td>
            </tr>
            
            <div class="modal fade" id="myModal<?= $driver->id; ?>" role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title" style='color:black;'>Change driver</h4>
                        </div>
                        <div class="modal-body" style='color:black;'>
                        <?php $form = ActiveForm::begin(['id' => 'UpdateDriver', 'action' => '../../company/driverupdate', 'class' => 'form-control', 'method' => 'POST']);?>
                            <input type="hidden" name='company_urlname' value='<?= $companyModel->urlname; ?>'>
                            <input type="hidden" name='driver_id' value='<?= $driver->id; ?>'>
                            <?= $form->field($driver, 'first_name')->textInput(['placeholder' => \Yii::t('app','Ім`я'), 'class' => 'form-control required']); ?>
                            <?= $form->field($driver, 'last_name')->textInput(['placeholder' => \Yii::t('app','Прізвище'),'class' => 'form-control required']); ?>
                            <?= $form->field($driver, 'phone_number')->textInput(['placeholder' => \Yii::t('app','Номер телефону'), 'class' => 'form-control']); ?>
                            <?= $form->field($driver, 'car_id')->dropDownList($carsArray); ?>
                        </div>
                        <div class="modal-footer">
                                <?php echo Html::submitButton(\Yii::t('app', 'Зберегти'), array('class' => 'btn btn-primary')); ?>
                                <?php ActiveForm::end(); ?>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('app','Закрити'); ?></button>
                        </div>
                    </div>
                </div>
            </div>
            
             <?php       
             } 
            ?>
             </div>   
                </table>
                    <?php  LinkPager::widget(['pagination'=>$pagination]); ?>
                </div>
                </div>
            </div>
            </div>
        
              
          </div>
          <!-- Row End -->
        
        </div>
        <!-- Dashboard Wrapper End -->

        <footer>
          <p>© Tracker system 2015</p>
        </footer>

      </div>
    </div>
