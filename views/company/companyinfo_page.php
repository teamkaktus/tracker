<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap\Alert;
    use app\assets\HomepageAsset;
    
    HomepageAsset::register($this);
?>
<div class="dashboard-container">
      <?php
        if(Yii::$app->session->hasFlash('CompanyUpdate')):
                echo Alert::widget([
                    'options' => [
                        'class' => 'alert-info',
                    ],
                    'body' => \Yii::t('app','Зміни збережено'),
                ]);
            endif;
            
            if(Yii::$app->session->hasFlash('CompanyNotUpdate')):
                echo Alert::widget([
                    'options' => [
                        'class' => 'alert-warning',
                    ],
                    'body' => \Yii::t('app','Зміни не збережено'),
                ]);

            endif;
      ?>  
      <div class="container">
        <!-- Top Nav Start -->
        <div id="cssmenu">
            <?php echo $this->render('menu',['urlname' => $urlname, 'parentCompany' => $parentCompany]); ?>
        </div>
        <!-- Top Nav End -->

        <!-- Sub Nav End -->
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <a href="site/index" style="margin-left:9px;font-size:15px;padding:0px;"><i class="fa fa-home"></i> Home </a><a style="font-size:12px;padding:0px"> <i class="fa fa-arrow-right"> </i> </a><a href="/admin/index" style="font-size:15px;padding:0px;"><?= \Yii::t('app','Сторінка компанії'); ?></a>
            </li>
          </ul>
        </div>
        <!-- Sub Nav End -->

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg" style="min-height:400px;">
          
          <!-- Row ends -->

          <!-- Row Start -->
          <div class="row wrap">
              
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    Company 
                    <span class="mini-title">
                    </span>
                    <i align="right" class="fa fa-edit" type="button" class="btn btn-info" data-toggle="modal" data-target="#myModalCompany"></i>
                  </div>
                </div>
                <div class=" widget-body" style="color:black;">    
                        <?php if($companyModel->phone_number != ''){ echo "<i class='fa fa-phone' style='color:rgba(91, 168, 255, 1);'><span style='color:black;'> ".$companyModel->phone_number."</span></i></br>"; } ?>
                        <?php if($companyModel->email != ''){ echo "<i class='fa fa-envelope' style='color:rgba(91, 168, 255, 1);'><span style='color:black;'>  ".$companyModel->email."</span></i></br>"; } ?>
                        <?php if($companyModel->country != ''){ echo "<i class='fa fa-flag' style='color:rgba(91, 168, 255, 1);'><span style='color:black;'>  ".$companyModel->country."</span></i></br>"; } ?>
                        <?php if($companyModel->city != ''){ echo "<i class='fa fa-university' style='color:rgba(91, 168, 255, 1);'><span style='color:black;'>  ".$companyModel->city."</span></i></br>"; } ?>
                        <?php if($companyModel->zip != ''){ echo "<i class='fa fa-ticket' style='color:rgba(91, 168, 255, 1);'><span style='color:black;'>  ".$companyModel->zip."</span></i></br>"; } ?>
                        <?php if($companyModel->discription != ''){ echo "<i class='fa fa-cloude'> ".$companyModel->discription."</i></br>"; } ?>
                </div>
            </div>
            </div>
        
              
          </div>
      </div>
          <!-- Row End -->
        
        </div>
        <!-- Dashboard Wrapper End -->

        <footer>
          <p>© Tracker system 2015</p>
        </footer>

      </div>
    </div>

<?php echo $this->render('form_update',['companyModel' => $companyModel]); ?>
