<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\widgets\LinkPager;
    use yii\bootstrap\Alert;
    use dosamigos\multiselect\MultiSelect;
    use app\assets\MyAsset;
    use app\assets\GraphUserAsset;
    use app\assets\HomepageAsset;
    use app\assets\AvtoAsset;
    
    AvtoAsset::register($this);
    MyAsset::register($this);
    HomepageAsset::register($this);
    GraphUserAsset::register($this);
?>
<div class="dashboard-container">
    
    <div class="table table-striped" class="files" id="previews">

        <div id="template" class="file-row">
          <!-- This is used as the file preview template -->
          <div class="col-sm-4">
              <span class="preview"><img data-dz-thumbnail /></span>
              <p class="name" data-dz-name></p>
              <strong class="error text-danger" data-dz-errormessage></strong>
          </div>
          <div class="col-sm-8">
              <p class="size" data-dz-size></p>
              <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
              </div>
          </div>
        </div>

      </div>
    
    <?php
    if(Yii::$app->session->hasFlash('CarAdded')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => \Yii::t('app','Автомобіль доданий!'),
        ]);
    endif;

    if(Yii::$app->session->hasFlash('CarNotAdded')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => \Yii::t('app','Автомобіль не доданий, повторіть спробу, або зверніться до адміністрації!'),
        ]);

    endif;
    
    if(Yii::$app->session->hasFlash('CarDelete')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => \Yii::t('app','Автомобіль видалений!'),
        ]);

    endif;
    
    
    if(Yii::$app->session->hasFlash('CarUpdate')): ?>
        <?php
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => \Yii::t('app','Дані автомобіля успішно змінено!'),
        ]);
    endif;

    if(Yii::$app->session->hasFlash('CarNotUpdate')): ?>
        <?php
        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => \Yii::t('app','Дані автомобіля не редаговано, повторіть спробу, або зверніться до адміністрації!'),
        ]);
    endif;
    ?>
      <div class="container">
        <!-- Top Nav Start -->
        <div id="cssmenu">
            <?php echo $this->render('menu',['urlname' => $urlname, 'parentCompany' => $parentCompany]); ?>
        </div>
        <!-- Top Nav End -->

        <!-- Sub Nav End -->
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <a href="../../../site/index" style="margin-left:9px;font-size:15px;padding:0px;"><i class="fa fa-home"></i> Home </a><a style="font-size:12px;padding:0px"> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;"><?= \Yii::t('app','Сторінка водіїв'); ?></a>
            </li>
          </ul>
        </div>
        <!-- Sub Nav End -->

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
 

          <!-- Row Start -->
          <div class="row wrap">
              
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <i class="icon-angle-right boxClick" data-action="show"> </i><?= \Yii::t('app','Додати автомобіль'); ?>
                    <span class="mini-title displayNone">
                        <?= \Yii::t('app','натисніть на іконку щоб відкрити форму'); ?>
                    </span>
                  </div>
                </div>
                <div class=" widget-body boxShow" <?=(Yii::$app->session->hasFlash('ErrorValidating'))?'':'style="display:none;color:black;"'?>>
                    <?php  $form = ActiveForm::begin(['id' => 'NewCar', 'action' => '../../company/'.$urlname.'/companycar', 'enableClientValidation'=>true, 'class' => 'form-control', 'method' => 'POST']); ?>
                    <?= $form->field($newModelCar, 'marka')->textInput(['placeholder' => 'Марка авто', 'class' => 'form-control required']); ?>
                    <?= $form->field($newModelCar, 'number')->textInput(['placeholder' => 'Номер авто','class' => 'form-control required']); ?>
                    <?= $form->field($newModelCar, 'change_wheels')->textInput(['placeholder' => 'Заміна коліс', 'class' => 'form-control']); ?>
                    <?= $form->field($newModelCar, 'change_oil')->textInput(['placeholder' => 'Заміна мастила', 'class' => 'form-control required']); ?>
                    <?= $form->field($newModelCar, 'period_change_wheels')->input('number',['placeholder' => 'Період заміни коліс/км', 'class' => 'form-control required']); ?>
                    <?= $form->field($newModelCar, 'period_change_oil')->input('number',['placeholder' => 'Період заміни мастила/км', 'class' => 'form-control required']); ?>
                    <a class="btn btn-primary newPhoto">Add photo</a>
                    <div class="col-sm-12 avtoPhoto">
                        
                    </div>
                    <?= $form->field($newModelCar, 'image_src')->textInput(['placeholder' => 'Фото автомобіля', 'class' => 'form-control required']); ?>
                    <?= $form->field($newModelCar, 'tracker_imei')->dropDownList($trackerArray); ?>
                    <?= $form->field($newModelCar, 'company_id')->hiddenInput(['value'=>$companyModel->id]); ?>
                    
                    <?= Html::submitButton(\Yii::t('app', 'Додати'), ['class' => 'btn-submit btn btn-primary']); ?>
                    <?php ActiveForm::end();  ?>
                </div>
              </div>
            </div>
            
        
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <?= \Yii::t('app','Автомобілі'); ?>
                    <span class="mini-title">
                        <?= \Yii::t('app','вивід всіх водіїв компанії'); ?>
                    </span>
                  </div>
                </div>
                <div class=" widget-body">
                <table class="table table-responsive table-striped table-bordered table-hover no-margin">
                
                        <tr style="font-weight:bold;color:black;">
                            <td class="hidden-xs"><b><?= \Yii::t('app','Марка авто'); ?></b></td>
                            <td><b><?= \Yii::t('app','Номер авто'); ?></b></td>
                            <td class="hidden-xs hidden-sm"><b><?= \Yii::t('app','Заміна коліс'); ?></b></td>
                            <td class="hidden-xs"><b><?= \Yii::t('app','Заміна мастила'); ?></b></td>
                            <td class="hidden-xs"><b><?= \Yii::t('app','Період заміни коліс/км'); ?></b></td>
                            <td class="hidden-xs"><b><?= \Yii::t('app','Період заміни мастила/км'); ?></b></td>
                            <td class="hidden-xs"><b><?= \Yii::t('app','Фото автомобіля'); ?></b></td>
                            <td class="hidden-xs"><b><?= \Yii::t('app','Трекер'); ?></b></td>
                            <td colspan="2"></td>
                        </tr>
                        <!--<tr style="font-weight:bold; color:black;">
                            <form method="get" class="form-control">
                            <td  class="hidden-xs"><input type="text" name="first_name" class="form-control" placeholder="<?= \Yii::t('app','Ім`я'); ?>" value="<?= $_GET['first_name']; ?>"></td>
                            <td><input type="text" name="last_name" class="form-control" placeholder="<?= \Yii::t('app','Прізвище'); ?>" value="<?= $_GET['last_name']; ?>"></td>
                            <td  class="hidden-xs hidden-sm"><input type="text" name="phone_number" class="form-control" placeholder="<?= \Yii::t('app','Номер телефону'); ?>" value="<?= $_GET['phone_number']; ?>"></td>
                            <td  class="hidden-xs"></td>
                            <td colspan="2"><input type="submit" name="SearchDriver" class="form-control" value="<?= \Yii::t('app','Пошук'); ?>"></td>
                            </form>
                        </tr> -->
                        <?php

                        foreach($modelCarinfo as $car){ ?>
                            <tr style="color:black;">
                                <td  class="hidden-xs"><?= $car->marka; ?></td>
                                <td><?= $car->number; ?></td>
                                <td  class="hidden-xs hidden-sm"><?= $car->change_wheels; ?></td>
                                <td  class="hidden-xs hidden-sm"><?= $car->change_oil; ?></td>
                                <td  class="hidden-xs hidden-sm"><?= $car->period_change_wheels; ?></td>
                                <td  class="hidden-xs hidden-sm"><?= $car->period_change_oil; ?></td>
                                <td  class="hidden-xs hidden-sm"><?= $car->image_src; ?></td>
                                <td class="hidden-xs"><a href='/../../../tracker/<?= $car->tracker_imei; ?>'><?= $car->tracker_imei; ?></a></td>
                                <td align="center">
                                    <a href='../../company/cardelete?id=<?= $car->id; ?>&company_id=<?= $companyModel->id; ?>' class="delete" title='delete'><i class="fa fa-times fa-1x" $carstyle="color:#3187bf;"></i></a>
                                </td>
                                <td align="center">
                                    <a data-toggle="modal" class="KursorShow" data-target="#myModal<?= $car->id; ?>"><i class='fa fa-pencil-square-o fa-1x' style="color:#3187bf;"></i></a>
                                </td>
                            </tr>
                        <div class="modal fade" id="myModal<?= $car->id; ?>" role="dialog">
                            <div class="modal-dialog">
                              <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title" style='color:black;'>Change car</h4>
                                    </div>
                                    <div class="modal-body" style='color:black;'>
                                    <?php $form = ActiveForm::begin(['id' => 'UpdateCar', 'action' => '../../company/carupdate', 'class' => 'form-control', 'method' => 'POST']);?>
                                        <input type="hidden" name='company_urlname' value='<?= $companyModel->urlname; ?>'>
                                        <input type="hidden" name='car_id' value='<?= $car->id; ?>'>
                                        <?= $form->field($car, 'marka')->textInput(['placeholder' => 'Марка авто', 'class' => 'form-control required']); ?>
                                        <?= $form->field($car, 'number')->textInput(['placeholder' => 'Номер авто','class' => 'form-control required']); ?>
                                        <?= $form->field($car, 'change_wheels')->textInput(['placeholder' => 'Заміна коліс', 'class' => 'form-control']); ?>
                                        <?= $form->field($car, 'change_oil')->textInput(['placeholder' => 'Заміна мастила', 'class' => 'form-control required']); ?>
                                        <?= $form->field($car, 'period_change_wheels')->input('number',['placeholder' => 'Період заміни коліс/км', 'class' => 'form-control required']); ?>
                                        <?= $form->field($car, 'period_change_oil')->input('number',['placeholder' => 'Період заміни мастила/км', 'class' => 'form-control required']); ?>
                                        <?= $form->field($car, 'image_src')->textInput(['placeholder' => 'Фото автомобіля', 'class' => 'form-control required']); ?>
                                        <?= $form->field($car, 'tracker_imei')->dropDownList($trackerArray); ?>
                                        <?= $form->field($car, 'company_id')->hiddenInput(['value'=>$companyModel->id]); ?>
                                    </div>
                                    <div class="modal-footer">
                                            <?php echo Html::submitButton(\Yii::t('app', 'Зберегти'), array('class' => 'btn btn-primary')); ?>
                                            <?php ActiveForm::end(); ?>
                                        <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('app','Закрити'); ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>   
                             <?php       
                             }
                            ?>
                             </div>   
                </table>
                    <?= LinkPager::widget(['pagination'=>$pagination]); ?>
                    
                    
                </div>
            </div>
            </div>
        
              
          </div>
          <!-- Row End -->
        
        </div>
        <!-- Dashboard Wrapper End -->

        <footer>
          <p>© Tracker system 2015</p>
        </footer>

      </div>
    </div>
