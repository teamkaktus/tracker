<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\widgets\LinkPager;
    use yii\bootstrap\Alert;
    use dosamigos\multiselect\MultiSelect;
    use app\assets\GraphAsset;
    use app\assets\GraphUserAsset;
    use app\assets\ReportAsset;
    use app\assets\HomepageAsset;
    use app\assets\MyAsset;
    
    MyAsset::register($this);
    HomepageAsset::register($this);
    GraphAsset::register($this);
    GraphUserAsset::register($this);
    ReportAsset::register($this);
?>
<div style="display:none;">
    <span id="max_speed"><?= \Yii::t('app','Максивальна швидкість'); ?></span>
    <span id="everage_speed"><?= \Yii::t('app','Середня швидкість'); ?></span>
    <span id="millage_chart"><?= \Yii::t('app','Графік пройденої дистанції'); ?></span>
    <span id="activity_chart"><?= \Yii::t('app','Графік активності'); ?></span>
    <span id="choice_parameter"><?= \Yii::t('app','Виберіть параметри...'); ?></span>
    <span id="choice_subcompany"><?= \Yii::t('app','Виберіть під компанію...'); ?></span>
    <span id="choice_tracker"><?= \Yii::t('app','Виберіть трекер...'); ?></span>
    <span id="choice_user"><?= \Yii::t('app','Виберіть користувача...'); ?></span>
    <span id="choice_opt"><?= \Yii::t('app','Невибрано всі необідні дані!А саме:'); ?></span>
    <span id="find_option"><?= \Yii::t('app','Параметер; '); ?></span>
    <span id="time_start"><?= \Yii::t('app','Час початку руху; '); ?></span>
    <span id="time_end"><?= \Yii::t('app','Час закінчення руху; '); ?></span>
    <span id="report_type"><?= \Yii::t('app','Тип звіту; '); ?></span>
    <span id="fuel_burned"><?= \Yii::t('app','Спалено пального - '); ?></span>
    <span id="millage_walk"><?= \Yii::t('app','Пройдено миль - '); ?></span>
</div>
<div class="dashboard-container">

      <div class="container">
        <!-- Top Nav Start -->
        <div id="cssmenu">
            <?php echo $this->render('menu',['urlname' => $urlname, 'parentCompany' => $parentCompany]); ?>
        </div>
        <!-- Top Nav End -->

        <!-- Sub Nav End -->
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <a href="../../../site/index" style="margin-left:9px;font-size:15px;padding:0px;"><i class="fa fa-home"></i> Home </a><a style="font-size:12px;padding:0px"> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;"><?= \Yii::t('app','Графіки/Звіти'); ?></a>
            </li>
          </ul>
        </div>
        <!-- Sub Nav End -->

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
          
          <!-- Row ends -->

          <!-- Row Start -->
          <div class="row wrap">
              
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone"  style="padding:0px;margin:0px;">
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone"  style="padding:0px;margin:0px;">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <?= \Yii::t('app','Графіки/Звіти'); ?>
                    <span class="mini-title">
                        <?= \Yii::t('app','Графіки/Звіти цієї компанії'); ?>
                    </span>
                  </div>
                </div>
                <div class=" widget-body">
                    <div class="row">
                            <div class="col-sm-2">
                                <select name="report_type" id="reportType" class="form-control">
                                    <option value="0"><?= \Yii::t('app','Виберіть тип...'); ?></option>
                                    <option value="1"><?= \Yii::t('app','Пройдена відстань'); ?></option>
                                    <option value="2"><?= \Yii::t('app','Швидкість їзди'); ?></option>
                                    <option value="3"><?= \Yii::t('app','Час в дорозі'); ?></option>
                                    <option value="4"><?= \Yii::t('app','Пальне'); ?></option>
                                    <option value="5"><?= \Yii::t('app','Мастило'); ?></option>
                                    <option value="6">Activity</option>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <select data-company_id="<?= $companyModel->id; ?>" id="TopOption" name="TopOption" class="form-control" disabled>
                                    <option value="0"><?= \Yii::t('app','Виберіть параметер...'); ?></option>
                                    <option value="1"><?= \Yii::t('app','Підкомпанії'); ?></option>
                                    <option value="2"><?= \Yii::t('app','Користувачі'); ?></option>
                                    <option value="3"><?= \Yii::t('app','Трекери'); ?></option>
                                </select>
                            </div>
                            <div class="col-sm-5">
                                <div class="col-sm-4"> 
                                    <div class="SubCompanys" style="display:none;">
                                    </div>
                                </div>
                                <div class="col-sm-4"> 
                                    <div class="UserSubCompanys" style="display:none;">
                                    </div>
                                </div>
                                <div class="col-sm-4"> 
                                    <div class="TrackerCompany" style="display:none;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="col-sm-4">
                                    <div id="datetimepicker1" class="input-group ">
                                       <input data-format="yyyy-MM-dd hh:mm:ss" type="text" class='form-control TimeStart'></input>
                                       <span class="add-on input-group-addon">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                        </i>
                                       </span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div id="datetimepicker2" class="input-group">
                                        <input data-format="yyyy-MM-dd hh:mm:ss" type="text" class='form-control TimeEnd'></input>
                                        <span class="add-on input-group-addon">
                                         <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                         </i>
                                        </span>
                                     </div>
                                </div>
                                
                                <div class="col-sm-1">
                                    <input type="submit" class="btn btn-default" id="goReport" name="Go" value="<?= \Yii::t('app','Пошук'); ?>">
                                </div>
                            </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="loader" style='display:none'></div>
                            <div id="chart_MaxSpeed" style="width: 100%; height: 500px; display:none;" ></div>
                            <div id="chart_EverageSpeed" style="width: 100%; height: 500px; display:none;" ></div>
                            <div id="chart_Millage" style="width: 100%; height: 500px; display:none;" ></div>
                            <div id="chart_Fuel" style="width: 100%; height: 500px; display:none;" ></div>
                            <div id="chart_activity" style="width: 100%; height: 500px; display:none;" ></div>
                        </div>
                        <div class="col-sm-12 divForTable" style="display:none;">
                            <div class='col-sm-12 tableData' style="display:none;">
                            </div>
                            <button id='btnExport' class='btn btn-primary' style='width: 100%;'><?= \Yii::t('app','Екпортувати в Exel'); ?></button>
                        </div>
                    </div>
             </div>
                </div>
            </div>
            </div>
        
              
          </div>
          <!-- Row End -->
        
        </div>
        <!-- Dashboard Wrapper End -->

        <footer>
          <p>© Tracker system 2015</p>
        </footer>

      </div>
    </div>

