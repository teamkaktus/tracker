<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\widgets\LinkPager;
    use yii\bootstrap\Alert;
    use app\assets\GraphUserAsset;
    use app\assets\HomepageAsset;
    use app\assets\MyAsset;
    
    MyAsset::register($this);
    HomepageAsset::register($this);
    GraphUserAsset::register($this);

?>
<div class="dashboard-container">
    <div class='companyid' style='display:none;' data-action='subcompany'><?= $companyModel->id; ?></div>
    <?php
    if(Yii::$app->session->hasFlash('SubCompanyAdd')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => \Yii::t('app','Під компанія додана'),
        ]);
    endif;
    if(Yii::$app->session->hasFlash('ErrorValidating')):

        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => 'Error Validating',
        ]);
    endif;
    
    if(Yii::$app->session->hasFlash('CompanyDeleted')):

        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => \Yii::t('app','Під компанія видалена'),
        ]);
    endif;
    ?>
      <div class="container">
        <!-- Top Nav Start -->
        <div id="cssmenu">
            <?php echo $this->render('menu',['urlname' => $urlname, 'parentCompany' => $parentCompany]); ?>
        </div>
        <!-- Top Nav End -->

        <!-- Sub Nav End -->
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <a href="../../../site/index" style="margin-left:9px;font-size:15px;padding:0px;"><i class="fa fa-home"></i> Home </a><a style="font-size:12px;padding:0px"> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;"><?= \Yii::t('app','Сторінка підкомпаній'); ?></a>
            </li>
          </ul>
        </div>
        <!-- Sub Nav End -->

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
          
          <!-- Row starts -->
          <div class="row displayNone">
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left"><?= \Yii::t('app','Всіх підкомпаній'); ?></div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 100<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-globe"></i>
                  </div>
                  <div class="pull-right number"><?= $statistic['subcompanyCount']; ?></div>
                </div>
<!--                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> -->
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left"><?= \Yii::t('app','Активованих підкомпаній'); ?></div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i><?= $statistic['activeSubcompanyProcent']; ?><sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-twitter"></i>
                  </div>
                  <div class="pull-right number"><?= $statistic['activeSubcompanyCount']; ?></div>
                </div>
<!--                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> -->
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left"><?= \Yii::t('app','Неактивованих підкомпаній'); ?></div>
                  <div class="pull-right"><i class="fa fa-angle-down"></i><?= $statistic['deactiveSubcompanyProcent']; ?><sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-upload"></i>
                  </div>
                  <div class="pull-right number"><?= $statistic['deactiveSubcompanyCount']; ?></div>
                </div>
<!--                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> -->
              </div>
            </div>
<!--            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="mini-widget mini-widget-grey">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Signups</div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 67.1<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-coffee"></i>
                  </div>
                  <div class="pull-right number">1135</div>
                </div>
                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> 
              </div>
            </div>-->
          </div>
          <!-- Row ends -->

          <!-- Row Start -->
          <div class="row wrap">
              
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 paddingNone">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  paddingNone">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <i class="icon-angle-right boxClick" data-action="show"> </i> <?= \Yii::t('app','Додати підкомпанію'); ?>
                    <span class="mini-title displayNone">
                        <?= \Yii::t('app','натисніть на іконку щоб відкрити форму'); ?>
                    </span>
                  </div>
                </div>
                <div class=" widget-body boxShow" <?=(Yii::$app->session->hasFlash('ErrorValidating'))?'':'style="display:none;"'?>>
                    <?php  $form = ActiveForm::begin(['id' => 'SubCompanyNew', 'action' => '/company/'.$urlname.'/companysubcompany','enableClientValidation' => true, 'class' => 'form-control', 'method' => 'POST']); ?>
                    <input type="hidden" name='company_name' value='<?= $companyModel->urlname; ?>'>
                    <?= $form->field($modelNewSubCompany, 'organization_name')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Назва підкомпанії'), 'class' => 'form-control'])->label(false); ?>
                    <?= $form->field($modelNewSubCompany, 'urlname')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Артикл підкомпанії')])->label(false); ?>
                    <?= $form->field($modelNewSubCompany, 'email')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Email')])->label(false); ?>
                    <?= $form->field($modelNewSubCompany, 'phone_number')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Номер телефону')])->label(false); ?>

                    <?= Html::input('submit', 'submit_save',  \Yii::t('app', 'Додати'), ['class' => 'btn-submit btn btn-primary']); ?>
                <?php ActiveForm::end();  ?>
                </div>
              </div>
            </div>
            
        
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  paddingNone">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <?= \Yii::t('app','Підкомпанії'); ?>
                    <span class="mini-title displayNone">
                        <?= \Yii::t('app','вивід всіх підкомпаній компанії'); ?>
                    </span>
                  </div>
                </div>
                <div class=" widget-body">
                    
                <table class="table table-responsive table-striped table-bordered table-hover no-margin">
                
        <tr style="font-weight:bold;color:black;">
            <td><b><?= \Yii::t('app','Назва підкомпанії'); ?></b></td>
            <td class="hidden-xs"><b><?= \Yii::t('app','Номер телефону'); ?></b></td>
            <td class="hidden-xs hidden-sm"><b><?= \Yii::t('app','Email'); ?></b></td>
            <td  class="hidden-xs hidden-sm"><b><?= \Yii::t('app','Користувачі підкомпанії'); ?></b></td>
            <td colspan="2"></td>
        </tr>
        <tr style="font-weight:bold;color:black;">
            <form method="get">
                <td><input type="text" class="form-control" name="organization_name" placeholder="<?= \Yii::t('app','Назва підкомпанії'); ?>" value="<?= $_GET['organization_name']; ?>"></td>
                <td  class="hidden-xs"><input type="text" class="form-control" name="phone_number" placeholder="<?= \Yii::t('app','Номер телефону'); ?>" value="<?= $_GET['phone_number']; ?>"></td>
                <td class="hidden-xs hidden-sm"><input type="text" class="form-control" name="email" placeholder="<?= \Yii::t('app','Email'); ?>" value="<?= $_GET['email']; ?>"></td>
                <td colspan="3"><input type="submit" name="SearchSubCompany" class="form-control" value="<?= \Yii::t('app','Пошук'); ?>"></td>
            </form>
        </tr>
    <?php
        foreach($modelSubCompany as $company){ ?>
            <tr style="color:black;">
                <td><a href="../../../../company/<?= $company->urlname; ?>" ><?= $company->organization_name; ?></a></td>
                <td  class="hidden-xs"><?= $company->phone_number; ?></td>
                <td  class="hidden-sm hidden-xs"><?= $company->email; ?></td>
                <td  class="hidden-sm hidden-xs"> no</td>
                <td>
                    <?php // echo Html::a('Delete', array('admin/companydelete', 'id' => $company->id, 'company_id' => $companyModel->id), array('class' =>'btn btn-default')); ?>
                    <a href='../../admin/companydelete?id=<?= $company->id; ?>&company_id=<?= $companyModel->id; ?>' class="delete" title='delete'><i style="color:#3187bf;" class="fa fa-times fa-1x"></i></a>
                </td>
                                
<!--                <td>
                    <?php if($company->active == '1'){ ?>
                        <button class='btn btn-primary activeCompany' id='deactive' data-id='<?= $company->id; ?>' data-action='deactive'>Deactive</button>
                    <?php }else{ ?>
                        <button class='btn btn-primary activeCompany' id='active' data-id='<?= $company->id; ?>' data-action='active'>Active</button>
                    <?php } ?>
                </td>-->
                <td  align="center">
                    <?php if($company->active == '1'){ ?>
                        <i title="active" class='fa fa-circle activeCompany KursorShow' id='deactive' data-id='<?= $company->id; ?>' data-action='Deactive' style="color:#3187bf;"></i>
                    <?php }else{ ?>
                        <i title="deactive" class='fa fa-circle-o activeCompany KursorShow' id='active' data-id='<?= $company->id; ?>' data-action='Active' style="color:#3187bf;"></i>
                    <?php }?>
                </td>
            </tr>
    <?php } ?>
                </table>
                    <?= LinkPager::widget(['pagination'=>$pagination]); ?>
                </div>
                </div>
            </div>
            </div>
              
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  paddingNone">
                  
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <?= \Yii::t('app','Графік'); ?>
                      <span class="mini-title displayNone">
                      <?= \Yii::t('app','Кількість трекерів в підкомпаніях'); ?>
                      </span>
                    </div>
                  </div>
                  <div class=" widget-body">
                    <div class='col-sm-12 tableData'></div>
                    <div class='preloader' style='display:none;'><?= \Yii::t('app','Будь ласка зачекайте...'); ?></div>
                    <div id="piechart_3d" style="width: 100%; height: 200px;"></div>
                    <button id='btnExport' class='btn btn-primary' style='width: 100%;'><?= \Yii::t('app','Екпортувати в Exel'); ?></button>
                  </div>
                  </div>
                </div>
              
          </div>
          <!-- Row End -->
     
        </div>
        <!-- Dashboard Wrapper End -->

        <footer>
          <p>© Tracker system 2015</p>
        </footer>

      </div>
    </div>
