<?php 
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\widgets\LinkPager;
    use yii\bootstrap\Alert;
?>
<div class='col-sm-12'>
   <?php echo $this->render('menu'); ?>    
</div>
<div class='col-sm-12'>
    <table class="table">
        <tr>
            <td><b>Organization name</b></td>
            <td><b>Phone number</b></td>
            <td><b>User name Company</b></td>
            <td><b>Company email</b></td>
        </tr>
        <tr>
            <td><?= $companyModel->organization_name; ?></td>
            <td><?= $companyModel->phone_number; ?></td>
            <td><?= $companyModel->username_company; ?></td>
            <td><?= $companyModel->email; ?></td>
        </tr>
    </table>
</div>
<h1>users company page</h1>