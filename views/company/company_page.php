<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\widgets\LinkPager;
    use yii\bootstrap\Alert;
    use app\assets\HomepageAsset;
    use app\assets\GraphUserAsset;
    
    HomepageAsset::register($this);
    GraphUserAsset::register($this);
?>

<div class="dashboard-container">
    <?php

            if(Yii::$app->session->hasFlash('EMAIL_sended')): ?>
                <?php
                echo Alert::widget([
                    'options' => [
                        'class' => 'alert-info',
                    ],
                    'body' => \Yii::t('app','Для того щоб активувати аккаунт, увійдіть на вказаний e-mail!'),
                ]);
            endif;

            if(Yii::$app->session->hasFlash('Email_error')): ?>
                <?php
                echo Alert::widget([
                    'options' => [
                        'class' => 'alert-warning',
                    ],
                    'body' =>  \Yii::t('app','Проблеми з вашею електронною адресою, або її не їснує, попробуйте знову, або зверніться до адміністрації сайту!'),
                ]);
            endif;

            if(Yii::$app->session->hasFlash('UserUpdate')):
                echo Alert::widget([
                    'options' => [
                        'class' => 'alert-info',
                    ],
                    'body' => \Yii::t('app','Змінені дані користувача, ЗБЕРЕЖЕНО!'),
                ]);
            endif;

            if(Yii::$app->session->hasFlash('UserAdd')):
                echo Alert::widget([
                    'options' => [
                        'class' => 'alert-info',
                    ],
                    'body' => \Yii::t('app','Користувач доданий!'),
                ]);
            endif;

            if(Yii::$app->session->hasFlash('UserDeleted')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-warning',
                        ],
                        'body' => \Yii::t('app','Користувач видалений!'),
                    ]);
            endif;
            if(Yii::$app->session->hasFlash('ErrorValidating')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-warning',
                        ],
                        'body' => 'Error Validating!',
                    ]);
            endif;

             if(Yii::$app->session->hasFlash('UserNotUpdate')):
                echo Alert::widget([
                    'options' => [
                        'class' => 'alert-warning',
                    ],
                    'body' => 'User Update!',
                ]);

            endif; ?>
      <div class="container">
        <!-- Top Nav Start -->
        <div id="cssmenu">
            <?php echo $this->render('menu',['urlname' => $urlname, 'parentCompany' => $parentCompany]); ?>
        </div>
        <!-- Top Nav End -->

        <!-- Sub Nav End -->
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <a href="../../../site/index" style="margin-left:9px;font-size:15px;padding:0px;"><i class="fa fa-home"></i> Home </a><a style="font-size:12px;padding:0px"> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;"><?php echo \Yii::t('app','Сторінка користувачів'); ?></a>
            </li>
          </ul>
        </div>
        <!-- Sub Nav End -->

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
          
          <!-- Row starts -->
          <div class="row displayNone">
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left"><?php echo \Yii::t('app','Всіх корисувачів'); ?></div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 100<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-globe"></i>
                  </div>
                  <div class="pull-right number"><?= $statistic['userCount']; ?></div>
                </div>
<!--                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> -->
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left"><?php echo \Yii::t('app','Активованих користувачів'); ?></div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i><?= $statistic['activeUserProcent']; ?><sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-twitter"></i>
                  </div>
                  <div class="pull-right number"><?= $statistic['activeUserCount']; ?></div>
                </div>
<!--                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> -->
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left"><?php echo \Yii::t('app','Неактивованих користувачів'); ?></div>
                  <div class="pull-right"><i class="fa fa-angle-down"></i><?= $statistic['deactiveUserProcent']; ?><sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-upload"></i>
                  </div>
                  <div class="pull-right number"><?= $statistic['deactiveUserCount']; ?></div>
                </div>
<!--                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> -->
              </div>
            </div>
<!--            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="mini-widget mini-widget-grey">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Signups</div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 67.1<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-coffee"></i>
                  </div>
                  <div class="pull-right number">1135</div>
                </div>
                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> 
              </div>
            </div>-->
          </div>
          <!-- Row ends -->

          <!-- Row Start -->
          <div class="row wrap">
              
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 paddingNone" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <i class="icon-angle-right boxClick" data-action="show"> </i> <?php echo \Yii::t('app', 'Додати користувача'); ?>
                    <span class="mini-title">
                        <?php echo \Yii::t('app', 'натисніть на іконку щоб відкрити форму'); ?>
                    </span>
                  </div>
                </div>
                <div class=" widget-body boxShow" <?=(Yii::$app->session->hasFlash('ErrorValidating'))?'':'style="display:none;"'?>>
                   <?php  $form = ActiveForm::begin(['id' => 'NewUser', 'action' => '/company/'.$urlname, 'class' => 'form-control', 'method' => 'POST']); ?>
                        <input type="hidden" name='company_name' value='<?= $companyModel->urlname; ?>'>
                        <?= $form->field($newModelUser, 'username')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Логін'), 'class' => 'form-control'])->label(false); ?>
                        <?= $form->field($newModelUser, 'email')->input('email',['placeholder' => \Yii::t('app', 'E-mail')])->label(false); ?>
                        <?= $form->field($newModelUser, 'phone_number')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Номер телефону'), 'class' => 'form-control'])->label(false);; ?>
                        <?= $form->field($newModelUser, 'users_type')->RadioList(['1' => \Yii::t('app', 'Адміністратор'), '2' => \Yii::t('app', 'Користувач')],['inline' =>true]); ?>
                        <?= Html::input('submit', 'submit_save',  \Yii::t('app', 'Додати'), ['class' => 'btn-submit btn btn-primary']); ?>
                    <?php ActiveForm::end(); ?>
                </div>
              </div>
            </div>
            
        
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <?php echo \Yii::t('app','Користувачі'); ?>
                    <span class="mini-title">
                        <?php echo \Yii::t('app','Вивід всіх користувачів компанії'); ?>
                    </span>
                  </div>
                </div>
                <div class=" widget-body">
                    
                <table class="table table-responsive table-striped table-bordered table-hover no-margin">
                    <tr style="font-weight:bold;">
                        <td><b><?php echo \Yii::t('app','Логін'); ?></b></td>
                        <td class="hidden-xs"><b><?php echo \Yii::t('app','E-mail'); ?></b></td>
                        <td class="hidden-xs"><b><?php echo \Yii::t('app','Тип користувача'); ?></b></td>
                        <td colspan='4'></td>
                    </tr>
                    <form method="GET" class="form-control">
                        <tr style="font-weight:bold;">
                            <td><input type="text" class="form-control" name="username" placeholder="username" value="<?= $_GET['username']?>"></td>
                            <td class="hidden-xs"><input type="text" class="form-control" name="email" placeholder="email" value="<?= $_GET['email']?>"></td>
                            <td  class="hidden-xs">
                                <select name="user_type" class="form-control">
                                    <option value="">Choise Type...</option>
                                    <option value="1" <?php if($_GET['user_type'] == 1){ echo 'selected'; } ?>><?php echo \Yii::t('app','Адміністратор'); ?></option>
                                    <option value="2" <?php if($_GET['user_type'] == 2){ echo 'selected'; } ?>><?php echo \Yii::t('app','Користувач'); ?></option>
                                </select>
                            </td>
                            <td colspan='4'><input class="form-control" type="submit" name="SearchUser" value="<?php echo \Yii::t('app','Пошук'); ?>"></td>
                        </tr>
                    </form>
                    <?php
                    $sub_company[0] = $companyModel->organization_name;;
                    ksort($sub_company);

                    $type = ['1' => \Yii::t('app','Адміністатор'),'2' => \Yii::t('app','Користувач')];
                     foreach($modelUser as $user){ ?>
                        <tr>
                            <td><?= $user->username; ?></td>
                            <td class="hidden-xs"><?= $user->email; ?></td>
                            <td class="hidden-xs"><?= $type[$user->users_type]; ?></td>
                            <?php if($companyModel->parent_id != 0){ ?><td><?= $sub_company[$user->company_id]; ?></td><?php } ?>
                            <td  align="center">
                                <a href='company/userdelete?id=<?= $user->id; ?>&company_id=<?= $companyModel->id; ?>' class="delete" title='delete'><i style="color:#3187bf;" class="fa fa-times fa-1x"></i></a>
                                <?php //  echo Html::a('Delete', array('company/userdelete', 'id' => $user->id, 'company_id' => $companyModel->id), array('class' =>'btn btn-default')); ?>
                            </td>
                            
                            <td align="center">
                                <?php if($companyModel->parent_id == 0){ ?>
                                    <a data-toggle="modal" class="KursorShow" data-target="#myModal<?= $user->id; ?>"><i style="color:#3187bf;" class='fa fa-pencil-square-o fa-1x'></i></a>
                                    <!--<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal<?php //= $user->id; ?>">Update</button>--> 
                                <?php } ?>
                            </td>
                            
                            <td  align="center">
                                <?php if($user->active == '1'){ ?>
                                    <i title="active" class='fa fa-circle activeUser KursorShow' id='deactive' data-id='<?= $user->id; ?>' data-action='Deactive' style="color:#3187bf;"></i>
                                    <!--<button class='btn btn-primary activeUser' id='deactive' data-id='<?= $user->id; ?>' data-action='deactive'>Deactive</button>-->
                                <?php }else{ ?>
                                    <i title="deactive" class='fa fa-circle-o activeUser KursorShow' id='active' data-id='<?= $user->id; ?>' data-action='Active' style="color:#3187bf;"></i>
                                    <!--<button class='btn btn-primary activeUser' id='active' data-id='<?= $user->id; ?>' data-action='active'>Active</button>-->
                                <?php }?>
                            </td>
                        </tr>

                        <?php if($companyModel->parent_id == 0){ ?>
                        <div class="modal fade" id="myModal<?= $user->id; ?>" role="dialog">
                            <div class="modal-dialog">

                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title" style='padding:0px;' align='center'><?php echo \Yii::t('app','Редагування аккаунта користувача'); ?></h4>
                                </div>
                                <div class="modal-body">
                                <?php // var_dump($user); ?>

                                <?php $form = ActiveForm::begin(['id' => 'UpdateUser', 'action' => '/company/companyupdateuser', 'class' => 'form-control', 'method' => 'POST']);
                                    ?>
                                    <input type="hidden" name='company_id' value='<?= $companyModel->id; ?>'>
                                    <input type="hidden" name='user_id' value='<?= $user->id; ?>'>
                                    <?= $form->field($user, 'username')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Логін'), 'class' => 'form-control']); ?>
                                    <?= $form->field($user, 'phone_number')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Номер телефону'), 'class' => 'form-control']); ?>
                                    <?= $form->field($user, 'email')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'E-mail'), 'class' => 'form-control']); ?>
                                    <?= $form->field($user, 'city')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Місто'), 'class' => 'form-control']); ?>
                                    <?= $form->field($user, 'address')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Адреса'), 'class' => 'form-control']); ?>
                                    <?= $form->field($user, 'country')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Країна'), 'class' => 'form-control']); ?>
                                    <?= $form->field($user, 'zip')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Zip/Postal Code'), 'class' => 'form-control']); ?>
                                    <?= $form->field($user, 'first_name')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Ім`я'), 'class' => 'form-control']); ?>
                                    <?= $form->field($user, 'last_name')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Прізвище'), 'class' => 'form-control']); ?>
                                    <?= $form->field($user, 'company_id')->dropDownList($sub_company)->label(\Yii::t('app','Під компанії'));  ?>
                                    <?= $form->field($user, 'users_type')->dropDownList($type)->label(\Yii::t('app','Тип користувача в компанії'));  ?>

                                </div>
                                <div class="modal-footer">
                                    <?php echo Html::submitButton($user->isNewRecord ? \Yii::t('app', 'Save') : \Yii::t('app', 'Update'), array('class' => 'btn btn-primary')); ?>
                                    <?php ActiveForm::end(); ?>
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo \Yii::t('app','Закрити'); ?></button>
                                </div>
                              </div>

                            </div>
                          </div>
                        <?php
                        }
                     }

                    ?>
                </table>
                    <?= LinkPager::widget(['pagination'=>$pagination]); ?>
                </div>
                </div>
            </div>
            </div>
              
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  paddingNone">
                  
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <?php echo \Yii::t('app','Графік/Звіт'); ?>
                      <span class="mini-title">
                          <?php echo \Yii::t('app','Кількість трекерів в користувача'); ?>
                      </span>
                    </div>
                  </div>
                  <div class=" widget-body">
                    <div class='col-sm-12 tableData'></div>
                    <div class='preloader' style='display:none;' align='center'><i class="fa fa-cog fa-spin fa-5x"></i></div>
                    <div id="piechart_3d" style="width: 100%; height: 200px;"></div>
                    <button id='btnExport' class='btn btn-primary' style='width: 100%;'><?php echo \Yii::t('app','Зберегти в EXEL'); ?></button>
                  </div>
                  </div>
                </div>
              </div>
              
          </div>
          <!-- Row End -->
          

          

        </div>
        <!-- Dashboard Wrapper End -->

        <footer>
          <p>© Tracker system 2015</p>
        </footer>

      </div>
    </div>