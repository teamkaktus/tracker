<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>
<div class="nav-collapse sidebar-nav">
    <ul >
        <li><a href="/admin/index"><i class="icon-user"></i><span class="hidden-tablet"> Users</span></a></li>
        <li><a href="/admin/addtrackerpage"><i class="icon-envelope"></i><span class="hidden-tablet"> Tracker</span></a></li>
        <li><a href="/admin/companypage"><i class="icon-tasks"></i><span class="hidden-tablet"> Company</span></a></li>
        <li><a href="/admin/reports"><i class="icon-tasks"></i><span class="hidden-tablet"> Reports</span></a></li>
        <li><a href="/admin/driverspage"><i class="icon-tasks"></i><span class="hidden-tablet"> Drivers</span></a></li>
    </ul>
</div>