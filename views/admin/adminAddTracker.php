<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\widgets\LinkPager;
    use yii\bootstrap\Alert;
    use app\assets\AdminAsset;
    use dosamigos\multiselect\MultiSelect;
    
    AdminAsset::register($this);
?>
<!-- Modal -->
<div class="modal fade" id="ChooseIcon" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo \Yii::t('app','Виберіть іконку'); ?></h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo \Yii::t('app','Закрити'); ?></button>
      </div>
    </div>

  </div>
</div>
<!-- Modal end -->

    <?php if(Yii::$app->session->hasFlash('TrackerAdd')): ?>
            <?php
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-info',
                ],
                'body' => \Yii::t('app','Трекер додано!'),
            ]);
            ?>
        <?php endif; ?>
        
    <?php if(Yii::$app->session->hasFlash('TrackerUpdate')): ?>
            <?php
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-info',
                ],
                'body' => 'Трекер редаговано!',
            ]);
            ?>
        <?php endif; ?>
        <?php if(Yii::$app->session->hasFlash('TrackerNotUdpate')):
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-warning',
                ],
                'body' => 'Error Update',
            ]);
        endif; ?>
        
    <?php if(Yii::$app->session->hasFlash('TrackerImei')): ?>
            <?php
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-warning',
                ],
                'body' => \Yii::t('app','Трекер з таким IMEI вже доданий'),
            ]);
            ?>
        <?php endif; ?>
        <?php if(Yii::$app->session->hasFlash('ErrorValidating')):

            echo Alert::widget([
                'options' => [
                    'class' => 'alert-warning',
                ],
                'body' => 'Error Validating',
            ]);
        endif; ?>
    <?php if(Yii::$app->session->hasFlash('TrackerDeleted')): ?>
            <?php
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-warning',
                ],
                'body' => \Yii::t('app','Трекер видалено'),
            ]);
            ?>
        <?php endif; ?>

 <!-- Main Container start -->
    <div class="dashboard-container">

      <div class="container">
        <!-- Top Nav Start -->
        <div id="cssmenu">
          <ul>
            <li>
                <a href="../../admin/index"><i class="fa fa-users"></i><?php echo \Yii::t('app','Користувачі'); ?></a>
            </li>
            <li>
              <a href="../../admin/companypage">
                <i class="fa fa-briefcase"></i>
                <?php echo \Yii::t('app','Компанії'); ?>
              </a>
            </li>
            
            <li class="active">
              <a href="../../admin/trackerpage">
                <i class="fa fa-map-marker"></i>
                <?php echo \Yii::t('app','Трекери'); ?>
              </a>
            </li>
            
            <li class="hidden-sm">
              <a href="../../admin/driverspage">
                <i class="fa fa-automobile"></i>
                <?php echo \Yii::t('app','Водії'); ?>
              </a>
            </li>
            
            <li class="hidden-sm">
              <a href="../../admin/paymentpage">
                <i class="fa fa-credit-card"></i>
                <?php echo \Yii::t('app','Оплата'); ?>
              </a>
            </li>
            
            <li class=""><span class="submenu-button"></span>
              <a href="../admin/reports"><i class="fa fa-bar-chart-o"></i><?php echo \Yii::t('app','Графіки/Звіти'); ?></a>
<!--              <ul>
                 <li><a href="http://iamsrinu.com/bluemoon-admin-theme8/flot.html">One Graphs</a></li>
                 <li><a href="http://iamsrinu.com/bluemoon-admin-theme8/graphs.html">Google Graph</a></li>
                 <li><a href="http://iamsrinu.com/bluemoon-admin-theme8/vector-maps.html">Vector Maps</a></li>
              </ul>-->
            </li>
            <li>
              <a href="../../lang"><i class="fa fa-language"></i><?php echo \Yii::t('app','Мови системи'); ?></a>
             
            </li>
          </ul>
        </div>
        <!-- Top Nav End -->

        <!-- Sub Nav End -->
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <a href="../../../site/index" style="margin-left:9px;font-size:15px;padding:0px;"><i class="fa fa-home"></i> Home </a><a style="font-size:10px;padding:0px"> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;"><?php echo \Yii::t('app','Сторінка трекерів'); ?></a>
            </li>
          </ul>
        </div>
        <!-- Sub Nav End -->

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
          
          <!-- Row starts -->
          <div class="row displayNone">
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left"><?php echo \Yii::t('app','Всіх трекерів'); ?></div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 100<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-globe"></i>
                  </div>
                  <div class="pull-right number"><?= $statistic['trackerCount']; ?></div>
                </div>
<!--                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> -->
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left"><?php echo \Yii::t('app','Активованих трекерів'); ?></div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i><?= $statistic['activeTrackerProcent']; ?><sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-twitter"></i>
                  </div>
                  <div class="pull-right number"><?= $statistic['activeTrackerCount']; ?></div>
                </div>
<!--                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> -->
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left"><?php echo \Yii::t('app','Неактивованих трекерів'); ?></div>
                  <div class="pull-right"><i class="fa fa-angle-down"></i><?= $statistic['deactiveTrackerProcent']; ?><sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-upload"></i>
                  </div>
                  <div class="pull-right number"><?= $statistic['deactiveTrackerCount']; ?></div>
                </div>
<!--                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> -->
              </div>
            </div>
<!--            <div class="col-lg-4 col-md-3 col-sm-6">
              <div class="mini-widget mini-widget-grey">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Signups</div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 67.1<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-coffee"></i>
                  </div>
                  <div class="pull-right number">1135</div>
                </div>
                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> 
              </div>
            </div>-->
          </div>
          <!-- Row ends -->

          <!-- Row Start -->
          <div class="row wrap">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <i class="icon-angle-right boxClick" data-action="show"> </i> <?php echo \Yii::t('app','Додати трекер'); ?>
                    <span class="mini-title displayNone">
                        <?php echo \Yii::t('app','натисніть на іконку щоб відкрити форму'); ?>
                    </span>
                  </div>
                </div>
                <div class=" widget-body boxShow" <?=(Yii::$app->session->hasFlash('ErrorValidating'))?'':'style="display:none;"'?>>
                    <?php   $form = ActiveForm::begin(['id' => 'TrackerNew', 'action' => '../admin/trackerpage','enableClientValidation' => true, 'class' => 'form-control', 'method' => 'POST']); ?>
                    <?= $form->field($modelNewTracker, 'IMEI')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'IMEI'), 'class' => 'form-control'])->label(false); ?>
                    <?= $form->field($modelNewTracker, 'name')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Назва')])->label(false); ?>
                    <?= $form->field($modelNewTracker, 'discription')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Опис')])->label(false); ?>
                    <?= $form->field($modelNewTracker, 'speed_limit')->input('number')->label(false); ?>
                    <?= $form->field($modelNewTracker, 'user_type')->RadioList(['1' => \Yii::t('app', 'Користувач'), '2' => \Yii::t('app', 'Компанія')],['inline' =>true]); ?>
                    <?= $form->field($modelNewTracker, 'user_id')->dropDownList($arrayUser,['style'=>'display:none;'])->label(false); ?>
                    <?= $form->field($modelNewTracker, 'company_id')->dropDownList($arrayCompany,['style'=>'display:none;'])->label(false); ?>
                    <?= $form->field($modelNewTracker, 'tracker_marker')->hiddenInput(['class' => 'tracker_marker','value'=>'car'])->label(false); ?>
                    <div class="ChoosenIcon col-sm-12">
                      <label class="control-label" for="marker"><?php echo \Yii::t('app','Виберіть іконку'); ?>:</label>
                      <img id="marker" class="showAllIcons" src="/web/content/icons/car.png">
                    </div>
                    <?= Html::input('submit', 'submit_save',  \Yii::t('app', 'Додати'), ['class' => 'btn-submit btn btn-primary']); ?>
                <?php ActiveForm::end(); ?>
                </div>
              </div>
            </div>
            
        
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
                <div class="widget">
                    <div class="widget-header">
                      <div class="title">
                        <?php echo \Yii::t('app','Трекери'); ?>
                        <span class="mini-title displayNone">
                            <?php echo \Yii::t('app','вивід всіх трекерів'); ?>
                        </span>
                      </div>
                    </div>
                    <div class=" widget-body">
                        <div class="row">
                            
                            <div class='col-sm-12'>
                            <table class='table'>
                                <tr>
                                    <td></td>
                                    <td><b><?php echo \Yii::t('app','IMEI трекера'); ?></b></td>
                                    <td class="hidden-xs"><b><?php echo \Yii::t('app','Назва трекера'); ?></b></td>
                                    <td class="hidden-xs hidden-sm"><b><?php echo \Yii::t('app','Час додавання'); ?></b></td>
                                    <td class="hidden-xs hidden-sm"><b><?php echo \Yii::t('app','Опис'); ?></b></td>
                                    <td class="hidden-xs hidden-sm"><b><?php echo \Yii::t('app','Ліміт швидкості'); ?></b></td>
                                    <td colspan="3"></td>
                                </tr>
                                <form method='get' action='../admin/addtrackerpage'>
                                    <tr>
                                        <td colspan="100%">
                                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 paddingNone">
                                                <input type='text'  name='searchInput' class='form-control'>
                                            </div>
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-2  paddingNone">
                                                <input type='submit' id='search' class='btn btn-primary' value='<?php echo \Yii::t('app','Пошук'); ?>'>
                                            </div>
                                        </td>
                                    </tr>
                                </form>
                                <?php
                                foreach($modelTracker as $tracker){ ?>
                                    <tr>
                                        <td><input type="checkbox" class="trackerOptions" data-id="<?= $tracker->IMEI; ?>" name="optionT"></td>
                                        <td><?= $tracker->IMEI; ?></td>
                                        <td  class="hidden-xs"><?= $tracker->name; ?></td>
                                        <td class="hidden-xs hidden-sm"><?= $tracker->time_added; ?></td>
                                        <td class="hidden-xs hidden-sm"><?= $tracker->discription; ?></td>
                                        <td class="hidden-xs hidden-sm"><?= $tracker->speed_limit; ?></td>
                                        <td>
                                            <a href='../../admin/trackerdelete?imei=<?= $user->id; ?>' class="delete" title='delete'><i class="fa fa-times fa-2x" style="color:#3693cf;"></i></a>
                                        </td>
                                        <td>
                                            <a data-toggle="modal" class="KursorShow" data-target="#myModal<?= $tracker->id; ?>"><i class='fa fa-pencil-square-o fa-2x' style="color:#3693cf;"></i></a>
                                        </td>
                                        <td>
                                            <?php if($tracker->active == '1'){ ?>
                                                <i title="active" style="color:#3693cf;" class='fa fa-circle activeTracker KursorShow' id='deactive' data-id='<?= $tracker->id; ?>' data-action='Deactive'></i>
                                            <?php }else{ ?>
                                                <i title="deactive" style="color:#3693cf;" class='fa fa-circle-o activeTracker KursorShow' id='active' data-id='<?= $tracker->id; ?>' data-action='Active'></i>
                                            <?php }?>
                                        </td>
                                    </tr>

                                    <?php 
                                        $array = [];
                                        foreach($tracker_user as $t_r){
                                            if($t_r['tracker_id'] == $tracker->IMEI){
                                                $array[] = $t_r['user_id'];
                                            }
                                        }
                                        $array = array_values($array);
                                    ?>

                                    <div class="modal fade" id="myModal<?= $tracker->id; ?>" role="dialog">
                                <div class="modal-dialog">

                                  <!-- Modal content-->
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title" style='color:black;'><?php echo \Yii::t('app','Редагування трекера'); ?></h4>
                                    </div>
                                    <div class="modal-body" style='color:black;'>
                                    <?php $form = ActiveForm::begin(['id' => 'UpdateTracker', 'action' => '../admin/updatetracker', 'class' => 'form-control', 'method' => 'POST']);?>
                                        <input type="hidden" name='tracker_id' value='<?= $tracker->id; ?>'>
                                        <?php echo $tracker->IMEI; ?><br>
                                        <?= $form->field($tracker, 'name')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Назва'), 'class' => 'form-control'])->label(false); ?>
                                        <?= $form->field($tracker, 'discription')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Опис'), 'class' => 'form-control'])->label(false); ?>
                                        <?= $form->field($tracker, 'speed_limit')->input('number')->label(false); ?>
                                        <?php
                                            if($tracker->company_id == 0){
                                                $selec = '1';
                                                $classUser = 'classInputShow';
                                                $classCompany = 'classInputHide';
                                            }else{
                                                $selec = '2';
                                                $classUser = 'classInputHide';
                                                $classCompany = 'classInputShow';
                                            }
                                        ?>
                                        <?= HTML::RadioList('TrackerTypeUpdate', $selec, ['1' => \Yii::t('app','Користувач'), '2' => \Yii::t('app','Під компанія')],['inline' =>true]); ?>
                                        <div class="inputUser <?= $classUser; ?>" id="inputUser">
                                            <?php
                                            echo MultiSelect::widget([
                                                'id'=>"multi".$tracker->id,
                                                'options' => ['multiple'=>"multiple"], // for the actual multiselect
                                                'data' => $arrayUser, // data as array
                                                'value' => $array, // if preselected
                                                'name' => 'tracker_user', // name for the form
                                                "clientOptions" =>
                                                    [
                                                        'numberDisplayed' => 2,
                                                        'enableFiltering' => true,
                                                    ],
                                            ]);
                                        ?>
                                        </div>
                                        <div class="inputCompany <?= $classCompany; ?>" id="inputCompany">
                                            <?= $form->field($tracker, 'company_id')->dropDownList($arrayCompany, ['class' => 'form-control'])->label(false);?>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                            <?php echo Html::submitButton(\Yii::t('app', 'Зберегти'), array('class' => 'btn btn-primary')); ?>
                                            <?php ActiveForm::end(); ?>

                                      <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo \Yii::t('app','Закрити'); ?></button>
                                    </div>
                                  </div>

                                </div>
                              </div>



                            <?php } ?>
                                    <tr>
                                        <td> <span class="fa fa-reply fa-rotate-180"></span></td>
                                        <td><input type="checkbox" class="trackerOptionsAll" name="optionTAll"> <?= \Yii::t('app', 'Відмітити всі'); ?></td>
                                        <td><b><i><?= \Yii::t('app', 'З відмічиними: '); ?></i></b> &nbsp;&nbsp;<span class="fa fa-line-chart"></span><a style="cursor:pointer;" class="checkedOptionClick"><?= \Yii::t('app', 'Настройки звітів'); ?></a></td>
                                    </tr>
                            </table>
                                 <div class="col-sm-12">
                                     <?= LinkPager::widget(['pagination'=>$pagination]); ?>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
              
              
              
          </div>
          <!-- Row End -->
          

          

        </div>
        <!-- Dashboard Wrapper End -->

        <footer>
          <p>© Tracker system 2015</p>
        </footer>

      </div>
    </div>

<div class="modal fade" id="modalChangeOption" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h3 class="modal-title" style='text-align: center; color:black;'><?php echo \Yii::t('app','Звіти трекера'); ?></h3>
            </div>
            <div class="modal-body" style='color:black;'>
                <?php foreach($reportModel as $report){ ?>
                    <input type="checkbox" class="report<?= $report->id; ?>" data-jsondata="no" data-id="<?= $report->id; ?>" name="reportSelect"><label><?= $report->name; ?></label></br>
                <?php } ?>
            </div>
            <div class="modal-footer">
                    <?php echo Html::submitButton(\Yii::t('app', 'Зберегти'), array('class' => 'saveReportSelect btn btn-primary')); ?>
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo \Yii::t('app','Закрити'); ?></button>
            </div>
        </div>
    </div>
</div>