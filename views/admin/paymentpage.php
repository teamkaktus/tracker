<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\widgets\LinkPager;
    use yii\bootstrap\Alert;
    use dosamigos\multiselect\MultiSelect;
    use app\assets\AdminAsset;
    use app\assets\GraphAsset;
    use app\assets\HomepageAsset;
    
    AdminAsset::register($this);
    GraphAsset::register($this);
   // HomepageAsset::register($this);
?>

 <!-- Main Container start -->
    <div class="dashboard-container">

      <div class="container">
        <!-- Top Nav Start -->
        <div id="cssmenu">
          <ul>
            <li>
                <a href="../../admin/index"><i class="fa fa-users"></i><?php echo \Yii::t('app','Користувачі'); ?></a>
            </li>
            <li>
              <a href="../../admin/companypage">
                <i class="fa fa-briefcase"></i>
                <?php echo \Yii::t('app','Компанії'); ?>
              </a>
            </li>
            
            <li>
              <a href="../../admin/trackerpage">
                <i class="fa fa-map-marker"></i>
                <?php echo \Yii::t('app','Трекери'); ?>
              </a>
            </li>
            
            <li class="hidden-sm">
              <a href="../../admin/driverspage">
                <i class="fa fa-automobile"></i>
                <?php echo \Yii::t('app','Водії'); ?>
              </a>
            </li>
            
            <li class="active hidden-sm">
              <a href="../../admin/paymentpage">
                <i class="fa fa-credit-card"></i>
                <?php echo \Yii::t('app','Оплата'); ?>
              </a>
            </li>
            
            <li class=""><span class="submenu-button"></span>
              <a href="../admin/reports"><i class="fa fa-bar-chart-o"></i><?php echo \Yii::t('app','Графіки/Звіти'); ?></a>
<!--              <ul>
                 <li><a href="../admin/reports">One Graphs</a></li>
                 <li><a href="../admin/reports">Google Graph</a></li>
                 <li><a href="../admin/reports">Vector Maps</a></li>
              </ul>-->
            </li>
            <li>
              <a href="../../lang"><i class="fa fa-language"></i><?php echo \Yii::t('app','Мови системи'); ?></a>
             
            </li>
          </ul>
        </div>
        <!-- Top Nav End -->

        <!-- Sub Nav End -->
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <a href="../../../site/index" style="margin-left:9px;font-size:15px;padding:0px;"><i class="fa fa-home"></i> Home </a><a style="font-size:10px;padding:0px"> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;"><?php echo \Yii::t('app','Сторінка оплати'); ?></a>
            </li>
          </ul>
        </div>
        <!-- Sub Nav End -->

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
          
          <!-- Row starts -->
<!--          <div class="row displayNone">
            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Visitors</div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 12.2<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-globe"></i>
                  </div>
                  <div class="pull-right number">8757</div>
                </div>
                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> 
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Shares</div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 18.3<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-twitter"></i>
                  </div>
                  <div class="pull-right number">3780</div>
                </div>
                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> 
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Downloads</div>
                  <div class="pull-right"><i class="fa fa-angle-down"></i> 21.9<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-upload"></i>
                  </div>
                  <div class="pull-right number">12658</div>
                </div>
                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> 
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="mini-widget mini-widget-grey">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Signups</div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 67.1<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-coffee"></i>
                  </div>
                  <div class="pull-right number">1135</div>
                </div>
                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> 
              </div>
            </div>
          </div>-->
          <div class="well" style="color:black;">
            <div class="row">
                <form method="GET" action="../../admin/paymentpage">
                    <div class="col-sm-5">
                      <div id="datetimepicker1" class="input-group date">
                          <input data-format="yyyy-MM-dd hh:mm:ss" type="text" name="time_vid" value="<?= $_GET['time_vid']; ?>" class='form-control time_vid'></input>
                         <span class="add-on input-group-addon">
                          <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                          </i>
                         </span>
                      </div>
                  </div>
                    <div class="col-sm-5">
                      <div id="datetimepicker2" class="input-group date">
                          <input data-format="yyyy-MM-dd hh:mm:ss" type="text" name="time_do" value="<?= $_GET['time_do']; ?>" class='form-control time_do'></input>
                          <span class="add-on input-group-addon">
                           <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                           </i>
                          </span>
                       </div>
                   </div>
                    <div class="col-sm-2">
                      <input type="submit" class="form-control" name="sortPayment" value="<?php echo \Yii::t('app','Застосувати'); ?>"></input>
                   </div>
                </form>
            </div>
          </div>
        <ul id="tabs" class="nav nav-tabs nav-justified" data-tabs="tabs">
            <li class="active"><a href="#user" data-toggle="tab"><?php echo \Yii::t('app','Користувачі'); ?></a></li>
            <li><a href="#company" data-toggle="tab"><?php echo \Yii::t('app','Компанії'); ?></a></li>
        </ul>
        <div id="my-tab-content" class="tab-content">
            <div class="tab-pane active" id="user">
                <div class="widget-body boxShow">
                    <table class='table' style="color:black;">
                        <tr>
                            <td><b><?php echo \Yii::t('app','Логін'); ?></b></td>
                            <td class="hidden-sm hidden-xs"><b><?php echo \Yii::t('app','Дата реєстрування'); ?></b></td>
                            <td class="hidden-sm hidden-xs"><b><?php echo \Yii::t('app','Номер телефону'); ?></b></td>
                            <td class="hidden-sm hidden-xs"><b><?php echo \Yii::t('app','Дата активації'); ?></b></td>
                            <td><b><?php echo \Yii::t('app','Дата деактивації'); ?></b></td>
                            <td class="hidden-xs"><b><?php echo \Yii::t('app','Сума оплати $'); ?></b></td>
                        </tr>
                        <?php foreach($userModel as $user){ ?>
                        <tr>
                            <td><?= $user['username']; ?></td>
                            <td class="hidden-sm hidden-xs"><?= $user['time_added']; ?></td>
                            <td class="hidden-sm hidden-xs"><?= $user['phone_number']; ?></td>
                            <td class="hidden-sm hidden-xs"><?= $user['time_active']; ?></td>
                            <td><?= $user['time_deactive']; ?></td>
                            <td class="hidden-xs"><?= $user['count_money']; ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                          <?= LinkPager::widget(['pagination' => $paginationUser]); ?>
            </div>
            </div>
            <div class="tab-pane" id="company">
                <table class='table' style="color:black;">
                    <tr>
                        <td><b><?php \Yii::t('app','Назва організації'); ?></b></td>
                        <td class="hidden-sm hidden-xs"><b><?php echo \Yii::t('app','Дата реєстрування'); ?></b></td>
                        <td class="hidden-sm hidden-xs"><b><?php echo \Yii::t('app','Номер телефону'); ?></b></td>
                        <td class="hidden-sm hidden-xs"><b><?php echo \Yii::t('app','Дата активації'); ?></b></td>
                        <td><b><?php \Yii::t('app','Дата деактивації'); ?></b></td>
                        <td class="hidden-xs"><b><?php echo \Yii::t('app','Сума оплати $'); ?></b></td>
                    </tr>
                    <?php foreach($companyModel as $company){ ?>
                    <tr>
                        <td><?= $company['organization_name']; ?></td>
                        <td class="hidden-sm hidden-xs"><?= $company['time_added']; ?></td>
                        <td class="hidden-sm hidden-xs"><?= $company['phone_number']; ?></td>
                        <td class="hidden-sm hidden-xs"><?= $company['time_active']; ?></td>
                        <td><?= $company['time_deactive']; ?></td>
                        <td class="hidden-xs"><?= $company['count_money']; ?></td>
                    </tr>
                    <?php } ?>
                      <?= LinkPager::widget(['pagination' => $paginationUser]); ?>
                </table>
            </div>
        </div>
        
          
        </div>
        <!-- Dashboard Wrapper End -->

        <footer>
          <p>© Tracker system 2015</p>
        </footer>

      </div>
    </div>
