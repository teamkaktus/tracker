<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\widgets\LinkPager;
    use yii\bootstrap\Alert;
    use app\assets\AdminAsset;
    
    AdminAsset::register($this);
?>

<?php
    if(Yii::$app->session->hasFlash('CompanyAdd')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => \Yii::t('app','Компанія додана!'),
        ]);
    endif;
    if(Yii::$app->session->hasFlash('ErrorValidating')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => 'Error Validating',
        ]);
    endif;

    if(Yii::$app->session->hasFlash('CompanyDeleted')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => \Yii::t('app','Компанія видалена!'),
        ]);
    endif;
?>

 <!-- Main Container start -->
    <div class="dashboard-container">

      <div class="container">
        <!-- Top Nav Start -->
        <div id="cssmenu">
          <ul>
            <li>
                <a href="../../admin/index"><i class="fa fa-users"></i><?php echo \Yii::t('app','Користувачі'); ?></a>
            </li>
            <li class="active">
              <a href="../../admin/companypage">
                <i class="fa fa-briefcase"></i>
                <?php echo \Yii::t('app','Компанії'); ?>
              </a>
            </li>
            
            <li>
              <a href="../../admin/trackerpage">
                <i class="fa fa-map-marker"></i>
                <?php echo \Yii::t('app','Трекери'); ?>
              </a>
            </li>
            
            <li class="hidden-sm">
              <a href="../../admin/driverspage">
                <i class="fa fa-automobile"></i>
                <?php echo \Yii::t('app','Водії'); ?>
              </a>
            </li>
            
            <li class="hidden-sm">
              <a href="../../admin/paymentpage">
                <i class="fa fa-credit-card"></i>
                <?php echo \Yii::t('app','Оплата'); ?>
              </a>
            </li>
            
            <li class=""><span class="submenu-button"></span>
              <a href="../admin/reports"><i class="fa fa-bar-chart-o"></i><?php echo \Yii::t('app','Графіки/Звіти'); ?></a>
<!--              <ul>
                 <li><a href="http://iamsrinu.com/bluemoon-admin-theme8/flot.html">One Graphs</a></li>
                 <li><a href="http://iamsrinu.com/bluemoon-admin-theme8/graphs.html">Google Graph</a></li>
                 <li><a href="http://iamsrinu.com/bluemoon-admin-theme8/vector-maps.html">Vector Maps</a></li>
              </ul>-->
            </li>
            <li>
              <a href="../../lang"><i class="fa fa-language"></i><?php echo \Yii::t('app','Мови системи'); ?></a>
             
            </li>
          </ul>
        </div>
        <!-- Top Nav End -->

        <!-- Sub Nav End -->
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <a href="../../../../site/index" style="margin-left:9px;font-size:15px;padding:0px;"><i class="fa fa-home"></i> Home </a><a style="font-size:10px;padding:0px"> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;"><?php echo \Yii::t('app','Сторінка компаній'); ?></a>
            </li>
          </ul>
        </div>
        <!-- Sub Nav End -->

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
          
          <!-- Row starts -->
          <div class="row displayNone">
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left"><?php echo \Yii::t('app','Всіх компаній'); ?></div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 100<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-globe"></i>
                  </div>
                  <div class="pull-right number"><?= $statistic['companyCount']; ?></div>
                </div>
<!--                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> -->
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left"><?php echo \Yii::t('app','Активованих компаній'); ?></div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i><?= $statistic['activeCompanyProcent']; ?><sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-twitter"></i>
                  </div>
                  <div class="pull-right number"><?= $statistic['activeCompanyCount']; ?></div>
                </div>
<!--                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> -->
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left"><?php echo \Yii::t('app','Неактивованих копанії'); ?></div>
                  <div class="pull-right"><i class="fa fa-angle-down"></i><?= $statistic['deactiveCompanyProcent']; ?><sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-upload"></i>
                  </div>
                  <div class="pull-right number"><?= $statistic['deactiveCompanyCount']; ?></div>
                </div>
<!--                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> -->
              </div>
            </div>
<!--            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="mini-widget mini-widget-grey">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Signups</div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 67.1<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-coffee"></i>
                  </div>
                  <div class="pull-right number">1135</div>
                </div>
                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> 
              </div>
            </div>-->
          </div>
          <!-- Row ends -->

          <!-- Row Start -->
          <div class="row wrap" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  paddingNone">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <i class="icon-angle-right boxClick" data-action="show"> </i> <?php echo \Yii::t('app','Додати компанію'); ?>
                    <span class="mini-title displayNone">
                        <?php echo \Yii::t('app','натисніть на іконку щоб відкрити форму'); ?>
                    </span>
                  </div>
                </div>
                <div class=" widget-body boxShow" <?=(Yii::$app->session->hasFlash('ErrorValidating'))?'':'style="display:none;"'?>>
                    <?php  $form = ActiveForm::begin(['id' => 'CompanyNew', 'action' => '../admin/companypage', 'class' => 'form-control', 'method' => 'POST']); ?>
                        <?= $form->field($modelNewCompany, 'organization_name')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Назва організації'), 'class' => 'form-control']); ?>
                        <?= $form->field($modelNewCompany, 'phone_number')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Номер телефону')]); ?>
                        <?= $form->field($modelNewCompany, 'urlname')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Артикул компанії')]); ?>
                        <?= $form->field($modelNewCompany, 'email')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'E-mail')]); ?>
                        <?= Html::input('submit', 'submit_save',  \Yii::t('app', 'Додати'), ['class' => 'btn-submit btn btn-primary']); ?>
                    <?php ActiveForm::end(); ?>
                </div>
              </div>
            </div>
            
        
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  paddingNone">
                <div class="widget">
                    <div class="widget-header">
                      <div class="title">
                        <?php echo \Yii::t('app','Компанії'); ?>
                        <span class="mini-title displayNone">
                            <?php echo \Yii::t('app','Вивід всіх компаній'); ?>
                        </span>
                      </div>
                    </div>
                    <div class=" widget-body">
                        <div class="row">
                            <div class='col-sm-12 col-xs-12'>
                            <table class='table'>
                                <tr>
                                    <td><b><?php echo \Yii::t('app', 'Назва організації')?></b></td>
                                    <td class="hidden-xs"><b><?php echo \Yii::t('app', 'E-mail'); ?></b></td>
                                    <td class="hidden-xs hidden-sm"><b><?php echo \Yii::t('app', 'Номер телефону'); ?></b></td>
                                    <td class="hidden-xs hidden-sm"><b><?php echo \Yii::t('app', 'Місто'); ?></b></td>
                                    <td class="hidden-xs hidden-sm"><b><?php echo \Yii::t('app', 'Останнє редагування'); ?></b></td>
                                    <td class="hidden-xs"><b><?php echo \Yii::t('app', 'Дата деактивації'); ?></b></td>
                                    <td colspan="2"></td>
                                </tr>
                                <form method='get' action='../admin/companypage'>
                                    <tr>
                                        <td colspan="100%">
                                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9" style="padding:0px;">
                                                <input type='text'  name='searchInput' value="<?= $_GET['searchInput']; ?>" class='form-control'>
                                            </div>
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-2" style="padding:0px;">
                                                <input type='submit' id='search' class='btn btn-primary' value='<?php echo \Yii::t('app', 'Пошук'); ?>'>
                                            </div>
                                        </td>
                                    </tr>
                                </form>
                                <?php
                                foreach($modelCompany as $company){ ?>
                                    <tr>
                                        <td><a href="../company/<?= $company->urlname; ?>"><?= $company->organization_name; ?></a></td>
                                        <td class="hidden-xs"><?= $company->email; ?></td>
                                        <td class="hidden-xs hidden-sm"><?= $company->phone_number; ?></td>
                                        <td class="hidden-xs hidden-sm"><?= $company->city; ?></td>
                                        <td class="hidden-xs hidden-sm"><?= $company->last_update; ?></td>
                                        <td class="hidden-xs deactiveTime<?= $company->id ?>"><?php if($company->time_deactive == '0000-00-00 00:00:00'){ echo \Yii::t('app', 'немає');}else{ echo $company->time_deactive; } ?></td>
                                        
                                        <td><a href='/admin/companydelete?id=<?= $company->id ?>' class="delete" title='delete'><i class="fa fa-times fa-2x" style="color:#3693cf;"></i></a></td>
                                        <td class="buttonActive<?= $company->id ?>">
                                            <?php if($company->active == '1'){ ?>
                                                <button class='btn btn-primary activeCompany' id='deactive' data-id='<?= $company->id; ?>' data-type="admin" data-action='deactive'>Deactive</button>
                                            <?php }else{ ?>
                                               <!-- <button class='btn btn-primary activeCompany' id='active' data-id='<?php //= $company->id; ?>' data-action='active'>Active</button> -->
                                                <button class='btn btn-primary KursorShow' data-toggle="modal" data-target="#myModalActiveCompany<?= $company->id; ?>">Active</button>
                                            <?php }?>
                                        </td>
                                    </tr>
                                    
                                        <div class="modal fade" name="myModalActiveCompany" id="myModalActiveCompany<?= $company->id; ?>" role="dialog">
                                            <div class="modal-dialog">
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h3 class="modal-title" style='padding:0px;' align='center'><?php echo \Yii::t('app', 'Активація компанії'); ?></h3>
                                                </div>
                                                <div class="modal-body">
                                                    <div class='row'>
                                                        <div class='col-sm-12'>
                                                            <ul id="tabs" class="nav nav-tabs nav-justified" data-tabs="tabs">
                                                                <li class="active"><a href="#user" class="onePay" data-id="<?= $company->id; ?>" data-toggle="tab"><?php echo \Yii::t('app', 'Активація по даті'); ?></a></li>
                                                                <li><a href="#company" class="twoPay" data-id="<?= $company->id; ?>" data-toggle="tab"><?php echo \Yii::t('app', 'Активація по сумі оплати'); ?></a></li>
                                                            </ul>
                                                        </div>
                                                        <div class='col-sm-12 containerOne<?= $company->id; ?>' data-active="1">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <label><?php echo \Yii::t('app', 'Введіть час активації'); ?></label>
                                                                        <div id="datetimepicker1" class="input-group date datetimepicker">
                                                                            <input data-format="yyyy-MM-dd hh:mm:ss" type="text" class='form-control thisdate companyTimeActive<?= $company->id; ?>'></input>
                                                                           <span class="add-on input-group-addon">
                                                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                                                            </i>
                                                                           </span>
                                                                        </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <label><?php echo \Yii::t('app', 'Введіть час деактивації'); ?></label>
                                                                        <div id="datetimepicker2" class="input-group date datetimepicker">
                                                                            <input data-format="yyyy-MM-dd hh:mm:ss" type="text" class='form-control companyTimeDeactive<?= $company->id; ?>'></input>
                                                                           <span class="add-on input-group-addon">
                                                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                                                            </i>
                                                                           </span>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class='containerTwo<?= $company->id; ?>' data-active="0" style='display:none' >
                                                            <div class="col-sm-12">
                                                                <label><?php echo \Yii::t('app', 'Введіть суму оплати'); ?></label>
                                                                <input class="form-control companyMoney<?= $company->id; ?>" type="text" >
                                                            </div>
                                                        </div>
                                                    </div>    
                                                </div>    
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default companyActive" data-id="<?= $company->id; ?>"><?php echo \Yii::t('app', 'Зберегти'); ?></button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo \Yii::t('app', 'Закрити'); ?></button>
                                                </div>
                                              </div>

                                            </div>
                                        </div>
                            <?php } ?>
                            </table>
                                 <div class="col-sm-12">
                                     <?= LinkPager::widget(['pagination'=>$pagination]); ?>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
              
              
              
          </div>
          <!-- Row End -->
          

          

        </div>
        <!-- Dashboard Wrapper End -->

        <footer>
          <p>© Tracker system 2015</p>
        </footer>

      </div>
    </div>
 