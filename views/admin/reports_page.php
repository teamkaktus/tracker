<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\widgets\LinkPager;
    use yii\bootstrap\Alert;
    use dosamigos\multiselect\MultiSelect;
    use app\assets\AdminAsset;
    use app\assets\GraphAsset;
    use app\assets\HomepageAsset;
    
    AdminAsset::register($this);
    GraphAsset::register($this);
    //HomepageAsset::register($this);
?>

 <!-- Main Container start -->
    <div class="dashboard-container">

      <div class="container">
        <!-- Top Nav Start -->
        <div id="cssmenu">
          <ul>
            <li>
                <a href="../../admin/index"><i class="fa fa-users"></i><?php echo \Yii::t('app','Користувачі'); ?></a>
            </li>
            <li>
              <a href="../../admin/companypage">
                <i class="fa fa-briefcase"></i>
                <?php echo \Yii::t('app','Компанії'); ?>
              </a>
            </li>
            
            <li>
              <a href="../../admin/trackerpage">
                <i class="fa fa-map-marker"></i>
                <?php echo \Yii::t('app','Трекери'); ?>
              </a>
            </li>
            
            <li class="hidden-sm">
              <a href="../../admin/driverspage">
                <i class="fa fa-automobile"></i>
                <?php echo \Yii::t('app','Водії'); ?>
              </a>
            </li>
            
            <li class="hidden-sm">
              <a href="../../admin/paymentpage">
                <i class="fa fa-credit-card"></i>
                <?php echo \Yii::t('app','Оплата'); ?>
              </a>
            </li>
            
            <li class="active"><span class="submenu-button"></span>
              <a href="../admin/reports"><i class="fa fa-bar-chart-o"></i><?php echo \Yii::t('app','Графіки/Звіти'); ?></a>
<!--              <ul>
                 <li><a href="../admin/reports">One Graphs</a></li>
                 <li><a href="../admin/reports">Google Graph</a></li>
                 <li><a href="../admin/reports">Vector Maps</a></li>
              </ul>-->
            </li>
            <li>
              <a href="../../lang"><i class="fa fa-language"></i><?php echo \Yii::t('app','Мови системи'); ?></a>
             
            </li>
          </ul>
        </div>
        <!-- Top Nav End -->

        <!-- Sub Nav End -->
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <a href="../../../site/index" style="margin-left:9px;font-size:15px;padding:0px;"><i class="fa fa-home"></i> Home </a><a style="font-size:10px;padding:0px"> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;"><?php echo \Yii::t('app','Сторінка графіків та звітів'); ?></a>
            </li>
          </ul>
        </div>
        <!-- Sub Nav End -->

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
          
          <!-- Row starts -->
<!--          <div class="row displayNone">
            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Visitors</div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 12.2<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-globe"></i>
                  </div>
                  <div class="pull-right number">8757</div>
                </div>
                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> 
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Shares</div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 18.3<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-twitter"></i>
                  </div>
                  <div class="pull-right number">3780</div>
                </div>
                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> 
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Downloads</div>
                  <div class="pull-right"><i class="fa fa-angle-down"></i> 21.9<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-upload"></i>
                  </div>
                  <div class="pull-right number">12658</div>
                </div>
                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> 
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="mini-widget mini-widget-grey">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Signups</div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 67.1<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-coffee"></i>
                  </div>
                  <div class="pull-right number">1135</div>
                </div>
                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> 
              </div>
            </div>
          </div>-->
          <!-- Row ends -->

          <!-- Row Start -->
          <div class="row wrap">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
              <div class="widget">
                <div class="widget-header">
                  <div class="title" style="margin:0px;">
                    <i class="icon-angle-right" data-action="show"> </i> <?php echo \Yii::t('app','Граффіки/Звіти'); ?>
                    <span class="mini-title displayNone">
                        <?php echo \Yii::t('app','Граффіки/Звіти'); ?>
                    </span>
                  </div>
                </div>
                <div class=" widget-body">
                     <div class='preloader' style='display:none;'> 
                        <?php echo \Yii::t('app','Будь ласка зачекайте...'); ?>
                     </div>
                     <div class='row'>
                         <div class='col-sm-12'>
                             <?php
                                 echo Html::dropDownList('listname', 'name', ['company' => \Yii::t('app','Компанії'), 'user' => \Yii::t('app','Користувачі')],['prompt' => \Yii::t('app','Виберіть параметри...'), 'class' => 'form-control']);
                             ?>
                         </div>
                         <div class='col-sm-12'>
                             <div id="piechart_3d" style="width: 100%; height: 500px;"></div>
                             <div id="piechart_3d_two" style="width: 100%; height: 500px;display:none;"></div>
                             <div id="piechart_3d_user" style="width: 100%; height: 500px;display:none;"></div>
                         </div>
                     </div>
                </div>
              </div>
            </div>
              
          </div>
          <!-- Row End -->
          
        </div>
        <!-- Dashboard Wrapper End -->

        <footer>
          <p>© Tracker system 2015</p>
        </footer>

      </div>
    </div>
