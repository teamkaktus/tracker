<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\widgets\LinkPager;
    use yii\bootstrap\Alert;
    use app\assets\AdminAsset;
    use dosamigos\multiselect\MultiSelect;
    
    AdminAsset::register($this);
?>


    <?php
    
    if(Yii::$app->session->hasFlash('DriverAdded')): ?>
        <?php
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => \Yii::t('app','Водій доданий!'),
        ]);
    endif;

    if(Yii::$app->session->hasFlash('DriverNotAdded')): ?>
        <?php
        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => \Yii::t('app','Водій не доданий, повторіть спробу, або зверніться до адміністрації!'),
        ]);
    endif;
    
    if(Yii::$app->session->hasFlash('DriverUpdate')): ?>
        <?php
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => \Yii::t('app','Дані водія успішно змінено!'),
        ]);
    endif;

    if(Yii::$app->session->hasFlash('DriverNotUpdate')): ?>
        <?php
        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => \Yii::t('app','Дані водія не редаговано, повторіть спробу, або зверніться до адміністрації!'),
        ]);
    endif;
    
    if(Yii::$app->session->hasFlash('DriverDeleted')): ?>
        <?php
        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => \Yii::t('app','Водій видалений!'),
        ]);
    endif;
    
    ?>
<!-- Main Container start -->
    <div class="dashboard-container">

      <div class="container">
        <!-- Top Nav Start -->
        <div id="cssmenu">
          <ul>
            <li>
                <a href="../../admin/index"><i class="fa fa-users"></i><?php echo \Yii::t('app','Користувачі'); ?></a>
            </li>
            <li>
              <a href="../../admin/companypage">
                <i class="fa fa-briefcase"></i>
                <?php echo \Yii::t('app','Компанії'); ?>
              </a>
            </li>
            
            <li>
              <a href="../../admin/trackerpage">
                <i class="fa fa-map-marker"></i>
                <?php echo \Yii::t('app','Трекери'); ?>
              </a>
            </li>
            
            <li class="active" class="hidden-sm">
              <a href="../../admin/driverspage">
                <i class="fa fa-automobile"></i>
                <?php echo \Yii::t('app','Водії'); ?>
              </a>
            </li>
            
            <li class="hidden-sm">
              <a href="../../admin/paymentpage">
                <i class="fa fa-credit-card"></i>
                <?php echo \Yii::t('app','Оплата'); ?>
              </a>
            </li>
            
            <li class=""><span class="submenu-button"></span>
              <a href="../admin/reports"><i class="fa fa-bar-chart-o"></i><?php echo \Yii::t('app','Графіки/Звіти'); ?></a>
<!--              <ul>
                 <li><a href="http://iamsrinu.com/bluemoon-admin-theme8/flot.html">One Graphs</a></li>
                 <li><a href="http://iamsrinu.com/bluemoon-admin-theme8/graphs.html">Google Graph</a></li>
                 <li><a href="http://iamsrinu.com/bluemoon-admin-theme8/vector-maps.html">Vector Maps</a></li>
              </ul>-->
            </li>
            <li>
              <a href="../../lang"><i class="fa fa-language"></i><?php echo \Yii::t('app','Мови системи'); ?></a>
             
            </li>
          </ul>
        </div>
        <!-- Top Nav End -->

        <!-- Sub Nav End -->
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <a href="../../../site/index" style="margin-left:9px;font-size:15px;padding:0px;"><i class="fa fa-home"></i> Home </a><a style="font-size:10px;padding:0px"> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;"><?php echo \Yii::t('app','Сторінка водія'); ?></a>
            </li>
          </ul>
        </div>
        <!-- Sub Nav End -->

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
          
          <!-- Row starts -->
<!--          <div class="row displayNone" >
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Visitors</div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 12.2<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-globe"></i>
                  </div>
                  <div class="pull-right number">8757</div>
                </div>
                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> 
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Shares</div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 18.3<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-twitter"></i>
                  </div>
                  <div class="pull-right number">3780</div>
                </div>
                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> 
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Downloads</div>
                  <div class="pull-right"><i class="fa fa-angle-down"></i> 21.9<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-upload"></i>
                  </div>
                  <div class="pull-right number">12658</div>
                </div>
                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> 
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="mini-widget mini-widget-grey">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Signups</div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 67.1<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-coffee"></i>
                  </div>
                  <div class="pull-right number">1135</div>
                </div>
                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> 
              </div>
            </div>
          </div>-->
          <!-- Row ends -->

          <!-- Row Start -->
          <div class="row wrap">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <i class="icon-angle-right boxClick" data-action="show"> </i> <?php echo \Yii::t('app','Додати водія'); ?>
                    <span class="mini-title displayNone">
                        <?php echo \Yii::t('app','натисніть на іконку щоб відкрити форму'); ?>
                    </span>
                  </div>
                </div>
                <div class=" widget-body boxShow" <?=(Yii::$app->session->hasFlash('ErrorValidating'))?'':'style="display:none;"'?>>
                   
                <?php  $form = ActiveForm::begin(['id' => 'NewUser', 'action' => '../admin/driverspage', 'enableClientValidation'=>true, 'class' => 'form-control', 'method' => 'POST']); ?>
                    <?= $form->field($newDriversModel, 'first_name')->textInput(['placeholder' => \Yii::t('app','Ім`я'), 'class' => 'form-control required'])->label(false); ?>
                    <?= $form->field($newDriversModel, 'last_name')->textInput(['placeholder' => \Yii::t('app','Прізвище'),'class' => 'form-control required'])->label(false); ?>
                    <?= $form->field($newDriversModel, 'phone_number')->textInput(['placeholder' => \Yii::t('app','Номер телефону'), 'class' => 'form-control'])->label(false);; ?>
                      
                    <?=  MultiSelect::widget([
                            'id'=>"multiXX",
                            'options' => [],
                            'data' => $trackerArray, 
                            'name' => 'tracker_driver', 
                            "clientOptions" =>
                                [
                                    'numberDisplayed' => 2,
                                    'enableFiltering' => true,
                                ],
                        ]);
                    echo '<br><br>';
                    ?>
                    
                    <?= Html::submitButton(\Yii::t('app', 'Додати'), ['class' => 'btn-submit btn btn-primary']); ?>
                <?php ActiveForm::end();  ?>
                </div>
              </div>
            </div>
            
        
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
                <div class="widget">
                    <div class="widget-header">
                      <div class="title">
                        <?php echo \Yii::t('app','Водії');?>
                        <span class="mini-title displayNone">
                        <?php echo \Yii::t('app','вивід всіх водіїв');?>
                        </span>
                      </div>
                    </div>
                    <div class="widget-body">
                        <div class="row">
                            <div class='col-sm-12'>
                                <table class='table'>
                                    <tr>
                                        <td><b><?php echo \Yii::t('app','Ім`я');?></b></td>
                                        <td><b><?php echo \Yii::t('app','Прізвище');?></b></td>
                                        <td class="hidden-xs hidden-sm"><b><?php echo \Yii::t('app','Номер телефону');?></b></td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <form method='get' action='../admin/index'>
                                        <tr>
                                            <td colspan="100%">
                                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 paddingNone">
                                                    <input type='text'  name='searchInput' class='form-control'>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-2 paddingNone">
                                                    <input type='submit' id='search' class='btn btn-primary' value='<?php echo \Yii::t('app','Пошук');?>'>
                                                </div>
                                            </td>
                                        </tr>
                                    </form>
                                    <?php
                                     foreach($driversModel as $driver){ ?>
                                        <tr>
                                            <td><?= $driver->first_name; ?></td>
                                            <td><?= $driver->last_name; ?></td>
                                            <td class="hidden-xs hidden-sm"><?= $driver->phone_number; ?></td>
                                            <td>
                                                <a href='../../admin/driverdelete?id=<?= $driver->id; ?>' class="delete" title='delete'><i class="fa fa-times fa-2x" style="color:#3693cf;"></i></a>
                                            </td>
                                            <td>
                                                <a data-toggle="modal" class="KursorShow" data-target="#myModal<?= $driver->id; ?>"><i class='fa fa-pencil-square-o fa-2x' style="color:#3693cf;"></i></a>
                                            </td>
                                            <?php 
                                                $driverChoice = [];
                                                foreach($driverTracker as $driverC){
                                                    if($driver->id == $driverC->driver_id){
                                                        $driverChoice = $driverC->tracker_id;
                                                    }
                                                } 
                                            ?>
                                 <div class="modal fade" id="myModal<?= $driver->id; ?>" role="dialog">
                                    <div class="modal-dialog">
                                      <!-- Modal content-->
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title" style='color:black;'><?php echo \Yii::t('app','Редагування водія');?></h4>
                                        </div>
                                        <div class="modal-body" style='color:black;'>
                                        <?php $form = ActiveForm::begin(['id' => 'UpdateDriver', 'action' => '/admin/driverupdate', 'class' => 'form-control', 'method' => 'POST']);?>
                                            <input type="hidden" name='driver_id' value='<?= $driver->id; ?>'>
                                            <?= $form->field($driver, 'first_name')->textInput(['placeholder' => \Yii::t('app','Ім`я'), 'class' => 'form-control required'])->label(false); ?>
                                            <?= $form->field($driver, 'last_name')->textInput(['placeholder' => \Yii::t('app','Прізвище'),'class' => 'form-control required'])->label(false); ?>
                                            <?= $form->field($driver, 'phone_number')->textInput(['placeholder' => \Yii::t('app','Номер телефону'), 'class' => 'form-control'])->label(false);; ?>
                                            <?=  MultiSelect::widget([
                                                    'id'=>"multi".$driver->id,
                                                    'options' => [],
                                                    'data' => $trackerArray, 
                                                    'value' => $driverChoice, 
                                                    'name' => 'tracker_driver', 
                                                    "clientOptions" =>
                                                        [
                                                            'numberDisplayed' => 2,
                                                            'enableFiltering' => true,
                                                        ],
                                                ]); ?>
                                        </div>
                                        <div class="modal-footer">
                                                <?php echo Html::submitButton(\Yii::t('app', 'Зберегти'), array('class' => 'btn btn-primary')); ?>
                                                <?php ActiveForm::end(); ?>
                                            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo \Yii::t('app','Закрити'); ?></button>
                                        </div>
                                      </div>

                                    </div>
                                  </div>   
                                     <?php       
                                     }
                                    ?>
                                     </div>
                                        </table>
                                     <div class="col-sm-12">
                                         <?= LinkPager::widget(['pagination'=>$pagination]); ?>
                                     </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
              
              
              
          </div>
          <!-- Row End -->
          

          

        </div>
        <!-- Dashboard Wrapper End -->

        <footer>
          <p>© Tracker system 2015</p>
        </footer>

      </div>
    </div>
