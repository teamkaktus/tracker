<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\widgets\LinkPager;
    use yii\bootstrap\Alert;
    use app\assets\AdminAsset;
    
    AdminAsset::register($this);
?>

<?php if(Yii::$app->session->hasFlash('UserAdd')): ?>
        <?php
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => \Yii::t('app','Користувач доданий!'),
        ]);
        ?>
    <?php endif;

    if(Yii::$app->session->hasFlash('UserUpdate')): ?>
        <?php
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => \Yii::t('app','Змінені дані користувача, ЗБЕРЕЖЕНО!'),
        ]);
    endif;

    if(Yii::$app->session->hasFlash('UserNotUpdate')): ?>
        <?php
        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => \Yii::t('app','Зміни не збережено, повторіть спробу, або зверніться до адміністрації сайту!'),
        ]);
    endif;
    
    
    if(Yii::$app->session->hasFlash('EMAIL_sended')): ?>
        <?php
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => \Yii::t('app','Для того щоб активувати аккаунт, увійдіть на вказаний e-mail!'),
        ]);
    endif;
    if(Yii::$app->session->hasFlash('ErrorValidating')): ?>
        <?php
        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => \Yii::t('app','Некоректно введено email'),
        ]);
    endif;

    if(Yii::$app->session->hasFlash('Email_error')): ?>
        <?php
        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => \Yii::t('app','Проблеми з вашею електронною адресою, або її не їснує, попробуйте знову, або зверніться до адміністрації сайту!'),
        ]);
    endif;

    if(Yii::$app->session->hasFlash('UserDeleted')): ?>
        <?php
        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => \Yii::t('app','Користувач видалений!'),
        ]);
        ?>
    <?php endif; ?>

 <!-- Main Container start -->
    <div class="dashboard-container">

      <div class="container">
        <!-- Top Nav Start -->
        <div id="cssmenu">
          <ul>
            <li class="active">
              <a href="../../admin/index"><i class="fa fa-users"></i><?php echo \Yii::t('app','Користувачі'); ?></a>
             
            </li>
            <li>
              <a href="../../admin/companypage">
                <i class="fa fa-briefcase"></i>
                <?php echo \Yii::t('app','Компанії'); ?>
              </a>
            </li>
            
            <li>
              <a href="../../admin/trackerpage">
                <i class="fa fa-map-marker"></i>
                <?php echo \Yii::t('app','Трекери'); ?>
              </a>
            </li>
            
            <li class="hidden-sm">
              <a href="../../admin/driverspage">
                <i class="fa fa-automobile"></i>
                <?php echo \Yii::t('app','Водії'); ?>
              </a>
            </li>
            
            <li class="hidden-sm">
              <a href="../../admin/paymentpage">
                <i class="fa fa-credit-card"></i>
                <?php echo \Yii::t('app','Оплата'); ?>
              </a>
            </li>
            
            <li class=""><span class="submenu-button"></span>
              <a href="../admin/reports"><i class="fa fa-bar-chart-o"></i><?php echo \Yii::t('app','Графіки/Звіти'); ?></a>
<!--              <ul>
                 <li><a href="http://iamsrinu.com/bluemoon-admin-theme8/flot.html">One Graphs</a></li>
                 <li><a href="http://iamsrinu.com/bluemoon-admin-theme8/graphs.html">Google Graph</a></li>
                 <li><a href="http://iamsrinu.com/bluemoon-admin-theme8/vector-maps.html">Vector Maps</a></li>
              </ul>-->
            </li>
            <li>
              <a href="../../lang"><i class="fa fa-language"></i><?php echo \Yii::t('app','Мови системи'); ?></a>
             
            </li>
          </ul>
        </div>
        <!-- Top Nav End -->

        <!-- Sub Nav End -->
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <a href="../../../site/index" style="margin-left:9px;font-size:15px;padding:0px;"><i class="fa fa-home"></i> Home </a><a style="font-size:12px;padding:0px"> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;"><?php echo \Yii::t('app','Сторінка користувачів'); ?></a>
            </li>
          </ul>
        </div>
        <!-- Sub Nav End -->

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
          
          <!-- Row starts -->
          <div class="row displayNone">
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left"><?php echo \Yii::t('app','Всіх корисувачів'); ?></div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 100<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-globe"></i>
                  </div>
                  <div class="pull-right number"><?= $statistic['userCount']; ?></div>
                </div>
<!--                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> -->
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left"><?php echo \Yii::t('app','Активованих користувачів'); ?></div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i><?= $statistic['activeUserProcent']; ?><sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-twitter"></i>
                  </div>
                  <div class="pull-right number"><?= $statistic['activeUserCount']; ?></div>
                </div>
<!--                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> -->
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left"><?php echo \Yii::t('app','Неактивованих користувачів'); ?></div>
                  <div class="pull-right"><i class="fa fa-angle-down"></i><?= $statistic['deactiveUserProcent']; ?><sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-upload"></i>
                  </div>
                  <div class="pull-right number"><?= $statistic['deactiveUserCount']; ?></div>
                </div>
<!--                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> -->
              </div>
            </div>
<!--            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="mini-widget mini-widget-grey">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Signups</div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 67.1<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-coffee"></i>
                  </div>
                  <div class="pull-right number">1135</div>
                </div>
                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> 
              </div>
            </div>-->
          </div>
          <!-- Row ends -->

          <!-- Row Start -->
          <div class="row wrap">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <i class="icon-angle-right boxClick" data-action="show"> </i> <?php echo \Yii::t('app', 'Додати користувача'); ?>
                    <span class="mini-title displayNone">
                        <?php echo \Yii::t('app', 'натисніть на іконку щоб відкрити форму'); ?>
                    </span>
                  </div>
                </div>
                <div class=" widget-body boxShow" <?=(Yii::$app->session->hasFlash('ErrorValidating'))?'':'style="display:none;"'?>>
                    <?php  $form = ActiveForm::begin(['id' => 'NewUser', 'action' => '../admin/adduser',    'enableClientValidation'=>true, 'class' => 'form-control', 'method' => 'POST']); ?>
                        <?= $form->field($newModelUser, 'username')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Логін'), 'class' => 'form-control required']); ?>
                        <?= $form->field($newModelUser, 'email')->input('email',['placeholder' => \Yii::t('app', 'E-mail')]); ?>
                        <?= $form->field($newModelUser, 'phone_number')->textInput(['rows' => 1 , 'placeholder' => \Yii::t('app', 'Номер телефону'), 'class' => 'form-control']); ?>
                        <?= HTML::RadioList('UserType', '1', ['1' => \Yii::t('app', 'Адміністратор'), '2' => \Yii::t('app', 'Користувач'), '3' => \Yii::t('app', 'Користувач компанії')],['inline' =>true]); ?>
                        <div class='well' id='showCompanyDetails' style='display:none'>
                            <?= $form->field($newModelUser, 'users_type')->dropDownList(['1' => \Yii::t('app', 'Адміністратор'),'2' => \Yii::t('app', 'Користувач')])->label(\Yii::t('app', 'Тип користувача в компанії'));; ?>
                            <?= $form->field($newModelUser, 'company_id')->dropDownList($modelCompany)->label(\Yii::t('app', 'Компанія')); ?>
                        </div>
                        <?= Html::submitButton(\Yii::t('app', 'Додати'), ['class' => 'btn-submit btn btn-primary']); ?>
                    <?php ActiveForm::end();  ?>
                </div>
              </div>
            </div>
            
        
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <?php echo \Yii::t('app','Користувачі'); ?>
                    <span class="mini-title displayNone">
                        <?php echo \Yii::t('app','Вивід всіх користувачів'); ?>
                    </span>
                  </div>
                </div>
                <div class=" widget-body">
              
                    <table class='table'>
                        <tr>
                            <td><b><?php echo \Yii::t('app','Логін'); ?></b></td>
                            <td class="hidden-xs"><b><?php echo \Yii::t('app','E-mail'); ?></b></td>
                            <td class="hidden-xs"><b><?php echo \Yii::t('app','Номер телефону'); ?></b></td>
                            <td class="hidden-xs hidden-sm"><b><?php echo \Yii::t('app','Тип користувача'); ?></b></td>
                            <td class="hidden-xs"><b><?php echo \Yii::t('app','Дата деактивації'); ?></b></td>
                            <td colspan="4"></td>
                        </tr>
                        <form method='get' action='../admin/index'>
                            <tr>
                                <td colspan="100%">
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9 paddingNone">
                                        <input type='text' name='searchInput' value="<?= $_GET['searchInput']; ?>" class='form-control'></div>
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-2 paddingNone">
                                        <input type='submit' id='search' class='btn btn-primary' value='<?php echo \Yii::t('app','Пошук'); ?>'>
                                    </div>
                                </td>
                            </tr>
                        </form>
                        <?php
                         foreach($modelUser as $user){ ?>
                            <tr>
                                <td><a href=''><?= HTML::a($user->username, ['user/show', 'username' => $user->username]) ?></a></td>
                                <td class="hidden-xs"><?= $user->email; ?></td>
                                <td class="hidden-xs"><?= $user->phone_number; ?></td>
                                <td class="hidden-xs hidden-sm"><?php if($user->users_type == 1){ echo  \Yii::t('app','Адміністратор'); }else{ echo \Yii::t('app','Користувач');} ?></td>
                                <td class="hidden-xs deactiveTime<?= $user->id ?>"><?php if($user->time_deactive == '0000-00-00 00:00:00'){ echo 'do not have';}else{ echo $user->time_deactive; } ?></td>
                                <td>
                                    <a href='/admin/userdelete?id=<?= $user->id; ?>' class="delete" title='delete'><i class="fa fa-times fa-2x" style="color:#3693cf;"></i></a>
                                </td>
                                <td>
                                    <a data-toggle="modal" class="KursorShow" data-target="#myModal<?= $user->id; ?>"><i class='fa fa-pencil-square-o fa-2x' style="color:#3693cf;"></i></a>
                                </td>
                                <td class="buttonActive<?= $user->id ?>">
                                    <?php if($user->active == '1'){ ?>
                                        <button class='btn btn-primary deactiveUser' id='deactive' data-id='<?= $user->id; ?>' data-type="admin" data-action='deactive'>Deactive</button>
                                    <?php }else{ ?>
                                        <button class='btn btn-primary KursorShow' data-toggle="modal" data-target="#myModalActiveUser<?= $user->id; ?>">Active</button>
                                    <?php }?>
                                </td>
                            </tr>

                            <div class="modal fade" name="myModalActiveUser" id="myModalActiveUser<?= $user->id; ?>" role="dialog">
                                            <div class="modal-dialog">
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h3 class="modal-title" style='padding:0px;' align='center'><?php echo \Yii::t('app','Активація користувача'); ?></h3>
                                                </div>
                                                <div class="modal-body">
                                                    <div class='row'>
                                                        <div class='col-sm-12'>
                                                            <ul id="tabs" class="nav nav-tabs nav-justified" data-tabs="tabs">
                                                                <li class="active"><a href="#user" class="onePay" data-id="<?= $user->id; ?>" data-toggle="tab"><?php echo \Yii::t('app','Активація по даті'); ?></a></li>
                                                                <li><a href="#company" class="twoPay" data-id="<?= $user->id; ?>" data-toggle="tab"><?php echo \Yii::t('app','Активація по сумі оплати'); ?></a></li>
                                                            </ul>
                                                        </div>
                                                        <div class='col-sm-12 containerOne<?= $user->id; ?>' data-active="1">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <label><?php echo \Yii::t('app','Введіть час активації'); ?></label>
                                                                        <div id="datetimepicker1" class="input-group date datetimepicker">
                                                                            <input data-format="yyyy-MM-dd hh:mm:ss" type="text" class='form-control thisdate userTimeActive<?= $user->id; ?>'></input>
                                                                           <span class="add-on input-group-addon">
                                                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                                                            </i>
                                                                           </span>
                                                                        </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <label><?php echo \Yii::t('app','Введіть час деактивації'); ?></label>
                                                                        <div id="datetimepicker2" class="input-group date datetimepicker">
                                                                            <input data-format="yyyy-MM-dd hh:mm:ss" type="text" class='form-control userTimeDeactive<?= $user->id; ?>'></input>
                                                                           <span class="add-on input-group-addon">
                                                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                                                            </i>
                                                                           </span>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class='containerTwo<?= $user->id; ?>' data-active="0" style='display:none' >
                                                            <div class="col-sm-12">
                                                                <label><?php echo \Yii::t('app','Введіть суму оплати'); ?></label>
                                                                <input class="form-control userMoney<?= $user->id; ?>" type="text" >
                                                            </div>
                                                        </div>
                                                    </div>    
                                                </div>    
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default userActive" data-id="<?= $user->id; ?>"><?php echo \Yii::t('app','Зберегти'); ?></button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo \Yii::t('app','Закрити'); ?></button>
                                                </div>
                                              </div>

                                            </div>
                                        </div>

                            
                            <div class="modal fade" id="myModal<?= $user->id; ?>" role="dialog">
                                        <div class="modal-dialog">

                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                              <h4 class="modal-title" style='padding:0px;' align='center'><?php echo \Yii::t('app','Редагування аккаунта користувача'); ?></h4>
                                            </div>
                                            <div class="modal-body">
                                                <h1 align="center"><b><?= $user->username; ?></b></h1>

                                                <?php $form = ActiveForm::begin(['id' => 'UpdateUser', 'action' => '../admin/userupdate', 'class' => 'form-control', 'method' => 'POST']);
                                                ?>
                                                <input type="hidden" name='user_id' value='<?= $user->id; ?>'>
                                                <?= $form->field($user, 'username')->textInput(); ?>
                                                <?= $form->field($user, 'phone_number')->textInput(); ?>
                                                <?= $form->field($user, 'first_name')->textInput(); ?>
                                                <?= $form->field($user, 'last_name')->textInput(); ?>
                                                <?= $form->field($user, 'city')->textInput(); ?>
                                                <?= $form->field($user, 'address')->textInput(); ?>
                                                <?= $form->field($user, 'country')->textInput(); ?>
                                                <?= $form->field($user, 'zip')->textInput(); ?>
                                                <?php //= $form->field($user, 'company_id')->dropDownList($modelCompany)->label('Company');  ?>
                                                <?php //= $form->field($user, 'users_type')->dropDownList($type)->label('user type in company');  ?>
                                                <?php if($user->company_id == 0){
                                                    if($user->users_type == 1){
                                                        $user_type_id = '1';
                                                        $display = 'display:none;';
                                                    }else{
                                                        $user_type_id = '2';
                                                        $display = 'display:none;';
                                                    }
                                                }else{
                                                    $user_type_id = '3';
                                                    $display = 'display:block;';
                                                } ?>
                                                <?= HTML::RadioList('UserTypeUpdate', $user_type_id, ['1' => 'admin', '2' => 'user', '3' => 'company user'],['inline' =>true]); ?>
                                                    <div class='well' id='showCompanyUpdate' style='<?= $display; ?>'>
                                                        <?= $form->field($user, 'users_type')->dropDownList(['1' => 'Administrator','2' => 'User'])->label('User type in company');; ?>
                                                        <?= $form->field($user, 'company_id')->dropDownList($modelCompany)->label('Company'); ?>
                                                    </div>
                                            </div>
                                            <div class="modal-footer">
                                                <?php echo Html::submitButton( \Yii::t('app', 'Зберегти'), array('class' => 'btn btn-primary')); ?>
                                                <?php ActiveForm::end(); ?>
                                                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo \Yii::t('app','Закрити'); ?></button>
                                            </div>
                                          </div>

                                        </div>
                                      </div>

                            <?php
                         }
                        ?>
                         </div>
                            </table>
                        <div class="row">
                            <div class="col-sm-12">
                                <?= LinkPager::widget(['pagination'=>$pagination]); ?>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
              
              
              
          </div>
          <!-- Row End -->
          

          

        </div>
        <!-- Dashboard Wrapper End -->

        <footer>
          <p>© Tracker system 2015</p>
        </footer>

      </div>
    </div>
    
    