<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\widgets\WLang;


/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php error_reporting(E_ERROR); ?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Tracker system</title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody();
        ?>
    <div class="wrap">

        <?php
            NavBar::begin([
                'brandLabel' => 'MonGPS',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            if(Yii::$app->user->isGuest){
                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-right'],
                    'items' => [
                        ['label' => \Yii::t('app','Головна сторінка'), 'url' => ['/site/index']],
                        //['label' => 'About', 'url' => ['/site/about']],
                        //['label' => 'Contact', 'url' => ['/site/contact']],
                        Yii::$app->user->isGuest ?
                            ['label' => \Yii::t('app','Ввійти'), 'url' => ['/site/login']] :
                            ['label' =>   \Yii::t('app','Вийти').'(' . Yii::$app->user->identity->username . ')',
                                'url' => ['/site/logout'],
                                'linkOptions' => ['data-method' => 'post']],
                    ],
                ]);
            }else{
                if((Yii::$app->user->identity->users_type == '1') && (Yii::$app->user->identity->company_id == '0')){
                    echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-right'],
                    'items' => [
                        ['label' =>  \Yii::t('app','Мої трекери'), 'url' => ['/user/'.Yii::$app->user->identity->getUsername(Yii::$app->user->id)]],
                        ['label' =>  \Yii::t('app','Адмін панель'), 'url' => ['/admin']],
                      //  ['label' => 'About', 'url' => ['/site/about']],
                        ['label' =>  \Yii::t('app','Мой аккаунт'), 'url' => ['user/account']],
                        Yii::$app->user->isGuest ?
                            ['label' =>  \Yii::t('app','Ввійти'), 'url' => ['/site/login']] :
                            ['label' =>   \Yii::t('app','Вийти').'(' . Yii::$app->user->identity->username . ')',
                                'url' => ['/site/logout'],
                                'linkOptions' => ['data-method' => 'post']],
                    ],
                    ]);
                }else{
                    if((Yii::$app->user->identity->users_type == '2') && (Yii::$app->user->identity->company_id == '0')){
                            echo Nav::widget([
                            'options' => ['class' => 'navbar-nav navbar-right'],
                            'items' => [
                                ['label' =>  \Yii::t('app','Мої трекери'), 'url' => ['/user/'.Yii::$app->user->identity->getUsername(Yii::$app->user->id)]],
                              //  ['label' => 'About', 'url' => ['/site/about']],
                                ['label' =>  \Yii::t('app','Мой аккаунт'), 'url' => ['user/account']],
                                Yii::$app->user->isGuest ?
                                    ['label' =>  \Yii::t('app','Ввійти'), 'url' => ['/site/login']] :
                                    ['label' =>   \Yii::t('app','Вийти').'(' . Yii::$app->user->identity->username . ')',
                                        'url' => ['/site/logout'],
                                        'linkOptions' => ['data-method' => 'post']],
                            ],
                            ]);
                        }else{
                            if(Yii::$app->user->identity->company_id != '0'){
                                echo Nav::widget([
                                'options' => ['class' => 'navbar-nav navbar-right'],
                                'items' => [
                                    ['label' =>  \Yii::t('app','Моя компанія'), 'url' => ['/company/'.Yii::$app->user->identity->getCompanyname(Yii::$app->user->id)]],
                                 //   ['label' => 'About', 'url' => ['/site/about']],
                                 //  ['label' => 'My account', 'url' => ['user/account']],
                                    Yii::$app->user->isGuest ?
                                        ['label' =>  \Yii::t('app','Ввійти'), 'url' => ['/site/login']] :
                                        ['label' =>  \Yii::t('app','Вийти').'(' . Yii::$app->user->identity->username . ')',
                                            'url' => ['/site/logout'],
                                            'linkOptions' => ['data-method' => 'post']],
                                ],
                                ]);
                        }
                    }
                }
            }
            echo WLang::widget();
            NavBar::end();
        ?>
        <div class="container" style='padding-top:50px; padding-left:0px;padding-right:0px; width: 100%'>
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>
<!--
    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
            <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer> -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
