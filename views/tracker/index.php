<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use app\assets\HomepageAsset;
    
HomepageAsset::register($this);
?>
<div class="dashboard-container">

      <div class="container">
        <!-- Top Nav End -->

        <!-- Sub Nav End -->
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <a href="site/index" style="margin-left:9px;font-size:15px;padding:0px;"><i class="fa fa-home"></i> Home </a><a style="font-size:12px;padding:0px"> <i class="fa fa-arrow-right"> </i> </a><a href="/admin/index" style="font-size:15px;padding:0px;"> User page</a>
            </li>
          </ul>
        </div>
        <!-- Sub Nav End -->

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
          
          <!-- Row ends -->

          <!-- Row Start -->
          <div class="row">
              
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    Report
                    <span class="mini-title">
                    </span>
                  </div>
                </div>
                <div class=" widget-body">
                </div>
                </div>
              
          </div>
          <!-- Row End -->
        
        </div>
        <!-- Dashboard Wrapper End -->

        <footer>
          <p>© Tracker system 2015</p>
        </footer>

      </div>
    </div>
