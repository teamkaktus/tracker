<?php
    use yii\helpers\Html;
    use yii\widgets\LinkPager;
    use yii\widgets\ActiveForm;
    use yii\helpers\Url;
    use app\assets\MyAsset;
    use app\assets\HomepageAsset;
    use app\assets\TrackerrouteAsset;
    use app\assets\LineAsset;

    LineAsset::register($this);
    HomepageAsset::register($this);
    MyAsset::register($this);
    TrackerRouteAsset::register($this);
?>
<div style="display:none;">
    <span id="draw_direction"><?= \Yii::t('app','Зєднання маршруту'); ?></span>
    <span id="show_activity"><?= \Yii::t('app','Показати дії'); ?></span>
    <span id="draw_polyline"><?= \Yii::t('app','Зєднання лініями'); ?></span>
    <span id="no_results_found"><?= \Yii::t('app','Нічого не знайдено'); ?></span>
    <span id="geocoder_failed"><?= \Yii::t('app','Помилка геокодування:'); ?></span>
    <span id="m_time"><?= \Yii::t('app',"Час: "); ?></span>
    <span id="m_speed"><?= \Yii::t('app',"Швидкість: "); ?></span>
    <span id="m_gps_status"><?= \Yii::t('app',"GPS статус: "); ?></span>
    <span id="m_open_gmaps"><?= \Yii::t('app',"Відкрити в Gmaps"); ?></span>
    <span id="m_acc_status"><?= \Yii::t('app',"Статус acc: "); ?></span>
    <span id="m_address"><?= \Yii::t('app',"Адресса: "); ?></span>
    <span id="a_no_data"><?= \Yii::t('app',"Немає даних за цей період"); ?></span>
    <span id="a_data_last_month"><?= \Yii::t('app',"Показати дані за останній місяць?"); ?></span>
    <span id="yes_show"><?= \Yii::t('app',"Так показати"); ?></span>
</div>
<div id="fixedButtonTrackerInfo" data-show="show" title="Інформація по точках">
    <i class="fa fa-location-arrow"></i>
</div>

<div id="fixedButtonTask" title="Завдання">
    <i class="fa fa-tasks"></i>
</div> 
<div id="fixedButtonDriver" title="Водії">
    <i class="fa fa-street-view"></i>
</div> 
<div id="fixedButtonAvto" title="Автомобіль">
    <i class="fa fa-automobile"></i>
</div> 
<div class="dashboard-container">
<div id="fixedBlockTask" style="overflow: scroll">
    <div class="col-sm-12">
        <h2>Завдання</h2>
        <i class="fa fa-times  fa-2x pull-right" id="hideBlockTask" style="color:#c43c35;margin-top: -60px;cursor:pointer;"></i>
    </div>
    <div class="col-sm-12">
        <div class="col-sm-12">
                <?php 
                foreach($tasksModel as $task){ ?>
                    <div class="col-sm-12" style="padding:0px;">
                        <div class="taskShow" style="cursor: pointer"><h4 style="margin-top:5px;padding:0px;"><?= $task->description; ?></h4></div>
                        <div class="taskBlockShow" style="display:none;">
                            <table class="table">
                                <tr>
                                    <td><b>Виконати: </b></td>
                                    <td><?= $task->due; ?></td>
                                </tr>
                                <tr>
                                    <td><b>Тип завдання: </b></td>
                                    <td><?php  echo ($task->type == 1)? \Yii::t('app','Завдання'):\Yii::t('app','Маршрут');  ?></td>
                                </tr>
                                <tr>
                                    <td><b>Час створення: </b></td>
                                    <td class="hidden-xs"><?= $task->time_added; ?></td>
                                </tr>
                                <tr>
                                <?php if($task->type == 2){ ?>
                                <td align="center">
                                    <a data-toggle="modal" class="KursorShow" data-target="#myModal<?= $task->id; ?>"><i class='fa fa-road fa-1x' style="color:#3187bf;"></i></a>
                                </td>
                                <?php } ?>
                                </tr>
                            </table>
                        </div>
                        <hr style="margin-top: 10px">
                    </div>
                        
                     <?php
                     }
                    ?>
        </div>
    </div>
</div>
    
<div id="fixedBlockDriver"  style="overflow: scroll">
    <div class="col-sm-12">
        <h2>Водії</h2>
        <i class="fa fa-times  fa-2x pull-right" id="hideBlockDriver" style="color:#c43c35;margin-top: -60px;cursor:pointer;"></i>
    </div>
    <div class="col-sm-12">
        <table class="table">
            <tr style="font-weight:bold;color:black;">
                <td class="hidden-xs"><b><?= \Yii::t('app','Ім`я'); ?></b></td>
                <td><b><?= \Yii::t('app','Прізвище'); ?></b></td>
                <td class="hidden-xs hidden-sm"><b><?= \Yii::t('app','Номер телефону'); ?></b></td>
            </tr>
        <?php
            foreach($driverModel as $driver){ ?>

                <tr style="color:black;">
                    <td  class="hidden-xs"><?= $driver->first_name; ?></td>
                    <td><?= $driver->last_name; ?></td>
                    <td  class="hidden-xs hidden-sm"><?= $driver->phone_number; ?></td>

                </tr>

                 <?php
                 }
                ?>
        </table>
    </div>
</div>
    
<div id="fixedBlockAvto"  style="overflow: scroll">
    <div class="col-sm-12">
        <h2>Автомобіль</h2>
        <i class="fa fa-times  fa-2x pull-right" id="hideBlockAvto" style="color:#c43c35;margin-top: -60px;cursor:pointer;"></i>
        <div class="col-sm-12" style="padding:0px;">
            <table class="table">
                <tr>
                    <td colspan="2" style="text-align: center">
                        <img src="<?= Url::home().'content/avto/'.$carModel->image_src; ?>" style="width: 100%; margin:0 auto;"></img>
                        <?php // $carModel->image_src; ?>
                    <td>
                </tr>
                <tr>
                    <td><b>Марка</b></td>
                    <td><?= $carModel->marka; ?></td>
                </tr>
                <tr>
                    <td><b>Номерний знак</b></td>
                    <td><?= $carModel->number; ?></td>
                </tr>
                <tr>
                    <td><b>Остання заміна коліс</b></td>
                    <td><?= $carModel->change_wheels; ?></td>
                </tr>
                <tr>
                    <td><b>Період заміни коліс</b></td>
                    <td><?= $carModel->period_change_wheels; ?></td>
                </tr>
                <tr>
                    <td><b>Остання заміна мастила</b></td>
                    <td><?= $carModel->change_oil; ?></td>
                </tr>
                <tr>
                    <td><b>Період заміни мастила</b></td>
                    <td><?= $carModel->period_change_oil; ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>

      <div class="container">
        <!-- Sub Nav End -->
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <a href="../../../site/index" style="margin-left:9px;font-size:15px;padding:0px;"><i class="fa fa-home"></i> Home </a><a style="font-size:12px;padding:0px"> <i class="fa fa-arrow-right"> </i> </a><a href="" style="font-size:15px;padding:0px;"><?= \Yii::t('app','Сторінка трекера'); ?></a>
            </li>
          </ul>
        </div>
        <!-- Sub Nav End -->
        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">

          <!-- Row ends -->

          <!-- Row Start -->
          <div class="row wrap">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
                    <div class="widget">
                        <div class="widget-header">
                          <div class="title">
                            <?= \Yii::t('app','Трекер'); ?>
                            <span class="mini-title displayNone">
                                <?=  \Yii::t('app','фільтр даних'); ?>
                            </span>
                          </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-lg-5 col-sm-4 col-xs-12">
                                    <div id="datetimepicker1" class="input-group date">
                                       <input data-format="yyyy-MM-dd hh:mm:ss" type="text" class='form-control from'></input>
                                       <span class="add-on input-group-addon">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                        </i>
                                       </span>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-sm-4 col-xs-12">
                                    <div id="datetimepicker2" class="input-group date">
                                        <input data-format="yyyy-MM-dd hh:mm:ss" type="text" class='form-control to'></input>
                                        <span class="add-on input-group-addon">
                                         <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                         </i>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-sm-4 col-xs-12">
                                    <button id='searchTracker' class='btn btn-primary' data-id='<?= $trackerModel->IMEI; ?>'><?= \Yii::t('app','Пошук даних'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 paddingNone classMapShow">
                        <div class="widget">
                            <div class="widget-header">
                              <div class="title">
                                <i class="fa fa-chevron-down trackerMap" style="cursor:pointer"></i><?= \Yii::t('app','Карта'); ?>
                                <span class="mini-title displayNone">
                                    <?= \Yii::t('app','вивід шляху трекера'); ?>
                                </span>
                              </div>
                                <button style="float:right;margin-top:5px;" id='showLast' class='btn btn-primary' data-id='<?= $trackerModel->IMEI; ?>' title="Find latest possition"><i class="fa fa-dot-circle-o"></i></button>
                            </div>
                            <div class="widget-body trackerMapInfo">
                                <div class="progress-wrap" style="display:none">
                                    <div align='center'><i class="fa fa-cog fa-spin fa-5x"></i>
                                      <div class="progress_bar_proc"></div>
                                    </div>
                                      <button id="cancel" ><i class="icon-remove-sign"></i></button>
                                </div>

                                <div id="map" style='border:1px solid grey;'></div>
                            </div>
                        </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 paddingNone classTrackerinfoShow">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="obd_info" style="display:none;padding:0px;">
                        <div class="widget animated slideInLeft">
                            <div class="widget-header">
                              <div class="title" style="font-size:12px;">
                                <?= \Yii::t('app','OBD дані'); ?>
                              </div>
                                <span class="mini-title pull-right displayNone" id="close_obd_info">
                                    <i class="fa fa-close" style="cursor:pointer;"></i>
                                </span>
                            </div>
                            <div class="widget-body" style="max-height:500px; overflow-y: scroll;" id="obd_info_marker">
                              <p id="fuel"></p>
                              <p id="oil"></p>
                              <p id="temperatura"></p>
                              <p id="instant_fuel"></p>
                              <p id="average_fuel"></p>
                              <p id="driving_time"></p>
                              <p id="power_load"></p>
                              <p id="water_temperature"></p>
                              <p id="engine_speed"></p>
                              <p id="battery_voltage"></p>
                              <p id="dtc1"></p>
                              <p id="dtc2"></p>
                              <p id="dtc3"></p>
                              <p id="dtc4"></p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0px;">
                            <div class="widget days zoomIn">
                                <div class="widget-header">
                                  <div class="title" style="font-size:12px;">
                                    <?= \Yii::t('app','Дні водіння'); ?>
                                  </div>
                                </div>
                                <div class="widget-body" style="max-height:300px;padding:5px; overflow-y: scroll;" id="days">
                                </div>
                                <div id="street-view" style="margin-top:3p;height:200px;"></div>
                            </div>
                            <div class="widget activity-day zoomIn" style="display:none">
                                <div class="widget-header">
                                  <div class="title" style="font-size:12px;">
                                      <a class="back-acivity" style='margin:0px;padding:0px;'><i class="fa fa-chevron-left"></i>Back</a>
                                   </div>
                                </div>
                                <div class="widget-body" style="max-height:500px;padding:5px; overflow-y: scroll;" >
                                    <table class="table table-responsive table-striped table-bordered table-hover no-margin" id="list-activities">

                                    </table>

                                </div>
                            </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
            </div>

          </div>
          <!-- Row End -->

        </div>
        <!-- Dashboard Wrapper End -->
        <?php foreach($tasksModel as $task){ ?>
            <?php if($task->type == 2){ ?>
                 <div class="modal fade" id="myModal<?= $task->id; ?>" name="modal" data-id_task="<?= $task->id; ?>" role="dialog">
                     <div class="modal-dialog">
                         <div class="modal-content">
                             <input type="hidden" class="travel_map" value="<?= $task->travel_mode;?>">
                             <div class="modal-header">
                               <button type="button" class="close" data-dismiss="modal">&times;</button>
                               <h4 class="modal-title" style='color:black;padding:0px;text-align:center;'><?= \Yii::t('app','Перегляд завдання'); ?></h4>
                             </div>
                             <div class="modal-body" style='color:black;'>
                                 <input type="hidden" class="for_map forMap<?= $task->id; ?>" name="for_map" value='<?= $task->for_map; ?>'>
                                     <div id="map-canvas<?= $task->id; ?>" class="map-canvas" style="border:1px solid red;width:100%;height:300px;"></div>
                             </div>
                             <div class="col-sm-12">
                                 <div class="for_way_list"></div>
                             </div>
                             <div class="modal-footer">
                                 <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('app', 'Закрити'); ?></button>
                             </div>
                         </div>
                     </div>
                 </div>
            <?php
              }
            ?>
         <?php
           }
         ?>
        <footer>
          <p>© Tracker system 2015</p>
        </footer>
<div id="shadow"></div>
<div id="preloader">
    <div id="loader"></div>
</div>

      </div>
    </div>
