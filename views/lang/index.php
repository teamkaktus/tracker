<?php

    use yii\helpers\Html;
    use yii\grid\GridView;
    use yii\bootstrap\Alert;
    use yii\widgets\ActiveForm;
    use app\assets\AdminAsset;
    
    AdminAsset::register($this);


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<?php
    if(Yii::$app->session->hasFlash('LangAdd')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => \Yii::t('app','Мову добавлено'),
        ]);
    endif;
    
    if(Yii::$app->session->hasFlash('LangNotAdd')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => \Yii::t('app','Мову не добавлено'),
        ]);
    endif;
?>
<div class="dashboard-container">
      <div class="container">
        <!-- Top Nav Start -->
        <div id="cssmenu">
          <ul>
            <li>
              <a href="../../admin/index"><i class="fa fa-users"></i><?php echo \Yii::t('app','Користувачі'); ?></a>
             
            </li>
            <li>
              <a href="../../admin/companypage">
                <i class="fa fa-briefcase"></i>
                <?php echo \Yii::t('app','Компанії'); ?>
              </a>
            </li>
            
            <li>
              <a href="../../admin/trackerpage">
                <i class="fa fa-map-marker"></i>
                <?php echo \Yii::t('app','Трекери'); ?>
              </a>
            </li>
            
            <li class="hidden-sm">
              <a href="../../admin/driverspage">
                <i class="fa fa-automobile"></i>
                <?php echo \Yii::t('app','Водії'); ?>
              </a>
            </li>
            
            <li class="hidden-sm">
              <a href="../../admin/paymentpage">
                <i class="fa fa-credit-card"></i>
                <?php echo \Yii::t('app','Оплата'); ?>
              </a>
            </li>
            
            <li class=""><span class="submenu-button"></span>
              <a href="../admin/reports"><i class="fa fa-bar-chart-o"></i><?php echo \Yii::t('app','Графіки/Звіти'); ?></a>
            </li>
            <li class="active">
              <a href="../../lang"><i class="fa fa-language"></i><?php echo \Yii::t('app','Мови системи'); ?></a>
             
            </li>
            
          </ul>
        </div>
        <!-- Top Nav End -->

        <!-- Sub Nav End -->
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <a href="../../../site/index" style="margin-left:9px;font-size:15px;padding:0px;"><i class="fa fa-home"></i> Home </a><a style="font-size:12px;padding:0px"> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;"><?php echo \Yii::t('app','Сторінка мови системи'); ?></a>
            </li>
          </ul>
        </div>
        <!-- Sub Nav End -->

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
<div class="row wrap">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
                  <div class="widget">
                    <div class="widget-header">
                      <div class="title">
                        <i class="icon-angle-right boxClick" data-action="show"> </i> <?php echo \Yii::t('app', 'Добавити мову'); ?>
                        <span class="mini-title displayNone">
                            <?php echo \Yii::t('app', 'натисніть на іконку щоб відкрити форму'); ?>
                        </span>
                      </div>
                    </div>
                      <div class="widget-body boxShow" <?=(Yii::$app->session->hasFlash('ErrorValidating'))?'':'style="display:none;"'?>>
                        <?php $form = ActiveForm::begin(); ?>
                        <?= $form->field($newModel, 'url')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($newModel, 'local')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($newModel, 'name')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($newModel, 'default')->textInput() ?>
                        <?= $form->field($newModel, 'date_update')->textInput() ?>
                        <?= $form->field($newModel, 'date_create')->textInput() ?>
                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('app', 'Create'),['class' => 'btn-submit btn btn-primary']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                      </div>
                  </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <i class="icon-angle-right" data-action="show"> </i> <?php echo \Yii::t('app', 'Мови системи'); ?>
                    <span class="mini-title displayNone">
                        <?php echo \Yii::t('app', 'список мов в системі'); ?>
                    </span>
                  </div>
                </div>
                <div class="widget-body">

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                                'id',
                                'url:url',
                                'local',
                                'name',
                                'default',
                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                </div>
              </div>
            </div>
</div>   

</div>
        <footer>
          <p>© Tracker system 2015</p>
        </footer>
</div>
</div>
