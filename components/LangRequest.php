<?php
namespace app\components;

use Yii;
use yii\web\Request;
use app\models\Lang;
error_reporting( E_ERROR );
class LangRequest extends Request
{
    public function resolve(){
      $resolve = parent::resolve();
      $lang_array = [];
      $lang = Lang::find()->all();
      foreach ($lang as $value) {
        $lang_array[] =$value->local;
      }
      // // Yii::$app->session->set('language', 'uk-UA');
      $user_lang = Yii::$app->user->identity->language;
      $session_lang = Yii::$app->session->get('language');
      $cookies_lang = Yii::$app->request->cookies->getValue('language');
      $browser_lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 5);
      // var_dump(['','','',$user_lang,$session_lang,$cookies_lang,$browser_lang]);

      if(!Yii::$app->user->isGuest){
        if(in_array($user_lang, $lang_array)){
          Yii::$app->language = $user_lang;
        }else if(isset($cookies_lang) && in_array($cookies_lang, $lang_array)){
          Yii::$app->language = $cookies_lang;
        }else if(isset($session_lang) && in_array($session_lang, $lang_array)){
          Yii::$app->language = $session_lang;
        }else if(isset($browser_lang)&&in_array($browser_lang, $lang_array)){
          Yii::$app->language = $browser_lang;
        }else {
          Yii::$app->language = 'en-US';
        }
      }else {
        if(isset($cookies_lang) && in_array($cookies_lang, $lang_array)){
          Yii::$app->language = $cookies_lang;
        }else if(isset($session_lang) && in_array($session_lang, $lang_array)){
          Yii::$app->language = $session_lang;
        }else if(isset($browser_lang)&&in_array($browser_lang, $lang_array)){
          Yii::$app->language = $browser_lang;
        }else {
          Yii::$app->language = 'en-US';
        }
      }
      return $resolve;
    }
    // private $_lang_url;
    //
    // public function getLangUrl()
    // {
    //     if ($this->_lang_url === null) {
	  //       $this->_lang_url = $this->getUrl();
    //
	  //   	$url_list = explode('/', $this->_lang_url);
    //
	  //   	$lang_url = isset($url_list[1]) ? $url_list[1] : null;
    //
	  //   	Lang::setCurrent($lang_url);
    //
    //             if( $lang_url !== null && $lang_url === Lang::getCurrent()->url &&
		// 		strpos($this->_lang_url, Lang::getCurrent()->url) === 1 )
    //             {
    //                  $this->_lang_url = substr($this->_lang_url, strlen(Lang::getCurrent()->url)+1);
    //             }
    //     }
    //
    //     return $this->_lang_url;
    // }
    //
    // protected function resolvePathInfo()
    // {
    //     $pathInfo = $this->getLangUrl();
    //
    //     if (($pos = strpos($pathInfo, '?')) !== false) {
    //         $pathInfo = substr($pathInfo, 0, $pos);
    //     }
    //
    //     $pathInfo = urldecode($pathInfo);
    //
    //     // try to encode in UTF8 if not so
    //     // http://w3.org/International/questions/qa-forms-utf-8.html
    //     if (!preg_match('%^(?:
    //         [\x09\x0A\x0D\x20-\x7E]              # ASCII
    //         | [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
    //         | \xE0[\xA0-\xBF][\x80-\xBF]         # excluding overlongs
    //         | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
    //         | \xED[\x80-\x9F][\x80-\xBF]         # excluding surrogates
    //         | \xF0[\x90-\xBF][\x80-\xBF]{2}      # planes 1-3
    //         | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
    //         | \xF4[\x80-\x8F][\x80-\xBF]{2}      # plane 16
    //         )*$%xs', $pathInfo)
    //     ) {
    //         $pathInfo = utf8_encode($pathInfo);
    //     }
    //
    //     $scriptUrl = $this->getScriptUrl();
    //     $baseUrl = $this->getBaseUrl();
    //     if (strpos($pathInfo, $scriptUrl) === 0) {
    //         $pathInfo = substr($pathInfo, strlen($scriptUrl));
    //     } elseif ($baseUrl === '' || strpos($pathInfo, $baseUrl) === 0) {
    //         $pathInfo = substr($pathInfo, strlen($baseUrl));
    //     } elseif (isset($_SERVER['PHP_SELF']) && strpos($_SERVER['PHP_SELF'], $scriptUrl) === 0) {
    //         $pathInfo = substr($_SERVER['PHP_SELF'], strlen($scriptUrl));
    //     } else {
    //         throw new InvalidConfigException('Unable to determine the path info of the current request.');
    //     }
    //
    //     if ($pathInfo[0] === '/') {
    //         $pathInfo = substr($pathInfo, 1);
    //     }
    //
    //     return (string) $pathInfo;
    // }
}
