<?php

return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@tracker-test.tk',
    'user.passwordResetTokenExpire' => '259200',
];
