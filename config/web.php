<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'uk-UA',
    'components' => [
      'i18n' => [
        'translations' => [
          'app*' => [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@app/messages',
            'sourceLanguage' => 'uk-UA',
            'fileMap' => [
              'app' => 'app.php',
            //  'app/error' => 'error.php',
                ],
              ],
            ],
        ],
        /* 'view' => [
			'theme' => [
				'pathMap' => ['@app/views' => '@app/themes/stargazers'],
				'baseUrl' => '@web/themes/stargazers',
			],
		], */
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        //    'enableStrictParsing' => true,
            'rules' => [
                '' => 'site/index',
                'about' => 'site/about',
                'login'=>'site/login',
                'login/<token:\w+>'=>'site/resetpassword',
                'signup'=>'site/signup',
                'logout'=>'site/logout',
                'user/account'=>'user/account',

                'tracker/connections'=>'tracker/connections',
                'tracker/trackergps'=>'tracker/trackergps',
                'tracker/trackeractions'=>'tracker/trackeractions',
                'tracker/showlast'=>'tracker/showlast',
                'tracker/subtasksview'=>'tracker/subtasksview',
                'tracker/newdata'=>'tracker/newdata',
                'tracker/<id:\d+>'=>'tracker/show',

                'tracker/<id:\w+>'=>'tracker/show',

                'trackerapi/'=>'trackerapi/index',
                'trackerapi/login'=>'trackerapi/login',
                'trackerapi/<imei:\w+>'=>'trackerapi/show',

                'admin/trackerpage'=>'admin/addtrackerpage',
                'user/useraccount'=>'user/useraccount',
                'company/taskdataformap'=>'company/taskdataformap',
                'company/obd'=>'company/obd',
                'company/getdataforreport'=>'company/getdataforreport',
                'company/getcompanyinfo'=>'company/getcompanyinfo',
                'company/taskupdate'=>'company/taskupdate',
                'company/taskdelete'=>'company/taskdelete',
                'company/savedropedfile'=>'company/savedropedfile',
                'company/deletefile'=>'company/deletefile',
                'company/taskspage'=>'company/taskspage',
                'company/driverupdate'=>'company/driverupdate',
                'company/carupdate'=>'company/carupdate',
                'company/companygettracker'=>'company/companygettracker',
                'company/companydriver'=>'company/companydriver',
                'company/usergraph'=>'company/usergraph',
                'company/driverdelete'=>'company/driverdelete',
                'company/cardelete'=>'company/cardelete',
                'company/companytrackergps'=>'company/companytrackergps',
                'company/companyupdate'=>'company/companyupdate',
                'company/userdelete'=>'company/userdelete',
                'company/companyupdateuser'=>'company/companyupdateuser',
                'company/companyupdatetracker'=>'company/companyupdatetracker',
                'company/trackerdelete'=>'company/trackerdelete',
                'company/companyadduser'=>'company/companyadduser',
                'company/<urlname:\w+>'=>'company/show',
                'company/<urlname:\w+>/<action:\w+>'=>'company/<action>',


                'user/<username:\w+>'=>'user/show',
                'user/<username:\w+>/tracker/<request:\w+>'=>'tracker/show',
            ],
        ],
        'assetManager' => [
                'basePath' => '@webroot/assets',
                'baseUrl' => '@web/assets'
            ],
        'request' => [
            'baseUrl' => '',
            'class' => 'app\components\LangRequest',
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'fG4d3SbhAHGAOvH9kU_huekg2gcvMSY8',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
              'class' => 'Swift_SmtpTransport',
              'host' => 'mx1.hostinger.com.ua',
              'username' => 'support@tracker-test.tk',
              'password' => '93179317',
              'port' => '587',
              'encryption' => 'tls',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
