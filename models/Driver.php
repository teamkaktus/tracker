<?php

namespace app\models;
use Yii;
use \yii\web\IdentityInterface;
use yii\helpers\Url;
use app\models\Company;

class Driver extends \yii\db\ActiveRecord 
{
    public $car_id;
            
    public static function tableName()
    {
        return 'drivers';
    }

    public static function primaryKey()
    {
        return array('id');
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'id',
            'first_name' => \Yii::t('app','Ім`я'),
            'last_name' => \Yii::t('app','Прізвище'),
            'phone_number' => \Yii::t('app','Номер телефону'),
        );
    } 
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->time_added = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function scenarios()
    {
        return [
            'adddriver' => ['first_name', 'last_name', 'phone_number','car_id','company_id'],
            'updatedriver' => ['first_name', 'last_name', 'phone_number','car_id'],
        ];
    }

    public function rules()
    {
        return [
            [['first_name', 'driver_id', 'last_name', 'phone_number'], 'required'],
        ];
    }
}
