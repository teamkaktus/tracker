<?php

namespace app\models;
use app\models\Tracker;
class Trackerinfo extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'tracker_info';
    }

    public static function primaryKey()
    {
        return array('id');
    }
    public  function getTracker()
    {
        return $this->hasMany(Tracker::className(), ['IMEI' => 'tracker_id']);
    }

    /* public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'Title',
            'content' => 'ВАШ КОНЕНТ',
            'post_time' => 'post_time',
            );
    } */

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
          //  $this->user_id = \Yii::$app->user->id;
            //$this->post_time = date("Y-m-d H:i:s");
//            $this->created = new Expression('NOW()');
//            $command = static::getDb()->createCommand("select max(id) as id from posts")->queryAll();
//            $this->id = $command[0]['id'] + 1;
        }

//        $this->updated = new Expression('NOW()');
        return parent::beforeSave($insert);
    }
/*
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['parent_id' => 'id']);
    } */

    public function rules()
    {
        return [
            [['lat', 'lng', 'tracker_id', 'time'], 'required'],
        ];
    }

}
