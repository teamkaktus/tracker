<?php

namespace app\models;
use app\models\Tracker;
use app\models\User;

class Trackeruser extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'tracker_user';
    }

    public static function primaryKey()
    {
        return array('id');
    }
    
    public function rules()
    {
        return [
            [['tracker_id', 'user_id'], 'required'],
        ];
    }

}
