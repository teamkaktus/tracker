<?php

namespace app\models;
use Yii;
use \yii\web\IdentityInterface;
use yii\helpers\Url;
use app\models\Company;

class Carinfo extends \yii\db\ActiveRecord 
{
    public static function tableName()
    {
        return 'car_info';
    }

    public static function primaryKey()
    {
        return array('id');
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'id',
            'marka' => \Yii::t('app','Марка авто'),
            'number' => \Yii::t('app','Номер авто'),
            'change_wheels' => \Yii::t('app','Заміна коліс'),
            'change_oil' => \Yii::t('app','Заміна мастила'),
            'period_change_wheels' => \Yii::t('app','Період заміни коліс/км'),
            'period_change_oil' => \Yii::t('app','Період заміни мастила/км'),
            'image_src' => \Yii::t('app','Фото автомобіля'),
            'tracker_imei' => \Yii::t('app','Трекер'),
            'company_id' => \Yii::t('app','Компанія'),
        );
    } 

    public function scenarios()
    {
        return [
            'addcar' => ['marka','number','change_wheels','change_oil','period_change_wheels','period_change_oil','image_src','tracker_imei', 'company_id'],
            'updatecar' => ['marka','number','change_wheels','change_oil','period_change_wheels','period_change_oil','image_src','tracker_imei'],
        ];
    }

    public function rules()
    {
        return [
            [['marka','number','tracker_imei'],'required'],
        ];
    }
}
