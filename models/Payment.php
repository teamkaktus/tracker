<?php

namespace app\models;
use Yii;
use \yii\web\IdentityInterface;
use yii\helpers\Url;

class Payment extends \yii\db\ActiveRecord 
{
    public static function tableName()
    {
        return 'payment';
    }

    public static function primaryKey()
    {
        return array('id');
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              //$this->time_active = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function scenarios()
    {
        return [
            'add_payment' => ['id_buyer', 'type', 'time_deactive','count_money','time_active'],
        ];
    }

    public function rules()
    {
        return [
            [['id_buyer', 'type', 'time_active', 'time_deactive','count_money'], 'required'],
        ];
    }
}
