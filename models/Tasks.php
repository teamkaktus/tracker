<?php

namespace app\models;
use Yii;
use \yii\web\IdentityInterface;
use yii\helpers\Url;
use app\models\Company;

class Tasks extends \yii\db\ActiveRecord 
{
    public static function tableName()
    {
        return 'tasks';
    }

    public static function primaryKey()
    {
        return array('id');
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'id',
            'discription' => \Yii::t('app','Опис'),
            'tracker_id' => \Yii::t('app','Трекер'),
            'due' => \Yii::t('app','Дія'), 
            'status' => \Yii::t('app','Статус'), 
            'company_id' => \Yii::t('app','Назва компанії'), 
            'type' => \Yii::t('app','Тип завдання'), 
            'time_start' => \Yii::t('app','Час початку завдання'), 
            'time_end' => \Yii::t('app','Час закінчення завдання'), 
            'travel_mode' => \Yii::t('app','Тип поїздки')
        );
    } 
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->time_added = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function scenarios()
    {
        return [
            'addtask' => ['description', 'due', 'company_id', 'status', 'type'],
            'addroute' => ['description', 'due', 'company_id', 'status', 'type', 'time_start', 'time_end', 'for_map', 'travel_mode'],
        ];
    }

    public function rules()
    {
        return [
            [['discription', 'tracker_id', 'due', 'status', 'company_id', 'type', 'time_start', 'time_end', 'for_map', 'travel_mode'], 'required'],
        ];
    }
}
