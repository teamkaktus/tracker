<?php

namespace app\models;
use Yii;
use \yii\web\IdentityInterface;
use yii\helpers\Url;

class OBD extends \yii\db\ActiveRecord 
{
    public static function tableName()
    {
        return 'obd';
    }

    public static function primaryKey()
    {
        return array('id');
    }

    public function rules()
    {
        return [
            [['imei', 'date', 'mileage', 'instant_fuel', 'average_fuel', 'driving_time', 'speed', 'power_load', 'water_temperature', 'engine_speed', 'battery_voltage', 'dtc1', 'dtc2', 'dtc3', 'dtc4', 'type'], 'required'],
        ];
    }
}
