<?php

namespace app\models;

use \yii\web\IdentityInterface;
use yii\helpers\Url;

class Company extends \yii\db\ActiveRecord implements IdentityInterface
{
    public static function tableName()
    {
        return 'company';
    }

    public static function primaryKey()
    {
        return array('id');
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'id',
            'organization_name' => \Yii::t('app','Назва організації'),
            'email' => \Yii::t('app','E-mail'),
            'phone_number' => \Yii::t('app','Номер телефону'), 
            'urlname' => \Yii::t('app','Назва компанії'),
            'address' => \Yii::t('app','Адреса'),
            'country' => \Yii::t('app','Країна'),
            'city' => \Yii::t('app','Місто'),
            'zip' => \Yii::t('app','Zip/Postal Code'),
            'discription' => \Yii::t('app','Опис'),
            'last_update' => \Yii::t('app','Останнє редагування'),
            'time_deactive' => \Yii::t('app','Дата деактивації'),
        );
    }
    
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }
    
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->time_added = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert);
    }

    public static function findByUsername($username)
    {
        return static::findOne(array('username' => $username));
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }
    
    public static function getCompanyname($id)
    {
        $companyModel = Company::find()->where(['id' => $id])->one();
        return $companyModel->organization_name;
    }

    public function validatePassword($password)
    {
        return \Yii::$app->security->validatePassword($password, $this->password_hash);
//        return $this->password === $password;
    }

    public function scenarios()
    {
        return [
            'addsubcompany' => ['organization_name', 'urlname', 'email', 'phone_number', 'parent_id', 'type_company'],
            'addcompany' => ['organization_name', 'urlname', 'email', 'phone_number', 'type_company'],
            'activecompany' => ['active'],
            'timeDeactive' => ['time_deactive','active'],
            'updatecompany' => ['organization_name', 'urlname', 'email', 'phone_number', 'country', 'city', 'address', 'zip', 'discription', 'last_update'],
        ];
    }
    public function validateUrlname()
   {
     if(preg_match("/[a-zA-Z0-9]/", $this->urlname)){
       $this->addError('urlname', 'Url name must have [a-zA-Z0-9]');
     }
   }

    public function rules()
    {
        return [
            [['organization_name','email','phone_number', 'urlname', 'active'], 'required'],
            ['email','email'],
            [['organization_name', 'email', 'urlname'],'unique'],
            ['urlname', 'string', 'min'=>5, 'max' => 14],
            ['urlname', 'match','pattern'=>'/[a-zA-Z0-9]/'],

        ];
    }
}
