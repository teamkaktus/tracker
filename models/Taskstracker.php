<?php

namespace app\models;
use app\models\Tracker;
use app\models\User;

class Taskstracker extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'tasks_tracker';
    }

    public static function primaryKey()
    {
        return array('id');
    }
    
    public function rules()
    {
        return [
            [['tracker_id', 'task_id'], 'required'],
        ];
    }

}
