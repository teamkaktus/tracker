<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lang".
 *
 * @property integer $id
 * @property string $url
 * @property string $local
 * @property string $name
 * @property integer $default
 * @property integer $date_update
 * @property integer $date_create
 */
class Lists extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lists';
    }
   
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type', 'parent_id','url_name'], 'required'],
        ];
    }
    
     public function scenarios()
    {
        return [
            'report' => ['type', 'name'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
            'type' => Yii::t('app', 'Тип'),
            'parent_id' => Yii::t('app', 'Батьківський елемент'),
            'url_name' => Yii::t('app', 'Артикль'),
        ];
    }
    
    static $current = null;


}
