<?php

namespace app\models;

class Tracker extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'tracker';
    }

    public static function primaryKey()
    {
        return array('id');
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'id',
            'IMEI' => \Yii::t('app','IMEI'),
            'time_added' => \Yii::t('app','Дата реєстрування'),
            'name' => \Yii::t('app','Назва'),
            'discription' => \Yii::t('app','Опис'),
            'company_id' => \Yii::t('app','Назва Компанії'),
            'user_type' => \Yii::t('app','Тип користувача'),
            'speed_limit' => \Yii::t('app','Максимально доступна швидкість'),
            'tracker_marker' => \Yii::t('app','Тип маркера'),
        );
    } 

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->time_added = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function rules()
    {
        return [
            [['IMEI', 'user_id', 'discription', 'name', 'company_id','user_type','speed_limit','tracker_marker'], 'required'],
            ['IMEI','unique']
        ];
    }

}
