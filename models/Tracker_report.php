<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lang".
 *
 * @property integer $id
 * @property string $url
 * @property string $local
 * @property string $name
 * @property integer $default
 * @property integer $date_update
 * @property integer $date_create
 */
class Tracker_report extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tracker_report';
    }
   
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_id', 'tracker_id'], 'required'],
        ];
    }
    
     public function scenarios()
    {
        return [
            'tracker_report' => ['report_id', 'tracker_id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'report_id' => Yii::t('app', 'Звіти'),
            'tracker_id' => Yii::t('app', 'Трекер'),
        ];
    }
    
    static $current = null;


}
