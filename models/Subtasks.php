<?php

namespace app\models;
use Yii;
use \yii\web\IdentityInterface;
use yii\helpers\Url;
use app\models\Company;

class Subtasks extends \yii\db\ActiveRecord 
{
    public static function tableName()
    {
        return 'sub_tasks';
    }

    public static function primaryKey()
    {
        return array('id');
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'id',
            'marking' => \Yii::t('app','Позначення'),
            'task_id' => \Yii::t('app','Завдання'),
            'address' => \Yii::t('app','Адреса'),
            'distance' => \Yii::t('app','Пройдена Відстань'),
        );
    } 
    
    public function scenarios()
    {
        return [
            'add_sub_task' => ['marking', 'task_id','address','distance','description','start_time','end_time','start_date','end_date'],
        ];
    }

    public function rules()
    {
        return [
            [['marking', 'task_id','address','distance'], 'required'],
        ];
    }
}
