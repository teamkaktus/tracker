<?php

namespace app\models;
use Yii;
use \yii\web\IdentityInterface;
use yii\helpers\Url;
use app\models\Company;

class Drivercar extends \yii\db\ActiveRecord 
{
    public static function tableName()
    {
        return 'driver_car';
    }

    public static function primaryKey()
    {
        return array('id');
    }

    public function rules()
    {
        return [
            [['driver_id', 'car_id'], 'required'],
        ];
    }
}
