<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/style.css',
        'css/style-responsive.css',
        'css/font-awesome.min.css',
        'css/glyphicons.css',
        'css/bootstrap-timepicker.min.css',
        'css/bootstrap-timepicker.css',
        'css/bootstrap-datetimepicker.min.css',
        'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext',
    ];
    public $js = [
        'js/site.js',
        'https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true',
        'http://www.google.com/jsapi',  
        'js/markerclusterer.js', 
        'js/mapsLine.js',  
        'js/bootstrap-timepicker.min.js',  
        'js/bootstrap-timepicker.js',  
        'js/bootstrap-datepicker.js',  
        'js/bootstrap-datetimepicker.min.js',  
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
