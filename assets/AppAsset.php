<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/animate.css',
        'css/font-awesome.min.css',
        'css/glyphicons.css',
        'css/bootstrap-timepicker.min.css',
        'css/bootstrap-timepicker.css',
        '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css',
        'css/bootstrap-datetimepicker.min.css',
        'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext',
        'css/bootstrap-multiselect.css',
        'css/sweetalert.css',
        'css/preloader.css',
    ];
    public $js = [
        'js/bootstrap-multiselect.js',
        'js/markerclusterer.js',
        'js/bootstrap-timepicker.min.js',
        'js/bootstrap-timepicker.js',
        'js/bootstrap-datepicker.js',
        'js/bootstrap-datetimepicker.min.js',
        'js/site.js',
        'js/trackerShow.js',
        'js/sweetalert.min.js',
        'js/date.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
