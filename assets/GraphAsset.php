<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

class GraphAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        'css/style.css',
//        'css/style-responsive.css'
    ];
    public $js = [
        'https://www.google.com/jsapi',
        'js/graph.js',
        'js/jquery.battatech.excelexport.min.js',
        'js/jquery.battatech.excelexport.js',
        
        'https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=geometry',        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
