<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

class NewhomeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/newdesign/default.css',
        'css/newdesign/demo.css',
        'css/newdesign/nivo-lightbox.css',
        'css/newdesign/owl.carousel.css',
        'css/newdesign/owl.theme.css',
        'css/newdesign/responsive.css',
        'css/newdesign/styles.css',
        'css/newdesign/blue.css',
        'css/newdesign/animate.min.css',
        'css/newdesign/styles.css',
        'css/newdesign/icons.css',
        'http://fonts.googleapis.com/css?family=Dosis:400,500,700',
        
    ];
    public $js = [
        'js/newdesign/custom.js',
        'js/newdesign/demo.js',
        'js/newdesign/jquery.simple-text-rotator.min.js',
        'js/newdesign/jquery.stellar.min.js',
        'js/newdesign/nivo-lightbox.min.js',
        'js/newdesign/owl.carousel.min.js',
        'js/newdesign/retina.min.js',
        'js/newdesign/wow.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
