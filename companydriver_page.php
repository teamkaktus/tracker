<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\widgets\LinkPager;
    use yii\bootstrap\Alert;
    use dosamigos\multiselect\MultiSelect;
    use app\assets\MyAsset;
    use app\assets\GraphUserAsset;
    use app\assets\HomepageAsset;
    
    MyAsset::register($this);
    HomepageAsset::register($this);
    GraphUserAsset::register($this);
?>
<div class="dashboard-container">
    <?php
    if(Yii::$app->session->hasFlash('DriverAdded')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Driver added!',
        ]);
    endif;

    if(Yii::$app->session->hasFlash('DriverNotAdded')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => 'Driver not added!',
        ]);

    endif;
    
    if(Yii::$app->session->hasFlash('DriverDelete')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => 'Driver delete!',
        ]);

    endif;
    
    
    if(Yii::$app->session->hasFlash('DriverUpdate')): ?>
        <?php
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Driver Update',
        ]);
    endif;

    if(Yii::$app->session->hasFlash('DriverNotUpdate')): ?>
        <?php
        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => 'Driver not update',
        ]);
    endif;
    ?>
      <div class="container">
        <!-- Top Nav Start -->
        <div id="cssmenu">
            <?php echo $this->render('menu',['urlname' => $urlname, 'parentCompany' => $parentCompany]); ?>
        </div>
        <!-- Top Nav End -->

        <!-- Sub Nav End -->
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <a href="site/index" style="margin-left:9px;font-size:15px;padding:0px;"><i class="fa fa-home"></i> Home </a><a style="font-size:12px;padding:0px"> <i class="fa fa-arrow-right"> </i> </a><a href="/admin/index" style="font-size:15px;padding:0px;"> User page</a>
            </li>
          </ul>
        </div>
        <!-- Sub Nav End -->

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
          
          <!-- Row starts -->
          <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">ALl driver</div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 100<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-globe"></i>
                  </div>
                  <div class="pull-right number"><?= $statistic['driverCount']; ?></div>
                </div>
<!--                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> -->
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Active drivers</div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i><?= $statistic['activeDriverProcent']; ?><sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-twitter"></i>
                  </div>
                  <div class="pull-right number"><?= $statistic['activeDriverCount']; ?></div>
                </div>
<!--                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> -->
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="mini-widget">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Deactive drivers</div>
                  <div class="pull-right"><i class="fa fa-angle-down"></i><?= $statistic['deactiveDriverProcent']; ?><sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-upload"></i>
                  </div>
                  <div class="pull-right number"><?= $statistic['deactiveDriverCount']; ?></div>
                </div>
<!--                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> -->
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="mini-widget mini-widget-grey">
                <div class="mini-widget-heading clearfix">
                  <div class="pull-left">Signups</div>
                  <div class="pull-right"><i class="fa fa-angle-up"></i> 67.1<sup>%</sup></div>
                </div>
                <div class="mini-widget-body clearfix">
                  <div class="pull-left">
                    <i class="fa fa-coffee"></i>
                  </div>
                  <div class="pull-right number">1135</div>
                </div>
<!--                 <div class="mini-widget-footer center-align-text">
                  <span>Better than last week</span>
                </div> -->
              </div>
            </div>
          </div>
          <!-- Row ends -->

          <!-- Row Start -->
          <div class="row">
              
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <i class="icon-angle-right boxClick" data-action="show"> </i> Add Driver
                    <span class="mini-title">
                        Click icon for show add form
                    </span>
                  </div>
                </div>
                <div class=" widget-body boxShow" <?=(Yii::$app->session->hasFlash('ErrorValidating'))?'':'style="display:none;"'?>>
                    <?php  $form = ActiveForm::begin(['id' => 'NewUser', 'action' => '../../company/'.$urlname.'/companydriver', 'enableClientValidation'=>true, 'class' => 'form-control', 'method' => 'POST']); ?>
                    <input type="hidden" name='company_id' value='<?= $companyModel->id; ?>'>
                    <?= $form->field($newDriverModel, 'first_name')->textInput(['placeholder' => 'First name', 'class' => 'form-control required']); ?>
                    <?= $form->field($newDriverModel, 'last_name')->textInput(['placeholder' => 'Last name','class' => 'form-control required']); ?>
                    <?= $form->field($newDriverModel, 'phone_number')->textInput(['placeholder' => 'Phone namber', 'class' => 'form-control']); ?>
                      
                    <?=  MultiSelect::widget([
                            'id'=>"multiXX",
                            'options' => [],
                            'data' => $trackerArray, 
                            'name' => 'tracker_driver', 
                            "clientOptions" =>
                                [
                                    'numberDisplayed' => 2,
                                    'enableFiltering' => true,
                                ],
                        ]);
                    echo '<br><br>';
                    ?>
                    
                    <?= Html::submitButton(\Yii::t('app', 'Add'), ['class' => 'btn-submit btn btn-primary']); ?>
                <?php ActiveForm::end();  ?>
                </div>
              </div>
            </div>
            
        
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    Driver
                    <span class="mini-title">
                        All driver with this company
                    </span>
                  </div>
                </div>
                <div class=" widget-body">
                    
                <table class="table table-responsive table-striped table-bordered table-hover no-margin">
                
        <tr style="font-weight:bold;color:black;">
            <td><b>First name</b></td>
            <td><b>Last name</b></td>
            <td><b>Phone number</td>
            <td><b>Tracker IMEI</b></td>
            <td colspan="2"></td>
        </tr>
        <tr style="font-weight:bold; color:black;">
            <form method="get" class="form-control">
            <td><input type="text" name="first_name" class="form-control" placeholder="First name" value="<?= $_GET['first_name']; ?>"></td>
            <td><input type="text" name="last_name" class="form-control" placeholder="Last name" value="<?= $_GET['last_name']; ?>"></td>
            <td><input type="text" name="phone_number" class="form-control" placeholder="Phone number" value="<?= $_GET['phone_number']; ?>"></td>
            <td>
                <?php 
                    $tracker_a = $trackerArray;
                    
                    array_unshift($tracker_a, 'Choise Tracker...')
                        ?>
                <?=  MultiSelect::widget([
                    'id'=>"multiSearch",
                    'options' => [],
                    'data' => $tracker_a,  
                    'value' => $_GET['tracker_driver_Search'],
                    'name' => 'tracker_driver_Search', 
                    "clientOptions" =>
                        [
                            'numberDisplayed' => 2,
                            'enableFiltering' => true,
                        ],
                ]); ?>
            </td>
            <td colspan="2"><input type="submit" name="SearchDriver" class="form-control" value="Search"></td>
            </form>
        </tr>
        <?php

        foreach($modelDriver as $driver){ ?>
            <?php
                $driverChoice = [];
                    foreach($driverTracker as $driverC){
                        if($driver->id == $driverC->driver_id){
                            $driverChoice = $driverC->tracker_id;
                        }
                    } 
            ?>
            <tr style="color:black;">
                <td><?= $driver->first_name; ?></td>
                <td><?= $driver->last_name; ?></td>
                <td><?= $driver->phone_number; ?></td>
                <td><a href='/../../../tracker/<?= $driverChoice; ?>'><?= $driverChoice; ?></a></td>
                <td align="center">
                    <a href='../../company/driverdelete?id=<?= $driver->id; ?>&company_id=<?= $companyModel->id; ?>' class="delete" title='delete'><i class="fa fa-times fa-1x" style="color:#3187bf;"></i></a>
                </td>
                <td align="center">
                    <a data-toggle="modal" class="KursorShow" data-target="#myModal<?= $driver->id; ?>"><i class='fa fa-pencil-square-o fa-1x' style="color:#3187bf;"></i></a>
                </td>
            </tr>
        <div class="modal fade" id="myModal<?= $driver->id; ?>" role="dialog">
            <div class="modal-dialog">
              <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title" style='color:black;'>Change driver</h4>
                    </div>
                    <div class="modal-body" style='color:black;'>
                    <?php $form = ActiveForm::begin(['id' => 'UpdateDriver', 'action' => '../../company/driverupdate', 'class' => 'form-control', 'method' => 'POST']);?>
                        <input type="hidden" name='company_urlname' value='<?= $companyModel->urlname; ?>'>
                        <input type="hidden" name='driver_id' value='<?= $driver->id; ?>'>
                        <?= $form->field($driver, 'first_name')->textInput(['placeholder' => 'First name', 'class' => 'form-control required']); ?>
                        <?= $form->field($driver, 'last_name')->textInput(['placeholder' => 'Last name','class' => 'form-control required']); ?>
                        <?= $form->field($driver, 'phone_number')->textInput(['placeholder' => 'Phone namber', 'class' => 'form-control']); ?>
                        <label>Tracker</label><br>
                        <?=  MultiSelect::widget([
                                'id'=>"multi".$driver->id,
                                'options' => [],
                                'data' => $trackerArray, 
                                'value' => $driverChoice, 
                                'name' => 'tracker_driver', 
                                "clientOptions" =>
                                    [
                                        'numberDisplayed' => 2,
                                        'enableFiltering' => true,
                                    ],
                            ]); ?>
                    </div>
                    <div class="modal-footer">
                            <?php echo Html::submitButton(\Yii::t('app', 'Save'), array('class' => 'btn btn-primary')); ?>
                            <?php ActiveForm::end(); ?>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>   
             <?php       
             }
            ?>
             </div>   
                </table>
                    <?= LinkPager::widget(['pagination'=>$pagination]); ?>
                </div>
                </div>
            </div>
            </div>
        
              
          </div>
          <!-- Row End -->
        
        </div>
        <!-- Dashboard Wrapper End -->

        <footer>
          <p>© Tracker system 2015</p>
        </footer>

      </div>
    </div>
