    google.load("visualization", "1", {packages:["corechart"]});

    function drawChart(aRes) {
        var data = google.visualization.arrayToDataTable(aRes);

        var options = {
          title: "Graph Tracker",
          hAxis: {title: 'Year',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }
    
    function drawChartTwo(data,data2) {
        var data = google.visualization.arrayToDataTable(data);

        var options = {
          title: data2,
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
    
    function drawChartThree(data) {
        var data = google.visualization.arrayToDataTable(data);

        var options = {
          title: 'Sub company',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d_two'));
        chart.draw(data, options);
      }
      
      function drawChartUser(data) {
        var data = google.visualization.arrayToDataTable(data);

        var options = {
          title: 'User company',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d_user'));
        chart.draw(data, options);
      }
$('document').ready(function(){
    
    $("#btnExport").click(function () {
        $("#tblExport").battatech_excelexport({
            containerid: "tblExport", 
            datatype: 'table'
        });
    });
    
    
    $('#preloader').show();
    //
    //company reports page
    var markerByDate = []
    $("#searchGrap").click(function(event){
        event.preventDefault();
        
        var tracker_id = [];
        $("#multiTracker option:selected").each(function() {
            tracker_id.push($( this ).val());
        });
        var textVid = $('.textVid').val();
        var textDo = $('.textDo').val();
        if(tracker_id == ''){
            alert('Sorry!');
        }else{
            $.ajax({
                type: 'POST',
                url: '../../../company/companygettracker',
                data: {tracker_id: tracker_id, newVid: textVid, newDo: textDo},
                dataType: "json",
                success: function(response){
                    //console.log(response);
                    //if(response == ' '){
                      
                    markerByDate = [];
                    for(var ArrVal in response) {
                        var marker = response[ArrVal];
                        if(typeof markerByDate[marker['time'].slice(0,marker['time'].indexOf(' '))] == "undefined" ){
                             markerByDate[marker['time'].slice(0,marker['time'].indexOf(' '))] =[]
                        }
                        if(typeof markerByDate[marker['time'].slice(0,marker['time'].indexOf(' '))][marker['IMEI']] == "undefined" ){
                             markerByDate[marker['time'].slice(0,marker['time'].indexOf(' '))][marker['IMEI']] ={}
                            markerByDate[marker['time'].slice(0,marker['time'].indexOf(' '))][marker['IMEI']]['path'] = []

                        }
                        
                        markerByDate[marker['time'].slice(0,marker['time'].indexOf(' '))][marker['IMEI']]["name"] = marker.name;
                        markerByDate[marker['time'].slice(0,marker['time'].indexOf(' '))][marker['IMEI']]['path'].push(new google.maps.LatLng(marker['lat'], marker['lng']))
                    }
                    for(var $key in markerByDate){
                        for(var $key2 in markerByDate[$key]){
//                        console.log(markerByDate[$key]);
                        markerByDate[$key][$key2]['width'] = google.maps.geometry.spherical.computeLength(markerByDate[$key][$key2]['path']) 
                        delete markerByDate[$key][$key2]['path']
                            //markerByDate[$key][$key2]['name'] = markerByDate[$key][$key2][0].name
                    }
                        
                    }
                    //console.log(lastArrayZ);
                    //console.log(markerByDate);
                    var lastArrayZ = [];
                    for(var val in markerByDate){
                        var qwe = markerByDate[val];
                        
                        for(var val2 in qwe){
                            var track = qwe[val2];
                            //console.log(track['name']);
                            lastArrayZ.push({'tracker_name' : track['name'], 'year' : val, 'widths_day' : track['width']});
                        }
                    }
                    
               var aRes=[];
                    aRes[0]=['Year'];
                    for(var ob in lastArrayZ){
                        if(aRes[0].indexOf(lastArrayZ[ob].tracker_name)<0){
                            aRes[0].push(lastArrayZ[ob].tracker_name);
                        }
                    }	
                    var leng = aRes[0].length
                    var years = []

                    for(var ob in lastArrayZ){
                        if(years.indexOf(lastArrayZ[ob].year)< 0){
                            years.push(lastArrayZ[ob].year);
                        }
                    }

                    for(j in years){
                        tmp =[years[j]] 
                        for(var i = 0; i < leng-1; i++) {
                                tmp.push('');
                        }
                        aRes.push(tmp)
                    }
                    console.log(lastArrayZ)
                    var aResTable = [];
                    for(var ob in lastArrayZ){
                        var tracker = lastArrayZ[ob];
                        if(typeof aResTable[tracker.year] == "undefined" ){
                            aResTable[tracker.year] = [];
                        }
                        aResTable[tracker.year][tracker.tracker_name] = tracker.widths_day;                        
                        for(var el in aRes){
                            if(aRes[el][0] == lastArrayZ[ob].year){
                                var tmp = [];
                                aRes[el][aRes[0].indexOf(lastArrayZ[ob].tracker_name)] =  lastArrayZ[ob].widths_day;
                                //aResTable = 
                            }
                        }
                    }
                    
                    var tableData = '<table id="tblExport" border="1" style="display:none;">';
                    for(var day in aResTable){
                        var arraytracker = aResTable[day];
                        tableData += '<tr><td colspan="2"><h3>'+day+'</h3></td></tr>';
                        
                        for(var tracker_info in arraytracker){
                            tableData += '<tr><td>'+tracker_info+'</td><td>'+arraytracker[tracker_info]+'</td></tr>';
                        }
                    }
                    
                    tableData += '</table>';
                    $('.tableData').append(tableData);
                    //console.log(aResTable);
                    
                    google.load("visualization", "1", {packages:["corechart"]});
                    drawChart(aRes);    
//                }else{
//                    alert('Sorry array empty');
//                }
                }
            });
            
        }
        //console.log(tracker_id); 
        
    });
    
    //end company reports page 
    //
    
    
    //
    //admin reports page
    $("select[name='listname']").change(function(){
        var role = $(this).val();
        //alert(role);
        $.ajax({
            type: 'POST',
            url: '../../../admin/reportsvalue',
            data: {data: $(this).val()},
            dataType: "json",
            beforeSend: function(){
                $('.preloader').show();
            },
            success: function(response){
                $('.preloader').hide();
                if(role == 'user'){
                    var res = [];
                    res.push(['user','tracker_count']);
                    for(var user in response){
                        res.push([user,response[user]]);
                    }
                    drawChartTwo(res, 'user tracker');
                    $('#piechart_3d_user').hide();
                    $('#piechart_3d_two').hide();
                }else{
                    var tracker_data = [];
                    var syb_company_data = [];
                    var user_data = [];
                    
                    tracker_data.push(['company','tracker_count']);
                    syb_company_data.push(['company','sub_company_count']);
                    user_data.push(['company','user_count']);
                    
                    for(var company in response){
                        var response2 = response[company];
                        
                        tracker_data.push([company,response2.trackers_company]);
                        syb_company_data.push([company,response2.sub_company]);
                        user_data.push([company,response2.user_company]);
                    }
                    $('#piechart_3d_user').show();
                    $('#piechart_3d_two').show();
                    
                    drawChartTwo(tracker_data, 'company tracker');
                    drawChartThree(syb_company_data);
                    drawChartUser(user_data);
                }
            }
        });
    });
    
    //end admin reports page
    //
    
});
