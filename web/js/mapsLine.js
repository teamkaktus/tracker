$(document).ready(function () {
    function renderDirections(result) {
        var lineSymbol = {
            path: 'M 1.5 1 L 1 0 L 1 2 M 0.5 1 L 1 0',
            fillColor: 'black',
            strokeColor: 'black',
            strokeWeight: 2,
            strokeOpacity: 1
        };
        directionsRenderer = new google.maps.DirectionsRenderer({
            polylineOptions: {
                strokeColor: '#426289',
                strokeOpacity: 0.8,
                strokeWeight: 5,
                icons: [{
                    icon: lineSymbol,
                    offset: '25px',
                    repeat: '100px'
                   }]

            },
            suppressMarkers: true,
            preserveViewport: true
        });
        directionsRenderer.setMap(map);
        directionsRenderer.setDirections(result);
        directions.push(directionsRenderer)
            //directionsRenderer.set('directions', null);
    }
    function drawPolyline(day) {
      for(i in lines)
            lines[i].setMap(null)
        var lineSymbol = {
            path: 'M 1.5 1 L 1 0 L 1 2 M 0.5 1 L 1 0',
            fillColor: 'black',
            strokeColor: 'black',
            strokeWeight: 2,
            strokeOpacity: 1
        };
        var path =[];
        var i = 0
        path[i] = []
        for(key in day){
          value = day[key]
          if(value['type']!='acc off'){
            path[i].push(new google.maps.LatLng(value.lat, value.lng)) //{lat:value.lat,lng:value.lng}
          }else{
            path[i++].push(new google.maps.LatLng(value.lat, value.lng))
            path[i] = []
          }
        }
        for(i in path){
            polyline = new google.maps.Polyline({
              path: path[i],
              strokeColor: '#426289',
              strokeOpacity: 0.8,
              strokeWeight: 5,
              icons: [{
                  icon: lineSymbol,
                  offset: '25px',
                  repeat: '100px'
                 }]
               })
               lines.push(polyline)
               polyline.setMap(map)
               console.log(lines);
          }
        $('#shadow').hide(500);
        $('#preloader').hide(500);
    }

    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
    function drawRouteRec(waypoints, $i) {
        if (($i < waypoints.length - 1) && (!stop)) {
            var start = $i
            if ($i + 9 >= waypoints.length - 1) {
                var end = waypoints.length - 1
            } else {
                var end = $i + 9
            }

            service.route({
                origin: waypoints[start]['location'],
                destination: waypoints[end]['location'],
                travelMode: google.maps.DirectionsTravelMode.DRIVING,
                waypoints: waypoints.slice(start + 1, end)
            }, function (result, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    var distance = 0;
                    for (i = 0; i < result.routes[0].legs.length; i++) {
                        distance += parseFloat(result.routes[0].legs[i].distance.value);
                        //for each 'leg'(route between two waypoints) we get the distance and add it to the total
                    }
                    width += distance
                    renderDirections(result)
                    drawRouteRec(waypoints, end)
                } else {
                    console.error("Directions request failed: " + status);
                    switch (status) {
                        case 'ZERO_RESULTS':
                            drawRouteRec(waypoints, $i + 1)

                            break;
                        case 'OVER_QUERY_LIMIT':
                            var until = new Date().getTime() + 500;
                            while (new Date().getTime() < until) {};
                            drawRouteRec(waypoints, $i)

                            break;
                        default:

                    }
                }
            });
        } else {
            $('#shadow').hide(500);
            $('#preloader').hide(500);
        }
    }

    function drawRoute(aData) {
        /*creating waypoints*/
        var waypoints = []
        for ($key in aData) {
            waypoints.push({
                location: new google.maps.LatLng(aData[$key].lat, aData[$key].lng),
                stopover: true
            })
        }
        var $i = 0;
        var start, end = 0
        width = 0
        for (j in directions)
            directions[j].set('directions', null);
        drawRouteRec(waypoints, 0)

    }

    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
    function getData($from, $to) {
        $('#shadow').show(500);
        $('#preloader').show(500);
        if (typeof $from == "undefined") {
          $from = new Date();
          $from = $from.getFullYear() + '-' +
              ('00' + ($from.getMonth())).slice(-2) + '-' +
              ('00' + $from.getDate()).slice(-2) + ' ' +
              ('00' + $from.getHours()).slice(-2) + ':' +
              ('00' + $from.getMinutes()).slice(-2) + ':' +
              ('00' + $from.getSeconds()).slice(-2);
        }
        if (typeof $to == "undefined") {
          $to = new Date();
          $to = $to.getFullYear() + '-' +
              ('00' + ($to.getMonth() + 1)).slice(-2) + '-' +
              ('00' + $to.getDate()).slice(-2) + ' ' +
              ('00' + $to.getHours()).slice(-2) + ':' +
              ('00' + $to.getMinutes()).slice(-2) + ':' +
              ('00' + $to.getSeconds()).slice(-2);
        }
        $.ajax({
            type: 'POST',
            url: '../../../tracker/trackergps',
            data: {
                newVid: $from,
                newDo: $to,
                tracker_id: tracker_id
            },
            dataType: "json",
            error: function (response) {
                console.error(response);
            },
            success: function (response) {
                if(response.length == 0){
                  swal({
                    title: $('#a_no_data').text(),
                    text: $('#a_data_last_month').text(),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $('#a_yes_show').text(),
                    closeOnConfirm: true
                  },
                  function(){
                        $from = new Date();
                        $from = $from.getFullYear() + '-' +
                            ('00' + ($from.getMonth())).slice(-2) + '-' +
                            ('00' + $from.getDate()).slice(-2) + ' ' +
                            ('00' + $from.getHours()).slice(-2) + ':' +
                            ('00' + $from.getMinutes()).slice(-2) + ':' +
                            ('00' + $from.getSeconds()).slice(-2);
                        $to = new Date();
                        $to = $to.getFullYear() + '-' +
                            ('00' + ($to.getMonth() + 1)).slice(-2) + '-' +
                            ('00' + $to.getDate()).slice(-2) + ' ' +
                            ('00' + $to.getHours()).slice(-2) + ':' +
                            ('00' + $to.getMinutes()).slice(-2) + ':' +
                            ('00' + $to.getSeconds()).slice(-2);
                    getData($from,$to)

                  });
                }
                aDataG = response
                sortMarkersByDate(response)
                //showMarkersOnMap(response)
            }
        });
    }
    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
    function getNewdata($from) {

                var today = new Date();
                today = today.getFullYear() + '-' +

              ('00' + (today.getMonth() + 1)).slice(-2) + '-' +
              ('00' + today.getDate()).slice(-2);
              $to = new Date();
              $to = $to.getFullYear() + '-' +
                  ('00' + ($to.getMonth() + 1)).slice(-2) + '-' +
                  ('00' + $to.getDate()).slice(-2) + ' ' +
                  ('00' + $to.getHours()).slice(-2) + ':' +
                  ('00' + $to.getMinutes()).slice(-2) + ':' +
                  ('00' + $to.getSeconds()).slice(-2);
            {

    $.ajax({
            type: 'POST',
            url: '../../../tracker/newdata',
            data: {
                from: (aDataG.length>0)?aDataG[aDataG.length-1].time:$to,
                tracker_id: tracker_id
            },
            dataType: "json",
            error: function (response) {
                console.error(response);
            },
            success: function (response) {
                    //getData($from,$to)
                //addMarkersToMap(response)
                if(response != []){
                            for (var $i in response) {
                                var marker = response[$i];

                                  if (typeof markersByDate[marker['time'].slice(0, marker['time'].indexOf(' '))] == "undefined") {
                                      markersByDate[marker['time'].slice(0, marker['time'].indexOf(' '))] = []
                                      $key = marker['time'].slice(0, marker['time'].indexOf(' '));
                                      $('#days').prepend('\
                                      <div class="panel panel-primary" id="' + $key + '">\
                                        <div class="panel-heading day"  data-key="' + $key + '" style="height: 35px">\
                                          <h6 style="padding:0px;margin:0px;"><i class="icon-angle-right" data-action="show"></i><span class="break"></span>' + $key + '</h4>\
                                        </div>\
                                        <div class="panel-body day-body" id="' + $key + '" style="display:none;padding:0px;">\
                                          <table class="table table-hover" style="margin-bottom:0px;margin-top:0px;">\
                                              <tbody><tr><td class=" direction" data-id="' + $key + '" style="cursor:pointer;">\
                                                  <a href="#" >'+$('#draw_direction').text()+'</a>\
                                              </td></tr>\
                                              <tr><td class=" activity" data-id="' + $key + '" style="cursor:pointer;">\
                                                  <a href="#">'+$('#show_activity').text()+'</a>\
                                              </td></tr>\
                                              <tr><td class=" polyline" data-id="' + $key + '" style="cursor:pointer;">\
                                                  <a href="#"  >'+$('#draw_polyline').text()+'</a>\
                                              </td></tr></tbody>\
                                          </table>\
                                        </div>\
                                      </div>')

                                  }
                                  markersByDate[marker['time'].slice(0, marker['time'].indexOf(' '))].push(marker);
                                  console.log([daySelected,marker['time'].slice(0, marker['time'].indexOf(' '))]);
                                  if(daySelected == marker['time'].slice(0, marker['time'].indexOf(' '))){
                                    if($('.activity-day').is(":visible")){
                                      if(!((marker.type == '001')||(marker.type == 'tracker'))){
                                        var time = Date.parse(marker['time']);
                                        $('#list-activities').prepend('<tr><td id="one-action" data-id="'+marker['id']+'"><b>'+ marker['type'] +'</b> '+ time.toString("hh:mm:ss tt") +'</td></tr>');
                                      }
                                    }
                                  }

                            }
                }

                if(daySelected == today){
                    addMarkersToMap(response)
                  }
                console.log(response)
                for($i in response)
                aDataG.push( response[$i]);
                // if(cont)
                  setTimeout(function(){getNewdata();},20000)
            }
        });
}
    }
    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
    function showMarkersOnMap(aData) {
      $('#shadow').show();
      $('#preloader').show();
        markersArray = [];
        markerCluster.clearMarkers()
        for(i in lines)
              lines[i].setMap(null)
        var bounds = new google.maps.LatLngBounds();
        var lat;
        var lng;
        var infoWindow = new google.maps.InfoWindow();

        for (var $i in aData) {
          boxText = document.createElement("div"),
//these are the options for all infoboxes
          infoboxOptions = {
            content: boxText,
            //disableAutoPan: false,
            maxWidth: 0,
            pixelOffset: new google.maps.Size(-140,0),
            zIndex: 0,
            boxStyle: {
              //  background: "url('http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/examples/tipbox.gif') no-repeat",
                opacity: 0.75,
                width: "280px"
            },
            closeBoxMargin: "-4px 0px -2px 0px",
            closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
            infoBoxClearance: new google.maps.Size(1, 1),
            isHidden: false,
            //pane: "floatPane",
            enableEventPropagation: true
          };
            var item = aData[$i];
            var icon;
            var atr = '';
            var lnglat = new google.maps.LatLng(item['lat'], item['lng'])
            switch(item['type']){
              // case 'tracker': console.log('here');
              //                       break;
              case "acc on":
                              url = '/content/icons/other_activity/on.png'
                              height = 53
                                    break;
              case "acc off":
                              url = '/content/icons/other_activity/off.png'
                              height = 53
                                    break;
              case "low battery":
                              url = '/content/icons/other_activity/battery_low.png'
                              height = 53
                                    break;
              default :
                          if(parseFloat(item['speed'])<parseFloat(item['speed_limit'])){
                              url = '/content/icons/'+ item['tracker_marker'] +'.png'
                              height = 37
                              break
                          }else{
                              atr = 'red'
                              height = 37
                              url = '/content/icons/red/'+ item['tracker_marker'] +'.png'
                              break
                          }
            }
            var icon = {
                url:url,
                scaledSize: new google.maps.Size(32,height)
            };
            var marker = new google.maps.Marker({
                position: lnglat,
                title: 'Tracker' + item['tracker_id'],
                icon: icon,
            });
            (item['acc'])? acc_status = 'on': acc_status = 'off';
            boxText.style.cssText = "border: 1px solid black; margin-top: 8px; background:#333; color:#FFF; font-family:Arial; color:white; font-size:12px; padding: 5px; border-radius:6px; -webkit-border-radius:6px; -moz-border-radius:6px;";
            boxText.classList.add("animated");
            boxText.classList.add("zoomIn");
//            (parseFloat(item['speed'])<parseFloat(item['speed_limit']))?  red = '': red= 'red';
            boxText.innerHTML = '<i class="fa fa-tachometer pull-right obd2Show"  data-id=' + item['time'].split(" ").join("$$") +' style="cursor:pointer;color:grey;" title="OBD"></i><i class="fa fa-arrow-circle-up pull-right" style="-webkit-transform: rotate(' + item['course']+'deg);-o-transform: rotate(' + item['course']+'deg);-moz-transform: rotate(' + item['course']+'deg);"></i>'+
                    labels.m_time + (Date.parse(item['time'])).toString('yyyy.MM.dd hh:mm:ss tt') + "<br>"+
                    "<p class='"+atr +"'>"+ labels.m_speed + item['speed'] + "</p>" +
                    ""+ labels.m_acc_status + acc_status + "<br>" +
                    labels.m_gps_status + item['gps_status'] + "<br>" +
                    "Share - <a title='Share with facebook' href='http://www.facebook.com/share.php?u=https://www.google.com/maps?q=loc:"+item['lat']+","+item['lng']+"/&title=Positionin google maps' style='color:#337ab7;'  target='_blank'><i class='fa fa-facebook'></i></a> ; "+
                    "<a title='Share with twitter' href='http://twitter.com/intent/tweet?status=TrackerSystem+http://tracker-test.tk+https://www.google.com/maps?q=loc:"+item['lat']+","+item['lng']+"'  style='color:#337ab7;'  target='_blank'><i class='fa fa-twitter'></i></a>  ;  "+
                    "<a title='Share with Google+' href='https://plus.google.com/share?url=https://www.google.com/maps?q=loc:"+item['lat']+","+item['lng']+"' style='color:#337ab7;'  target='_blank'><i class='fa fa-google-plus'></i></a>  ; "+
                    "<a title='Share with Vkontakte' href='http://vk.com/share.php?url=https://www.google.com/maps?q=loc:"+item['lat']+","+item['lng']+"' style='color:#337ab7;'  target='_blank'><i class='fa fa-vk'></i></a>"+
                    "<br>" +
                    "<a href='https://www.google.com/maps?q=loc:"+item['lat']+","+item['lng']+"' style='color:white'>" + labels.m_open_gmaps + "</a><br>";
            marker['id'] = item['id']
            marker['time'] = item['time']
            marker.infobox = new InfoBox(infoboxOptions);
            marker['infobox'].id = item['id']
            google.maps.event.addListener(marker, 'click', function () {
                var clickedMarker = this
                   var oldIcon = clickedMarker.getIcon();
                   var size_x = oldIcon.scaledSize.width;
                   var size_y = oldIcon.scaledSize.height;
                   if((size_y==37)||(size_y)==53){
                   size_x = parseInt(size_x*1.25)
                    size_y = parseInt(size_y*1.25)
                    }
                   var newIcon = {url: oldIcon.url, scaledSize: new google.maps.Size(size_x,size_y)};
                   clickedMarker.setIcon(newIcon);
                   panorama.setPosition(clickedMarker.getPosition());
                  //  panorama.position {lat: 42.345573, lng: -71.098326}
                  //  $('#street-view').show();
                if (clickedMarker['infobox']['content_'].innerHTML.indexOf('position') < 0) {
                    geocoder.geocode({
                        'location': clickedMarker.position
                    }, function (results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                clickedMarker['infobox']['content_'].innerHTML = clickedMarker['infobox']['content_'].innerHTML + $('#m_address').text() + (results[0].formatted_address)
                            } else {
                                window.alert($('#no_results_found').text());
                            }
                        } else {
                            window.alert($('#geocoder_failed').text() + status);
                        }
                    });
                }
                this['infobox'].open(map, clickedMarker)
            });
                google.maps.event.addListener(marker['infobox'],'closeclick',function(){
                    var clickedMarker = markersArray[findWithAttr(markersArray,'id',this.id)]
                   var oldIcon = clickedMarker.getIcon();
                   var size_x = oldIcon.scaledSize.width;
                   var size_y = oldIcon.scaledSize.height;
                   if((size_y==46)||(size_y)==66){
                   size_x = 32
                    size_y = (size_y==46)?37:53
                    }
                   var newIcon = {url: oldIcon.url, scaledSize: new google.maps.Size(size_x,size_y)};
                   markersArray[findWithAttr(markersArray,'id',this.id)].setIcon(newIcon);
                    //removes the marker
                   // then, remove the infowindows name from the array
});
            bounds.extend(marker.position);
            markersArray.push(marker)
        }
        map.fitBounds(bounds);
        //        for (var $k in markersArray)
        //            markersArray[$k].setMap(map);
        markerCluster = new MarkerClusterer(map, markersArray, {maxZoom:14});

        $('#shadow').hide();
        $('#preloader').hide();
    }
    function addMarkersToMap(aData) {
        var lat;
        var lng;
        var addedMarkers = [];
        var bounds = new google.maps.LatLngBounds();
        var infoWindow = new google.maps.InfoWindow();
        for (var $i in aData) {
          boxText = document.createElement("div"),
//these are the options for all infoboxes
          infoboxOptions = {
            content: boxText,
            //disableAutoPan: false,
            maxWidth: 0,
            pixelOffset: new google.maps.Size(-140,0),
            zIndex: 0,
            boxStyle: {
                background: "url('http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/examples/tipbox.gif') no-repeat",
                opacity: 0.75,
                width: "280px"
            },
            closeBoxMargin: "-4px 0px -2px 0px",
            closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
            infoBoxClearance: new google.maps.Size(1, 1),
            isHidden: false,
            //pane: "floatPane",
            enableEventPropagation: true
          };
            var item = aData[$i];
            var icon;
            var atr = '';
            var lnglat = new google.maps.LatLng(item['lat'], item['lng'])
            switch(item['type']){
              // case 'tracker': console.log('here');
              //                       break;
              case "acc on":
                              url = '/content/icons/other_activity/on.png'
                              atr = ''
                              height = 53
                                    break;
              case "acc off":
                              url = '/content/icons/other_activity/off.png'
                              atr = ''
                              height = 53
                                    break;
              case "low battery":
                              url = '/content/icons/other_activity/battery_low.png'
                              atr = ''
                              height = 53
                                    break;
              default :
                          if(parseFloat(item['speed'])<parseFloat(item['speed_limit'])){
                              url = '/content/icons/'+ item['tracker_marker'] +'.png'
                              height = 37
                              break
                          }else{
                              atr = 'red'
                              height = 37
                              url = '/content/icons/red/'+ item['tracker_marker'] +'.png'
                              break
                          }
            }
            var icon = {
                url:url,
                scaledSize: new google.maps.Size(32,height)
            };
            var marker = new google.maps.Marker({
                position: lnglat,
                title: '<h1>Tracker </h1>' + item['tracker_id'],
                icon: icon,
            });
            boxText.style.cssText = "border: 1px solid black; margin-top: 8px; background:#333; color:#FFF; font-family:Arial; color:white; font-size:12px; padding: 5px; border-radius:6px; -webkit-border-radius:6px; -moz-border-radius:6px;";
            boxText.classList.add("animated");
            boxText.classList.add("zoomIn");
//            (parseFloat(item['speed'])<parseFloat(item['speed_limit']))?  red = '': red= 'red';
            (item['acc'])? acc_status = 'on': acc_status = 'off';
  boxText.innerHTML = '<i class="fa fa-tachometer pull-right obd2Show"  data-id=' + item['time'].split(" ").join("$$") +' style="cursor:pointer;color:grey;" title="OBD"></i><i class="fa fa-arrow-circle-up pull-right" style="-webkit-transform: rotate(' + item['course']+'deg);-o-transform: rotate(' + item['course']+'deg);-moz-transform: rotate(' + item['course']+'deg);"></i>'+
        labels.m_time + (Date.parse(item['time'])).toString('yyyy.MM.dd hh:mm:ss tt') + "<br>"+
        "<p class='"+atr +"'>"+ labels.m_speed + item['speed'] + "</p>" +
        ""+ labels.m_acc_status + acc_status + "<br>" +
        labels.m_gps_status + item['gps_status'] + "<br>" +
        "Share - <i class='fa fa-facebook'></i><br>" +
        "<a href='https://www.google.com/maps?q=loc:"+item['lat']+","+item['lng']+"' style='color:white'>" + labels.m_open_gmaps + "</a><br>"
            marker['id'] = item['id']
            marker['time'] = item['time']
            marker.infobox = new InfoBox(infoboxOptions);
            marker['infobox'].id = item['id']
            google.maps.event.addListener(marker, 'click', function () {
                var clickedMarker = this
                   var oldIcon = clickedMarker.getIcon();
                   var size_x = oldIcon.scaledSize.width;
                   var size_y = oldIcon.scaledSize.height;
                   if((size_y==37)||(size_y)==53){
                   size_x = parseInt(size_x*1.25)
                    size_y = parseInt(size_y*1.25)
                    }
                   var newIcon = {url: oldIcon.url, scaledSize: new google.maps.Size(size_x,size_y)};
                   clickedMarker.setIcon(newIcon);
                   panorama.setPosition(clickedMarker.getPosition());
                if (clickedMarker['infobox']['content_'].innerHTML.indexOf('position') < 0) {
                    geocoder.geocode({
                        'location': clickedMarker.position
                    }, function (results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                clickedMarker['infobox']['content_'].innerHTML = clickedMarker['infobox']['content_'].innerHTML + labels.m_address + (results[0].formatted_address)
                            } else {
                                window.alert($('#no_results_found').text());
                            }
                        } else {
                            window.alert($('#geocoder_failed').text() + status);
                        }
                    });
                }
                this['infobox'].open(map, clickedMarker)
            });
                google.maps.event.addListener(marker['infobox'],'closeclick',function(){
                    var clickedMarker = markersArray[findWithAttr(markersArray,'id',this.id)]
                   var oldIcon = clickedMarker.getIcon();
                   var size_x = oldIcon.scaledSize.width;
                   var size_y = oldIcon.scaledSize.height;
                   if((size_y==46)||(size_y)==66){
                   size_x = 32
                    size_y = (size_y==46)?37:53
                    }
                   var newIcon = {url: oldIcon.url, scaledSize: new google.maps.Size(size_x,size_y)};
                   markersArray[findWithAttr(markersArray,'id',this.id)].setIcon(newIcon);
                    //removes the marker
                   // then, remove the infowindows name from the array
});
            bounds.extend(marker.position);
            markersArray.push(marker)
            addedMarkers.push(marker)
        }
        //map.fitBounds(bounds);
         for (var $k in addedMarkers)
        //            markersArray[$k].setMap(map);
        markerCluster.addMarker(addedMarkers[$k])

        return addedMarkers[addedMarkers.length - 1]

    }
    function findWithAttr(array, attr, value) {
        for (var i = 0; i < array.length; i += 1) {
            if (array[i][attr] == value) {
                return i;
            }
        }
    }
    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
    function sortMarkersByDate(aData) {
        markersByDate = []
        for (var $i in aData) {
            var marker = aData[$i];

            if (typeof markersByDate[marker['time'].slice(0, marker['time'].indexOf(' '))] == "undefined") {
                markersByDate[marker['time'].slice(0, marker['time'].indexOf(' '))] = []
            }

            markersByDate[marker['time'].slice(0, marker['time'].indexOf(' '))].push(marker);
        }

        showMarkersOnMap(markersByDate[Object.keys(markersByDate).pop()])
        $('#days').html('')
        for ($key in markersByDate) {
            $('#days').prepend('\
            <div class="panel  panel-primary" id="' + $key + '">\
              <div class="panel-heading day"  data-key="' + $key + '" style="height: 35px">\
                <h6 style="padding:0px;margin:0px;"><i class="icon-angle-right" data-action="show"></i><span class="break"></span>' + $key + '</h4>\
              </div>\
              <div class="panel-body day-body" id="' + $key + '" style="display:none;padding:0px;">\
                <table class="table table-hover" style="margin-bottom:0px;margin-top:0px;">\
                    <tbody><tr><td class=" direction" data-id="' + $key + '" style="cursor:pointer;">\
                        <a href="#" >'+$('#draw_direction').text()+'</a>\
                    </td></tr>\
                    <tr><td class=" activity" data-id="' + $key + '" style="cursor:pointer;">\
                        <a href="#">'+$('#show_activity').text()+'</a>\
                    </td></tr>\
                    <tr><td class=" polyline" data-id="' + $key + '" style="cursor:pointer;">\
                        <a href="#"  >'+$('#draw_polyline').text()+'</a>\
                    </td></tr></tbody>\
                </table>\
              </div>\
            </div>')
        }
        $('#' + Object.keys(markersByDate).pop() + ' .day-body').show()
        daySelected = Object.keys(markersByDate).pop()
        getNewdata()

    }

    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
    /*                                          Main block                                             */
    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

    // $('#street-view').hide()
    var panorama = new google.maps.StreetViewPanorama(
    document.getElementById('street-view'),
    {
      position: {lat: 37.869260, lng: -122.254811},
      pov: {heading: 165, pitch: 0},
      zoom: 1
    });
    // panorama.position ={lat: 37.869260, lng: -122.254811}
    var tracker_id = $('#searchTracker').data('id');


    var markersByDate = [];
    var addMC = [];
    var cont = true;
    var markersArray = [];
    var directions = [];
    var lines = []
    var element = document.getElementById("map");
    var mapTypeIds = [];
    for (var type in google.maps.MapTypeId) {
        mapTypeIds.push(google.maps.MapTypeId[type]);
    }
    mapTypeIds.push("OSM");
    var map = new google.maps.Map(element, {
        center: new google.maps.LatLng(48, 11),
        zoom: 15,
        minZoom: 1,
        maxZoom: 17,
        fullscreenControl:true,
        mapTypeControlOptions: {
            mapTypeIds: mapTypeIds
        }
    });
    map.mapTypes.set("OSM", new google.maps.ImageMapType({
        getTileUrl: function (coord, zoom) {
            // See above example if you need smooth wrapping at 180th meridian
            return "http://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
        },
        tileSize: new google.maps.Size(256, 256),
        name: "OpenStreetMap",
        maxZoom: 18,
        minZoom: 2,
        streetViewControl: false

    }));
    // map.setStreetView(panorama)
    var trafficLayer = new google.maps.TrafficLayer();
        trafficLayer.setMap(map);
    var geocoder = new google.maps.Geocoder;

    var markerCluster = new MarkerClusterer(map, []);
    var service = new google.maps.DirectionsService()
    var directionsRenderer = new google.maps.DirectionsRenderer
    var stop = false //temporary
    var width = 0 //temporary
    var actionsMarkers = [];
    var aDataG = []
    var daySelected = '';
    var labels = {
      'm_time' : $('#m_time').text(),
      'm_speed' : $('#m_speed').text(),
      'm_gps_status' : $('#m_gps_status').text(),
      'm_open_gmaps' : $('#m_open_gmaps').text(),
      'm_acc_status' : $('#m_acc_status').text(),
      'm_address' : $('#m_address').text()
    }
    getData()


    $('#searchTracker').click(function () {
        if ($('.from').val() != "") {
            var $from = $('.from').val()
        }
        if ($('.to').val() != "") {
            var $to = $('.to').val()
        }
        cont = false
        aData = getData($from, $to)
    })
    $(document).on('click', '.day', function () {
        actionsMarkers = [];
        cont = false
        $day = $(this).data('key')
        daySelected = $(this).data('key')
        if ($('#' + $day + '> .day-body').css('display') == 'none') {
            $('.day-body').hide()
            $('#' + $day + '> .day-body').show()
            for (j in directions)
                directions[j].set('directions', null);
            showMarkersOnMap(markersByDate[$day])
        }

    })
    $(document).on('click', '.direction', function (e) {
        e.preventDefault()
        cont = false
        $day = $(this).data('id');
        $('#shadow').show(500);
        $('#preloader').show(500);
        for(i in lines)
              lines[i].setMap(null)
        drawRoute(markersByDate[$day]);
    })
    $(document).on('click', '.polyline', function (e) {
        e.preventDefault()
        $day = $(this).data('id');
        $('#shadow').show(500);
        $('#preloader').show(500);
        for (j in directions)
            directions[j].set('directions', null);
        drawPolyline(markersByDate[$day]);
    })
    $(document).on('click', '.activity', function (e) {
        e.preventDefault()
        // cont = true
        console.log("here");
        console.log(lines);
        $day = $(this).data('id')
//        $('#loader').show()
//        $('#loader').hide()

        var actionsMarkers = []
        $.each(markersByDate[$day],function(i,item){
          if(!((item.type == '001')||(item.type == 'tracker'))){
            var time = Date.parse(item['time']);
            $('#list-activities').prepend('<tr><td id="one-action" data-id="'+item['id']+'"><b>'+ item['type'] +'</b> '+ time.toString("hh:mm:ss tt") +'</td></tr>');
          }
        })
                // for($i in actionsMarkers){
                //     var action = actionsMarkers[$i]
                //     //aDataG.push(action)
                //
                //     var time = Date.parse(action['time']);
                //     $('#list-activities').prepend('<tr><td id="one-action" data-id="'+action['id']+'"><b>'+ action['type'] +'</b> '+ time.toString("hh:mm:ss tt") +'</td></tr>');
                // }
                $('.days').hide(500);
                $('.activity-day').show(500);
                $('#shadow').hide(500);
                 $('#preloader').hide(500);

    })
    $(document).on('click', '.back-acivity', function (e) {
        e.preventDefault()
        //$day = $(this).data('id')
        $('#list-activities').empty()

        $('.activity-day').hide(500);
        $('.days').show(500);
    })
    $(document).on('click', '#one-action', function (e) {
        e.preventDefault()
        $id = $(this).data('id')
        var ind = findWithAttr(markersArray,'id',$id)
        if(typeof ind != 'undefined'){
        map.setZoom(17)
        map.panTo(markersArray[ind].getPosition());
        google.maps.event.trigger(markersArray[ind], 'click');
      }else{
        swal('some error')
      }
    })
    $(document).on('click', '.obd2Show', function (e) {
        e.preventDefault()
        $id = $(this).data('id').split("$$").join(" ")
        //$('#obd_info_marker').empty();
        var ind = findWithAttr(aDataG,'time',$id)
        if(typeof ind != 'undefined'){
          var item = aDataG[ind]
          for($k in item){
            $("#"+$k).text($k + ':'+item[$k])
          }
        }
        $('#obd_info').show();
    });
    $(document).on('click', '#showLast', function (e) {
        e.preventDefault()
        $('.day-body').hide()
        $('.activity-day').hide(500);
        $('.days').show(500);

        $('#' + Object.keys(markersByDate).pop() + ' .day-body').show()
        $id = $(this).data('id')
        $.ajax({
            type: 'POST',
            url: '../../../tracker/showlast',
            data: {
                tracker_id: $id
            },
            dataType: "json",
            error: function (response) {
                console.error(response);
            },
            success: function (response) {
              // if(false){
              $day = response[0]['time'].slice(0, response[0]['time'].indexOf(' '))
                if(typeof markersByDate[$day] !='undefined'){
                  showMarkersOnMap(markersByDate[$day])
                  var ind = findWithAttr( markersByDate[$day],'id',response[0].id)
                }
                if(typeof ind !='undefined'){
                  var i = findWithAttr( addMC,'id',response[0].id)
                  if(typeof i != 'undefined'){
                var pos = addMC[i].getPosition()
                google.maps.event.trigger(addMC[ind], 'click');}else {
                  var i = findWithAttr( markersArray,'id',response[0].id)
                  var pos = markersArray[i].getPosition()
                  google.maps.event.trigger(markersArray[ind], 'click');
                }
                }else{
                    var marker = addMarkersToMap(response)
                    var pos = marker.getPosition()
                    aDataG.push(response[0])
                    if(typeof markersByDate[marker['time'].slice(0, marker['time'].indexOf(' '))] =='undefined'){
                        markersByDate[marker['time'].slice(0, marker['time'].indexOf(' '))] = [];
                        $key = marker['time'].slice(0, marker['time'].indexOf(' '));
                        $('#days').prepend('\
                        <div class="panel panel-primary" id="' + $key + '">\
                          <div class="panel-heading day"  data-key="' + $key + '" style="height: 35px">\
                            <h6 style="padding:0px;margin:0px;"><i class="icon-angle-right" data-action="show"></i><span class="break"></span>' + $key + '</h4>\
                          </div>\
                          <div class="panel-body day-body" id="' + $key + '" style="display:none;padding:0px;">\
                            <table class="table table-hover" style="margin-bottom:0px;margin-top:0px;">\
                                <tbody><tr><td class=" direction" data-id="' + $key + '" style="cursor:pointer;">\
                                    <a href="#" >'+$('#draw_direction').text()+'</a>\
                                </td></tr>\
                                <tr><td class=" activity" data-id="' + $key + '" style="cursor:pointer;">\
                                    <a href="#">'+$('#show_activity').text()+'</a>\
                                </td></tr>\
                                <tr><td class=" polyline" data-id="' + $key + '" style="cursor:pointer;">\
                                    <a href="#"  >'+$('#draw_polyline').text()+'</a>\
                                </td></tr></tbody>\
                            </table>\
                          </div>\
                        </div>')
                    }
                    markersByDate[marker['time'].slice(0, marker['time'].indexOf(' '))].push(response[0]);
                    addMC.push(marker)
                    google.maps.event.trigger(marker, 'click');

                }
                map.panTo(pos);
                map.setZoom(17)
                // cont = true
                // //getNewdata()
            }
        });
    });
    $(document).on('click', '#close_obd_info', function(){
        $('#obd_info').hide();
        // $('#obd_info_marker').empty();

    });
})
