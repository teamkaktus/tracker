google.load("visualization", "1", {packages:["corechart"]});

function MaxSpeedChart(aRes) {
    var data = google.visualization.arrayToDataTable(aRes);

    var options = {
      title: $('#max_speed').text(),
      hAxis: {title: 'Year',  titleTextStyle: {color: '#333'}},
      vAxis: {minValue: 0}
    };

    var chart = new google.visualization.AreaChart(document.getElementById('chart_MaxSpeed'));
    chart.draw(data, options);
}

function EverageChart(aRes) {
    var data = google.visualization.arrayToDataTable(aRes);

    var options = {
      title: $('#everage_speed').text(),
      hAxis: {title: 'Year',  titleTextStyle: {color: '#333'}},
      vAxis: {minValue: 0}
    };

    var chart = new google.visualization.AreaChart(document.getElementById('chart_EverageSpeed'));
    chart.draw(data, options);
}

function MillageChart(aRes) {
    var data = google.visualization.arrayToDataTable(aRes);

    var options = {
      title: $('#millage_chart').text(),
      hAxis: {title: 'Day',  titleTextStyle: {color: '#333'}},
      vAxis: {minValue: 0}
    };

    var chart = new google.visualization.AreaChart(document.getElementById('chart_Millage'));
    chart.draw(data, options);
}

function FuelChart(aRes) {
    var data = google.visualization.arrayToDataTable(aRes);

    var options = {
      title: $('#fuel_chart').text(),
      hAxis: {title: 'Day',  titleTextStyle: {color: '#333'}},
      vAxis: {minValue: 0}
    };

    var chart = new google.visualization.AreaChart(document.getElementById('chart_Fuel'));
    chart.draw(data, options);
}

function activityChart(aRes) {
    var data = google.visualization.arrayToDataTable(aRes);

    var options = {
        title: $('#activity_chart').text(),
        hAxis: {title: 'Day',  titleTextStyle: {color: '#333'}},
        vAxis: {minValue: 0}
    };

    var chart = new google.visualization.AreaChart(document.getElementById('chart_activity'));
    chart.draw(data, options);
}

$(document).ready(function(){
        $('#ms').change(function() {
            //console.log($(this).val());
        }).multipleSelect({
            width: '100%',
            placeholder: $('#choice_parameter').text(),
        });
        
        $("#btnExport").click(function () {
            $("#tblExport").battatech_excelexport({
                containerid: "tblExport", 
                datatype: 'table'
            });
        });
        
        $('#TopOption').change(function(){
            var TopOptionSelect = $(this).select().val();
            var reportTypeValue = $('#reportType').val();
            if(TopOptionSelect == 1){
                $('.SubCompanys').css('display','none');
                $('.UserSubCompanys').css('display','none');
                $('.TrackerCompany').css('display','none');
                //Sub Company
                $('.SubCompanys').show();
                $('.TrackerCompany').show();
                var company_id = $(this).data('company_id');
                //alert(company_id);
                $('.SubCompanys').text(' ');
                $('.TrackerCompany').text(' ');
                $.ajax({
                    type: 'POST',
                    url: '../../../company/getcompanyinfo',
                    data: {company_id :company_id, reportTypeValue:reportTypeValue},
                    dataType: "json",
                    success: function(response){
                        //console.log(response);
                        //
                        //response.subCompanyArray
                        $('.SubCompanys').append('<select id="SubCompanySelect" multiple="multiple"></select>');
                        for(var key in response.subCompanyArray){
                            $('#SubCompanySelect').append($('<option>', {
                                value: key,
                                text: response.subCompanyArray[key]
                            }));
                        }
                        $('#SubCompanySelect').change(function() {
                            getDataUserSubCompany($(this).val());
                        }).multipleSelect({
                            width: '100%',
                            placeholder: $('#choice_subcompany').text(),
                        });
                        //end response.subCompanyArray
                        //
                        
                        //
                        //response.subCompanyTrackerArray
                        $('.TrackerCompany').append('<select id="TrackerCompany" multiple="multiple"></select>');
                        for(var key in response.subCompanyTrackerArray){
                            $('#TrackerCompany').append($('<option>', {
                                value: key,
                                text: response.subCompanyTrackerArray[key]
                            }));
                        }
                        $('#TrackerCompany').change(function() {
                            //getDataUserSubCompany($(this).val());
                            //console.log($(this).val());
                        }).multipleSelect({
                            width: '100%',
                            placeholder: $('#choice_tracker').text(),
                        });
                        //end subCompanyTrackerArray
                        //
                    }
                });
            }
            
            if(TopOptionSelect == 2){
                $('.SubCompanys').css('display','none');
                $('.UserSubCompanys').css('display','none');
                $('.TrackerCompany').css('display','none');
                //console.log('User');
                //User Company
                $('.SubCompanys').hide();
                $('.SubCompanys').text(' ');
                $('.UserSubCompanys').hide();
                $('.UserSubCompanys').text(' ');
                $('.TrackerCompany').text(' ');
                var companyId = $(this).data('company_id');
                //alert(companyId);
                $.ajax({
                    type: 'POST',
                    url: '../../../company/getcompanyinfo',
                    data: {role:'User',companyId :companyId, reportTypeValue:reportTypeValue},
                    dataType: "json",
                    success: function(response){
                        //console.log(response.modelTracker);
                        $('.UserSubCompanys').show();
                        $('.TrackerCompany').show();
                        
                        $('.UserSubCompanys').append('<select id="userSelect" multiple="multiple"></select>');
                        for(var key in response.modelUser){
                            //console.log(key);
                            $('#userSelect').append($('<option>', {
                                value: key,
                                text: response.modelUser[key]
                            }));
                        }
                        $('#userSelect').change(function() {
                            //getDataUserSubCompany($(this).val());
                            getDataTracker($(this).val());
                        }).multipleSelect({
                            width: '100%',
                            placeholder: $('#choice_user').text(),
                        });
                        
                        
                        $('.TrackerCompany').append('<select id="TrackerCompany" multiple="multiple"></select>');
                        for(var key in response.modelTracker){
                            $('#TrackerCompany').append($('<option>', {
                                value: key,
                                text: response.modelTracker[key]
                            }));
                        }
                        $('#TrackerCompany').change(function() {
                            //getDataTrackerForUser($(this).val());
                        }).multipleSelect({
                            width: '100%',
                            placeholder: $('#choice_tracker').text(),
                        });
                    }
                });
                
            }
            
            if(TopOptionSelect == 3){
                $('.SubCompanys').css('display','none');
                $('.UserSubCompanys').css('display','none');
                $('.TrackerCompany').css('display','none');
                //console.log('Tracker');
                //Tracker Company
                $('.SubCompanys').hide();
                $('.SubCompanys').text(' ');
                $('.UserSubCompanys').hide();
                $('.UserSubCompanys').text(' ');
                $('.TrackerCompany').text(' ');
                var companyId = $(this).data('company_id');
                
                $.ajax({
                    type: 'POST',
                    url: '../../../company/getcompanyinfo',
                    data: {role:'Tracker',companyId :companyId, reportTypeValue:reportTypeValue},
                    dataType: "json",
                    success: function(response){
                        console.log(response);
                        $('.TrackerCompany').text(' ');
                        $('.TrackerCompany').show();
                        
                        $('.TrackerCompany').append('<select id="TrackerCompany" multiple="multiple"></select>');
                        for(var key in response.TrackerData){
                            $('#TrackerCompany').append($('<option>', {
                                value: key,
                                text: response.TrackerData[key]
                            }));
                        }
                        $('#TrackerCompany').change(function() {
                            //getDataTrackerForUser($(this).val());
                        }).multipleSelect({
                            width: '100%',
                            placeholder: $('#choice_tracker').text(),
                        });
                    }
                });
                
            }
        });
        
        function getDataTracker(dataUserArray){
            //console.log(dataUserArray);
            if(dataUserArray == null){
               dataUserArray = 'maybe null'; 
            }
            var companyId = $('#TopOption').data('company_id');
            //alert(dataUserArray);
            $.ajax({
                type: 'POST',
                url: '../../../company/getcompanyinfo',
                data: {subCompanyUserArray: dataUserArray, companyId:companyId, forUser:'User'},
                dataType: "json",
                success: function(response){
                    //console.log(response);
                    $('.TrackerCompany').text(' ');
                    $('.TrackerCompany').append('<select id="TrackerCompany" multiple="multiple"></select>');
                        for(var key in response.subCompanyTrackerArray){
                            $('#TrackerCompany').append($('<option>', {
                                value: key,
                                text: response.subCompanyTrackerArray[key]
                            }));
                        }
                        $('#TrackerCompany').change(function() {
                            //getDataTrackerForUser($(this).val());
                        }).multipleSelect({
                            width: '100%',
                            placeholder: $('#choice_tracker').text(),
                        });
                }
            });
        }
        
        //
        //Sub Company Select 
        function getDataUserSubCompany(subCompanyArray){
            $('.UserSubCompanys').show();
            $('.UserSubCompanys').text(' ');
            $('.TrackerCompany').text(' ');
            var companyId = $('#TopOption').data('company_id');
            if(subCompanyArray == null){
                subCompanyArray = 'maybe null';
                //alert(subCompanyArray+' - '+companyId);
            }
            $.ajax({
                type: 'POST',
                url: '../../../company/getcompanyinfo',
                data: {subCompanyArray: subCompanyArray, companyId:companyId},
                dataType: "json",
                success: function(response){
                    //console.log(response);
                    $('.UserSubCompanys').append('<select id="userSelect" multiple="multiple"></select>');
                    for(var key in response.userArray){
                        //console.log(key);
                        $('#userSelect').append($('<option>', {
                            value: key,
                            text: response.userArray[key]
                        }));
                    }
                    $('#userSelect').change(function() {
                        //getDataUserSubCompany($(this).val());
                        getDataTrackerForUser($(this).val());
                    }).multipleSelect({
                        width: '100%',
                        placeholder: $('#choice_user').text(),
                    });
                    
                    $('.TrackerCompany').append('<select id="TrackerCompany" multiple="multiple"></select>');
                        for(var key in response.subCompanyTrackerArray){
                            $('#TrackerCompany').append($('<option>', {
                                value: key,
                                text: response.subCompanyTrackerArray[key]
                            }));
                        }
                        $('#TrackerCompany').change(function() {
                            //getDataTrackerForUser($(this).val());
                        }).multipleSelect({
                            width: '100%',
                            placeholder: $('#choice_tracker').text(),
                        });
                }
            });
        };
        
        function getDataTrackerForUser(subCompanyUserArray){
            //console.log(subCompanyUserArray);
            $('.TrackerCompany').text(' ');
            var companyId = $('#TopOption').data('company_id');
            if(subCompanyUserArray == null){
                subCompanyUserArray = 'maybe null';
                //alert(subCompanyUserArray+' - '+companyId);
            }
            
            $.ajax({
                type: 'POST',
                url: '../../../company/getcompanyinfo',
                data: {subCompanyUserArray: subCompanyUserArray, companyId:companyId},
                dataType: "json",
                success: function(response){
                    //console.log(response);
                    $('.TrackerCompany').append('<select id="TrackerCompany" multiple="multiple"></select>');
                        for(var key in response.subCompanyTrackerArray){
                            $('#TrackerCompany').append($('<option>', {
                                value: key,
                                text: response.subCompanyTrackerArray[key]
                            }));
                        }
                        $('#TrackerCompany').change(function() {
                            //getDataTrackerForUser($(this).val());
                        }).multipleSelect({
                            width: '100%',
                            placeholder: $('#choice_tracker').text(),
                        });
                }
            });
            
        }
        
        $('#reportType').change(function(){
            if($(this).val() != 0){
                $('#TopOption').prop('disabled', false);
                $('#TopOption').val('0');
                $('.SubCompanys').css('display','none');
                $('.UserSubCompanys').css('display','none');
                $('.TrackerCompany').css('display','none');
            }else{
                $('#TopOption').prop('disabled', true);
                $('#TopOption').val('0');
                $('.SubCompanys').css('display','none');
                $('.UserSubCompanys').css('display','none');
                $('.TrackerCompany').css('display','none');
            }
        });
        
        $('#goReport').click(function(){
            //console.log('Report');
            //console.log($('.TimeStart').val());
            //console.log($('.TimeEnd').val());
            //console.log($('#TopOption').val())
            if(($('#TopOption').val() != 0) && ($('.TimeStart').val() != 0) && ($('.TimeEnd').val() != 0) && ($('#reportType').val() != 0) ){
                
                var trackerArraySelected = [];
                $("#TrackerCompany option:selected").each(function() {
                    trackerArraySelected.push($( this ).val());
                });
                var lastTracekerArray = [];
                if(trackerArraySelected.length == 0){
                    var trackerArray = [];
                    $("#TrackerCompany option").each(function() {
                        trackerArray.push($( this ).val());
                    });
                    lastTracekerArray = trackerArray;
                }else{
                    lastTracekerArray = trackerArraySelected;
                }
                getDataForReport(lastTracekerArray, $('.TimeStart').val(), $('.TimeEnd').val(), $('#reportType').val())
                
            }else{
                if(($('#TopOption').val() != 0) != 0){ var findOption = " "; }else{ var findOption = $('#find_option').text(); }
                if($('.TimeStart').val() != 0){ var timeStart = " "; }else{ var timeStart = $('#time_start').text(); }
                if($('.TimeEnd').val() != 0){ var timeEnd = " "; }else{ var timeEnd = $('#time_end').text(); }
                if($('#reportType').val() != 0){ var reportType = " "; }else{ var reportType = $('#report_type').text(); }
                
                swal($('#choice_opt').text()+findOption+timeStart+timeEnd+reportType);
            }
            
            function getDataForReport(TrackerArray, TimeStart, TimeEnd, ReportType){
                $.ajax({
                    type: 'POST',
                    url: '../../../company/getdataforreport',
                    data: {TrackerArray:TrackerArray, TimeStart:TimeStart, TimeEnd:TimeEnd, ReportType:ReportType},
                    beforeSend: function(){
                        $('#loader').css('display', 'block');
                        $('#chart_Millage').hide();
                        $('#chart_MaxSpeed').hide();
                        $('#chart_EverageSpeed').hide();
                        $('#chart_activity').hide();
                        $('.divForTable').hide();
                    },
                    dataType: "json",
                    success: function(response){
                        $('#loader').css('display', 'none');
                        switch(ReportType) {
                            case '1': 
                                //console.log(response);
                                $('#chart_Millage').show();
                                var trackerArray = [];
                                for(var trackerImei in response){
                                    if(typeof trackerArray[trackerImei] == 'undefined'){
                                        trackerArray[trackerImei] = [];
                                    }
                                    var trackersData = response[trackerImei];
                                    for(var trackerData in trackersData){
                                        var dateS = trackersData[trackerData][2].substring(0, 11);
                                        var lat = trackersData[trackerData][0];
                                        var lng = trackersData[trackerData][1];
                                        //console.log(dateS);
                                        if(typeof trackerArray[trackerImei][dateS] == 'undefined'){
                                            trackerArray[trackerImei][dateS] = [];
                                        }
                                        trackerArray[trackerImei][dateS].push(new google.maps.LatLng(lat, lng));
                                    }
                                }
                                var trackersDataLast = [];
                                for(var trackerIMEI in trackerArray){
                                    //console.log(trackerIMEI);
                                    var trackerData = trackerArray[trackerIMEI];
                                    for(var trackerDay in trackerData){
                                        trackersDataLast.push({'tracker_name':trackerIMEI, 'year':trackerDay, 'kilk':google.maps.geometry.spherical.computeLength(trackerData[trackerDay])});
                                    }
                                }
                                MillageChart(SortArray(trackersDataLast));
                                
                                var tableData = '<table id="tblExport" border="1" style="display:none;">';
                                    for(var trackerIMEI in trackerArray){
                                        tableData += '<tr><td><h2>'+trackerIMEI+'</h2></td></tr>';
                                        var trackerData = trackerArray[trackerIMEI];
                                        for(var trackerDay in trackerData){
                                            tableData += '<tr><td><b>'+trackerDay+'</b></td>';
                                            tableData += '<td><b>'+$("#millage_walk").text()+google.maps.geometry.spherical.computeLength(trackerData[trackerDay])+'</td>';
                                            tableData += '</tr>';
                                        }
                                    }
                                        
                                    tableData += '</table>';

                                    $('.tableData').empty();
                                    $('.tableData').append(tableData);
                                    $('.divForTable').show();
                            break;
                                
                                
                            case '4':
                                $('#chart_Fuel').show();
                                var trackersDataLast = [];
                                for(var trackerIMEI in response){
                                    //console.log(trackerIMEI);
                                    var trackerData = response[trackerIMEI];
                                    for(var trackerDay in trackerData){
                                        trackersDataLast.push({'tracker_name':trackerIMEI, 'year':trackerDay, 'kilk':trackerData[trackerDay]});
                                    }
                                }
                                //alert(reportType);
                                FuelChart(SortArray(trackersDataLast));
                                //console.log(trackersDataLast);
                                
                                var tableData = '<table id="tblExport" border="1" style="display:none;">';
                                    for(var trackerIMEI in response){
                                        tableData += '<tr><td><h2>'+trackerIMEI+'</h2></td></tr>';
                                        var trackerData = response[trackerIMEI];
                                        for(var trackerDay in trackerData){
                                            tableData += '<tr><td><b>'+trackerDay+'</b></td>';
                                            tableData += '<td><b>'+$("#fuel_burned").text()+' - </b>'+trackerData[trackerDay]+'</td>';
                                            tableData += '</tr>';
                                        }
                                    }
                                        
                                    tableData += '</table>';

                                    $('.tableData').empty();
                                    $('.tableData').append(tableData);
                                    $('.divForTable').show();
                            break;
                            
                            
                            case '2':
                                $('#chart_MaxSpeed').show();
                                $('#chart_EverageSpeed').show();

                                var trackerArrayMaxSpeed = [];    
                                for(var TrackerIMEI in response){
                                    //console.log(TrackerModelDay);
                                    var qwer = response[TrackerIMEI];
                                    for(var val in qwer){
                                        trackerArrayMaxSpeed.push({'tracker_name' : TrackerIMEI, 'year' : val, 'kilk' : parseFloat(qwer[val]['MaxSpeed'])});
                                    }
                                }
                                var resultDataForMaxSpeed = SortArray(trackerArrayMaxSpeed);
                                MaxSpeedChart(resultDataForMaxSpeed);    

                                var trackerArrayEverageSpeed = [];    
                                for(var TrackerIMEI in response){
                                    //console.log(TrackerModelDay);
                                    var qwer = response[TrackerIMEI];
                                    for(var val in qwer){
                                        trackerArrayEverageSpeed.push({'tracker_name' : TrackerIMEI, 'year' : val, 'kilk' : parseFloat(qwer[val]['averageSpeed'])});
                                    }
                                }
                                //console.log(SortArray(trackerArrayEverageSpeed));
                                var resultDataForEverageSpeed = SortArray(trackerArrayEverageSpeed);
                                EverageChart(resultDataForEverageSpeed);
                                
                                
                                var tableData = '<table id="tblExport" border="1" style="display:none;">';
                                    for(var trackerIMEI in response){
                                        tableData += '<tr><td><h2>'+trackerIMEI+'</h2></td></tr>';
                                        var trackerData = response[trackerIMEI];
                                        for(var trackerDay in trackerData){
                                            tableData += '<tr><td><b>'+trackerDay+'</b></td>';
                                            var trackerSpeed = trackerData[trackerDay];
                                            for(var trackerDay in trackerSpeed){
                                                tableData += '<td>';
                                                    tableData += '<b>'+trackerDay+'</b>-'+trackerSpeed[trackerDay];
                                                tableData += '</td>';
                                            }
                                            tableData += '</tr>';
                                        }
                                    }
                                        
                                    tableData += '</table>';

                                    $('.tableData').empty();
                                    $('.tableData').append(tableData);
                                    $('.divForTable').show();
                            break;
                            
                            case '6':
                                //console.log(response);
                                $('#chart_activity').show();
                                
                                var trackerArrayActivity = [];
                                
                                for(var imei in response){
                                    var trackerData = response[imei];
                                    for(var day in trackerData){
                                        trackerArrayActivity.push({'tracker_name' : imei, 'year' : day, 'kilk' : parseFloat(trackerData[day])});
                                    }
                                    var dataForChart = SortArray(trackerArrayActivity);
                                    activityChart(dataForChart);
                                }
                                
                                var tableData = '<table id="tblExport" border="1" style="display:none;">';
                                    for(var trackerIMEI in response){
                                        tableData += '<tr><td><h2>'+trackerIMEI+'</h2></td></tr>';
                                        var trackerData = response[trackerIMEI];
                                        for(var trackerDay in trackerData){
                                            tableData += '<tr><td><b>'+trackerDay+'</b></td>';
                                            var trackerActivity = trackerData[trackerDay];
                                                //alert(String(trackerActivity));
                                                tableData += '<td>';
                                                    tableData += trackerActivity;
                                                tableData += '</td>';
                                            tableData += '</tr>';
                                        }
                                    }
                                        
                                    tableData += '</table>';

                                    $('.tableData').empty();
                                    $('.tableData').append(tableData);
                                    $('.divForTable').show();
                            break;
                        }
                         
                    }
                });
            }
        });
        function SortArray(trackerArray){
           var aRes=[];
                aRes[0]=['Year'];
                for(var ob in trackerArray){
                    if(aRes[0].indexOf(trackerArray[ob].tracker_name)<0){
                        aRes[0].push(trackerArray[ob].tracker_name);
                    }
                }	
                var leng = aRes[0].length
                var years = []
                for(var ob in trackerArray){
                    if(years.indexOf(trackerArray[ob].year)< 0){
                        years.push(trackerArray[ob].year);
                    }
                }

                for(j in years){
                    tmp =[years[j]] 
                    for(var i = 0; i < leng-1; i++) {
                            tmp.push(0);
                    }
                    aRes.push(tmp)
                }
                //console.log(aRes)
                for(var ob in trackerArray){
                    for(var el in aRes){
                        if(aRes[el][0] == trackerArray[ob].year){
                            var tmp = [];
                            aRes[el][aRes[0].indexOf(trackerArray[ob].tracker_name)] = trackerArray[ob].kilk;
                        }
                    }
                }
                return aRes;
            }     
        
});