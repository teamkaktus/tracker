/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var map;
var waypoints = [];
var markers = [];
function initialize(id,waitpoints, travelMode) {
  directionsDisplay = new google.maps.DirectionsRenderer();
  //var chicago = new google.maps.LatLng(41.850033, -87.6500523);
  var mapOptions = {
    zoom:5,
    center: new google.maps.LatLng(41.850033, -87.6500523)
  };
  var map = new google.maps.Map(document.getElementById('map-canvas'+id), mapOptions);
  directionsDisplay.setMap(map);
  
  var geocoder = new google.maps.Geocoder();

//  document.getElementById('submitAdderes').addEventListener('click', function() {
//    geocodeAddress(geocoder, map);
//  });
  
  function geocodeAddress(geocoder, resultsMap) {
    var address = document.getElementById('address').value;
    geocoder.geocode({'address': address}, function(results, status) {
      if (status === google.maps.GeocoderStatus.OK) {
        resultsMap.setCenter(results[0].geometry.location);
        
//        var marker = new google.maps.Marker({
//            map: resultsMap,
//            position: results[0].geometry.location
//        });
//        
//        markers.push(marker);
        
        if(waypoints.length<10){
            waypoints.push(
            {
                'location':results[0].geometry.location,
                'stopover':true
            })
                placeMarker(results[0].geometry.location, map);
            }else{
                alert('MAX POINTS - 10!')
            }
        }else{
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
    }
//	google.maps.event.addListener(map, 'click', function(event){
//            if(waypoints.length<10){
//		waypoints.push({
//                    'location':event.latLng,
//                    'stopover':true
//		});
//                    placeMarker(event.latLng, map);
//		}else{
//                    alert('MAX POINTS - 10!')
//		}
//	});
        if(waitpoints != ' '){
            console.log(waitpoints,travelMode);
            calcRoute(waitpoints,travelMode);
        }
}

function setAllMap(map) {
  for (var i = 0; i < markers.length; i++) {
    //console.log(markers[i]);
    markers[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
  setAllMap(null);
}
function deleteMarkers() {
  clearMarkers();
  markers = [];
}

  function placeMarker(position, map) {
    var marker = new google.maps.Marker({
        position: position,
	draggable:true,
        map: map
    });
    //console.log(marker);
    //google.maps.event.addListener(marker, 'dragend', qwerty);
    
	markers.push(marker);	
    map.panTo(position);
  }
function calcRoadF(travelType){
	if(waypoints.length>=2){
		calcRoute(waypoints, travelType);
		deleteMarkers();
	}
        return waypoints;
}
			
function calcRemove() {
        deleteMarkers();
	directionsDisplay.setDirections({routes: []});
	waypoints = [];
}

function calcRoute(data,travelType) {
  var start;
  var TravelMode;
  
  var end;
  //start = new google.maps.LatLng(data);
  end = new google.maps.LatLng(40.7143528, -74.0059731);
    if(travelType == 'DRIVING'){
        TravelMode = google.maps.TravelMode.DRIVING;
    }
    if(travelType == 'WALKING'){
        TravelMode = google.maps.TravelMode.WALKING;
    }
    if(travelType == 'BICYCLING'){
        TravelMode = google.maps.TravelMode.BICYCLING;
    }
    if(travelType == 'TRANSIT'){
        TravelMode = google.maps.TravelMode.TRANSIT;
    }
    console.log(TravelMode);
        var request = {
            origin:data[0].location,
            destination:data[data.length-1].location,
            waypoints:data.slice(1,data.length-1),
            travelMode: TravelMode
        };
  //console.log(request);
  //console.log(waypoints);
  
  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
        console.log(response);
    }else{
        console.log("Directins service failed: "+status)
    }
  });
}
 $('document').ready(function(){ 
//     $('.button_remove').click(function(event){
//        event.preventDefault();
//        calcRemove();
//    });
//    
//     $('.button_click').click(function(event){
//        event.preventDefault();
//        var travelType = $("#TravelType :selected").val();
//       alert(travelType);
//        var qwe = calcRoadF(travelType);
//        //var obj = JSON.parse(qwe);
//        var json_text = JSON.stringify(qwe, null, 2);
//        alert('jsaon');
//        console.log(json_text);
//        $('#for_map').val(json_text);
//        var task_id = $(this).data('task_id');
//        $('.forMap'+task_id).val(json_text);
//     });
     
     $("[name='modal']").on('shown.bs.modal', function(){
        //initialize('');
        var thisTaskElement = $(this);    
        var qwer = $(this).find('.for_map');
        var travelMode = $(this).find('.travel_map');
        //console.log(qwer);
        var data = qwer.val();
        data = jQuery.parseJSON(data);
        for(var i in data){
            data[i].location = new google.maps.LatLng(data[i].location['lat'], data[i].location['lng']);
        }
        console.log(data);
        
        
        initialize($(this).data("id_task"), data, travelMode.val());
        
        
        var task_id = $(this).data('id_task');
        //alert(task_id);
        $.ajax({
            type: 'POST',
            url: '../../../tracker/subtasksview',
            data: {id_task:task_id},
            dataType: "json",
            success: function(response){
                //alert('ok');
                //console.log(response);
                
            var wayResult = '<table class="table table-hover">';
            wayResult += '<tr><td><b>Marker</b></td><td><b>Address</b></td><td><b>Distance</b></td></tr>';
            var allDistance = 0;
            for(var Id in response){
                var pointAddress = response[Id];
                wayResult += '<tr>';
                    wayResult += '<td>'+pointAddress['marking']+'</td>';
                    wayResult += '<td>'+pointAddress['address']+'</td>';
                    if(pointAddress['distance'] >= 1000){
                        wayResult += '<td>'+pointAddress['distance']/100+' km</td>';
                    }else{
                        wayResult += '<td>'+pointAddress['distance']+' m</td>';
                    }
                    allDistance += parseFloat(pointAddress['distance']);
                wayResult += '</tr>';
            }
            if(allDistance >= 1000){
                wayResult += '<tr><td></td><td>Всього:</td><td>'+allDistance/100+' km</td></tr>';
            }else{
                wayResult += '<tr><td></td><td>Всього:</td><td>'+allDistance+' m</td></tr>';
            }
            wayResult += '</table>';
            
            thisTaskElement.find('.for_way_list').empty();
            thisTaskElement.find('.for_way_list').append(wayResult);
            }
        });
    });     
 });