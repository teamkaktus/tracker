$('document').ready(function(){
    
    $('#fixedButtonTrackerInfo').click(function(){
        if($(this).data('show') == 'show'){
            $('.classTrackerinfoShow').hide();

            $('.classMapShow').removeClass('col-lg-9 col-md-9 col-sm-9 col-xs-12 paddingNone');
            $('.classMapShow').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone');
            
            
            $(this).data('show','hide');
        }else{
            $('.classMapShow').removeClass('col-lg-12 col-md-12 col-sm-12 col-xs-12 paddingNone');
            $('.classMapShow').addClass('col-lg-9 col-md-9 col-sm-9 col-xs-12 paddingNone');
            $('.classTrackerinfoShow').show();
            
            
            $(this).data('show','show');
        }
        
    });
    
    
    
    $('.trackerOptionsAll').change(function(){
        if($(this).is(':checked')){
            $('.trackerOptions').prop('checked', true);
        }else{
            $('.trackerOptions').prop('checked', false);
        }
    });


    $('.saveReportSelect').click(function(){
        var inputChecked = $("input[name='reportSelect']:checked");
        var arrayIdReportChecked = [];
        inputChecked.each(function(){
            arrayIdReportChecked.push($(this).data('id'));
        });


        var arrayIdReportIndeterminate = [];
        $('input[name=reportSelect]:indeterminate').each(function(){
            arrayIdReportIndeterminate[$(this).data('id')] = jQuery.parseJSON($(this).data('jsondata'));
        });

        var inputChecked = $("input[name='optionT']:checked");
        var arrayIdTrackerForReportChecked = [];
        inputChecked.each(function(){
            arrayIdTrackerForReportChecked.push($(this).data('id'));
        });

        //
            //arrayIdReportChecked;
           // arrayIdTrackerForReportChecked;
        //
           // console.log(arrayIdReportIndeterminate,arrayIdReportChecked,arrayIdReportIndeterminate);
        //
        $.ajax({
            type: 'POST',
            url: '../../../admin/reporttype',
            data: {arrayIdReportChecked:arrayIdReportChecked,
                    arrayIdTrackerForReportChecked:arrayIdTrackerForReportChecked,
                    arrayIdReportIndeterminate:JSON.stringify(arrayIdReportIndeterminate)
                },
            dataType: "json",
            success: function(response){
                //console.log(response);
                $('#modalChangeOption').modal('hide')
                swal('Дані збережено');
            }
        });

//        var  reportSelect = $('input[name=reportSelect]');
//        reportSelect.each(function(){
//            alert($(this).data('jsondata'));
//            console.log($(this).data('jsondata'));
//        });
    });

    $('.checkedOptionClick').click(function(){
        var inputChecked = $("input[name='optionT']:checked");
        var arrayIdTracker = [];
        inputChecked.each(function(){
            arrayIdTracker.push($(this).data('id'));
        });
       // console.log(arrayIdTracker);
        var trackerCountLength = arrayIdTracker.length;
        //console.log(arrayIdTracker);
        if(arrayIdTracker.length != '0'){

                $.ajax({
                    type: 'POST',
                    url: '../../../admin/reportselected',
                    data: {tracker_id:arrayIdTracker},
                    dataType: "json",
                    success: function(response){
                        $('input[name=reportSelect]').prop('checked', false);
                        $('input[name=reportSelect]').prop('indeterminate', false);
                        $('input[name=reportSelect]').css('opacity', '1');
                        for(var numberReport in response){
                            //console.log(numberReport+' - '+response[numberReport].length);
                            if(response[numberReport].length == trackerCountLength){
                                $('.report'+numberReport).prop('checked', true);
                                $('.report'+numberReport).data("jsondata", JSON.stringify(response[numberReport]));
                            }else{
                                $('.report'+numberReport).data("jsondata", JSON.stringify(response[numberReport]));
                                $('.report'+numberReport).prop('indeterminate', true);
                                $('.report'+numberReport).css('opacity', '0.9');
                            }
                        }

                    }
                });

            $('#modalChangeOption').modal('show');
        }else{
            swal('Немає вибраних трекерів');
        }
    });

    $('.trackerTask').click(function(){
        if($(this).hasClass('fa fa-chevron-down trackerTask')){
            $(this).toggleClass();
            $(this).toggleClass("fa fa-chevron-up trackerTask");
        }else{
            $(this).toggleClass();
            $(this).toggleClass("fa fa-chevron-down trackerTask");
        }

        $('.trackerTaskInfo').toggle();
    });

    $('.trackerDriver').click(function(){
        if($(this).hasClass('fa fa-chevron-down trackerDriver')){
            $(this).toggleClass();
            $(this).toggleClass("fa fa-chevron-up trackerDriver");
        }else{
            $(this).toggleClass();
            $(this).toggleClass("fa fa-chevron-down trackerDriver");
        }

        $('.trackerDriverInfo').toggle();
    });

    $('.trackerMap').click(function(){
        if($(this).hasClass('fa fa-chevron-down trackerMap')){
            $(this).toggleClass();
            $(this).toggleClass("fa fa-chevron-up trackerMap");
        }else{
            $(this).toggleClass();
            $(this).toggleClass("fa fa-chevron-down trackerMap");
        }

        $('.trackerMapInfo').toggle();
    });

    $('#tabs').tab();

    $('.onePay').click(function(){
        var companyId = $(this).data('id');
        $('.containerOne'+companyId).show();
        $('.containerTwo'+companyId).hide();
        $('.containerOne'+companyId).data('active','1');
        $('.containerTwo'+companyId).data('active','0');

    });

    $('.twoPay').click(function(){
        var companyId = $(this).data('id');
        $('.containerOne'+companyId).hide();
        $('.containerTwo'+companyId).show();
        $('.containerOne'+companyId).data('active','0');
        $('.containerTwo'+companyId).data('active','1');
    });

    $("li#qwerty a").click(function(event) {
        event.preventDefault();
    });

    $('#datetimepicker1').datetimepicker({
        language: 'pt-BR'
      });

    $('#datetimepicker2').datetimepicker({
        language: 'pt-BR'
      });

    $("[name='myModalActiveCompany']").on('shown.bs.modal', function(){
        //alert('dvd');
         var year;
            var month;
            var day;
            var hour;
            var minute;
            var second;
        function js_yyyy_mm_dd_hh_mm_ss() {
            var now = new Date();
            now.setDate(now.getDate());
            year = "" + now.getFullYear();
            month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
            day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
            hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
            minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
            second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
            return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
        }
        var thisDate = js_yyyy_mm_dd_hh_mm_ss();
        $('.thisdate').val(thisDate);

        $('.datetimepicker').datetimepicker({
            language: 'pt-BR'
          });
    });

    $("[name='myModalActiveUser']").on('shown.bs.modal', function(){
            var year;
            var month;
            var day;
            var hour;
            var minute;
            var second;
        function js_yyyy_mm_dd_hh_mm_ss() {
            var now = new Date();
            now.setDate(now.getDate());
            year = "" + now.getFullYear();
            month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
            day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
            hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
            minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
            second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
            return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
        }
        var thisDate = js_yyyy_mm_dd_hh_mm_ss();
        $('.thisdate').val(thisDate);

        $('.datetimepicker').datetimepicker({
            language: 'pt-BR'
          });
    });

    $('.delete').click(function(event){

        if(confirm("You want delete?")){
        }else{
            event.preventDefault();
        }
    });


    $('#trackerTaskClick').click(function(){
        $('#trackerTask').slideToggle(700);
    });

    $('#companyInfoClick').click(function(){
        $('#companyInfoShowHide').slideToggle(700);
    });



    $('#example-single-selected').multiselect();

    if($('#trackers_view').text() == '1'){
        var company_id = $('#trackers_view').data('action');

        $.ajax({
            type: 'POST',
            url: '../../../company/companytrackergps',
            data: {company_id :company_id},
            dataType: "json",
            success: function(response){
                console.log(response);

                        var path = [];
                        var map;
                        map = new google.maps.Map(document.getElementById('map'), {
                          zoom: 10,
                          center: new google.maps.LatLng('49.35559', '23.530916666666666'),
                          mapTypeId: google.maps.MapTypeId.ROADMAP,
                          gridSize: 50
                        });
                        var lat;
                        var lng;
                        var markerss = [];
                        var infoWindow = new google.maps.InfoWindow();

                            for(var ArrVal in response) {
                                var marker = response[ArrVal];
                                path.push(new google.maps.LatLng(marker['lat'], marker['lng']));
                                lat = marker['lat'];
                                lng = marker['lng'];
                                var imei = marker['imei'];
                                var time = marker['time'];
                                var name = marker['name'];
                                var id = marker['id'];
                                var time = marker['time'];
                                var speed = marker['speed'];
                                var lnglat = new google.maps.LatLng(lat,lng);

                                var contentString = id+'--<h1>'+imei+'</h1>'+time;

                                 var markers = new google.maps.Marker({
                                        position: lnglat,
                                        title: '<h1>Tracker </h1>'+imei,
                                    });
                                    markers['infowindow']= new google.maps.InfoWindow({
                                        content : "<h2>imei: "+imei+'</h2><br>'+"time: "+ time+'<br>'+"speed: "+ speed+'<br>'
                                    });
                                    google.maps.event.addListener(markers, 'click', function() {
                                        this['infowindow'].open(map, this);
                                    });
                                  markerss.push(markers);
                        }
                //markerss.setMap(map);
                var markerCluster = new MarkerClusterer(map, markerss);


            }
        });
    }


    $('.boxClick').click(function(){
        $('.boxShow').toggle(300);
    });


    $("input[name='TrackerTypeUpdate']").change(function(){
        if($(this).val() == 1){
            $(".inputUser").show(500);
            $(".inputCompany").hide(500);
        }else{
            $(".inputUser").hide(500);
            $(".inputCompany").show(500);
        }
    });


    $("input[name='UserTypeUpdate']").change(function(){
        if($(this).val() == 3){
            $("#showCompanyUpdate").show(500);
        }else{
            $("#showCompanyUpdate").hide(500);
        }
    });


    $("input[name='UserType']").change(function(){
        if($(this).val() == 3){
            $("#showCompanyDetails").show(500);
        }else{
            $("#showCompanyDetails").hide(500);
        }
    });

    $("input[name='TypeT']").change(function(){
        if($(this).val() == 1){
            $("select[name='Tracker[user_id]']").css('display','block');
            $("select[name='Tracker[company_id]']").css('display','none');
            $(".divCompany").css('display','none');
            $(".divUser").css('display','block');
        }else{
            $("select[name='Tracker[user_id]']").css('display','none');
            $("select[name='Tracker[company_id]']").css('display','block');
            $(".divCompany").css('display','block');
            $(".divUser").css('display','none');
        }
    });

    $('.boxClick').click(function(){
        $("select[name='Tracker[user_id]']").css('display','none');
        $("select[name='Tracker[company_id]']").css('display','none');
    });

    $("input[name='Tracker[user_type]'").change(function(){
            if($(this).val() == 1){
                $("select[name='Tracker[user_id]']").css('display','block');
                $("select[name='Tracker[company_id]']").css('display','none');
                $("select[name='Tracker[company_id] option:selected").each(function(){
                    this.selected=false;
                });
            }else{
                $("select[name='Tracker[company_id]']").css('display','block');
                $("select[name='Tracker[user_id]']").css('display','none');
                $("select[name='Tracker[user_id] option:selected").each(function(){
                    this.selected=false;
                });
            }
    });

    $(document).on( "click", ".activeCompany", function(){
        var id = $(this).data('id');
        var data = $(this).data('action');
        var qwe = $(this);

        if((qwe.data('type') == 'admin') && ($(this).text() == "Deactive")){
            swal({
                title: "Are you sure?",
                text: "Deactive company?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, deactive company!!",
                closeOnConfirm: false
            }, function(){
                ajaxActive(id, data);

            });

        }else{
            ajaxActive($(this).data('id'), data);
        }
        function ajaxActive(companyId,actionData){
            $.ajax({
                type: 'POST',
                url: '../../admin/companyactive',
                data: {id :companyId, action : actionData},
                dataType: "json",
                success: function(response){

                    if(qwe.data('type') == 'admin'){
                        $('.deactiveTime'+id).empty();
                        $('.deactiveTime'+id).append('not active');

                        $('.buttonActive'+id).empty();
                        $('.buttonActive'+id).append('<button class="btn btn-primary KursorShow" data-toggle="modal" data-target="#myModalActiveCompany'+id+'">Active</button>');

                    }

                    if(response == 'Active'){
                        qwe.removeClass('fa fa-circle-o activeCompany');
                        qwe.addClass('fa fa-circle activeCompany');
                        qwe.data("action","Deactive");
                    }else{
                        qwe.removeClass('fa fa-circle activeCompany');
                        qwe.addClass('fa fa-circle-o activeCompany');
                        qwe.data("action","Active");
                    }
                }
            });
        }

    });


    $('.companyActive').click(function(){
            var companyId = $(this).data('id');
            var timeDeactive;
            var timeActive;
            var typeU;
            if($('.containerOne'+companyId).data('active') == 1){
                if(($('.companyTimeDeactive'+companyId).val() != '')){
                    timeDeactive = $('.companyTimeDeactive'+companyId).val();
                    timeActive = $('.companyTimeActive'+companyId).val();

                    function days_between(date1, date2) {
                        var ONE_DAY = 1000 * 60 * 60 * 24
                        var date1_ms = date1.getTime()
                        var date2_ms = date2.getTime()
                        var difference_ms = Math.abs(date1_ms - date2_ms)

                        return Math.round(difference_ms/ONE_DAY);
                    }
                    var moneyOnMonth = 30;
                    var moneyOnYear = moneyOnMonth * 12;
                    var moneyOnDay = moneyOnYear / 365;
                    var countDayPay = days_between(new Date(timeDeactive), new Date());
                    var companyMoney = countDayPay * moneyOnDay;
                }else{
                    timeDeactive = null;
                }
            }else{
                if(($('.companyMoney'+companyId).val() != '')){
                    var companyMoney = $('.companyMoney'+companyId).val();
                    var moneyOnMonth = 30;
                    var moneyOnYear = moneyOnMonth * 12;
                    var moneyOnDay = moneyOnYear / 365;
                    var countDayPay = companyMoney / moneyOnDay;
                    var intCountDayPay = Math.round(countDayPay);
                            var year;
                            var month;
                            var day;
                            var hour;
                            var minute;
                            var second;
                            function js_yyyy_mm_dd_hh_mm_ss(intCountDayPay) {
                                var now = new Date();
                                now.setDate(now.getDate() + intCountDayPay);
                                year = "" + now.getFullYear();
                                month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
                                day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
                                hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
                                minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
                                second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
                                return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
                            }
                    timeDeactive = js_yyyy_mm_dd_hh_mm_ss(intCountDayPay);
                    timeActive = js_yyyy_mm_dd_hh_mm_ss(0);
                }else{
                    timeDeactive = null;
                }
            }
            if(timeDeactive != null){
                $.ajax({
                    type: 'POST',
                    url: '../../admin/companyactivepayment',
                    data: {company_id : companyId,  timeActive: timeActive, timeDeactive : timeDeactive, company_money: companyMoney},
                    dataType: "json",
                    success: function(response){
                        $('.deactiveTime'+companyId).empty();
                        $('.deactiveTime'+companyId).append(timeDeactive);

                            $('.buttonActive'+companyId).empty();
                            $('.buttonActive'+companyId).append("<button class='btn btn-primary activeCompany' id='deactive' data-id='"+companyId+"' data-type='admin' data-action='deactive'>Deactive</button>");
                        $('#myModalActiveCompany'+companyId).modal('hide');
                        if((response['company_update_status'] == 'update') && (response['payment_add_status'] == 'update')){
                            sweetAlert("Good!", "Good, company is active!", "success");
                        }else{
                            sweetAlert("Oops...", "Sorry, but company not active!" , "error");
                        }
                    }
                });
            }else{
                alert('This fild is reckvaired');
            }
    });



    $('.activeTracker').click(function(){
        var id = $(this).data('id');
        var data = $(this).data('action');
        var qwe = $(this);

        $.ajax({
            type: 'POST',
            url: '../../admin/trackeractive',
            data: {id :$(this).data('id'), action : data},
            dataType: "json",
            success: function(response){
                //alert(response);
                if(response == 'Active'){
                    qwe.removeClass('fa fa-circle-o activeTracker');
                    qwe.addClass('fa fa-circle activeTracker');
                    qwe.data("action","Deactive");
                }else{
                    qwe.removeClass('fa fa-circle activeTracker');
                    qwe.addClass('fa fa-circle-o activeTracker');
                    qwe.data("action","Active");
                }
            }
        });

    });


$('.userActive').click(function(){
            var userId = $(this).data('id');
            var timeDeactive;
            var timeActive;
            var typeU;
            if($('.containerOne'+userId).data('active') == 1){
                if(($('.userTimeDeactive'+userId).val() != '')){
                    timeDeactive = $('.userTimeDeactive'+userId).val();
                    timeActive = $('.userTimeActive'+userId).val();

                    function days_between(date1, date2) {
                        var ONE_DAY = 1000 * 60 * 60 * 24;
                        var date1_ms = date1.getTime();
                        var date2_ms = date2.getTime();
                        var difference_ms = Math.abs(date1_ms - date2_ms);

                        return Math.round(difference_ms/ONE_DAY);
                    }

                    var moneyOnMonth = 30;
                    var moneyOnYear = moneyOnMonth * 12;
                    var moneyOnDay = moneyOnYear / 365;
                    var countDayPay = days_between(new Date(timeDeactive), new Date());
                    var userMoney = countDayPay * moneyOnDay;
                }else{
                    timeDeactive = null;
                }
            }else{
                if(($('.userMoney'+userId).val() != '')){
                    var userMoney = $('.userMoney'+userId).val();
                    var moneyOnMonth = 30;
                    var moneyOnYear = moneyOnMonth * 12;
                    var moneyOnDay = moneyOnYear / 365;
                    var countDayPay = userMoney / moneyOnDay;
                    var intCountDayPay = Math.round(countDayPay);
                            var year;
                            var month;
                            var day;
                            var hour;
                            var minute;
                            var second;
                            function js_yyyy_mm_dd_hh_mm_ss(intCountDayPay) {
                                var now = new Date();
                                now.setDate(now.getDate() + intCountDayPay);
                                year = "" + now.getFullYear();
                                month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
                                day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
                                hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
                                minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
                                second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
                                return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
                            }
                    timeDeactive = js_yyyy_mm_dd_hh_mm_ss(intCountDayPay);
                    timeActive = js_yyyy_mm_dd_hh_mm_ss(0);
                }else{
                    timeDeactive = null;
                }
            }
            if(timeDeactive != null){
                $.ajax({
                    type: 'POST',
                    url: '../../admin/useractivepayment',
                    data: {user_id : userId, timeActive: timeActive, timeDeactive : timeDeactive,user_money: userMoney},
                    dataType: "json",
                    success: function(response){
                        $('.deactiveTime'+userId).empty();
                        $('.deactiveTime'+userId).append(timeDeactive);

                            $('.buttonActive'+userId).empty();
                            $('.buttonActive'+userId).append("<button class='btn btn-primary deactiveUser' id='deactive' data-id='"+userId+"' data-type='admin' data-action='deactive'>Deactive</button>");
                        $('#myModalActiveUser'+userId).modal('hide');
                    }
                });
            }else{
                alert('This fild is reckvaired');
            }
    });


    $(document).on( "click", ".deactiveUser", function(){
        var id = $(this).data('id');
        var qwe = $(this);

        $.ajax({
            type: 'POST',
            url: '../admin/userdeactive',
            data: {user_id :$(this).data('id')},
            dataType: "json",
            success: function(response){
                $('.deactiveTime'+id).empty();
                $('.deactiveTime'+id).append('not active');

                $('.buttonActive'+id).empty();
                $('.buttonActive'+id).append('<button class="btn btn-primary KursorShow" data-toggle="modal" data-target="#myModalActiveUser'+id+'">Active</button>');
            }
        });
    });

    $('.activeUser').click(function(){
        var id = $(this).data('id');
        var data = $(this).data('action');
        var qwe = $(this);
        //alert(data);

        $.ajax({
            type: 'POST',
            url: '../admin/useractive',
            data: {id :$(this).data('id'), action : data},
            dataType: "json",
            success: function(response){
                //alert(response);
                if(response == 'Active'){
                    //qwe.text('Deactive');
                    qwe.removeClass('fa fa-circle-o activeUser');
                    qwe.addClass('fa fa-circle activeUser');
                    qwe.data("action","Deactive");
                }else{
                    qwe.removeClass('fa fa-circle activeUser');
                    qwe.addClass('fa fa-circle-o activeUser');
                    qwe.data("action","Active");
                }
            }
        });

    });


    $('#ChooseIcon').on('hidden.bs.modal', function () {
        $('.modal-body').html('');
    })
    $(document).on("click",'img.icon',function(){
      $('.tracker_marker').val($(this).attr('value'))
      $('#ChooseIcon').modal('hide');
      $('.ChoosenIcon').html('');
      $('.ChoosenIcon').append('<label class="control-label" for="marker">Choose Icon:</label><img class="showAllIcons" src="/web/content/icons/'+ $('.tracker_marker').val()+'.png">')
    })
    $(document).on("click",'.showAllIcons',function(){
      var dir = "/web/content/icons/";
      var fileextension = ".png";
      $.ajax({
          //This will retrieve the contents of the folder if the folder is configured as 'browsable'
          url: dir,
          success: function (data) {
              $(data).find("a:contains(" + fileextension + ")").each(function () {
                  var filename = this.href.replace(window.location.host+window.location.pathname.substring(0, window.location.pathname.lastIndexOf('/')), "").replace("http:///", "");
                  $(".modal-body").append($("<img class='icon' src=" + dir + filename + " value='"+filename.substring(0, filename.lastIndexOf('.')) +"'></img>"));
              });
            $('#ChooseIcon').modal('show');
          console.log(window.location.host);
          }
        });
      })
      
        $(document).on('click','#fixedButtonTask',function(){
            $('#fixedBlockTask').show(100);
            $('#fixedBlockAvto').hide();
            $('#fixedBlockDriver').hide();
        });
        
        $(document).on('click','#fixedButtonDriver',function(){
            $('#fixedBlockDriver').show();
            $('#fixedBlockTask').hide();
            $('#fixedBlockAvto').hide();
        });
        
        $(document).on('click','#fixedButtonAvto',function(){
            $('#fixedBlockAvto').show();
            $('#fixedBlockDriver').hide();
            $('#fixedBlockTask').hide();
        });
        
        
        $(document).on('click','#hideBlockTask',function(){
            $('#fixedBlockTask').hide(100);
        });
        $(document).on('click','#hideBlockAvto',function(){
            $('#fixedBlockAvto').hide(100);
        });
        $(document).on('click','#hideBlockDriver',function(){
            $('#fixedBlockDriver').hide(100);
        });
        
        $(document).on('click','.taskShow',function(){
            $(this).next().toggle();
        });
});
