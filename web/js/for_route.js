/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var map;
    var Addr = []
var waypoints = [];
var markers = [];
var pointListWay = [];
var distance = [];
var allDistance = [];
var timeDescription = [];
function initialize(id,waitpoints, travelMode) {
   // alert('dvdv');
  directionsDisplay = new google.maps.DirectionsRenderer();
  //var chicago = new google.maps.LatLng(41.850033, -87.6500523);
  var mapOptions = {
    zoom:5,
    center: new google.maps.LatLng(41.850033, -87.6500523)
  };
  var map = new google.maps.Map(document.getElementById('map-canvas'+id), mapOptions);
  directionsDisplay.setMap(map);
  
  var geocoder = new google.maps.Geocoder();

  document.getElementById('submitAdderes').addEventListener('click', function() {
    geocodeAddress(geocoder, map);
  });
  
  function geocodeAddress(geocoder, resultsMap) {
    var address = document.getElementById('address').value;
    geocoder.geocode({'address': address}, function(results, status) {
      if (status === google.maps.GeocoderStatus.OK) {
        resultsMap.setCenter(results[0].geometry.location);
        
//        var marker = new google.maps.Marker({
//            map: resultsMap,
//            position: results[0].geometry.location
//        });
//        
//        markers.push(marker);
        
        if(waypoints.length<10){
            var Local = results[0].geometry.location;
            var latLng = {lat:parseFloat(Local.lat()),lng:parseFloat(Local.lng())};
            var geocoderr = new google.maps.Geocoder();
            geocoderr.geocode({'location': latLng}, function(resultss, statuss) {
                if (statuss === google.maps.GeocoderStatus.OK) {
                    var address = resultss[1].formatted_address;
                }
                    pointListWay.push(address);
                });
            
                
            waypoints.push(
            {
                'location':results[0].geometry.location,
                'stopover':true
            });
                placeMarker(results[0].geometry.location, map);
            }else{
                alert('MAX POINTS - 10!')
            }
        }else{
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
    }
	google.maps.event.addListener(map, 'click', function(event){
            if(waypoints.length<10){
                
                var Local = event.latLng;
                var latLng = {lat:parseFloat(Local.lat()),lng:parseFloat(Local.lng())};
                var geocoderr = new google.maps.Geocoder();
                geocoderr.geocode({'location': latLng}, function(resultss, statuss) {
                    if (statuss === google.maps.GeocoderStatus.OK) {
                        var address = resultss[1].formatted_address;
                    }
                    pointListWay.push(address);
                });
                
                
                
                
                waypoints.push({
                    'location':event.latLng,
                    'stopover':true
		});
                    placeMarker(event.latLng, map);
		}else{
                    alert('MAX POINTS - 10!')
		}
	});
        if(waitpoints != ' '){
            console.log(waitpoints,travelMode);
            calcRoute(waitpoints,travelMode);
        }
}

function setAllMap(map) {
  for (var i = 0; i < markers.length; i++) {
    //console.log(markers[i]);
    markers[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
  setAllMap(null);
}
function deleteMarkers() {
  clearMarkers();
  markers = [];
}

  function placeMarker(position, map) {
    var marker = new google.maps.Marker({
        position: position,
	draggable:true,
        map: map
    });
    //console.log(marker);
    //google.maps.event.addListener(marker, 'dragend', qwerty);
    
	markers.push(marker);	
    map.panTo(position);
  }
function calcRoadF(travelType){
	if(waypoints.length>=2){
		calcRoute(waypoints, travelType);
		deleteMarkers();
	}
        return waypoints;
}
			
function calcRemove() {
        deleteMarkers();
	directionsDisplay.setDirections({routes: []});
	waypoints = [];
        pointListWay = [];
}

function calcRoute(data,travelType) {
  var start;
  var TravelMode;
  
  var end;
  //start = new google.maps.LatLng(data);
  end = new google.maps.LatLng(40.7143528, -74.0059731);
    if(travelType == 'DRIVING'){
        TravelMode = google.maps.TravelMode.DRIVING;
    }
    if(travelType == 'WALKING'){
        TravelMode = google.maps.TravelMode.WALKING;
    }
    if(travelType == 'BICYCLING'){
        TravelMode = google.maps.TravelMode.BICYCLING;
    }
    if(travelType == 'TRANSIT'){
        TravelMode = google.maps.TravelMode.TRANSIT;
    }
    //console.log(TravelMode);
        var request = {
            origin:data[0].location,
            destination:data[data.length-1].location,
            waypoints:data.slice(1,data.length-1),
            travelMode: TravelMode
        };
  //console.log(request);
  //console.log(waypoints);
  
  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
        distance = [];
        allDistance = 0;
        for (i = 0; i < response.routes[0].legs.length; i++) {
            distance.push(parseFloat(response.routes[0].legs[i].distance.value));
            allDistance += parseFloat(response.routes[0].legs[i].distance.value);
        }
    }else{
        console.log("Directins service failed: "+status)
    }
  });
}
function geocodeList(latlngList){
    var latLng = {lat:parseFloat(latlngList.lat()),lng:parseFloat(latlngList.lng())};
    var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'location': latLng}, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            var address = results[1].formatted_address;
                alert(address);
                return address;
        }else{
            return 'false';
        }
    });
}

 $('document').ready(function(){
        $('body').on('change','.changeTime',function(){
            alert($(this).attr('name'));
            alert($(this).val());
        });
     $('.datetimepicker3').datetimepicker({
        language: 'pt-BR'
      });

    $('.datetimepicker4').datetimepicker({
        language: 'pt-BR'
      });
     
//     $('.saveChangeInput').click(function(){
//         $(..)
//     })
     
     $('#tasks-type').change(function(){
        if($(this).val() == '2'){
            $('#mapContainer').slideDown(500);
            waypoints = [];
            pointListWay = [];
            timeDescription = [];
            initialize('','');
        }else{
            $('#mapContainer').slideUp(500);
            waypoints = [];
            pointListWay = [];
            timeDescription = [];
        }
    });
     
     $('.button_remove').click(function(event){
        event.preventDefault();
        calcRemove();
    });
    
    function WayList(task_id){
        //alert(task_id);
        //alert('function');
        //console.log(pointListWay);
          var TaskDistance = [];
            var count = 0;
            for(var key in pointListWay){
                var address = pointListWay[key];
                //alert(count);
                if(count == 0){
                    //alert('Null');
                    TaskDistance.push({'address':address,'distance':'0'});    
                }else{
                    console.log(distance);
                    for(var keyD in distance){
                        if(keyD == (count-1)){
                            TaskDistance.push({'address':address,'distance':distance[keyD]});    
                        }
                    }
                }
                count = count + 1;
            }
            //console.log(TaskDistance);
            //alert('dv');
            var arrayInputData = [];
            var alfav = ['A','B','C','D','E','F','G','H','I','J']
            var wayResult = '<table class="table table-hover">';
                wayResult += '<tr><td><b>Marker</b></td><td><b>Address</b></td><td>Date/Time start</td><td>Date/Time end</td><td>Description</td><td><b>Distance</b></td></tr>';
            for(var taskId in TaskDistance){
                var pointAddress = TaskDistance[taskId];
                if(typeof timeDescription[alfav[taskId]] == 'undefined'){
                    var descriptionValue = '';
                    var start_timeValue = '';
                    var end_timeValue = '';
                    var start_dateValue = '';
                    var end_dateValue = '';
                    var classTR = '';
                }else{
                    var descriptionValue =  timeDescription[alfav[taskId]]['description'];
                    var start_timeValue = timeDescription[alfav[taskId]]['start_time'];
                    var end_timeValue = timeDescription[alfav[taskId]]['end_time'];
                    var start_dateValue = timeDescription[alfav[taskId]]['start_date'];
                    var end_dateValue = timeDescription[alfav[taskId]]['end_date'];
                    var classTR = 'changeTime';
                }
                arrayInputData.push(timeDescription[alfav[taskId]]);
                //console.log(timeDescription[alfav[taskId]]);
                wayResult += '<tr>';
                    wayResult += '<td>'+alfav[taskId]+'</td>';
                    wayResult += '<td>'+pointAddress['address']+'</td>';
                    wayResult += '<td><div class="input-group" style="color:black;width:200px;">'+
                                   '<input type="date" value="'+start_dateValue+'" max="2050-12-31" min="1979-12-31" name="date_start'+alfav[taskId]+'">'+
                                   '<input type="time" value="'+start_timeValue+'" value="00:02" class="'+classTR+'" min="00:01" max="24:00" name="time_start'+alfav[taskId]+'">'+
                                '</div>';
                                
                   wayResult += '<td><div class="input-group datetimepicker4" style="color:black;width:200px;">'+
                                   '<input type="date" value="'+end_dateValue+'" max="2050-12-31" min="1979-12-31" name="date_end'+alfav[taskId]+'">'+
                                    '<input  type="time" value="'+end_timeValue+'" min="00:01" max="24:00"  name="time_end'+alfav[taskId]+'">'+
                                '</div></td>';
                    wayResult += '<td><textarea class="form-control" name="description'+alfav[taskId]+'" >'+descriptionValue+'</textarea></td>';
                    if(pointAddress['distance'] >= 1000){
                        wayResult += '<td>'+pointAddress['distance']/100+' km</td>';
                    }else{
                        wayResult += '<td>'+pointAddress['distance']+' m</td>';
                    }
                wayResult += '</tr>';
            }
            if(allDistance >= 1000){
                wayResult += '<tr><td colspan="3"></td><td>Всього:</td><td>'+allDistance/100+' km</td></tr>';
            }else{
                wayResult += '<tr><td colspan="3"></td><td>Всього:</td><td>'+allDistance+' m</td></tr>';
            }
            wayResult += '</table>';
            
        if(task_id == ''){
            $('.for_way_list').empty();
            $('.for_way_list').append(wayResult);
            var jsonDistance = JSON.stringify(TaskDistance, null, 2);
            $('.jsonArrayDistance').val(jsonDistance);
        }else{
            console.log(arrayInputData);
            var jsonTD = JSON.stringify(arrayInputData, null, 2);
            alert(timeDescription)
            $('.for_way_list_input'+task_id).val(jsonTD);
            
            $('.for_way_list'+task_id).empty();
            $('.for_way_list'+task_id).append(wayResult);
            var jsonDistance = JSON.stringify(TaskDistance, null, 2);
            $('.jsonArrayDistance'+task_id).val(jsonDistance);
        }
    }
    
     $('.button_click').click(function(event){
        //alert('dvd');
        event.preventDefault();
        
        var travelType = $("#TravelType :selected").val();
        var qwe = calcRoadF(travelType);
        var json_text = JSON.stringify(qwe, null, 2);
        $('#for_map').val(json_text);
        var task_id = $(this).data('task_id');
        $('.forMap'+task_id).val(json_text);
        var thisElemen = $(this).data('task_id');
        //alert(thisElemen);
        
        if(typeof thisElemen != 'undefined'){
            setTimeout(function(){ WayList(thisElemen); }, 3000);
        }else{
            var task_id = '';
            setTimeout(function(){ WayList(task_id); }, 2000);
        }
        //console.log(waypoints);
        //console.log(pointListWay);
     });
     
   $("[name='modal']").on('shown.bs.modal', function(){
        var task_id = $(this).data('id_task');
       $('<input/>').attr({ type: 'text', id: 'test', name: 'test'}).appendTo('#UpdateTask'+task_id);
        //initialize('');
        var thisTaskElement = $(this);
        
        var travelMode = $(this).find('.travel_map');
        $.ajax({
            type: 'POST',
            url: '../../../../../company/taskdataformap',
            data: {task_id:task_id},
            dataType: "json",
            success: function(response){
                var dataT = jQuery.parseJSON(response);
                waypoints = dataT;
                for(var i in dataT){
                    dataT[i].location = new google.maps.LatLng(dataT[i].location['lat'], dataT[i].location['lng']);
                }
                //console.log(dataT);
                initialize(thisTaskElement.data("id_task"), dataT, travelMode.val());
            }
        });
        
        var qwer = $(this).find('.for_map');
        var data = qwer.val();
        
        
        $.ajax({
            type: 'POST',
            url: '../../../../../tracker/subtasksview',
            data: {id_task:task_id},
            dataType: "json",
            success: function(response){
                pointListWay = [];
                var wayResult = '<table class="table table-hover">';
                wayResult += '<tr><td><b>Marker</b></td><td><b>Address</b></td><td>Date/Time start</td><td>Date/Time end</td><td>Description</td><td><b>Distance</b></td></tr>';
                var allDistance = 0;
                for(var Id in response){
                    var pointAddress = response[Id];
                    wayResult += '<tr>';
                        wayResult += '<td>'+pointAddress['marking']+'</td>';
                        pointListWay.push(pointAddress['address']); 
                        wayResult += '<td>'+pointAddress['address']+'</td>';
                        wayResult += '<td><div class="input-group" style="color:black;width:200px;">'+
                                       '<input type="date" value="'+pointAddress['start_date']+'" max="2050-12-31" min="1979-12-31" name="date_start'+pointAddress['marking']+'">'+
                                       '<input type="time"  value="'+pointAddress['start_time']+'" min="00:01" class="changeTime" max="24:00" name="time_start'+pointAddress['marking']+'">'+
                                    '</div>';

                        wayResult += '<td><div class="input-group datetimepicker4" style="color:black;width:200px;">'+
                                       '<input type="date"  value="'+pointAddress['end_date']+'" max="2050-12-31" min="1979-12-31" name="date_end'+pointAddress['marking']+'">'+
                                        '<input  type="time"  value="'+pointAddress['end_time']+'" min="00:01" max="24:00"  name="time_end'+pointAddress['marking']+'">'+
                                    '</div></td>';
                        wayResult += '<td><textarea class="form-control" name="description'+pointAddress['marking']+'" >'+pointAddress['description']+'</textarea></td>';
                        if(typeof timeDescription[pointAddress['marking']] == 'undefined'){
                            timeDescription[pointAddress['marking']] = [];
                        }
                        timeDescription[pointAddress['marking']] = {'start_time':pointAddress['start_time'], 'start_date':pointAddress['start_date'], 'end_date':pointAddress['end_date'], 'end_time':pointAddress['end_time'], 'description':pointAddress['description']};
                    
                        if(pointAddress['distance'] >= 1000){
                            wayResult += '<td>'+pointAddress['distance']/100+' km</td>';
                        }else{
                            wayResult += '<td>'+pointAddress['distance']+' m</td>';
                        }
                        allDistance += parseFloat(pointAddress['distance']);
                    wayResult += '</tr>';
                }
                if(allDistance >= 1000){
                    wayResult += '<tr><td></td><td>Всього:</td><td>'+allDistance/100+' km</td></tr>';
                }else{
                    wayResult += '<tr><td></td><td>Всього:</td><td>'+allDistance+' m</td></tr>';
                }
                wayResult += '</table>';

                thisTaskElement.find('.for_way_list'+task_id).empty();
                thisTaskElement.find('.for_way_list'+task_id).append(wayResult);
            }
        });
    });  

 });