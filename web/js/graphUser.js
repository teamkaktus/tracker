    google.load("visualization", "1", {packages:["corechart"]});
        
    function drawChartUser(data) {
        var data = google.visualization.arrayToDataTable(data);

        var options = {
          title: 'User company',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
    }

$(document).ready(function(){   
    
    $('.companyInfoClick').click(function(){
        $('.companyInfoShowHide').slideToggle(2000);  
    });
    
    $("#btnExport").click(function () {
        $("#tblExport").battatech_excelexport({
            containerid: "tblExport", 
            datatype: 'table'
        });
    });
    
    if($('.companyid').text() != ''){
    var role = $('.companyid').data('action');
        
        //alert($('.companyid').data('action'));
        $.ajax({
            type: 'POST',
            url: '../../../company/usergraph',
            data: {data: $('.companyid').text(), role: role},
            dataType: "json",
            beforeSend: function(){
                $('.preloader').show();
            },
            success: function(response){
                if(role == 'user'){
                    $('.preloader').hide(); 
                    //console.log(response.tracker_count);

                    var trackerCount = [];
                    trackerCount = response.tracker_count;
                    var data = [];
                    data.push(['user', 'tracker_count']);

                    for(var userTracker in trackerCount){
                       data.push([userTracker, trackerCount[userTracker]]);
                    }
                    //console.log(response.trackerModel);
                    var dataForTable = [];
                    for(var userTracker in response.UserTracker){
                        var qwer = response.UserTracker[userTracker];
                        for(var userT in qwer){
                            if(typeof dataForTable[userTracker] == "undefined" ){
                                dataForTable[userTracker] = [];
                            }

                            dataForTable[userTracker].push(response.trackerModel[qwer[userT]]);
                        }
                    }

                    var tableData = '<table id="tblExport" border="1" style="display:none;">';
                        for(var userName in dataForTable){
                            tableData += "<tr><td><h3>"+userName+"<h3></td></tr>";
                            var userTrackerInfo = dataForTable[userName];
                            for(var trackerInfo in userTrackerInfo){
                                if(typeof userTrackerInfo[trackerInfo] != "undefined" ){
                                    tableData += "</tr>"
                                        tableData += "<td>Tracker - "+userTrackerInfo[trackerInfo]['name']+"</td>"+
                                                     "<td>"+userTrackerInfo[trackerInfo]['discription']+"</td>";
                                    tableData += "</tr>";
                                }
                            }
                        }
                        tableData += '</table>';

                        $('.tableData').empty();
                        $('.tableData').append(tableData);

                        //console.log(data);

                    drawChartUser(data);
                }else{
                    $('.preloader').hide(); 
                    
                    var data = [];
                    data.push(['sub company', 'tracker count']);

                    for(var subCompanyTracker in response.dataGraph){
                       data.push([subCompanyTracker, response.dataGraph[subCompanyTracker]]);
                    }
                    drawChartUser(data);
                    console.log(response.dataTable);
                    
                    var dataForTable = response.dataTable;
                    var tableData = '<table id="tblExport" border="1" style="display:none;">';
                        for(var userName in  dataForTable){
                            tableData += "<tr><td><h3>"+userName+"<h3></td></tr>";
                            var userTrackerInfo = dataForTable[userName];
                            for(var trackerInfo in userTrackerInfo){
                                tableData += "</tr>"
                                    tableData += "<td>Tracker - "+userTrackerInfo[trackerInfo]['name']+"</td>"+
                                                 "<td>"+userTrackerInfo[trackerInfo]['discription']+"</td>";
                                tableData += "</tr>";
                            }
                        }
                        tableData += '</table>';

                        $('.tableData').empty();
                        $('.tableData').append(tableData);
                    
                    
                    
                }
        }
        });
    }
});