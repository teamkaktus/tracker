$('document').ready(function(){
    
    function deleteFile(file_name){
        $.ajax({
            type: 'POST',
            url: '../../../../company/deletefile',
            data: {file_name:file_name},
            success: function(response){
            }
        });
    }
    
    var previewNode = document.querySelector("#previews");
    previewNode.id = "";
    var previewTemplate = previewNode.parentNode.innerHTML;
    previewNode.parentNode.removeChild(previewNode);

    var myDropzoneA = new Dropzone($('.newPhoto')[0], {
        maxFiles:1,
        uploadMultiple:false,
        url: "../../../company/savedropedfile",
        previewTemplate:previewTemplate,
        clickable: ".newPhoto",
        previewsContainer: ".avtoPhoto",
    });

    myDropzoneA.on("maxfilesexceeded", function(file) {
            myDropzoneA.removeAllFiles();
            myDropzoneA.addFile(file);

    });

    myDropzoneA.on("complete", function(response) {
        if (response.status == 'success') {
            $('#carinfo-image_src').val(response.xhr.response);
            //$('.eventButton').show();
        }
    });

    myDropzoneA.on("removedfile", function(response) {
        if(response.xhr != null){
           deleteFile(response.xhr.response);
        }
    });
});
