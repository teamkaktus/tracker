/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var map;
var waypoints = [];
var markers = [];
function initialize(id,waitpoints) {
   // alert('dvdv');
  directionsDisplay = new google.maps.DirectionsRenderer();
  var chicago = new google.maps.LatLng(41.850033, -87.6500523);
  var mapOptions = {
    zoom:5,
    center: new google.maps.LatLng(41.850033, -87.6500523)
  };
  map = new google.maps.Map(document.getElementById('map-canvas'+id), mapOptions);
  directionsDisplay.setMap(map);
  
	google.maps.event.addListener(map, 'click', function(event) {
	if(waypoints.length<10){
		waypoints.push(
		{
          'location':event.latLng,
          'stopover':true
		  })
		 placeMarker(event.latLng, map);
		}else{
		alert('MAX POINTS - 10!')
		}
	});
        if(waitpoints != ' '){
            calcRoute(waitpoints);
        }
}

function setAllMap(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
  setAllMap(null);
}
function deleteMarkers() {
  clearMarkers();
  markers = [];
}
  function placeMarker(position, map) {
    var marker = new google.maps.Marker({
      position: position,
	  draggable:true,
      map: map
    });
	markers.push(marker);	
    map.panTo(position);
  }
function calcRoadF(){
	if(waypoints.length>=2){
		calcRoute(waypoints);
		deleteMarkers()
	}
        console.log(waypoints);
        return waypoints;
}

			
function calcRemove() {
	directionsDisplay.setDirections({routes: []});
	waypoints = [];
}

function calcRoute(data) {
  //var start = document.getElementById('start').value;
  //var end = document.getElementById('end').value;
  //alert(data);
  var start;
  
  var end;
  //start = new google.maps.LatLng(data);
  end = new google.maps.LatLng(40.7143528, -74.0059731);
    var request = {
        origin:data[0].location,
        destination:data[data.length-1].location,
	waypoints:data.slice(1,data.length-1),
        travelMode: google.maps.TravelMode.DRIVING
    };
  console.log(request);
  //console.log(waypoints);
  
  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
        console.log(response);
    }else{
        console.log("Directins service failed: "+status)
    }
  });
}
 $('document').ready(function(){ 
     $('.button_remove').click(function(event){
        event.preventDefault();
        calcRemove();
    });
    
     $('.button_click').click(function(event){
        event.preventDefault();
        
        var qwe = calcRoadF();
        //var obj = JSON.parse(qwe);
        var json_text = JSON.stringify(qwe, null, 2);
        $('#for_map').val(json_text);
        var task_id = $(this).data('task_id');
        $('.forMap'+task_id).val(json_text);
     });
     
     $("[name='modal']").on('shown.bs.modal', function(){
        
        var task_id = $(this).data('id_task');
        
        //initialize('');
        var data = $('.for_map'+task_id).text();
        //console.log();
        data = jQuery.parseJSON(data);
        for(var i in data){
            data[i].location = new google.maps.LatLng(data[i].location['G'], data[i].location['K']);
        }
        
        initialize(task_id, data);
    });     
 });